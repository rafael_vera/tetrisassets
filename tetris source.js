'use strict';

function Game() {
    this['canvas'] = document['getElementById']('myCanvas');
    this['holdCanvas'] = document['getElementById']('holdCanvas');
    this['queueCanvas'] = document['getElementById']('queueCanvas');
    this['v'] = null;
    this['bgCanvas'] = document['getElementById']('bgLayer');
    this['bgctx'] = this['bgCanvas']['getContext']('2d');
    this['GS'] = new GameSlots(this);
    this['roomCapacity'] = 6;
    this['connectStatusElement'] = document['getElementById']('connectStatus');
    this['rInfoBox'] = document['getElementById']('rInfoBox');
    this['practiceMenu'] = document['getElementById']('practiceMenu');
    this['practiceMenuBig'] = document['getElementById']('practice-menu-big');
    this['teamInfo'] = document['getElementById']('teamInfo');
    this['sprintInfo'] = document['getElementById']('sprintInfo');
    this['lrem'] = document['getElementById']('lrem');
    this['fpsElement'] = document['getElementById']('fps');
    this['block_size'] = 24;
    this['debug'] = false;
    this['SEenabled'] = true;
    this['VSEenabled'] = false;
    this['SEStartEnabled'] = true;
    this['SEFaultEnabled'] = false;
    this['SErotate'] = false;
    createjs['Sound']['registerPlugins']([createjs['WebAudioPlugin']]);
    createjs['Sound']['volume'] = 0.6;
    this['tex'] = new Image();
    this['tex2'] = new Image();
    this['tex2']['src'] = '/res/tex2.png';
    this['drawScale'] = 1;
    this['skinId'] = 0;
    this['ghostTex'] = null;
    this['ghostSkinId'] = 0;
    this['ghostSkins'] = [{
        id: 0,
        name: 'Default',
        data: ''
    }, {
        id: 1,
        name: 'Custom',
        data: '',
        w: 36
    }];
    this['SFXset'] = null;
    this['VSFXset'] = null;
    this['play'] = false;
    this['gameEnded'] = false;
    this['hdAbort'] = false;
    this['lastSeen'] = null;
    this['isTabFocused'] = true;
    this['pmode'] = 0;
    this['livePmode'] = 0;
    this['selectedPmode'] = 1;
    this['sprintMode'] = 1;
    this['sprintModeToRun'] = 1;
    this['sprintModes'] = {
        1: 40,
        2: 20,
        3: 100,
        4: 1000
    };
    this['cheeseModes'] = {
        1: 10,
        2: 18,
        3: 100,
        100: 1000000
    };
    this['ultraModes'] = {
        1: 120
    };
    this['cheeseLevel'] = undefined;
    this['maxCheeseHeight'] = 9;
    this['minCheeseHeight'] = 3;
    this['lastHolePos'] = null;
    this['starting'] = false;
    this['activeBlock'] = new Block(0);
    this['ghostPiece'] = {
        pos: {
            x: 0,
            y: 0
        }
    };
    this['timer'] = 0;
    this['lastSnapshot'] = 0;
    this['clock'] = 0;
    this['frames'] = 0;
    this['baseGravity'] = null;
    this['currentGravity'] = [0.9, 0];
    this['softDrop'] = false;
    this['softDropId'] = 2;
    this['holdPressed'] = false;
    this['hardDropPressed'] = false;
    this['lastDAS'] = 0;
    this['firstDAS'] = true;
    this['DASdebug'] = false;
    this['ARRtime'] = 0;
    this['pressedDir'] = {
        "\x2D\x31": false,
        "\x31": false
    };
    this['ARRon'] = {
        "\x2D\x31": false,
        "\x31": false
    };
    this['DASto'] = {
        "\x2D\x31": null,
        "\x31": null
    };
    this['DASmethod'] = 1;
    this['lockDelayActive'] = false;
    this['lockDelayActivated'] = undefined;
    this['lastAction'] = 0;
    this['lastGeneration'] = 0;
    this['lockDelay'] = null;
    this['maxLockDelayWithoutLock'] = null;
    this['maxWithoutLock'] = null;
    this['holdUsedAlready'] = false;
    this['temporaryBlockSet'] = null;
    this['redBar'] = 0;
    this['incomingGarbage'] = [];
    this['solidHeight'] = 0;
    this['solidToAdd'] = 0;
    this['solidInterval'] = null;
    this['solidProfiles'] = [
        [0, 3],
        [0, 3, 2.8, 2.6, 2.4, 2.2, 2, 1.8, 1.6, 1.4, 1.2, 1, 31, 1, 1, 1, 1, 1, 1, 1], null, null
    ];
    this['garbageCols'] = [];
    this['blockInHold'] = null;
    this['focusState'] = 0;
    this['statsEnabled'] = false;
    this['statsMode'] = 0;
    this['placedBlocks'] = 0;
    this['lastPlacements'] = [];
    this['finesse'] = 0;
    this['totalFinesse'] = 0;
    this['totalKeyPresses'] = 0;
    this['place'] = null;
    this['redrawBlocked'] = false;
    this['linesAttackDef'] = [0, 0, 1, 2, 4, 4, 6, 2, 0, 10, 1];
    this['linesAttack'] = this['linesAttackDef'];
    this['cheeseHeight'] = 10;
    this['ghostEnabled'] = true;
    this['getPPS'] = this['getCumulativePPS'];
    this['comboAttackDef'] = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 5];
    this['comboAttack'] = this['comboAttackDef'];
    this['comboCounter'] = -1;
    this['fourWideFlag'] = false;
    this['PCdata'] = {
        blocks: 0,
        lines: 0
    };
    this['linesRemaining'] = 0;
    this['inactiveGamesCount'] = 0;
    this['xbuffMask'] = 1;
    this['replayPartsSent'] = 0;
    this['transmitMode'] = 0;
    this['fragmentCounter'] = 0;
    this['liveSnapRate'] = 100;
    this['snapRate'] = 1000;
    this['soundQ'] = new SoundQueue();
    this['RNG'] = alea(this['timestamp']());
    this['blockRNG'] = this['RNG'];
    this['blockSeed'] = '';
    this['bigTriggered'] = false;
    this['bigChance'] = 100000000;
    this['interval'] = null;
    this['animator'] = null;
    GameCore['call'](this, true);
    this['RulesetManager'] = new RulesetManager(this);
    this['conf'] = [{}, {}];
    this['RulesetManager']['applyRule']({}, this['conf'][0]);
    this['RulesetManager']['applyRule']({}, this['conf'][1]);
    this['R'] = this['conf'][0];
    this['RulesetManager']['ruleSetChange'](0);
    this['RulesetManager']['setActiveMode'](1);
    this['Settings'] = new Settings(this);
    this['Settings']['startWebGL'](false);
    this['Settings']['init']();
    this['Caption'] = new GameCaption(document['getElementById']('stage'));
    this['Live'] = new Live(this);
    this['Replay'] = new Replay();
    this['Scoring'] = new Scoring();
    this['MapManager'] = new MapManager(this);
    this['ModeManager'] = new ModeManager(this);
    this['GameStats'] = new StatsManager(this['v']);
    this['Mobile'] = new Mobile(this);
    this['Report'] = new Report(this);
    this['setLockDelay'](this['R']['lockDelay']);
    this['applyGravityLvl'](this['R']['gravityLvl']);
    this['changeSkin'](this['skinId']);
    this['initSFX']();
    this['Settings']['setupSwipeControl']()
}
var GameProto = Object['create'](GameCore['prototype']);
Game['prototype'] = GameProto;
Game['prototype']['constructor'] = Game;
Game['prototype']['rollBigSpawn'] = function() {
    if (!this['bigChance']) {
        return
    };
    var _0x3d81x3 = Math['floor'](Math['random']() * (this['bigChance']));
    var _0x3d81x4 = new Date();
    if (this['Live']['LiveGameRunning'] && !this['pmode'] && _0x3d81x3 === 0 && (_0x3d81x4['getDate']() === 1 || _0x3d81x4['getDate']() === 2 || _0x3d81x4['getDate']() === 31)) {
        this['bigTriggered'] = true
    }
};
Game['prototype']['loadGhostSkin'] = function(_0x3d81x5, _0x3d81x6) {
    this['ghostSkins'][1]['w'] = _0x3d81x6;
    this['ghostTex'] = new Image();
    this['ghostTex']['src'] = this['ghostSkins'][1]['data'] = _0x3d81x5;
    this['ghostSkinId'] = 1;
    if (this['v']['NAME'] === 'webGL') {
        this['v']['loadTexture'](1, _0x3d81x5)
    };
    this['redraw']()
};
Game['prototype']['changeSkin'] = function(_0x3d81x7) {
    if (_0x3d81x7 >= 1000 || (typeof this['skins'][_0x3d81x7] !== 'undefined' && this['skins'][_0x3d81x7]['data'])) {
        this['skinId'] = _0x3d81x7
    } else {
        _0x3d81x7 = this['skinId'] = 0
    };
    var _0x3d81x5;
    var _0x3d81x8 = (_0x3d81x7 >= 1000 || typeof this['skins'][_0x3d81x7]['cdn'] === 'undefined' || this['skins'][_0x3d81x7]['cdn']);
    if (_0x3d81x7 >= 1000) {
        _0x3d81x5 = '/res/b/' + this['customSkinPath'] + '.png'
    } else {
        _0x3d81x5 = this['skins'][_0x3d81x7]['data']
    };
    var _0x3d81x9 = _0x3d81x8 ? CDN_URL(_0x3d81x5) : _0x3d81x5;
    if (_0x3d81x7 > 0) {
        this['tex']['src'] = _0x3d81x9;
        if (this['v']['NAME'] === 'webGL') {
            this['v']['loadTexture'](0, _0x3d81x9)
        };
        if (_0x3d81x7 >= 1000) {
            this['skins'][_0x3d81x7] = {
                id: _0x3d81x7,
                data: _0x3d81x5,
                w: 32
            }
        }
    } else {
        if (_0x3d81x7 === 0 && this['v']['NAME'] === 'webGL') {
            this['v']['loadTexture'](0, null)
        }
    };
    this['redrawAll']()
};
Game['prototype']['initSFX'] = function() {
    createjs['Sound']['removeAllSounds']();
    if (!SFXsets[this['Settings']['SFXsetID']]) {
        this['Settings']['SFXsetID'] = 2
    };
    if (this['VSEenabled'] && !VSFXsets[this['Settings']['VSFXsetID']]) {
        this['VSEenabled'] = false;
        this['Settings']['VSFXsetID'] = 1
    };
    this['changeSFX'](new SFXsets[this['Settings']['SFXsetID']]['data']);
    if (this['VSEenabled']) {
        this['changeSFX'](new VSFXsets[this['Settings']['VSFXsetID']]['data'], 1)
    }
};
Game['prototype']['changeSFX'] = function(_0x3d81xa, _0x3d81xb) {
    if (!_0x3d81xb) {
        _0x3d81xb = 0
    };
    var _0x3d81xc;
    if (_0x3d81xb === 0) {
        this['SFXset'] = _0x3d81xa;
        _0x3d81xc = ''
    } else {
        if (_0x3d81xb === 1) {
            this['VSFXset'] = _0x3d81xa;
            _0x3d81xc = 'v_'
        }
    };
    this['loadSounds'](_0x3d81xa, _0x3d81xc)
};
Game['prototype']['loadSounds'] = function(_0x3d81xd, _0x3d81xc) {
    var _0x3d81xe = ['hold', 'linefall', 'lock', 'harddrop', 'rotate', 'success', 'garbage', 'b2b', 'land', 'move', 'died', 'ready', 'go', 'golive', 'ding', 'msg', 'fault', 'item', 'pickup'];
    if (_0x3d81xd['harddrop'] === null) {
        _0x3d81xd['harddrop'] = _0x3d81xd['lock']
    };
    if (!this['SErotate']) {
        var _0x3d81xf = _0x3d81xe['indexOf']('rotate');
        if (_0x3d81xf !== -1) {
            _0x3d81xe['splice'](_0x3d81xf, 1)
        }
    };
    var _0x3d81x10 = function(_0x3d81x7, _0x3d81x11) {
        if (!_0x3d81x7 || !_0x3d81x11) {
            return
        };
        let _0x3d81x5 = _0x3d81xd['getSoundUrlFromObj'](_0x3d81x11);
        _0x3d81x7 = _0x3d81xc + _0x3d81x7;
        if (_0x3d81x5) {
            let _0x3d81x12 = createjs['Sound']['registerSound'](_0x3d81x5, _0x3d81x7);
            if (!_0x3d81x12 || !createjs['Sound']['_idHash'][_0x3d81x7]) {
                console['error']('loadSounds error: src parse / cannot init plugins, id=' + _0x3d81x7 + ((_0x3d81x12 === false) ? ', rs=false' : ', no _idHash'));
                return
            };
            createjs['Sound']['_idHash'][_0x3d81x7]['sndObj'] = _0x3d81x11
        }
    };
    if (_0x3d81xd['scoring']) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xd['scoring']['length']; ++_0x3d81x13) {
            if (_0x3d81xd['scoring'][_0x3d81x13]) {
                _0x3d81x10('s' + _0x3d81x13, _0x3d81xd['scoring'][_0x3d81x13])
            }
        }
    };
    if (_0x3d81xd['b2bScoring'] && Array['isArray'](_0x3d81xd['b2bScoring'])) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xd['b2bScoring']['length']; ++_0x3d81x13) {
            if (_0x3d81xd['b2bScoring'][_0x3d81x13]) {
                _0x3d81x10('bs' + _0x3d81x13, _0x3d81xd['b2bScoring'][_0x3d81x13])
            }
        }
    };
    if (_0x3d81xd['spawns']) {
        for (var _0x3d81x14 in _0x3d81xd['spawns']) {
            _0x3d81x10('b_' + _0x3d81x14, _0x3d81xd['spawns'][_0x3d81x14])
        }
    };
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xe['length']; ++_0x3d81x13) {
        let _0x3d81x7 = _0x3d81xe[_0x3d81x13];
        _0x3d81x10(_0x3d81x7, _0x3d81xd[_0x3d81x7])
    };
    if (_0x3d81xd['comboTones'] && Array['isArray'](_0x3d81xd['comboTones'])) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xd['comboTones']['length']; ++_0x3d81x13) {
            var _0x3d81x15 = _0x3d81xd['comboTones'][_0x3d81x13];
            if (_0x3d81x15) {
                createjs['Sound']['registerSound'](_0x3d81xd['getSoundUrlFromObj'](_0x3d81x15), _0x3d81xc + 'c' + _0x3d81x13)
            }
        };
        _0x3d81xd['maxCombo'] = _0x3d81xd['comboTones']['length'] - 1
    } else {
        if (_0x3d81xd['comboTones']) {
            var _0x3d81x16 = [];
            for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xd['comboTones']['cnt']; ++_0x3d81x13) {
                _0x3d81x16['push']({
                    id: _0x3d81xc + 'c' + _0x3d81x13,
                    startTime: _0x3d81x13 * (_0x3d81xd['comboTones']['duration'] + _0x3d81xd['comboTones']['spacing']),
                    duration: _0x3d81xd['comboTones']['duration']
                })
            };
            _0x3d81xd['maxCombo'] = _0x3d81xd['comboTones']['cnt'] - 1;
            var _0x3d81x17 = [{
                src: _0x3d81xd['getSoundUrl']('comboTones'),
                data: {
                    audioSprite: _0x3d81x16
                }
            }];
            createjs['Sound']['registerSounds'](_0x3d81x17, '')
        }
    }
};
Game['prototype']['drawBgGrid'] = function(_0x3d81xb) {
    _0x3d81xb = (typeof _0x3d81xb === 'undefined') ? 1 : _0x3d81xb;
    this['bgctx']['rect'](0, 0, this['canvas']['width'], this['canvas']['height']);
    this['bgctx']['fillStyle'] = '#000000';
    this['bgctx']['fill']();
    this['bgctx']['beginPath']();
    this['bgctx']['lineWidth'] = 1;
    if (_0x3d81xb === 1) {
        for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
            this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
            this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
        };
        for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
            this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
            this['bgctx']['lineTo'](241, _0x3d81x13 * this['block_size'] + 0.5)
        };
        this['bgctx']['strokeStyle'] = '#101010';
        this['bgctx']['stroke']();
        this['bgctx']['beginPath']();
        for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
            for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
                this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5);
                this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5)
            }
        };
        for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
            for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
                this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] + 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
                this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] + 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
            }
        };
        this['bgctx']['strokeStyle'] = '#202020';
        this['bgctx']['stroke']();
        this['bgctx']['beginPath']()
    } else {
        if (_0x3d81xb === 2) {
            for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
                for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
                    this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] - 0.5);
                    this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] - 0.5)
                }
            };
            for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
                for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
                    this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] - 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
                    this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] - 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
                }
            }
        } else {
            if (_0x3d81xb === 3) {
                for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                    this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                    this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
                }
            } else {
                if (_0x3d81xb === 4) {
                    for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                        this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                        this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
                    };
                    for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
                        this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
                        this['bgctx']['lineTo'](241, _0x3d81x13 * this['block_size'] + 0.5)
                    }
                }
            }
        }
    };
    this['bgctx']['strokeStyle'] = '#393939';
    this['bgctx']['stroke']();
    this['bgctx']['lineWidth'] = 2;
    this['bgctx']['strokeRect'](1, 1, 240, this['canvas']['height'] - 2)
};
Game['prototype']['isPmode'] = function(_0x3d81x1a) {
    if (_0x3d81x1a) {
        return this['pmode']
    } else {
        if (this['livePmode']) {
            return this['livePmode']
        } else {
            return this['pmode']
        }
    }
};
Game['prototype']['startPractice'] = function(_0x3d81xb, _0x3d81x1b, _0x3d81x1c) {
    if (typeof _0x3d81x1c === 'undefined') {
        _0x3d81x1c = {
            callback: null,
            isLivePmode: false,
            mapId: null,
            mapForOpponents: false
        }
    };
    if ((!this['starting'] || _0x3d81x1c['isLivePmode']) && (!(this['play'] && this['Live']['LiveGameRunning']) || this['pmode'])) {
        if (!this['Live']['socket'] && !this['Live']['playingOfflineWarningShown']) {
            this['Live']['showOfflineWarning']()
        };
        if (this['pmode'] !== _0x3d81xb) {
            this['RulesetManager']['setActiveMode'](_0x3d81xb)
        };
        if (_0x3d81x1b) {
            this['sprintMode'] = -1
        };
        this['pmode'] = this['selectedPmode'] = _0x3d81xb;
        this['RulesetManager']['adjustToValidMode'](_0x3d81xb, this['sprintMode']);
        this['play'] = false;
        this['Replay'] = new Replay();
        this['Replay']['config']['m'] = this['pmode'];
        if (!_0x3d81x1c['isLivePmode']) {
            this['Replay']['config']['r'] = this['RulesetManager']['pmodeRuleId']
        };
        this['R'] = (_0x3d81x1c['isLivePmode']) ? this['conf'][0] : this['conf'][1];
        this['temporaryBlockSet'] = null;
        this['generatePracticeQueue'](this['R']['rnd']);
        this['updateQueueBox']();
        this['sprintInfoLineContent'](0);
        hideElem(this['practiceMenu']);
        this['Live']['toggleMorePractice'](true, false);
        hideElem(this['Live']['teamOptions']);
        hideElem(this['teamInfo']);
        this['ModeManager']['resetUI']();
        var _0x3d81x1d = (_0x3d81x1c['callback']) ? _0x3d81x1c['callback'] : function() {
            this['Live']['sendPracticeModeStarting']();
            this['readyGo']()
        };
        let _0x3d81x1e = 9999999999;
        if (this['pmode'] === 1) {
            this['Replay']['config']['m'] = this['sprintMode'];
            this['linesRemaining'] = this['sprintModes'][this['sprintMode']];
            this['lrem']['textContent'] = this['linesRemaining'];
            showElem(this['sprintInfo'])
        } else {
            if (this['pmode'] === 2) {
                this['Replay']['config']['m'] = 0x00020000;
                this['linesRemaining'] = _0x3d81x1e;
                hideElem(this['sprintInfo'])
            } else {
                if (this['pmode'] === 3) {
                    this['Replay']['config']['m'] = 0x00030000 | this['sprintMode'];
                    this['linesRemaining'] = this['cheeseModes'][this['sprintMode']];
                    this['setLrem'](this['linesRemaining']);
                    showElem(this['sprintInfo'])
                } else {
                    if (this['pmode'] === 4) {
                        this['Replay']['config']['m'] = 0x00040000;
                        this['linesRemaining'] = _0x3d81x1e;
                        this['setLrem'](_0x3d81x1e);
                        hideElem(this['sprintInfo'])
                    } else {
                        if (this['pmode'] === 5) {
                            this['sprintMode'] = 1;
                            this['Replay']['config']['m'] = 0x00050000 | this['sprintMode'];
                            showElem(this['sprintInfo']);
                            this['linesRemaining'] = this['ultraModes'][this['sprintMode']];
                            this['lrem']['textContent'] = sprintTimeFormat(this['linesRemaining'], -1);
                            this['sprintInfoLineContent'](1)
                        } else {
                            if (this['pmode'] === 6) {
                                hideElem(this['sprintInfo']);
                                this['Replay']['config']['m'] = 0x00060000;
                                this['linesRemaining'] = _0x3d81x1e;
                                this['setLrem'](_0x3d81x1e);
                                this['v']['clearMainCanvas']();
                                this['v']['clearQueueCanvas']();
                                this['v']['clearHoldCanvas']();
                                this['Caption']['mapLoading']();
                                var _0x3d81x1f = function() {
                                    if (_0x3d81x1c['mapForOpponents']) {
                                        this['Live']['loadMapForOpponents'] = true;
                                        for (var _0x3d81x20 in this['GS']['cidSlots']) {
                                            if (!this['Live']['clients'][_0x3d81x20]['rep']) {
                                                continue
                                            };
                                            this['Live']['clients'][_0x3d81x20]['rep']['loadMap'](this['MapManager']['matrix'], this['MapManager']['mapData']['queue'])
                                        }
                                    };
                                    this['matrix'] = copyMatrix(this['MapManager']['matrix']);
                                    this['Replay']['config']['map'] = this['sprintMode'] = this['MapManager']['mapId'];
                                    this['redrawMatrix']();
                                    if (this['MapManager']['mapData']['queue']) {
                                        var _0x3d81x21 = this['MapManager']['mapData']['queue'];
                                        this['queue'] = [];
                                        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x21['length']; ++_0x3d81x13) {
                                            this['queue']['push'](new Block(this['blockIds'][_0x3d81x21[_0x3d81x13]]))
                                        }
                                    };
                                    this['updateQueueBox']();
                                    _0x3d81x1d['call'](this)
                                };
                                this['MapManager']['onMapReady'] = _0x3d81x1f['bind'](this);
                                this['MapManager']['prepare'](_0x3d81x1c['mapId']);
                                return
                            } else {
                                if (this['pmode'] === 7) {
                                    this['sprintMode'] = 1;
                                    this['Replay']['config']['m'] = 0x00070000 | this['sprintMode'];
                                    showElem(this['sprintInfo']);
                                    this['linesRemaining'] = _0x3d81x1e;
                                    this['lrem']['textContent'] = 0;
                                    this['sprintInfoLineContent'](2)
                                } else {
                                    if (this['pmode'] === 8) {
                                        this['sprintMode'] = 1;
                                        this['Replay']['config']['m'] = 0x00080000 | this['sprintMode'];
                                        this['PCdata'] = {
                                            blocks: 0,
                                            lines: 0
                                        };
                                        showElem(this['sprintInfo']);
                                        this['linesRemaining'] = _0x3d81x1e;
                                        this['lrem']['textContent'] = 0;
                                        this['sprintInfoLineContent'](3);
                                        this['RulesetManager']['appendRule']({
                                            grav: 10
                                        }, this['conf'][1])
                                    } else {
                                        if (this['pmode'] === 9) {
                                            hideElem(this['sprintInfo']);
                                            this['Replay']['config']['m'] = 0x00090000;
                                            this['linesRemaining'] = _0x3d81x1e;
                                            this['garbageCols'] = [];
                                            this['gameEnded'] = false;
                                            this['blockInHold'] = null;
                                            this['clock'] = 0;
                                            this['deadline'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                                            this['clearMatrix']();
                                            this['RulesetManager']['applyRule']({}, this.R);
                                            this['generatePracticeQueue'](this['R']['rnd']);
                                            this['setLrem'](_0x3d81x1e);
                                            this['v']['clearMainCanvas']();
                                            this['v']['clearQueueCanvas']();
                                            this['v']['clearHoldCanvas']();
                                            this['Caption']['mapLoading'](1);
                                            this['GameStats']['adjustToGameMode']();
                                            var _0x3d81x22 = function() {
                                                this['Caption']['hide']();
                                                this['ModeManager']['initialExecCommands'](() => {
                                                    _0x3d81x1d['call'](this)
                                                });
                                                this['updateQueueBox']()
                                            };
                                            this['ModeManager']['onReady'] = _0x3d81x22['bind'](this);
                                            this['ModeManager']['prepare'](_0x3d81x1c['mapId']);
                                            this['starting'] = true;
                                            return
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        _0x3d81x1d['call'](this)
    }
};
Game['prototype']['readyGo'] = function() {
    this['starting'] = true;
    this['Caption']['hideExcept'](this['Caption'].MODE_INFO);
    this['livePmode'] = 0;
    this['Live']['loadMapForOpponents'] = false;
    if (!this['pmode']) {
        this['Live']['beforeReset']();
        this['Replay']['config']['m'] = -1;
        this['Replay']['config']['m'] = 6553600;
        this['Replay']['config']['seed'] = this['blockSeed'];
        delete this['Replay']['config']['r'];
        this['v']['clearQueueCanvas']();
        for (var _0x3d81x13 = 0; _0x3d81x13 < this['Live']['maxSlots']; _0x3d81x13++) {
            this['GS']['slots'][_0x3d81x13]['clear']()
        };
        this['R'] = this['conf'][0];
        hideElem(this['rInfoBox']);
        hideElem(this['practiceMenuBig']);
        hideElem(this['Live']['teamOptions']);
        if (this['Live']['liveMode'] === 2) {
            hideElem(this['sprintInfo']);
            hideElem(this['practiceMenu']);
            showElem(this['teamInfo']);
            showElem(this['rInfoBox'])
        } else {
            if (this['Live']['liveMode'] === 3) {
                this['pmode'] = this['Live']['livePmodeTypes'][0];
                this['sprintMode'] = this['Live']['livePmodeTypes'][1];
                if (this['pmode'] === 6) {
                    this['sprintMode'] = this['Live']['liveMap']
                };
                var _0x3d81x23 = function() {
                    this['livePmode'] = this['pmode'];
                    this['pmode'] = 0;
                    showElem(this['rInfoBox']);
                    if (!(this['livePmode'] === 6 && this['MapManager']['mapData']['queue'])) {
                        this['generateLiveQueue']();
                        this['updateQueueBox']()
                    };
                    this['startReadyGo']();
                    return
                };
                var _0x3d81x1c = {
                    callback: _0x3d81x23['bind'](this),
                    isLivePmode: true,
                    mapId: this['sprintMode'],
                    mapForOpponents: true
                };
                this['startPractice'](this['pmode'], false, _0x3d81x1c);
                return
            }
        }
    };
    this['startReadyGo']()
};
Game['prototype']['startReadyGo'] = function() {
    if (this['isPmode'](false) !== 9) {
        this['blockInHold'] = null
    };
    this['redrawHoldBox']();
    if (this['isPmode'](false) !== 6 && this['isPmode'](false) !== 9) {
        this['v']['clearMainCanvas']()
    };
    var _0x3d81x24 = 2;
    if (this['SEStartEnabled']) {
        this['playSound']('ready')
    };
    this['Caption']['readyGo'](0);
    var _0x3d81x25 = this;
    this['interval'] = setInterval(function() {
        _0x3d81x24 -= 1;
        if (_0x3d81x24 === 1) {
            _0x3d81x25['Caption']['readyGo'](1);
            if (_0x3d81x25['SEStartEnabled']) {
                _0x3d81x25['playSound'](_0x3d81x25['isPmode'](true) ? 'go' : 'golive')
            }
        } else {
            if (_0x3d81x24 === 0) {
                clearInterval(_0x3d81x25['interval']);
                _0x3d81x25['restart']();
                _0x3d81x25['starting'] = false
            }
        }
    }, 900)
};
Game['prototype']['getPlace'] = function(_0x3d81x26, _0x3d81x27) {
    var _0x3d81x28 = 7;
    if (_0x3d81x27) {
        if (!_0x3d81x26) {
            if (this['place'] !== null) {
                _0x3d81x28 = this['place']
            } else {
                var _0x3d81x29 = 0;
                for (var _0x3d81x13 = 0; _0x3d81x13 < this['Live']['players']['length']; _0x3d81x13++) {
                    if (this['Live']['notPlaying']['indexOf'](this['Live']['players'][_0x3d81x13]) !== -1) {
                        _0x3d81x29 += 1
                    }
                };
                _0x3d81x28 = this['Live']['players']['length'] - _0x3d81x29 + 1
            }
        } else {
            _0x3d81x28 = this['Live']['players']['length'] + 1
        };
        this['place'] = _0x3d81x28;
        this['Caption']['gamePlace'](this)
    } else {
        if (!_0x3d81x26) {
            _0x3d81x28 = this['Live']['players']['length'] - this['Live']['notPlaying']['length'] + 1
        } else {
            _0x3d81x28 = this['Live']['players']['length'] + 1
        }
    };
    return _0x3d81x28
};
Game['prototype']['getPlaceColor'] = function(_0x3d81x28) {
    var _0x3d81x12 = {
            str: '',
            color: ''
        },
        _0x3d81x2a = i18n['th'],
        _0x3d81x2b = _0x3d81x28 / 10;
    if (_0x3d81x2b < 0.4 || _0x3d81x2b > 2) {
        switch (_0x3d81x28 % 10) {
            case 1:
                _0x3d81x2a = i18n['st'];
                break;
            case 2:
                _0x3d81x2a = i18n['nd'];
                break;
            case 3:
                _0x3d81x2a = i18n['rd'];
                break;
            default:
                _0x3d81x2a = i18n['th'];
                break
        }
    };
    _0x3d81x12['str'] = _0x3d81x28 + _0x3d81x2a;
    switch (_0x3d81x28) {
        case 1:
            _0x3d81x12['color'] = 'yellow';
            break;
        case 2:
            _0x3d81x12['color'] = 'orange';
            break;
        case 3:
            _0x3d81x12['color'] = '#FC6D3D';
            break;
        default:
            _0x3d81x12['color'] = '#00BFFF';
            break
    };
    return _0x3d81x12
};
Game['prototype']['start'] = function() {
    this['generateQueue']();
    this['getNextBlock']();
    this['redraw']();
    this['run']();
    this['Live']['onReset']()
};
Game['prototype']['restart'] = function() {
    if (this['starting'] && (this['Live']['LiveGameRunning'] || this['pmode'])) {
        if (this['inactiveGamesCount'] === 2 && !this['isPmode'](true)) {
            this['Live']['spectatorMode']();
            return
        };
        if (this['isPmode'](false) !== 6 && this['isPmode'](false) !== 9) {
            this['clearMatrix']();
            if (this['pmode'] !== 9) {
                this['ModeManager']['resetUI']()
            }
        };
        this['gameEnded'] = false;
        this['Replay']['clear']();
        this['Replay']['mode'] = 1;
        this['lastDAS'] = 0;
        this['firstDAS'] = true;
        this['holdPressed'] = false;
        this['holdUsedAlready'] = false;
        this['hardDropPressed'] = false;
        this['comboCounter'] = -1;
        this['bigTriggered'] = false;
        this['isBack2Back'] = false;
        this['lastHolePos'] = null;
        this['temporaryBlockSet'] = null;
        this['redrawHoldBox']();
        this['deadline'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this['activeBlock'] = new Block(0);
        this['place'] = null;
        this['frames'] = 0;
        this['timer'] = 0;
        this['clock'] = 0;
        this['redBar'] = 0;
        this['placedBlocks'] = 0;
        this['lastPlacements'] = [];
        this['totalFinesse'] = 0;
        this['totalKeyPresses'] = 0;
        this['finesse'] = 0;
        this['incomingGarbage'] = [];
        this['solidToAdd'] = 0;
        this['solidHeight'] = 0;
        this['applyGravityLvl'](this['R']['gravityLvl']);
        this['setLockDelay'](this['R']['lockDelay']);
        this['setSpeedLimit'](this['R']['speedLimit']);
        this['resetGameData']();
        this['Items']['onReset']();
        this['soundQ']['stop']();
        if (this['isPmode'](true) !== 9) {
            this['GameStats']['adjustToGameMode']();
            this['blockInHold'] = null
        };
        if (!this['isPmode'](true)) {
            if (!this['livePmode']) {
                this['generateLiveQueue']()
            };
            this['Live']['onReset']();
            if (this['Live']['liveMode'] !== 2 && this['Live']['liveMode'] !== 3) {
                hideElem(this['rInfoBox'])
            };
            this['Live']['toggleMorePractice'](true, false);
            if (this['R']['ext'] === 1) {
                this['comboAttack'] = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 0];
                if (this['comboTableTimer']) {
                    clearTimeout(this['comboTableTimer'])
                };
                this['comboTableTimer'] = setTimeout(() => {
                    this['comboAttack'] = this['comboAttackDef'];
                    this['comboTableTimer'] = null
                }, 20000)
            }
        };
        if (this['isPmode'](false)) {
            if (this['isPmode'](false) === 3) {
                this['cheeseModeStart']()
            } else {
                if (this['isPmode'](false) === 4) {
                    this['lastGarbage'] = 0
                }
            }
        };
        if (this['Live']['liveMode'] === 1 && !this['isPmode'](true)) {
            for (var _0x3d81x13 = 0; _0x3d81x13 < this['cheeseHeight']; _0x3d81x13++) {
                this['addGarbage'](1)
            }
        };
        if (this['softDrop']) {
            if (this['isSoftDropFasterThanGravity']()) {
                this['softDropSet'](true, 0)
            } else {
                this['softDropSet'](false, null)
            }
        };
        this['Caption']['hide']();
        if (this['focusState'] === 1) {
            this['Caption']['outOfFocus']()
        };
        this['GameStats']['reorder']();
        this['Settings']['onGameStart']();
        this['cancelSolid']();
        this['getNextBlock']();
        this['redrawBlocked'] = false;
        this['redraw']();
        var _0x3d81x2c = this['timestamp']();
        this['play'] = true;
        if (this['Replay']['mode'] !== -1) {
            this['Replay']['mode'] = 0
        };
        this['Replay']['config']['softDropId'] = this['softDropId'];
        this['Replay']['config']['gameStart'] = _0x3d81x2c;
        this['Replay']['config']['bs'] = this['skinId'];
        if (this['R']['baseBlockSet']) {
            this['Replay']['config']['bbs'] = this['R']['baseBlockSet']
        };
        if (this['monochromeSkin']) {
            this['Replay']['config']['bs'] = 7;
            this['Replay']['config']['mClr'] = this['monochromeSkin']
        };
        if (this['Settings']['touchControlsEnabled']) {
            this['Replay']['config']['touch'] = 1
        };
        if (this['isInvisibleSkin']) {
            this['Replay']['config']['bs'] += 100
        };
        if (this['skinId'] >= 1000 && this['customSkinPath']) {
            this['Replay']['config']['bp'] = this['customSkinPath']['substring'](0, 6)
        } else {
            delete this['Replay']['config']['bp']
        };
        this['Replay']['config']['se'] = this['Settings']['SFXsetID'];
        this['Replay']['config']['das'] = this['Settings']['DAS'];
        this['Replay']['config']['arr'] = (this['Settings']['ARR'] > 0) ? this['Settings']['ARR'] : undefined;
        delete this['Replay']['config']['sc'];
        this['Replay']['onMoveAdded'] = this['onMove']['bind'](this);
        if (!this['isPmode'](true)) {
            this['replayPartsSent'] = 0;
            this['Live']['sendReplayConfig']()
        };
        if (this['DASmethod'] === 1) {
            this['evalDefferedDAS'](null, _0x3d81x2c)
        }
    }
};
Game['prototype']['setFocusState'] = function(_0x3d81x24) {
    this['focusState'] = _0x3d81x24;
    if (_0x3d81x24 === 1 && this['play']) {
        this['Caption']['outOfFocus']()
    } else {
        this['Caption']['hide'](this['Caption'].OUT_OF_FOCUS)
    }
};
Game['prototype']['keyInput2'] = function(_0x3d81x2d) {
    if (_0x3d81x2d['keyCode'] === 113 || _0x3d81x2d['keyCode'] === this['Settings']['controls'][9]) {
        if (this['focusState'] === 1) {
            if (_0x3d81x2d['keyCode'] < 112 || _0x3d81x2d['keyCode'] > 123) {
                return
            };
            this['canvas']['focus']();
            this['setFocusState'](0)
        };
        if (_0x3d81x2d['keyCode'] >= 112 && _0x3d81x2d['keyCode'] <= 123) {
            _0x3d81x2d['preventDefault']()
        };
        this['Live']['sendRestartEvent']()
    } else {
        if (_0x3d81x2d['keyCode'] === 115 || _0x3d81x2d['keyCode'] === this['Settings']['controls'][8]) {
            if (this['focusState'] === 1) {
                if (_0x3d81x2d['keyCode'] < 112 || _0x3d81x2d['keyCode'] > 123) {
                    return
                };
                this['canvas']['focus']();
                this['setFocusState'](0)
            };
            if (_0x3d81x2d['keyCode'] >= 112 && _0x3d81x2d['keyCode'] <= 123) {
                _0x3d81x2d['preventDefault']()
            };
            this['startPractice'](this['selectedPmode']);
            if (this['pmode'] === 9) {
                _0x3d81x2d['stopImmediatePropagation']()
            }
        } else {
            if (_0x3d81x2d['keyCode'] === 13) {
                if (this['focusState'] !== 1) {
                    this['setFocusState'](1);
                    this['Live']['chatInput']['focus']()
                }
            }
        }
    };
    if (this['focusState'] === 0 && (_0x3d81x2d['keyCode'] === 38 || _0x3d81x2d['keyCode'] === 40 || _0x3d81x2d['keyCode'] === 32)) {
        _0x3d81x2d['preventDefault']()
    };
    if ((!this['play'] && !this['redrawBlocked']) && !this['starting']) {
        return
    };
    if (this['focusState'] === 0) {
        if (_0x3d81x2d['repeat']) {
            _0x3d81x2d['preventDefault']();
            return
        };
        if (this['starting']) {
            if (_0x3d81x2d['keyCode'] === this['Settings']['ml']) {
                this['pressedDir']['-1'] = this['timestamp']()
            } else {
                if (_0x3d81x2d['keyCode'] === this['Settings']['mr']) {
                    this['pressedDir']['1'] = this['timestamp']()
                } else {
                    if (_0x3d81x2d['keyCode'] === this['Settings']['sd'] && !this['softDrop']) {
                        this['softDropSet'](true, null)
                    }
                }
            };
            return
        };
        if (_0x3d81x2d['keyCode'] === this['Settings']['rl']) {
            this['rotateCurrentBlock'](-1);
            this['Replay']['add'](new ReplayAction(this['Replay']['Action'].ROTATE_LEFT, this['timestamp']()));
            this['lastAction'] = this['timestamp']()
        } else {
            if (_0x3d81x2d['keyCode'] === this['Settings']['rr']) {
                this['rotateCurrentBlock'](1);
                this['Replay']['add'](new ReplayAction(this['Replay']['Action'].ROTATE_RIGHT, this['timestamp']()));
                this['lastAction'] = this['timestamp']()
            } else {
                if (_0x3d81x2d['keyCode'] === this['Settings']['dr']) {
                    this['rotateCurrentBlock'](2);
                    this['Replay']['add'](new ReplayAction(this['Replay']['Action'].ROTATE_180, this['timestamp']()));
                    this['lastAction'] = this['timestamp']()
                } else {
                    if (_0x3d81x2d['keyCode'] === this['Settings']['ml'] && !this['pressedDir']['-1']) {
                        var _0x3d81x2e = this['timestamp']();
                        this['moveCurrentBlock'](-1, false, _0x3d81x2e);
                        this['pressedDir']['-1'] = _0x3d81x2e;
                        this['Replay']['add'](new ReplayAction(this['Replay']['Action'].MOVE_LEFT, this['pressedDir']['-1']));
                        if (this['Settings']['DAScancel'] && this['pressedDir']['1']) {
                            this['directionCancel'](1, -1)
                        };
                        if (this['DASmethod'] === 1) {
                            this['setDASto'](-1, true, null)
                        }
                    } else {
                        if (_0x3d81x2d['keyCode'] === this['Settings']['mr'] && !this['pressedDir']['1']) {
                            var _0x3d81x2e = this['timestamp']();
                            this['moveCurrentBlock'](1, false, _0x3d81x2e);
                            this['pressedDir']['1'] = _0x3d81x2e;
                            this['Replay']['add'](new ReplayAction(this['Replay']['Action'].MOVE_RIGHT, this['pressedDir']['1']));
                            if (this['Settings']['DAScancel'] && this['pressedDir']['-1']) {
                                this['directionCancel'](-1, -1)
                            };
                            if (this['DASmethod'] === 1) {
                                this['setDASto'](1, true, null)
                            }
                        } else {
                            if (_0x3d81x2d['keyCode'] === this['Settings']['hk'] && !this['holdPressed']) {
                                this['holdBlock']();
                                this['holdPressed'] = true
                            } else {
                                if (this['redrawBlocked']) {
                                    return
                                } else {
                                    if (!this['hardDropPressed'] && _0x3d81x2d['keyCode'] === this['Settings']['hd'] && this['isHardDropAllowed']()) {
                                        this['hardDropPressed'] = true;
                                        this['hardDrop'](this['timestamp']())
                                    } else {
                                        if (_0x3d81x2d['keyCode'] === this['Settings']['sd'] && !this['softDrop']) {
                                            this['softDropSet'](true, this['timestamp']())
                                        } else {
                                            if (false && _0x3d81x2d['keyCode'] === this['Items']['key']) {
                                                this['Items']['use'](this['timestamp']())
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        if (_0x3d81x2d['keyCode'] < 113 || _0x3d81x2d['keyCode'] > 123) {
            _0x3d81x2d['preventDefault']();
            _0x3d81x2d['stopPropagation']();
            return false
        }
    }
};
Game['prototype']['keyInput3'] = function(_0x3d81x2d) {
    if (_0x3d81x2d['keyCode'] === this['Settings']['ml']) {
        this['directionCancel'](-1, false);
        if (this['pressedDir']['1'] === -1) {
            this['pressedDir']['1'] = this['timestamp']()
        };
        if (this['DASmethod'] === 1) {
            this['setDASto'](-1, false)
        }
    } else {
        if (_0x3d81x2d['keyCode'] === this['Settings']['mr']) {
            this['directionCancel'](1, false);
            if (this['pressedDir']['-1'] === -1) {
                this['pressedDir']['-1'] = this['timestamp']()
            };
            if (this['DASmethod'] === 1) {
                this['setDASto'](1, false)
            }
        } else {
            if (_0x3d81x2d['keyCode'] === this['Settings']['sd'] && this['softDrop']) {
                this['softDropSet'](false, this['timestamp']())
            } else {
                if (_0x3d81x2d['keyCode'] === this['Settings']['hk']) {
                    this['holdPressed'] = false
                } else {
                    if (_0x3d81x2d['keyCode'] === this['Settings']['hd']) {
                        this['hardDropPressed'] = false
                    }
                }
            }
        }
    }
};
Game['prototype']['directionCancel'] = function(_0x3d81x2f, _0x3d81x30) {
    if (_0x3d81x2f === -1) {
        this['pressedDir']['-1'] = _0x3d81x30;
        this['ARRon']['-1'] = false
    } else {
        this['pressedDir']['1'] = _0x3d81x30;
        this['ARRon']['1'] = false
    };
    if (this['DASmethod'] === 1 && _0x3d81x30 === false) {
        var _0x3d81x31 = (_0x3d81x2f === -1) ? 1 : -1;
        var _0x3d81x2e = this['timestamp']();
        if (this['pressedDir'][_0x3d81x31]) {
            if (this['Settings']['DAScancel']) {
                this['setDASto'](_0x3d81x31, true, null)
            } else {
                this['evalDefferedDAS'](_0x3d81x31, _0x3d81x2e)
            }
        }
    }
};
Game['prototype']['evalDefferedDAS'] = function(_0x3d81x2f, _0x3d81x2e) {
    if (_0x3d81x2f === null) {
        if (this['pressedDir']['-1']) {
            _0x3d81x2f = -1
        };
        if (this['pressedDir']['1'] && (_0x3d81x2f === null || this['pressedDir']['1'] < this['pressedDir']['-1'])) {
            _0x3d81x2f = 1
        };
        if (_0x3d81x2f === null) {
            return
        }
    } else {
        if (!this['pressedDir'][_0x3d81x2f]) {
            return
        }
    };
    var _0x3d81x32 = Math['max'](0, this['Settings']['DAS'] - (_0x3d81x2e - this['pressedDir'][_0x3d81x2f]));
    this['setDASto'](_0x3d81x2f, true, _0x3d81x32)
};
Game['prototype']['setDASto'] = function(_0x3d81x2f, _0x3d81x30, _0x3d81x33) {
    var _0x3d81x32 = (_0x3d81x33 === null) ? this['Settings']['DAS'] : _0x3d81x33;
    if (!_0x3d81x30 && this['DASto'][_0x3d81x2f] !== null) {
        window['clearTimeout'](this['DASto'][_0x3d81x2f]);
        this['DASto'][_0x3d81x2f] = null
    } else {
        if (_0x3d81x30 && this['DASto'][_0x3d81x2f] === null) {
            var _0x3d81x25 = this;
            this['DASto'][_0x3d81x2f] = window['setTimeout'](function() {
                _0x3d81x25['DASto'][_0x3d81x2f] = null;
                var _0x3d81x31 = (_0x3d81x2f === -1) ? 1 : -1;
                if (_0x3d81x25['lastDAS'] !== _0x3d81x2f && (_0x3d81x25['play'] || _0x3d81x25['animator'] !== null)) {
                    var _0x3d81x2e = _0x3d81x25['timestamp']();
                    if (_0x3d81x25['pressedDir'][_0x3d81x2f] === -1 || (_0x3d81x25['pressedDir']['-1'] > 0 && _0x3d81x25['pressedDir']['1'] > 0 && _0x3d81x25['pressedDir'][_0x3d81x31] > _0x3d81x25['pressedDir'][_0x3d81x2f])) {} else {
                        _0x3d81x25['activateDAS'](_0x3d81x2f, _0x3d81x2e);
                        if (_0x3d81x25['DASdebug'] && _0x3d81x33 === null) {
                            _0x3d81x25['plotForDASDebug'](_0x3d81x2e - _0x3d81x25['pressedDir'][_0x3d81x2f])
                        }
                    }
                }
            }, _0x3d81x32)
        }
    }
};
Game['prototype']['redrawMatrix'] = function() {
    this['v']['redrawMatrix']()
};
Game['prototype']['drawGhostAndCurrent'] = function() {
    var _0x3d81x34 = this['blockSets'][this['activeBlock']['set']],
        _0x3d81x35 = (_0x3d81x34['scale'] === 1) ? _0x3d81x34['blocks'][this['activeBlock']['id']]['blocks'][this['activeBlock']['rot']] : _0x3d81x34['previewAs']['blocks'][this['activeBlock']['id']]['blocks'][this['activeBlock']['rot']],
        _0x3d81x36 = _0x3d81x35['length'];
    this['drawScale'] = _0x3d81x34['scale'];
    if (this['ghostEnabled'] && !this['gameEnded']) {
        for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
            for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
                if (_0x3d81x35[_0x3d81x18][_0x3d81x19] > 0) {
                    this['v']['drawGhostBlock'](this['ghostPiece']['pos']['x'] + _0x3d81x19 * this['drawScale'], this['ghostPiece']['pos']['y'] + _0x3d81x18 * this['drawScale'], _0x3d81x34['blocks'][this['activeBlock']['id']]['color']);
                    if (this['activeBlock']['item'] && _0x3d81x35[_0x3d81x18][_0x3d81x19] === this['activeBlock']['item']) {
                        this['v']['drawBrickOverlay'](this['ghostPiece']['pos']['x'] + _0x3d81x19 * this['drawScale'], this['ghostPiece']['pos']['y'] + _0x3d81x18 * this['drawScale'], true)
                    }
                }
            }
        }
    };
    if (!this['gameEnded']) {
        for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
            for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
                if (_0x3d81x35[_0x3d81x18][_0x3d81x19] > 0) {
                    this['v']['drawBlock'](this['activeBlock']['pos']['x'] + _0x3d81x19 * this['drawScale'], this['activeBlock']['pos']['y'] + _0x3d81x18 * this['drawScale'], _0x3d81x34['blocks'][this['activeBlock']['id']]['color'], 0);
                    if (this['activeBlock']['item'] && _0x3d81x35[_0x3d81x18][_0x3d81x19] === this['activeBlock']['item']) {
                        this['v']['drawBrickOverlay'](this['activeBlock']['pos']['x'] + _0x3d81x19 * this['drawScale'], this['activeBlock']['pos']['y'] + _0x3d81x18 * this['drawScale'], false)
                    }
                }
            }
        }
    };
    this['drawScale'] = 1
};
Game['prototype']['redraw'] = function() {
    if (this['redrawBlocked']) {
        return
    };
    this['redrawMatrix']();
    this['drawGhostAndCurrent']();
    this['v']['redrawRedBar'](false);
    if (this['statsEnabled'] && this['statsMode'] === 1) {
        this['stats']['update']()
    }
};
Game['prototype']['redrawAll'] = function() {
    this['redraw']();
    this['redrawHoldBox']();
    this['updateQueueBox']()
};
Game['prototype']['updateLiveMatrix'] = function(_0x3d81x37, _0x3d81x38, _0x3d81x39) {
    var _0x3d81x20 = this['GS']['slots'][_0x3d81x37]['cid'];
    if (!this['GS']['extendedAvailable'] || arrayContains(this['Live']['bots'], _0x3d81x20) || this['Live']['clients'][_0x3d81x20]['rep'] === null) {
        if (!this['GS']['slots'][_0x3d81x37]['v']['g']) {
            this['GS']['slots'][_0x3d81x37]['v']['g'] = this
        };
        this['GS']['slots'][_0x3d81x37]['v']['updateLiveMatrix'](_0x3d81x38, _0x3d81x39)
    }
};
Game['prototype']['playSound'] = function(_0x3d81x7, _0x3d81xb) {
    if (!this['SEenabled'] || !this['R']['sfx']) {
        return
    };
    if (!_0x3d81xb) {
        _0x3d81xb = 0
    };
    var _0x3d81x25 = this,
        _0x3d81x3a = false;
    var _0x3d81x3b = function(_0x3d81x7, _0x3d81x3c) {
        if (!_0x3d81x7 || !(_0x3d81x7 in createjs['Sound']['_idHash'])) {
            return
        };
        let _0x3d81x11 = createjs['Sound']['_idHash'][_0x3d81x7]['sndObj'];
        if (_0x3d81x11 && _0x3d81x11['q']) {
            if (_0x3d81x11['q'] === 1 && !_0x3d81x3a) {
                _0x3d81x3a = true;
                _0x3d81x25['soundQ']['stop']()
            };
            _0x3d81x25['soundQ']['add'](_0x3d81x7, _0x3d81x3c)
        } else {
            var _0x3d81x3d = createjs['Sound']['play'](_0x3d81x7);
            _0x3d81x3d['volume'] = _0x3d81x3c
        }
    };
    if (!Array['isArray'](_0x3d81x7)) {
        if (_0x3d81xb !== 2) {
            _0x3d81x3b(_0x3d81x7, this['SFXset']['volume'])
        };
        if (_0x3d81xb !== 1 && this['VSEenabled'] && this['R']['vsfx']) {
            _0x3d81x3b('v_' + _0x3d81x7, this['VSFXset']['volume'])
        }
    } else {
        if (_0x3d81xb !== 2) {
            _0x3d81x7['forEach'](function(_0x3d81x13) {
                _0x3d81x3b(_0x3d81x13, _0x3d81x25['SFXset']['volume'])
            })
        };
        if (_0x3d81xb !== 1 && this['VSEenabled'] && this['R']['vsfx']) {
            _0x3d81x7['forEach'](function(_0x3d81x13) {
                _0x3d81x3b('v_' + _0x3d81x13, _0x3d81x25['VSFXset']['volume'])
            })
        }
    }
};
Game['prototype']['moveCurrentBlock'] = function(_0x3d81x3e, _0x3d81x3f, _0x3d81x40) {
    if (!_0x3d81x3f) {
        this['finesse']++
    };
    _0x3d81x3e *= this['blockSets'][this['activeBlock']['set']]['step'];
    if (!this['checkIntersection'](this['activeBlock']['pos']['x'] + _0x3d81x3e, this['activeBlock']['pos']['y'], null)) {
        this['activeBlock']['pos']['x'] = this['activeBlock']['pos']['x'] + _0x3d81x3e;
        this['lastAction'] = _0x3d81x40;
        this['updateGhostPiece'](true);
        this['redraw']();
        if (!_0x3d81x3f) {
            this['playSound']('move')
        };
        return true
    };
    return false
};
Game['prototype']['beforeHardDrop'] = function(_0x3d81x40) {
    if (false && this['Items']['active']['length']) {
        this['Items']['onHardDrop']()
    };
    if (this['bigTriggered'] && this['Live']['LiveGameRunning'] && !this['pmode']) {
        this['bigTriggered'] = false;
        var _0x3d81x41 = 2;
        var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].AUX, _0x3d81x40);
        _0x3d81x42['d'] = [this['Replay']['AUX']['BLOCK_SET'], 0, _0x3d81x41];
        this['Replay']['add'](_0x3d81x42);
        this['temporaryBlockSet'] = _0x3d81x41
    }
};
Game['prototype']['hardDrop'] = function(_0x3d81x40) {
    this['beforeHardDrop'](_0x3d81x40);
    if (this['hdAbort']) {
        this['hdAbort'] = false;
        return
    };
    this['Replay']['add'](new ReplayAction(this['Replay']['Action'].HARD_DROP, _0x3d81x40));
    var _0x3d81x34 = this['blockSets'][this['activeBlock']['set']];
    var _0x3d81x43 = this['activeBlock']['pos']['x'] + _0x3d81x34['blocks'][this['activeBlock']['id']]['cc'][this['activeBlock']['rot']];
    var _0x3d81x44 = this['finesse'] - ((this['activeBlock']['set'] === 0) ? finesse[this['activeBlock']['id']][this['activeBlock']['rot']][_0x3d81x43] : 0);
    if (_0x3d81x44 > 0) {
        this['totalFinesse'] += _0x3d81x44;
        if (this['pmode'] === 1 && this['Settings']['restartSprintOnFF']) {
            this.GameOver();
            this['startPractice'](1)
        }
    };
    this['totalKeyPresses'] += this['finesse'];
    this['finesse'] = 0;
    if (!this['ghostEnabled']) {
        this['updateGhostPiece'](true)
    };
    var _0x3d81x18 = this['ghostPiece']['pos']['y'];
    if (this['spinPossible'] && _0x3d81x18 !== this['activeBlock']['pos']['y']) {
        this['spinPossible'] = false
    };
    this['score'](this['Scoring']['A'].HARD_DROP, _0x3d81x18 - this['activeBlock']['pos']['y']);
    this['GameStats']['get']('FINESSE')['set'](this['totalFinesse']);
    this['placeBlock'](this['ghostPiece']['pos']['x'], _0x3d81x18, _0x3d81x40);
    this['redraw']();
    if (this['pmode'] === 9) {
        this['ModeManager']['on'](this['ModeManager'].BLOCK, this['placedBlocks'])
    };
    if (this['SEenabled'] && this['play']) {
        if (_0x3d81x44 > 0 && this['SEFaultEnabled']) {
            this['playSound']('fault')
        } else {
            this['playSound'](this['hardDropPressed'] ? 'harddrop' : 'lock')
        }
    }
};
Game['prototype']['isSoftDropFasterThanGravity'] = function() {
    return this['R']['gravityLvl'] <= 1 || (this['softDropSpeeds'][this['softDropId']]['time'] / (this['softDropSpeeds'][this['softDropId']]['steps'] + 1) < this['baseGravity'][0] / this['baseGravity'][1])
};
Game['prototype']['softDropSet'] = function(_0x3d81x30, _0x3d81x45) {
    let _0x3d81x46 = false;
    if (_0x3d81x30 === true) {
        if (this['isSoftDropFasterThanGravity']()) {
            this['softDrop'] = true;
            this['currentGravity'][0] = this['softDropSpeeds'][this['softDropId']]['time'];
            this['currentGravity'][1] = this['softDropSpeeds'][this['softDropId']]['steps'];
            _0x3d81x46 = true
        }
    } else {
        _0x3d81x46 = this['softDrop'];
        this['softDrop'] = false;
        this['currentGravity'] = this['baseGravity']['slice'](0);
        this['timer'] = 0
    };
    if (_0x3d81x45 !== null && _0x3d81x46) {
        this['Replay']['add'](new ReplayAction(this['Replay']['Action'].SOFT_DROP_BEGIN_END, _0x3d81x45))
    }
};
Game['prototype']['GameOver'] = function() {
    this['paintMatrixWithColor'](9);
    this['gameEnded'] = true;
    this['play'] = false;
    if (!this['isPmode'](true)) {
        this['sendRepFragment']();
        this['Live']['sendGameOverEvent']();
        this['getPlace'](false, true);
        if (!this['Replay']['hasUserInputs']()) {
            if (++this['inactiveGamesCount'] === 2) {
                this['Live']['showInChat']('', i18n['inactive1'])
            } else {
                this['Live']['showInChat']('<span style=\'color:yellow\'>' + i18n['warning'] + '</span>', i18n['inactive2'])
            }
        }
    };
    if (this['isPmode'](false)) {
        if (this['isPmode'](false) === 1) {} else {
            if (this['isPmode'](true) === 4) {
                this['Live']['sendGameModeResult'](this.Replay)
            } else {
                if (this['isPmode'](true) === 7) {
                    this['practiceModeCompleted']()
                } else {
                    if (this['isPmode'](true) === 9) {
                        this['ModeManager']['saveScore'](false)
                    }
                }
            }
        }
    };
    this['Live']['onGameEnd']();
    this['Settings']['onGameEnd']();
    this['Items']['reset']();
    this['updateTextBar']();
    this['playSound']('died');
    this['GS']['setTarget'](-1);
    this['cancelSolid']()
};
Game['prototype']['paintMatrixWithColor'] = function(_0x3d81x47) {
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            if (this['matrix'][_0x3d81x18][_0x3d81x19] > 0) {
                this['matrix'][_0x3d81x18][_0x3d81x19] = _0x3d81x47
            }
        }
    }
};
Game['prototype']['generatePracticeQueue'] = function(_0x3d81x48) {
    var _0x3d81x49 = 0;
    do {
        var _0x3d81x4a = this.RNG().toString(36)['substring'](7);
        this['blockRNG'] = alea(_0x3d81x4a);
        this['Replay']['config']['seed'] = _0x3d81x4a;
        this['initRandomizer'](_0x3d81x48);
        this['generateQueue']()
    } while (++_0x3d81x49 < 1000 && this['isBannedStartSequence'](this['queue'], false));
};
Game['prototype']['getAdjustedLiveSeed'] = function(_0x3d81x4b) {
    var _0x3d81x49 = 0,
        _0x3d81x4c = [],
        _0x3d81x4d = _0x3d81x4b;
    do {
        _0x3d81x4b = _0x3d81x4d + ((_0x3d81x49 === 0) ? '' : _0x3d81x49.toString(36));
        let _0x3d81x4e = alea(_0x3d81x4b),
            _0x3d81x4f = this['randomizerFactory'](this['conf'][0]['rnd'], _0x3d81x4e);
        _0x3d81x4c = this['getQueuePreview'](_0x3d81x4f)
    } while (++_0x3d81x49 < 1000 && this['isBannedStartSequence'](_0x3d81x4c, true));;
    return _0x3d81x4b
};
Game['prototype']['generateLiveQueue'] = function() {
    this['blockRNG'] = alea(this['blockSeed']);
    this['RNG'] = alea(this['blockSeed']);
    this['Replay']['config']['seed'] = this['blockSeed'];
    this['initRandomizer'](this['conf'][0]['rnd']);
    this['generateQueue']()
};
Game['prototype']['isBannedStartSequence'] = function(_0x3d81x4c, _0x3d81x50) {
    _0x3d81x4c = _0x3d81x4c || this['queue'];
    let _0x3d81x51 = (_0x3d81x50) ? ((this['Live']['liveMode'] === 3) ? this['Live']['livePmodeTypes'][0] : this['livePmode']) : this['pmode'],
        _0x3d81x52 = (_0x3d81x50) ? this['conf'][0]['baseBlockSet'] : this['conf'][1]['baseBlockSet'];
    return (_0x3d81x4c['length'] >= 2 && _0x3d81x52 <= 3 && (!_0x3d81x50 || this['conf'][0]['rnd'] === 0) && ((_0x3d81x51 === 1 && (_0x3d81x4c[0]['id'] >= 5 || (_0x3d81x4c[0]['id'] === 1 && _0x3d81x4c[1]['id'] >= 5))) || (_0x3d81x51 !== 2 && _0x3d81x4c[0]['id'] >= 5 && _0x3d81x4c[1]['id'] >= 5)))
};
Game['prototype']['refillQueue'] = function() {
    let _0x3d81x53 = (this['pmode'] === 6 && this['MapManager']['mapData']['queue'] != null) || (this['pmode'] === 9 && this['ModeManager']['noQueueRefill']);
    if (!_0x3d81x53) {
        let _0x3d81x54 = this['getRandomizerBlock']();
        if (false && this['blockSets'][_0x3d81x54['set']]['items']) {
            _0x3d81x54['item'] = this['Items']['genItem']()
        };
        this['queue']['push'](_0x3d81x54)
    }
};
Game['prototype']['getNextBlock'] = function(_0x3d81x40) {
    _0x3d81x40 = _0x3d81x40 || this['timestamp']();
    this['lockDelayActive'] = false;
    this['lastGeneration'] = _0x3d81x40;
    this['activeBlock'] = this['getBlockFromQueue']();
    this['setCurrentPieceToDefaultPos']();
    this['updateGhostPiece'](true);
    this['checkAutoRepeat'](_0x3d81x40, false);
    if (!this['isPmode'](true) && this['transmitMode'] === 0 && this['snapRate'] <= 1000 && _0x3d81x40 - this['lastSnapshot'] > this['snapRate'] / 4) {
        this['sendSnapshot']();
        this['lastSnapshot'] = _0x3d81x40
    };
    if (this['VSEenabled'] && this['VSFXset']['spawns']) {
        this['playCurrentPieceSound']()
    };
    this['timer'] = 0
};
Game['prototype']['playCurrentPieceSound'] = function() {
    this['playSound']('b_' + this['blockSets'][this['activeBlock']['set']]['blocks'][this['activeBlock']['id']]['name'])
};
Game['prototype']['practiceModeCompleted'] = function(_0x3d81x40) {
    _0x3d81x40 = _0x3d81x40 || this['timestamp']();
    this['play'] = false;
    this['gameEnded'] = true;
    this['Replay']['config']['gameEnd'] = _0x3d81x40;
    this['updateTextBar']();
    this['paintMatrixWithColor'](9);
    hideElem(this['sprintInfo']);
    showElem(this['practiceMenu']);
    if (this['isPmode'](true) === 2 || this['isPmode'](true) === 9) {
        return
    };
    if (this['livePmode']) {
        this['Live']['raceCompleted']()
    } else {
        if (this['pmode']) {
            this['Live']['sendGameModeResult'](this.Replay)
        }
    }
};
Game['prototype']['getComboAttack'] = function(_0x3d81x55) {
    return (_0x3d81x55 <= 12) ? this['comboAttack'][_0x3d81x55] : this['comboAttack'][this['comboAttack']['length'] - 1]
};
Game['prototype']['deleteFromGarbageQueue'] = function(_0x3d81x56) {
    var _0x3d81x57 = this['incomingGarbage']['length'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x57; _0x3d81x13++) {
        if (this['incomingGarbage'][_0x3d81x13][0] >= _0x3d81x56) {
            this['incomingGarbage'][_0x3d81x13][0] -= _0x3d81x56;
            break
        } else {
            _0x3d81x56 -= this['incomingGarbage'][_0x3d81x13][0];
            this['incomingGarbage'][_0x3d81x13][0] = 0
        }
    }
};
Game['prototype']['blockOrSendAttack'] = function(_0x3d81x56, _0x3d81x2e) {
    this['gamedata']['attack'] += _0x3d81x56;
    if (this['redBar'] > 0 && this['R']['gblock'] < 2) {
        if (this['redBar'] > _0x3d81x56) {
            this['redBar'] -= _0x3d81x56;
            this['deleteFromGarbageQueue'](_0x3d81x56);
            _0x3d81x56 = 0
        } else {
            _0x3d81x56 -= this['redBar'];
            this['redBar'] = 0;
            this['incomingGarbage'] = []
        };
        this['recordRedbarChange'](_0x3d81x2e)
    };
    if (_0x3d81x56 > 0 && this['Live']['liveMode'] === 0 || this['Live']['liveMode'] === 2) {
        return _0x3d81x56
    };
    return null
};
Game['prototype']['frame'] = function() {
    let _0x3d81x58 = this['timestamp']();
    this['update'](Math['min'](1, (_0x3d81x58 - this['last']) / 1000.0), _0x3d81x58);
    this['last'] = _0x3d81x58;
    if (this['statsEnabled'] && this['statsMode'] === 0) {
        this['stats']['update']()
    };
    window['requestAnimFrame'](this['frame']['bind'](this), this['canvas'])
};
Game['prototype']['run'] = function() {
    this['last'] = this['timestamp']();
    this['frame']()
};
Game['prototype']['onMove'] = function(_0x3d81x42) {
    if (_0x3d81x42['a'] !== this['Replay']['Action']['DAS_LEFT'] && _0x3d81x42['a'] !== this['Replay']['Action']['DAS_RIGHT']) {
        this['lastDAS'] = 0
    } else {
        if (this['firstDAS']) {
            this['firstDAS'] = false
        }
    }
};
Game['prototype']['checkAutoRepeat'] = function(_0x3d81x40, _0x3d81x59) {
    let _0x3d81x12 = false,
        _0x3d81x5a = false;
    if (!_0x3d81x59 && !this['redrawBlocked']) {
        _0x3d81x5a = true;
        this['redrawBlocked'] = true
    };
    if (this['pressedDir']['-1'] > 0 && this['pressedDir']['1'] > 0) {
        if (this['pressedDir']['-1'] > this['pressedDir']['1']) {
            _0x3d81x12 = this['autoRepeat'](-1, this['pressedDir']['-1'], _0x3d81x40)
        } else {
            _0x3d81x12 = this['autoRepeat'](1, this['pressedDir']['1'], _0x3d81x40)
        }
    } else {
        if (this['pressedDir']['-1'] > 0) {
            _0x3d81x12 = this['autoRepeat'](-1, this['pressedDir']['-1'], _0x3d81x40)
        } else {
            if (this['pressedDir']['1'] > 0) {
                _0x3d81x12 = this['autoRepeat'](1, this['pressedDir']['1'], _0x3d81x40)
            }
        }
    };
    if (!_0x3d81x59 && _0x3d81x5a) {
        this['redrawBlocked'] = false
    };
    return _0x3d81x12
};
Game['prototype']['autoRepeat'] = function(_0x3d81x3e, _0x3d81x5b, _0x3d81x40) {
    if (this['DASmethod'] === 1 && this['ARRon'][_0x3d81x3e] && !this['Settings']['ARR']) {
        return this['activateDAS'](_0x3d81x3e, _0x3d81x40)
    } else {
        if (this['Settings']['ARR'] && this['ARRon'][_0x3d81x3e]) {
            if (_0x3d81x40 - this['ARRtime'] >= this['Settings']['ARR']) {
                this['ARRtime'] = _0x3d81x40;
                if (this['moveCurrentBlock'](_0x3d81x3e, true, _0x3d81x40)) {
                    var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].ARR_MOVE, _0x3d81x40);
                    _0x3d81x42['d'] = [(_0x3d81x3e === -1) ? 0 : 1];
                    this['Replay']['add'](_0x3d81x42);
                    return true
                }
            }
        } else {
            if (this['DASmethod'] === 0 && _0x3d81x40 - _0x3d81x5b >= this['Settings']['DAS'] && this['lastDAS'] !== _0x3d81x3e) {
                if (this['DASdebug'] && !this['ARRon'][_0x3d81x3e]) {
                    this['plotForDASDebug'](_0x3d81x40 - _0x3d81x5b)
                };
                return this['activateDAS'](_0x3d81x3e, _0x3d81x40)
            }
        }
    };
    return false
};
Game['prototype']['plotForDASDebug'] = function(_0x3d81x5c) {
    if (!this['firstDAS']) {
        var _0x3d81x5d = Math['max'](0, _0x3d81x5c - this['Settings']['DAS']);
        this['statsDASPanel']['update'](_0x3d81x5d, 32)
    }
};
Game['prototype']['activateDAS'] = function(_0x3d81x3e, _0x3d81x40) {
    this['ARRon'][_0x3d81x3e] = true;
    this['lastDAS'] = _0x3d81x3e;
    if (this['Settings']['ARR'] === 0) {
        if (this['moveBlockToTheWall'](_0x3d81x3e)) {
            this['Replay']['add'](new ReplayAction(((_0x3d81x3e === -1) ? this['Replay']['Action']['DAS_LEFT'] : this['Replay']['Action']['DAS_RIGHT']), _0x3d81x40));
            return true
        }
    } else {
        this['ARRtime'] = _0x3d81x40
    };
    return false
};
Game['prototype']['update'] = function(_0x3d81x5e, _0x3d81x40) {
    this['frames']++;
    if (this['v']['videoSkin']) {
        if (this['v']['videoOpts']['fpsUpdate']) {
            this['v']['updateTexture'](0)
        };
        this['redrawAll']()
    };
    if (this['Settings']['gamepadFound']) {
        this['Settings']['processGamepad']()
    };
    if (this['animator'] !== null) {
        this['animator']['render'](_0x3d81x5e)
    };
    if (this['play']) {
        this['timer'] += _0x3d81x5e;
        this['clock'] += _0x3d81x5e;
        if (this['timer'] >= this['currentGravity'][0]) {
            this['timer'] = this['timer'] - this['currentGravity'][0];
            var _0x3d81x5f = this['gravityStep'](this['currentGravity'][1], _0x3d81x40);
            if (_0x3d81x5f) {
                if (this['softDrop'] || _0x3d81x5f === 1) {
                    this['Replay']['add'](new ReplayAction(this['Replay']['Action'].GRAVITY_STEP, _0x3d81x40));
                    if (this['softDrop']) {
                        this['score'](this['Scoring']['A'].SOFT_DROP, _0x3d81x5f)
                    }
                } else {
                    var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].AUX, _0x3d81x40);
                    _0x3d81x42['d'] = [this['Replay']['AUX']['MOVE_TO'], this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y']];
                    this['Replay']['add'](_0x3d81x42)
                }
            };
            this['redraw']()
        };
        this['checkAutoRepeat'](_0x3d81x40, true);
        if (this['lockDelayActive']) {
            if (_0x3d81x40 - this['lastAction'] >= this['lockDelay']) {
                if (this['checkIntersection'](this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y'] + 1, null)) {
                    this['hardDrop'](_0x3d81x40)
                } else {
                    if (this['pmode'] || (this['solidInterval'] === null && !this['solidToAdd'])) {
                        this['lockDelayActive'] = false
                    }
                }
            };
            if (_0x3d81x40 - this['lockDelayActivated'] >= this['maxLockDelayWithoutLock']) {
                this['hardDrop'](_0x3d81x40)
            }
        };
        if (!this['isPmode'](true)) {
            if (this['transmitMode'] === 1 && _0x3d81x40 - this['lastSnapshot'] > this['liveSnapRate']) {
                this['sendRepFragment']();
                this['lastSnapshot'] = _0x3d81x40;
                if (++this['fragmentCounter'] >= 10) {
                    this['fragmentCounter'] = 0;
                    this['sendSnapshot']()
                }
            } else {
                if (this['transmitMode'] === 0 && _0x3d81x40 - this['lastSnapshot'] > this['snapRate']) {
                    this['sendSnapshot']();
                    this['lastSnapshot'] = _0x3d81x40
                }
            }
        };
        if (this['isPmode'](false) === 4 && this['clock'] - this['lastGarbage'] > 1) {
            var _0x3d81x5d = this['clock'] - this['lastGarbage'] - 1;
            this['addAsyncGarbage'](1);
            this['redraw']();
            this['lastGarbage'] = this['clock'] - _0x3d81x5d
        } else {
            if (this['isPmode'](false) === 5) {
                var _0x3d81x60 = this['ultraModes'][this['sprintMode']] - this['clock'];
                if (this['linesRemaining'] - _0x3d81x60 >= 1) {
                    this['linesRemaining']--;
                    this['lrem']['textContent'] = sprintTimeFormat(this['linesRemaining'], -1)
                };
                if (this['linesRemaining'] <= 0) {
                    this['practiceModeCompleted'](_0x3d81x40)
                }
            } else {
                if (this['pmode'] === 9 && this['ModeManager']['timeTriggers']['length']) {
                    let _0x3d81x61 = this['ModeManager']['timeTriggers'][0];
                    if (this['clock'] >= _0x3d81x61['t']) {
                        this['ModeManager']['execCommands'](_0x3d81x61['c']);
                        this['ModeManager']['timeTriggers']['shift']()
                    }
                }
            }
        };
        if ((this['frames'] & 7) === 7 || this['GameStats']['dirty']) {
            this['updateTextBar']()
        };
        if ((this['frames'] & 31) === 31) {
            if (!this['pmode'] && ((this['frames'] & 63) === 63)) {
                this['Live']['changeTarget']()
            };
            if (_0x3d81x40 - this['lastGeneration'] >= this['maxWithoutLock']) {
                this['hardDrop'](_0x3d81x40)
            };
            var _0x3d81x62 = this['solidHeight'] + this['solidToAdd'];
            if (_0x3d81x62 !== this['maxWithoutLock']) {
                var _0x3d81x63 = Math['max'](2500, this['maxWithoutLock'] - _0x3d81x62 * (this['maxWithoutLock'] / 10));
                if (_0x3d81x40 - this['lastGeneration'] > _0x3d81x63) {
                    this['hardDrop'](_0x3d81x40)
                }
            }
        }
    };
    if (this['Live']['xbufferEnabled'] && ((this['frames'] & this['xbuffMask']) === this['xbuffMask'])) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < this['GS']['shownSlots']; _0x3d81x13++) {
            var _0x3d81x20 = this['GS']['slots'][_0x3d81x13]['cid'];
            if (this['Live']['xbuffer']['hasOwnProperty'](_0x3d81x20)) {
                var _0x3d81x64 = this['Live']['xbuffer'][_0x3d81x20];
                this['updateLiveMatrix'](_0x3d81x13, _0x3d81x64[0], _0x3d81x64[1]);
                delete this['Live']['xbuffer'][_0x3d81x20]
            }
        }
    }
};
Game['prototype']['getCumulativePPS'] = function() {
    return this['placedBlocks'] / this['clock']
};
Game['prototype']['updateTextBar'] = function() {
    this['GameStats']['get']('CLOCK')['set'](sprintTimeFormat(this['clock'], 2));
    this['GameStats']['get']('PPS')['set'](this['getPPS']()['toFixed'](2));
    this['GameStats']['get']('APM')['set'](this['getAPM']());
    this['GameStats']['get']('VS')['set'](this['getVS']());
    this['GameStats']['render']()
};
Game['prototype']['sendRepFragment'] = function() {
    if (this['transmitMode'] !== 1) {
        return
    };
    var _0x3d81x65 = [],
        _0x3d81x66 = null,
        _0x3d81x67 = false;
    for (var _0x3d81x68 = null; this['replayPartsSent'] < this['Replay']['actions']['length']; ++this['replayPartsSent']) {
        var _0x3d81x42 = this['Replay']['actions'][this['replayPartsSent']];
        var _0x3d81x69 = {};
        for (var _0x3d81x14 in _0x3d81x42) {
            _0x3d81x69[_0x3d81x14] = _0x3d81x42[_0x3d81x14]
        };
        if (_0x3d81x68 === null) {
            _0x3d81x66 = _0x3d81x42
        } else {
            if (_0x3d81x42['t'] - _0x3d81x66['t'] > 10) {
                _0x3d81x67 = true
            }
        };
        _0x3d81x69['t'] = (_0x3d81x68 === null) ? 0 : Math['max'](0, _0x3d81x42['t'] - _0x3d81x68);
        _0x3d81x65['push'](_0x3d81x69);
        _0x3d81x68 = _0x3d81x42['t']
    };
    if (_0x3d81x65['length']) {
        this['Live']['sendRepFragment'](_0x3d81x65, _0x3d81x67)
    }
};
Game['prototype']['sendSnapshot'] = function() {
    var _0x3d81x6a = copyMatrix(this['matrix']),
        _0x3d81x6b = this['activeBlock']['pos']['x'],
        _0x3d81x6c = this['activeBlock']['pos']['y'],
        _0x3d81x35 = this['blockSets'][this['activeBlock']['set']]['blocks'][this['activeBlock']['id']],
        _0x3d81x36 = _0x3d81x35['blocks'][this['activeBlock']['rot']]['length'],
        _0x3d81x6d, _0x3d81x6e;
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
            if (_0x3d81x35['blocks'][this['activeBlock']['rot']][_0x3d81x18][_0x3d81x19] > 0) {
                _0x3d81x6d = _0x3d81x6b + _0x3d81x19;
                _0x3d81x6e = _0x3d81x6c + _0x3d81x18;
                if (_0x3d81x6d >= 0 && _0x3d81x6e >= 0 && _0x3d81x6e < _0x3d81x6a['length'] && _0x3d81x6d < _0x3d81x6a[0]['length']) {
                    _0x3d81x6a[_0x3d81x6e][_0x3d81x6d] = _0x3d81x35['color']
                }
            }
        }
    };
    this['Live']['sendSnapshot'](_0x3d81x6a)
};
Game['prototype']['addGarbageFromQueue'] = function(_0x3d81x2e) {
    if (this['redBar'] > 0) {
        var _0x3d81x6f = this['incomingGarbage']['length'],
            _0x3d81x13 = 0,
            _0x3d81x70 = 0,
            _0x3d81x71;
        for (_0x3d81x13 = 0; _0x3d81x13 < _0x3d81x6f; _0x3d81x13++) {
            if (_0x3d81x2e - this['incomingGarbage'][_0x3d81x13][1] <= this['R']['gDelay']) {
                break
            };
            _0x3d81x71 = this['addGarbage'](this['incomingGarbage'][_0x3d81x13][0])
        };
        this['redBar'] = 0;
        if (_0x3d81x71 === null) {
            this['recordRedbarChange'](_0x3d81x2e)
        };
        while (_0x3d81x13 < _0x3d81x6f) {
            this['incomingGarbage'][_0x3d81x70++] = this['incomingGarbage'][_0x3d81x13];
            this['redBar'] += this['incomingGarbage'][_0x3d81x13][0];
            _0x3d81x13++
        };
        this['incomingGarbage']['length'] = _0x3d81x70
    }
};
Game['prototype']['garbageQueue'] = function(_0x3d81x72) {
    if (this['R']['gblock'] !== 3) {
        this['garbageQueue'](_0x3d81x72)
    } else {
        this['addAsyncGarbage'](_0x3d81x72)
    }
};
Game['prototype']['garbageQueue'] = function(_0x3d81x72) {
    var _0x3d81x73 = this['timestamp']();
    if (this['R']['mess'] === 0) {
        this['incomingGarbage']['push']([_0x3d81x72, _0x3d81x73])
    } else {
        var _0x3d81x74 = 0,
            _0x3d81x75 = 0,
            _0x3d81x76 = (this['R']['mess'] > 0) ? this['R']['mess'] : 100;
        while (_0x3d81x74 < _0x3d81x72) {
            _0x3d81x75++;
            if (_0x3d81x76 === 100 || Math['random']() < (_0x3d81x76 / 100)) {
                this['incomingGarbage']['push']([_0x3d81x75, _0x3d81x73]);
                _0x3d81x75 = 0
            };
            _0x3d81x74++
        };
        if (_0x3d81x75) {
            this['incomingGarbage']['push']([_0x3d81x75, _0x3d81x73])
        }
    };
    this['redBar'] += _0x3d81x72;
    this['recordRedbarChange'](_0x3d81x73);
    this['v']['redrawRedBar'](true)
};
Game['prototype']['addAsyncGarbage'] = function(_0x3d81x77) {
    var _0x3d81x12 = this['addGarbage'](_0x3d81x77);
    while (this['checkIntersection'](this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y'], null)) {
        this['activeBlock']['pos']['y']--
    };
    this['updateGhostPiece'](true);
    if (this['isPmode'](false) === 4 && this['activeBlock']['pos']['y'] === -4) {
        this.GameOver()
    };
    return _0x3d81x12
};
Game['prototype']['addAsyncGarbageFromTheQueue'] = function() {
    this['addGarbageFromQueue'](this['timestamp']() + 10000);
    while (this['checkIntersection'](this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y'], null)) {
        this['activeBlock']['pos']['y']--
    };
    this['updateGhostPiece'](true);
    if (this['activeBlock']['pos']['y'] <= -4) {
        this.GameOver()
    }
};
Game['prototype']['addGarbage'] = function(_0x3d81x77) {
    var _0x3d81x12 = undefined,
        _0x3d81x51 = this['isPmode'](false);
    if (_0x3d81x77 <= 0) {
        return 0
    };
    if (!this['R']['solidAttack']) {
        this['gamedata']['linesReceived'] += _0x3d81x77;
        var _0x3d81x78 = [8, 8, 8, 8, 8, 8, 8, 8, 8, 8];
        if (_0x3d81x51 === 9 && this['garbageCols']['length']) {
            _0x3d81x12 = this['garbageCols']['shift']()
        } else {
            if (_0x3d81x51 !== 3 && _0x3d81x51 !== 4) {
                if (this['R']['mess'] >= 0) {
                    _0x3d81x12 = this['random'](0, 9)
                } else {
                    var _0x3d81x76 = 100 + this['R']['mess'];
                    if (!this['lastHolePos'] || (_0x3d81x76 > 0 && this.RNG() < (_0x3d81x76 / 100))) {
                        this['lastHolePos'] = this['random'](0, 9)
                    };
                    _0x3d81x12 = this['lastHolePos']
                }
            } else {
                _0x3d81x12 = this['lastHolePos'] = this['randomExcept'](0, 9, this['lastHolePos'])
            }
        };
        if (this['R']['gapW'] === 1) {
            _0x3d81x78[_0x3d81x12] = 0
        } else {
            if (this['R']['baseBlockSet'] === 1) {
                _0x3d81x12 = _0x3d81x12 - (_0x3d81x12 % 2)
            };
            if (_0x3d81x12 + this['R']['gapW'] > 10) {
                _0x3d81x12 = 10 - this['R']['gapW']
            };
            for (let _0x3d81x13 = 0; _0x3d81x13 < this['R']['gapW']; ++_0x3d81x13) {
                _0x3d81x78[_0x3d81x12 + _0x3d81x13] = 0
            }
        };
        if (this['R']['gInv']) {
            for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x78['length']; ++_0x3d81x13) {
                _0x3d81x78[_0x3d81x13] = (_0x3d81x78[_0x3d81x13] === 8) ? 0 : 8
            }
        };
        if (_0x3d81x77 <= this['matrix']['length']) {
            this['deadline'] = this['matrix'][_0x3d81x77 - 1]['slice'](0)
        } else {
            this['deadline'] = _0x3d81x78['slice'](0)
        };
        var _0x3d81x79 = this['matrix']['length'] - this['solidHeight'];
        for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x79; _0x3d81x18++) {
            if ((_0x3d81x79 - _0x3d81x18) > _0x3d81x77) {
                this['matrix'][_0x3d81x18] = this['matrix'][_0x3d81x18 + _0x3d81x77]['slice'](0)
            } else {
                this['matrix'][_0x3d81x18] = _0x3d81x78['slice'](0)
            }
        };
        this['GameStats']['get']('RECV')['set'](this['gamedata']['linesReceived']);
        if (!this['R']['gInv'] && this['R']['gapW'] === 1) {
            var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].GARBAGE_ADD, this['timestamp']());
            _0x3d81x42['d'] = [_0x3d81x77, _0x3d81x12]
        } else {
            var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].AUX, this['timestamp']());
            _0x3d81x42['d'] = [this['Replay']['AUX']['WIDE_GARBAGE_ADD'], _0x3d81x77, _0x3d81x12, this['R']['gapW'], this['R']['gInv'] ? 1 : 0]
        };
        this['Replay']['add'](_0x3d81x42);
        this['updateGhostPiece'](true)
    } else {
        for (var _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x77; ++_0x3d81x70) {
            this['addSolidGarbage']()
        };
        _0x3d81x12 = null
    };
    return _0x3d81x12
};
Game['prototype']['cancelSolid'] = function() {
    if (this['solidInterval'] !== null) {
        clearTimeout(this['solidInterval'])
    };
    this['solidInterval'] = null
};
Game['prototype']['solidStartRaising'] = function() {
    var _0x3d81x25 = this,
        _0x3d81x7a = 0,
        _0x3d81x7b = this['solidProfiles'][this['Live']['sgProfile']];
    if (!_0x3d81x7b) {
        _0x3d81x7b = this['R']['sgProfile']
    };
    this['cancelSolid']();
    var _0x3d81x7c = function() {
        let _0x3d81x73 = _0x3d81x7b[Math['min'](_0x3d81x7b['length'] - 1, _0x3d81x7a)] * 1000;
        _0x3d81x25['solidInterval'] = setTimeout(_0x3d81x7d, _0x3d81x73)
    };
    var _0x3d81x7d = function() {
        _0x3d81x25['solidToAdd']++;
        _0x3d81x7a++;
        if (_0x3d81x7a === 20) {
            _0x3d81x25['cancelSolid']()
        } else {
            _0x3d81x7c()
        }
    };
    _0x3d81x7c()
};
Game['prototype']['toggleStats'] = function(_0x3d81xb) {
    if (!this['statsEnabled']) {
        this['stats'] = new Stats();
        this['statsDASPanel'] = this['stats']['addPanel'](new Stats.Panel('\u0394DAS', '#ff8', '#221'));
        this['stats']['domElement']['id'] = 'fps';
        this['fpsElement']['appendChild'](this['stats']['domElement']);
        this['statsEnabled'] = true;
        this['statsMode'] = _0x3d81xb;
        this['fpsElement']['style']['marginTop'] = '15px'
    } else {
        this['statsEnabled'] = false;
        this['stats'] = null;
        this['fpsElement']['textContent'] = ''
    }
};
Game['prototype']['cheeseModeStart'] = function() {
    this['lastHolePos'] = null;
    this['cheeseLevel'] = this['maxCheeseHeight'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['cheeseLevel']; _0x3d81x13++) {
        this['addGarbage'](1)
    }
};
Game['prototype']['setLrem'] = function(_0x3d81x7e) {
    if (_0x3d81x7e < 10000) {
        this['lrem']['textContent'] = _0x3d81x7e
    } else {
        this['lrem']['innerHTML'] = '&infin;'
    }
};
Game['prototype']['applyGravityLvl'] = function(_0x3d81x7f) {
    this['R']['gravityLvl'] = _0x3d81x7f;
    this['baseGravity'] = this['getGravityLevel'](_0x3d81x7f);
    this['currentGravity'] = this['baseGravity']['slice'](0);
    if (this['softDrop'] && !this['isSoftDropFasterThanGravity']() && !this['starting']) {
        this['softDropSet'](false, this['timestamp']())
    }
};
Game['prototype']['setLockDelay'] = function(_0x3d81x80) {
    this['lockDelay'] = _0x3d81x80[0];
    this['maxLockDelayWithoutLock'] = _0x3d81x80[1];
    this['maxWithoutLock'] = _0x3d81x80[2]
};
Game['prototype']['pageTitle'] = function(_0x3d81x81) {
    document['title'] = _0x3d81x81
};
Game['prototype']['timestamp'] = function() {
    return new Date()['getTime']()
};
Game['prototype']['random'] = function(_0x3d81x82, _0x3d81x83) {
    return Math['round'](_0x3d81x82 + (this.RNG() * (_0x3d81x83 - _0x3d81x82)))
};
Game['prototype']['randomExcept'] = function(_0x3d81x82, _0x3d81x83, _0x3d81x84) {
    while (1) {
        var _0x3d81x12 = this['random'](_0x3d81x82, _0x3d81x83);
        if (_0x3d81x12 !== _0x3d81x84) {
            return _0x3d81x12
        }
    }
};

function showElem(_0x3d81x86) {
    if (_0x3d81x86) {
        _0x3d81x86['style']['display'] = 'block'
    }
}

function hideElem(_0x3d81x86) {
    if (_0x3d81x86) {
        _0x3d81x86['style']['display'] = 'none'
    }
}
window['requestAnimFrame'] = (function() {
    return window['requestAnimationFrame'] || window['webkitRequestAnimationFrame'] || window['mozRequestAnimationFrame'] || window['oRequestAnimationFrame'] || window['msRequestAnimationFrame'] || function(_0x3d81x88, _0x3d81x89) {
        window['setTimeout'](_0x3d81x88, 1000 / 60)
    }
})();
Game['prototype']['recordRedbarChange'] = function(_0x3d81x73) {
    _0x3d81x73 = _0x3d81x73 || this['timestamp']();
    var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].REDBAR_SET, _0x3d81x73);
    _0x3d81x42['d'] = [((this['redBar'] <= 20) ? this['redBar'] : 20)];
    this['Replay']['add'](_0x3d81x42)
};
Game['prototype']['browserTabFocusChange'] = function(_0x3d81xb) {
    if (_0x3d81xb === 0) {
        this['isTabFocused'] = false;
        if (this['play']) {
            this['play'] = false;
            this['lastSeen'] = this['timestamp']();
            if (!this['pmode'] && (this['solidInterval'] !== null || this['solidToAdd'])) {
                this.GameOver();
                this['Live']['showInChat']('<span style=\'color:yellow\'>NOTE</span>', 'Game lost focus during the final hurry-up phase, your game was halted to avoid stalling.');
                this['lastSeen'] = null
            }
        }
    } else {
        this['isTabFocused'] = true;
        if (this['lastSeen'] !== null && !this['play']) {
            this['play'] = true;
            var _0x3d81x73 = this['timestamp']();
            var _0x3d81x5d = _0x3d81x73 - this['lastSeen'];
            if (_0x3d81x5d > 1000) {
                this['Replay']['config']['afk'] = (typeof this['Replay']['config']['afk'] === 'undefined') ? _0x3d81x5d : this['Replay']['config']['afk'] + _0x3d81x5d;
                if (_0x3d81x5d > 65535) {
                    _0x3d81x5d = 65535
                };
                var _0x3d81x42 = new ReplayAction(this['Replay']['Action'].AUX, this['lastSeen']);
                _0x3d81x42['d'] = [this['Replay']['AUX']['AFK'], _0x3d81x5d];
                this['Replay']['add'](_0x3d81x42)
            }
        };
        if (this['lastSeen'] === null && this['play'] && this['pmode']) {
            this['startPractice'](this['pmode'])
        };
        this['lastSeen'] = null
    }
};
Game['prototype']['score'] = function(_0x3d81x7, _0x3d81x8a) {
    if (typeof _0x3d81x8a === 'undefined') {
        _0x3d81x8a = 1
    };
    var _0x3d81x8b;
    _0x3d81x8a *= this['R']['scoreMult'];
    this['gamedata']['score'] += (_0x3d81x8b = Math['round'](_0x3d81x8a * this['Scoring']['get'](_0x3d81x7, this['wasBack2Back'])));
    if (_0x3d81x8b > 0 && this['debug']) {
        this['Live']['showInChat']('', Object['keys'](this['Scoring'].A)[_0x3d81x7] + ' * ' + _0x3d81x8a + ' = <b>' + _0x3d81x8b + '</b>')
    };
    this['GameStats']['get']('SCORE')['set'](this['gamedata']['score'])
};
Game['prototype']['sprintInfoLineContent'] = function(_0x3d81xb) {
    hideElem(document['getElementById']('stLrem'));
    hideElem(document['getElementById']('stTSD'));
    hideElem(document['getElementById']('stPC'));
    hideElem(document['getElementById']('oRem'));
    if (_0x3d81xb === 0) {
        showElem(document['getElementById']('stLrem'))
    } else {
        if (_0x3d81xb === 1) {} else {
            if (_0x3d81xb === 2) {
                showElem(document['getElementById']('stTSD'))
            } else {
                if (_0x3d81xb === 3) {
                    showElem(document['getElementById']('stPC'))
                } else {
                    if (typeof _0x3d81xb === 'string' || _0x3d81xb instanceof String) {
                        let _0x3d81x8c = document['getElementById']('oRem');
                        showElem(_0x3d81x8c);
                        _0x3d81x8c['textContent'] = _0x3d81xb
                    }
                }
            }
        }
    }
};
Game['prototype']['evalPCmodeEnd'] = function() {
    let _0x3d81x8d = false,
        _0x3d81x8e = 4;
    if (this['PCdata']['blocks'] >= 10) {
        _0x3d81x8d = true
    };
    if (!_0x3d81x8d) {
        let _0x3d81x18 = 19 - (_0x3d81x8e - this['PCdata']['lines']);
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            if (this['matrix'][_0x3d81x18][_0x3d81x19] !== 0) {
                _0x3d81x8d = true;
                break
            }
        }
    };
    if (_0x3d81x8d) {
        this['Caption']['gameWarning'](i18n['notPC'], i18n['notPCInfo']);
        this['practiceModeCompleted']()
    }
};
Game['prototype']['savePlacementTime'] = function() {
    if (this['lastPlacements']['length'] >= 8) {
        this['lastPlacements']['shift']()
    };
    this['lastPlacements']['push'](this['clock'])
};
Game['prototype']['getCurrentPPS'] = function() {
    var _0x3d81x8f = this['lastPlacements']['length'];
    if (_0x3d81x8f > 1) {
        return (_0x3d81x8f - 1) / (this['clock'] - this['lastPlacements'][0])
    } else {
        return _0x3d81x8f / this['clock']
    }
};
Game['prototype']['isHardDropAllowed'] = function() {
    if (this['R']['speedLimit'] === 0 || this['lastPlacements']['length'] < 5) {
        return true
    };
    if (this['getCurrentPPS']() <= this['R']['speedLimit']) {
        return true
    } else {
        this['Caption']['speedWarning'](this['R']['speedLimit']);
        return false
    }
};
Game['prototype']['setSpeedLimit'] = function(_0x3d81x90) {
    if (_0x3d81x90 < 0.1) {
        this['R']['speedLimit'] = 0;
        this['getPPS'] = this['getCumulativePPS']
    } else {
        this['R']['speedLimit'] = _0x3d81x90;
        this['getPPS'] = this['getCurrentPPS']
    }
};

function LineClearAnimator(_0x3d81x38, _0x3d81x92, _0x3d81x93) {
    this['g'] = _0x3d81x93;
    this['matrix'] = _0x3d81x38;
    this['clearPositions'] = _0x3d81x92;
    this['clearDelay'] = this['g']['R']['clearDelay'] / 1000;
    this['t'] = 0;
    this['IS_SOLID'] = true
}
LineClearAnimator['prototype']['render'] = function(_0x3d81x94) {
    this['t'] += _0x3d81x94;
    var _0x3d81x95 = Math['max'](0, 1 - this['t'] / this['clearDelay']);
    this['g']['v']['clearMainCanvas']();
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
        if (this['clearPositions']['indexOf'](_0x3d81x18) !== -1) {
            if (this['IS_SOLID']) {
                this['g']['v']['drawClearLine'](_0x3d81x18, _0x3d81x95)
            } else {
                this['g']['v']['setAlpha'](_0x3d81x95);
                for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
                    this['g']['v']['drawBlock'](_0x3d81x19, _0x3d81x18, this['matrix'][_0x3d81x18][_0x3d81x19], 0)
                };
                this['g']['v']['setAlpha'](1)
            }
        } else {
            for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
                this['g']['v']['drawBlock'](_0x3d81x19, _0x3d81x18, this['matrix'][_0x3d81x18][_0x3d81x19], 0)
            }
        }
    };
    this['g']['v']['redrawRedBar'](false);
    if (this['t'] > this['clearDelay']) {
        this['finished']()
    }
};
LineClearAnimator['prototype']['finished'] = function() {
    this['g']['animator'] = null;
    if (!this['g']['gameEnded']) {
        this['g']['play'] = true
    };
    this['g']['redrawBlocked'] = false;
    this['g']['redraw']();
    this['g']['updateQueueBox']();
    this['g']['redrawHoldBox']()
};

function Ctx2DView(_0x3d81x93) {
    this['g'] = _0x3d81x93;
    this['MAIN'] = 0;
    this['HOLD'] = 1;
    this['QUEUE'] = 2;
    this['NAME'] = '2d'
}
Ctx2DView['prototype']['isAvailable'] = function() {
    return true
};
Ctx2DView['prototype']['initRenderer'] = function() {
    this['ctx'] = this['g']['canvas']['getContext']('2d');
    this['hctx'] = this['g']['holdCanvas']['getContext']('2d');
    this['qctx'] = this['g']['queueCanvas']['getContext']('2d')
};
Ctx2DView['prototype']['redrawMatrix'] = function() {
    this['clearMainCanvas']();
    if (this['g']['isInvisibleSkin']) {
        return
    };
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            this['drawBlock'](_0x3d81x19, _0x3d81x18, this['g']['matrix'][_0x3d81x18][_0x3d81x19] & 15);
            if (this['g']['matrix'][_0x3d81x18][_0x3d81x19] & 16) {
                this['drawBrickOverlay'](_0x3d81x19, _0x3d81x18, false)
            }
        }
    }
};
Ctx2DView['prototype']['clearMainCanvas'] = function() {
    this['ctx']['clearRect'](0, 0, this['g']['canvas']['width'], this['g']['canvas']['height'])
};
Ctx2DView['prototype']['clearHoldCanvas'] = function() {
    this['hctx']['clearRect'](0, 0, this['g']['holdCanvas']['width'], this['g']['holdCanvas']['height'])
};
Ctx2DView['prototype']['clearQueueCanvas'] = function() {
    this['qctx']['clearRect'](0, 0, this['g']['queueCanvas']['width'], this['g']['queueCanvas']['height'])
};
Ctx2DView['prototype']['drawBlockOnCanvas'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97, _0x3d81x98) {
    var _0x3d81x99 = (_0x3d81x98 === this['HOLD']) ? this['hctx'] : this['qctx'];
    if (this['g']['skinId'] === 0) {
        var _0x3d81x9a = (this['g']['monochromeSkin'] && _0x3d81x97 <= 7) ? this['g']['monochromeSkin'] : this['g']['colors'][_0x3d81x97];
        this['drawRectangle'](_0x3d81x99, _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], this['g']['block_size'], this['g']['block_size'], _0x3d81x9a)
    } else {
        _0x3d81x99['drawImage'](this['g']['tex'], this['g']['coffset'][_0x3d81x97] * this['g']['skins'][this['g']['skinId']]['w'], 0, this['g']['skins'][this['g']['skinId']]['w'], this['g']['skins'][this['g']['skinId']]['w'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], this['g']['block_size'], this['g']['block_size'])
    }
};
Ctx2DView['prototype']['drawBrickOverlayOnCanvas'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x98) {
    var _0x3d81x99 = (_0x3d81x98 === this['HOLD']) ? this['hctx'] : this['qctx'];
    _0x3d81x99['drawImage'](this['g']['tex2'], 0, 0, 32, 32, _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], this['g']['block_size'], this['g']['block_size'])
};
Ctx2DView['prototype']['drawBrickOverlay'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x9b) {
    if (_0x3d81x19 >= 0 && _0x3d81x18 >= 0 && _0x3d81x19 < 10 && _0x3d81x18 < 20) {
        if (_0x3d81x9b) {
            this['ctx']['globalAlpha'] = 0.5
        };
        var _0x3d81x9c = this['g']['drawScale'] * this['g']['block_size'];
        this['ctx']['drawImage'](this['g']['tex2'], 0, 0, 32, 32, _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c);
        if (_0x3d81x9b) {
            this['ctx']['globalAlpha'] = 1
        }
    }
};
Ctx2DView['prototype']['drawBlock'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97) {
    if (_0x3d81x97 && _0x3d81x19 >= 0 && _0x3d81x18 >= 0 && _0x3d81x19 < 10 && _0x3d81x18 < 20) {
        var _0x3d81x9c = this['g']['drawScale'] * this['g']['block_size'];
        if (this['g']['skinId']) {
            this['ctx']['drawImage'](this['g']['tex'], this['g']['coffset'][_0x3d81x97] * this['g']['skins'][this['g']['skinId']]['w'], 0, this['g']['skins'][this['g']['skinId']]['w'], this['g']['skins'][this['g']['skinId']]['w'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c)
        } else {
            var _0x3d81x9a = (this['g']['monochromeSkin'] && _0x3d81x97 <= 7) ? this['g']['monochromeSkin'] : this['g']['colors'][_0x3d81x97];
            this['drawRectangle'](this['ctx'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c, _0x3d81x9a)
        }
    }
};
Ctx2DView['prototype']['drawGhostBlock'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97) {
    if (_0x3d81x19 >= 0 && _0x3d81x18 >= 0 && _0x3d81x19 < 10 && _0x3d81x18 < 20) {
        var _0x3d81x9c = this['g']['drawScale'] * this['g']['block_size'];
        if (this['g']['ghostSkinId'] === 0) {
            this['ctx']['globalAlpha'] = 0.5;
            if (this['g']['skinId'] > 0) {
                this['ctx']['drawImage'](this['g']['tex'], this['g']['coffset'][_0x3d81x97] * this['g']['skins'][this['g']['skinId']]['w'], 0, this['g']['skins'][this['g']['skinId']]['w'], this['g']['skins'][this['g']['skinId']]['w'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c)
            } else {
                this['drawBlock'](_0x3d81x19, _0x3d81x18, _0x3d81x97)
            };
            this['ctx']['globalAlpha'] = 1
        } else {
            var _0x3d81x9d = this['g']['ghostSkins'][this['g']['ghostSkinId']];
            this['ctx']['drawImage'](this['g']['ghostTex'], (this['g']['coffset'][_0x3d81x97] - 2) * _0x3d81x9d['w'], 0, _0x3d81x9d['w'], _0x3d81x9d['w'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c)
        }
    }
};
Ctx2DView['prototype']['drawRectangle'] = function(_0x3d81x99, _0x3d81x6b, _0x3d81x6c, _0x3d81x9e, _0x3d81x9f, _0x3d81x9a) {
    _0x3d81x99['beginPath']();
    _0x3d81x99['rect'](_0x3d81x6b, _0x3d81x6c, _0x3d81x9e, _0x3d81x9f);
    _0x3d81x99['fillStyle'] = _0x3d81x9a;
    _0x3d81x99['fill']()
};
Ctx2DView['prototype']['drawLine'] = function(_0x3d81x99, _0x3d81xa0, _0x3d81xa1, _0x3d81xa2, _0x3d81xa3) {
    _0x3d81x99['beginPath']();
    _0x3d81x99['moveTo'](_0x3d81xa0, _0x3d81xa1);
    _0x3d81x99['lineTo'](_0x3d81xa2, _0x3d81xa3);
    _0x3d81x99['strokeStyle'] = 'grey';
    _0x3d81x99['lineWidth'] = 1;
    _0x3d81x99['stroke']()
};
Ctx2DView['prototype']['redrawRedBar'] = function(_0x3d81xa4) {
    if (!_0x3d81xa4 && !this['g']['redBar']) {
        return
    };
    if (_0x3d81xa4) {
        this['ctx']['clearRect'](240, 0, 8, (20 - this['g']['redBar']) * this['g']['block_size'])
    };
    this['drawRectangle'](this['ctx'], 240, (20 - this['g']['redBar']) * this['g']['block_size'], 8, this['g']['redBar'] * this['g']['block_size'], '#FF270F')
};
Ctx2DView['prototype']['drawClearLine'] = function(_0x3d81x18, _0x3d81x95) {
    this['ctx']['globalAlpha'] = _0x3d81x95;
    this['drawRectangle'](this['ctx'], 0, _0x3d81x18 * this['g']['block_size'], 10 * this['g']['block_size'], this['g']['block_size'], '#FFFFFF');
    this['ctx']['globalAlpha'] = 1
};
Ctx2DView['prototype']['setAlpha'] = function(_0x3d81xa5) {
    this['ctx']['globalAlpha'] = _0x3d81xa5
};
Ctx2DView['prototype']['clearRect'] = function(_0x3d81x19, _0x3d81x18, _0x3d81xa6, _0x3d81xa7) {
    this['ctx']['clearRect'](_0x3d81x19, _0x3d81x18, _0x3d81xa6, _0x3d81xa7)
};
Ctx2DView['prototype']['createFastFont'] = function() {
    return new FastFont2D()
};

function FastFont2D() {
    this['fontSize'] = 16;
    this['canvas'] = document['getElementById']('glstats');
    this['ctx'] = this['canvas']['getContext']('2d');
    this['availableLines'] = -1;
    this['resizeCanvas']()
}
FastFont2D['prototype']['init'] = function(_0x3d81xa9) {
    _0x3d81xa9()
};
FastFont2D['prototype']['resizeCanvas'] = function() {
    if (this['canvas']['height'] < this['canvas']['clientHeight']) {
        this['canvas']['height'] = this['canvas']['clientHeight']
    }
};
FastFont2D['prototype']['renderLines'] = function(_0x3d81x72) {
    this['resizeCanvas']();
    this['ctx']['clearRect'](0, 0, this['canvas']['width'], this['canvas']['height']);
    var _0x3d81xaa = this['canvas']['height'];
    if (_0x3d81x72['length'] > this['availableLines']) {
        this['availableLines'] = _0x3d81x72['length']
    };
    var _0x3d81xab = _0x3d81xaa / this['availableLines'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x72['length']; ++_0x3d81x13) {
        this['draw'](_0x3d81x72[_0x3d81x13]['value'].toString(), 0, _0x3d81xab * _0x3d81x13 + this['fontSize'])
    }
};
FastFont2D['prototype']['draw'] = function(_0x3d81xac, _0x3d81x19, _0x3d81x18) {
    this['ctx']['font'] = 'bold ' + this['fontSize'] + 'px Verdana,serif';
    this['ctx']['fillStyle'] = '#808080';
    this['ctx']['fillText'](_0x3d81xac, _0x3d81x19, _0x3d81x18)
};

function WebGLUtils() {}
WebGLUtils['getWebGLcontext'] = function(_0x3d81xae) {
    var _0x3d81xaf = {
        preserveDrawingBuffer: false,
        antialias: false,
        powerPreference: 'high-performance',
        alpha: true,
        premultipliedalpha: false
    };
    var _0x3d81xb0 = _0x3d81xae['getContext']('webgl', _0x3d81xaf) || _0x3d81xae['getContext']('experimental-webgl', _0x3d81xaf);;;
    if (!_0x3d81xb0) {
        alert('Your browser does not support WebGL!')
    };
    _0x3d81xb0['clearColor'](0, 0, 0, 0);
    _0x3d81xb0['clear'](_0x3d81xb0['COLOR_BUFFER_BIT'] | _0x3d81xb0['DEPTH_BUFFER_BIT']);
    return _0x3d81xb0
};
WebGLUtils['createProgram'] = function(_0x3d81xb0, _0x3d81xb1) {
    var _0x3d81xb2 = _0x3d81xb0['createProgram']();
    var _0x3d81xb3 = _0x3d81xb0['createShader'](_0x3d81xb0.VERTEX_SHADER);
    var _0x3d81xb4 = _0x3d81xb0['createShader'](_0x3d81xb0.FRAGMENT_SHADER);
    _0x3d81xb0['shaderSource'](_0x3d81xb3, _0x3d81xb1['vertex']);
    _0x3d81xb0['shaderSource'](_0x3d81xb4, _0x3d81xb1['fragment']);
    _0x3d81xb0['compileShader'](_0x3d81xb3);
    if (!_0x3d81xb0['getShaderParameter'](_0x3d81xb3, _0x3d81xb0.COMPILE_STATUS)) {
        console['error']('Vertex shader: compile err', _0x3d81xb0['getShaderInfoLog'](_0x3d81xb3));
        return
    };
    _0x3d81xb0['compileShader'](_0x3d81xb4);
    if (!_0x3d81xb0['getShaderParameter'](_0x3d81xb4, _0x3d81xb0.COMPILE_STATUS)) {
        console['error']('Fragment shader: compile err', _0x3d81xb0['getShaderInfoLog'](_0x3d81xb4));
        return
    };
    _0x3d81xb0['attachShader'](_0x3d81xb2, _0x3d81xb3);
    _0x3d81xb0['attachShader'](_0x3d81xb2, _0x3d81xb4);
    _0x3d81xb0['linkProgram'](_0x3d81xb2);
    if (!_0x3d81xb0['getProgramParameter'](_0x3d81xb2, _0x3d81xb0.LINK_STATUS)) {
        console['error']('Linking error', _0x3d81xb0['getProgramInfoLog'](_0x3d81xb2));
        return
    };
    _0x3d81xb0['validateProgram'](_0x3d81xb2);
    if (!_0x3d81xb0['getProgramParameter'](_0x3d81xb2, _0x3d81xb0.VALIDATE_STATUS)) {
        console['error']('Validation error', _0x3d81xb0['getProgramInfoLog'](_0x3d81xb2));
        return
    };
    return _0x3d81xb2
};
WebGLUtils['registerContextAttrUnifs'] = function(_0x3d81xb0, _0x3d81x99) {
    var _0x3d81xb2 = _0x3d81x99['program'];
    var _0x3d81xb5 = _0x3d81xb0['getProgramParameter'](_0x3d81xb2, _0x3d81xb0.ACTIVE_ATTRIBUTES);
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xb5; _0x3d81x13++) {
        var _0x3d81xb6 = _0x3d81xb0['getActiveAttrib'](_0x3d81xb2, _0x3d81x13);
        _0x3d81x99[_0x3d81xb6['name']] = _0x3d81xb0['getAttribLocation'](_0x3d81xb2, _0x3d81xb6['name'])
    };
    var _0x3d81xb7 = _0x3d81xb0['getProgramParameter'](_0x3d81xb2, _0x3d81xb0.ACTIVE_UNIFORMS);
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xb7; _0x3d81x13++) {
        var _0x3d81xb8 = _0x3d81xb0['getActiveUniform'](_0x3d81xb2, _0x3d81x13);
        _0x3d81x99[_0x3d81xb8['name']] = _0x3d81xb0['getUniformLocation'](_0x3d81xb2, _0x3d81xb8['name'])
    }
};

function WebGLView(_0x3d81x93) {
    this['g'] = _0x3d81x93;
    this['ctxs'] = [];
    this['MAIN'] = 0;
    this['HOLD'] = 1;
    this['QUEUE'] = 2;
    this['NAME'] = 'webGL';
    this['colorsInTexture'] = [9, 8, 1, 2, 3, 4, 5, 6, 7];
    this['shaders'] = {
        vertex: 'attribute vec4 a_position;     attribute vec2 a_texcoord;     uniform mat4 u_matrix;     uniform mat4 u_textureMatrix;     varying vec2 v_texcoord;     void main() {     gl_Position = u_matrix * a_position;     v_texcoord = (u_textureMatrix * vec4(a_texcoord, 0, 1)).xy;     }',
        fragment: 'precision mediump float;     varying vec2 v_texcoord;     uniform sampler2D u_texture;     uniform float globalAlpha;     void main() {     gl_FragColor = texture2D(u_texture, v_texcoord);     gl_FragColor.rgb *= gl_FragColor.a * globalAlpha;     }'
    };
    this['videoSkin'] = false;
    this['video'] = null
}
WebGLView['prototype']['isAvailable'] = function() {
    var _0x3d81xba = this['g']['canvas'];
    if (_0x3d81xba['getContext']('webgl') || _0x3d81xba['getContext']('experimental-webgl')) {
        return true
    } else {
        return false
    }
};
WebGLView['prototype']['initGLContext'] = function(_0x3d81x99) {
    _0x3d81x99['gl'] = WebGLUtils['getWebGLcontext'](_0x3d81x99['elem']);
    _0x3d81x99['program'] = WebGLUtils['createProgram'](_0x3d81x99['gl'], this['shaders']);
    _0x3d81x99['m4'] = new Float32Array(16);
    var _0x3d81xb0 = _0x3d81x99['gl'],
        _0x3d81xb2 = _0x3d81x99['program'];
    _0x3d81xb0['useProgram'](_0x3d81xb2);
    _0x3d81x99['positionLocation'] = _0x3d81xb0['getAttribLocation'](_0x3d81xb2, 'a_position');
    _0x3d81x99['texcoordLocation'] = _0x3d81xb0['getAttribLocation'](_0x3d81xb2, 'a_texcoord');
    _0x3d81x99['matrixLocation'] = _0x3d81xb0['getUniformLocation'](_0x3d81xb2, 'u_matrix');
    _0x3d81x99['textureMatrixLocation'] = _0x3d81xb0['getUniformLocation'](_0x3d81xb2, 'u_textureMatrix');
    _0x3d81x99['globalAlpha'] = _0x3d81xb0['getUniformLocation'](_0x3d81xb2, 'globalAlpha');
    _0x3d81xb0['uniform1f'](_0x3d81x99['globalAlpha'], 1);
    _0x3d81x99['positionBuffer'] = _0x3d81xb0['createBuffer']();
    _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['positionBuffer']);
    var _0x3d81xbb = [0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1];
    _0x3d81xb0['bufferData'](_0x3d81xb0.ARRAY_BUFFER, new Float32Array(_0x3d81xbb), _0x3d81xb0.STATIC_DRAW);
    _0x3d81x99['texcoordBuffer'] = _0x3d81xb0['createBuffer']();
    _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['texcoordBuffer']);
    var _0x3d81xbc = [0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1];
    _0x3d81xb0['bufferData'](_0x3d81xb0.ARRAY_BUFFER, new Float32Array(_0x3d81xbc), _0x3d81xb0.STATIC_DRAW);
    _0x3d81x99['textureInfos'] = [];
    _0x3d81x99['boundBuffers'] = false;
    _0x3d81x99['boundTexture'] = null;
    this['initEmptyTexture'](_0x3d81x99, 0);
    this['initEmptyTexture'](_0x3d81x99, 1);
    this['initRedbarTexture'](_0x3d81x99, 2)
};
WebGLView['prototype']['initRenderer'] = function() {
    this['ctxs'] = [{
        elem: this['g']['canvas'],
        mesh: {
            w: 10,
            h: 20
        }
    }, {
        elem: this['g']['holdCanvas'],
        mesh: {
            w: 4,
            h: 4
        }
    }, {
        elem: this['g']['queueCanvas'],
        mesh: {
            w: 4,
            h: 15
        }
    }];
    this['initGLContext'](this['ctxs'][0]);
    this['initGLContext'](this['ctxs'][1]);
    this['initGLContext'](this['ctxs'][2]);
    if (typeof this['g']['skinId'] !== 'undefined' && this['g']['skinId']) {
        this['g']['changeSkin'](this['g']['skinId'])
    }
};
WebGLView['prototype']['initRedbarTexture'] = function(_0x3d81x99, _0x3d81x7) {
    var _0x3d81xb0 = _0x3d81x99['gl'];
    var _0x3d81xbd = _0x3d81xb0['createTexture']();
    _0x3d81xbd['id'] = _0x3d81x7;
    _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81xbd);
    _0x3d81xb0['texImage2D'](_0x3d81xb0.TEXTURE_2D, 0, _0x3d81xb0.RGBA, 2, 1, 0, _0x3d81xb0.RGBA, _0x3d81xb0.UNSIGNED_BYTE, new Uint8Array([255, 39, 15, 255, 255, 255, 255, 255]));
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MAG_FILTER, _0x3d81xb0.NEAREST);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_WRAP_S, _0x3d81xb0.CLAMP_TO_EDGE);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_WRAP_T, _0x3d81xb0.CLAMP_TO_EDGE);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MIN_FILTER, _0x3d81xb0.LINEAR);
    var _0x3d81xbe = {
        width: 2,
        height: 1,
        texture: _0x3d81xbd
    };
    _0x3d81x99['textureInfos'][_0x3d81x7] = _0x3d81xbe
};
WebGLView['prototype']['initEmptyTexture'] = function(_0x3d81x99, _0x3d81x7) {
    var _0x3d81xb0 = _0x3d81x99['gl'];
    var _0x3d81xbd = _0x3d81xb0['createTexture']();
    _0x3d81xbd['id'] = _0x3d81x7;
    _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81xbd);
    var _0x3d81x8f = this['colorsInTexture']['length'];
    var _0x3d81xbf = new Uint8Array(_0x3d81x8f * 4);
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x8f; ++_0x3d81x13) {
        var _0x3d81xc0 = this['g']['colorsV3'][this['colorsInTexture'][_0x3d81x13]];
        if (this['g']['monochromeSkin'] && _0x3d81x13 > 1) {
            _0x3d81xc0 = hexToRgb(this['g']['monochromeSkin'], 255)
        };
        _0x3d81xbf[_0x3d81x13 * 4] = Math['round'](_0x3d81xc0[0] * 255);
        _0x3d81xbf[_0x3d81x13 * 4 + 1] = Math['round'](_0x3d81xc0[1] * 255);
        _0x3d81xbf[_0x3d81x13 * 4 + 2] = Math['round'](_0x3d81xc0[2] * 255);
        _0x3d81xbf[_0x3d81x13 * 4 + 3] = 255
    };
    _0x3d81xb0['texImage2D'](_0x3d81xb0.TEXTURE_2D, 0, _0x3d81xb0.RGBA, _0x3d81x8f, 1, 0, _0x3d81xb0.RGBA, _0x3d81xb0.UNSIGNED_BYTE, _0x3d81xbf);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_WRAP_S, _0x3d81xb0.CLAMP_TO_EDGE);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_WRAP_T, _0x3d81xb0.CLAMP_TO_EDGE);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MIN_FILTER, _0x3d81xb0.LINEAR);
    _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MAG_FILTER, _0x3d81xb0.NEAREST);
    var _0x3d81xbe = {
        width: _0x3d81x8f,
        height: 1,
        texture: _0x3d81xbd
    };
    _0x3d81x99['textureInfos'][_0x3d81x7] = _0x3d81xbe
};
WebGLView['prototype']['loadTexture'] = function(_0x3d81x7, _0x3d81x5) {
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['ctxs']['length']; ++_0x3d81x13) {
        this['initEmptyTexture'](this['ctxs'][_0x3d81x13], _0x3d81x7)
    };
    if (_0x3d81x5 === null) {
        return
    };
    var _0x3d81xc1 = new Image();
    var _0x3d81x25 = this;
    _0x3d81xc1['addEventListener']('load', function() {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x25['ctxs']['length']; ++_0x3d81x13) {
            var _0x3d81xb0 = _0x3d81x25['ctxs'][_0x3d81x13]['gl'];
            var _0x3d81xbe = _0x3d81x25['ctxs'][_0x3d81x13]['textureInfos'][_0x3d81x7];
            _0x3d81xbe['width'] = _0x3d81xc1['width'];
            _0x3d81xbe['height'] = _0x3d81xc1['height'];
            _0x3d81x25['ctxs'][_0x3d81x13]['boundTexture'] = _0x3d81xbe['texture'];
            _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81xbe['texture']);
            _0x3d81xb0['texImage2D'](_0x3d81xb0.TEXTURE_2D, 0, _0x3d81xb0.RGBA, _0x3d81xb0.RGBA, _0x3d81xb0.UNSIGNED_BYTE, _0x3d81xc1);
            _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MAG_FILTER, _0x3d81xb0.LINEAR)
        };
        _0x3d81x25['g']['redrawAll']()
    });
    _0x3d81xc1['crossOrigin'] = 'anonymous';
    _0x3d81xc1['src'] = _0x3d81x5;
    if (_0x3d81x7 === 0) {
        this['videoSkin'] = false
    }
};
WebGLView['prototype']['drawImage'] = function(_0x3d81x99, _0x3d81xbd, _0x3d81xc2, _0x3d81xc3, _0x3d81xc4, _0x3d81xc5, _0x3d81xc6, _0x3d81xc7, _0x3d81xc8, _0x3d81xc9, _0x3d81xca, _0x3d81xcb) {
    var _0x3d81xb0 = _0x3d81x99['gl'];
    if (_0x3d81x99['boundTexture'] !== _0x3d81xbd) {
        _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81xbd);
        _0x3d81x99['boundTexture'] = _0x3d81xbd
    };
    if (!_0x3d81x99['boundBuffers']) {
        _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['positionBuffer']);
        _0x3d81xb0['enableVertexAttribArray'](_0x3d81x99['positionLocation']);
        _0x3d81xb0['vertexAttribPointer'](_0x3d81x99['positionLocation'], 2, _0x3d81xb0.FLOAT, false, 0, 0);
        _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['texcoordBuffer']);
        _0x3d81xb0['enableVertexAttribArray'](_0x3d81x99['texcoordLocation']);
        _0x3d81xb0['vertexAttribPointer'](_0x3d81x99['texcoordLocation'], 2, _0x3d81xb0.FLOAT, false, 0, 0);
        _0x3d81x99['boundBuffers'] = true
    };
    Matrix['orthographic'](0, _0x3d81x99['elem']['width'], _0x3d81x99['elem']['height'], 0, -1, 1, _0x3d81x99['m4']);
    Matrix['translate'](_0x3d81x99['m4'], _0x3d81xc8, _0x3d81xc9, 0, _0x3d81x99['m4']);
    Matrix['scale'](_0x3d81x99['m4'], _0x3d81xca, _0x3d81xcb, 1, _0x3d81x99['m4']);
    _0x3d81xb0['uniformMatrix4fv'](_0x3d81x99['matrixLocation'], false, _0x3d81x99['m4']);
    Matrix['translation'](_0x3d81xc4 / _0x3d81xc2, _0x3d81xc5 / _0x3d81xc3, 0, _0x3d81x99['m4']);
    Matrix['scale'](_0x3d81x99['m4'], _0x3d81xc6 / _0x3d81xc2, _0x3d81xc7 / _0x3d81xc3, 1, _0x3d81x99['m4']);
    _0x3d81xb0['uniformMatrix4fv'](_0x3d81x99['textureMatrixLocation'], false, _0x3d81x99['m4']);
    _0x3d81xb0['uniform1i'](_0x3d81x99['textureLocation'], 0);
    _0x3d81xb0['drawArrays'](_0x3d81xb0.TRIANGLES, 0, 6)
};
WebGLView['prototype']['redrawMatrix'] = function() {
    this['clearMainCanvas']();
    if (this['g']['isInvisibleSkin']) {
        return
    };
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            this['drawBlock'](_0x3d81x19, _0x3d81x18, this['g']['matrix'][_0x3d81x18][_0x3d81x19], this.MAIN)
        }
    }
};
WebGLView['prototype']['clearMainCanvas'] = function() {
    var _0x3d81xb0 = this['ctxs'][0]['gl'];
    _0x3d81xb0['clear'](_0x3d81xb0.COLOR_BUFFER_BIT)
};
WebGLView['prototype']['clearHoldCanvas'] = function() {
    var _0x3d81xb0 = this['ctxs'][this['HOLD']]['gl'];
    _0x3d81xb0['clear'](_0x3d81xb0.COLOR_BUFFER_BIT)
};
WebGLView['prototype']['clearQueueCanvas'] = function() {
    var _0x3d81xb0 = this['ctxs'][this['QUEUE']]['gl'];
    _0x3d81xb0['clear'](_0x3d81xb0.COLOR_BUFFER_BIT)
};
WebGLView['prototype']['drawBlockOnCanvas'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97, _0x3d81x98) {
    this['drawBlock'](_0x3d81x19, _0x3d81x18, _0x3d81x97, _0x3d81x98)
};
WebGLView['prototype']['drawBrickOverlayOnCanvas'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x98) {};
WebGLView['prototype']['drawBrickOverlay'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x9b) {};
WebGLView['prototype']['drawBlock'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97, _0x3d81xcc) {
    if (_0x3d81x97) {
        var _0x3d81x9c = this['g']['drawScale'] * this['g']['block_size'];
        var _0x3d81x99 = this['ctxs'][_0x3d81xcc],
            _0x3d81xcd = _0x3d81x99['textureInfos'][0];
        this['drawImage'](_0x3d81x99, _0x3d81xcd['texture'], _0x3d81xcd['width'], _0x3d81xcd['height'], this['g']['coffset'][_0x3d81x97] * _0x3d81xcd['height'], 0, _0x3d81xcd['height'], _0x3d81xcd['height'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c)
    }
};
WebGLView['prototype']['drawGhostBlock'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97) {
    var _0x3d81x99 = this['ctxs'][0];
    if (this['g']['ghostSkinId'] === 0) {
        _0x3d81x99['gl']['uniform1f'](_0x3d81x99['globalAlpha'], 0.5);
        this['drawBlock'](_0x3d81x19, _0x3d81x18, _0x3d81x97, 0);
        _0x3d81x99['gl']['uniform1f'](_0x3d81x99['globalAlpha'], 1)
    } else {
        var _0x3d81x9c = this['g']['drawScale'] * this['g']['block_size'];
        var _0x3d81xcd = _0x3d81x99['textureInfos'][1];
        this['drawImage'](_0x3d81x99, _0x3d81xcd['texture'], _0x3d81xcd['width'], _0x3d81xcd['height'], (this['g']['coffset'][_0x3d81x97] - 2) * _0x3d81xcd['height'], 0, _0x3d81xcd['height'], _0x3d81xcd['height'], _0x3d81x19 * this['g']['block_size'], _0x3d81x18 * this['g']['block_size'], _0x3d81x9c, _0x3d81x9c)
    }
};
WebGLView['prototype']['redrawRedBar'] = function(_0x3d81xa4) {
    if (!_0x3d81xa4 && !this['g']['redBar']) {
        return
    };
    var _0x3d81x99 = this['ctxs'][this['MAIN']],
        _0x3d81xcd = _0x3d81x99['textureInfos'][2];
    if (_0x3d81xa4) {
        _0x3d81x99['gl']['clear'](_0x3d81x99['gl'].COLOR_BUFFER_BIT);
        this['g']['redrawMatrix']();
        this['g']['drawGhostAndCurrent']()
    };
    this['drawImage'](_0x3d81x99, _0x3d81xcd['texture'], _0x3d81xcd['width'], _0x3d81xcd['height'], 0, 0, 1, 1, 240, (20 - this['g']['redBar']) * this['g']['block_size'], 8, this['g']['redBar'] * this['g']['block_size'])
};
WebGLView['prototype']['drawClearLine'] = function(_0x3d81x18, _0x3d81x95) {
    var _0x3d81x99 = this['ctxs'][this['MAIN']],
        _0x3d81xcd = _0x3d81x99['textureInfos'][2];
    _0x3d81x99['gl']['uniform1f'](_0x3d81x99['globalAlpha'], _0x3d81x95);
    this['drawImage'](_0x3d81x99, _0x3d81xcd['texture'], _0x3d81xcd['width'], _0x3d81xcd['height'], 1, 0, 1, 1, 0, _0x3d81x18 * this['g']['block_size'], 10 * this['g']['block_size'], this['g']['block_size']);
    _0x3d81x99['gl']['uniform1f'](_0x3d81x99['globalAlpha'], 1)
};
WebGLView['prototype']['setAlpha'] = function(_0x3d81xa5) {
    var _0x3d81x99 = this['ctxs'][this['MAIN']];
    _0x3d81x99['gl']['uniform1f'](_0x3d81x99['globalAlpha'], _0x3d81xa5)
};
WebGLView['prototype']['clearRect'] = function(_0x3d81x19, _0x3d81x18, _0x3d81xa6, _0x3d81xa7) {
    var _0x3d81x99 = this['ctxs'][this['MAIN']],
        _0x3d81xb0 = _0x3d81x99['gl'];
    _0x3d81x18 = Math['ceil'](_0x3d81x99['elem']['height'] - _0x3d81x18 - _0x3d81xa7);
    _0x3d81xb0['enable'](_0x3d81xb0.SCISSOR_TEST);
    _0x3d81xb0['scissor'](_0x3d81x19, _0x3d81x18, _0x3d81xa6, _0x3d81xa7);
    _0x3d81xb0['clear'](_0x3d81xb0.COLOR_BUFFER_BIT);
    _0x3d81xb0['disable'](_0x3d81xb0.SCISSOR_TEST);
    _0x3d81xb0['clearColor'](0, 0, 0, 0)
};
WebGLView['prototype']['setupGif'] = function(_0x3d81x5, _0x3d81xce) {
    this['videoOpts'] = _0x3d81xce;
    if (typeof gifler === 'undefined') {
        let _0x3d81xcf = CDN_URL('/js/vendor/gifler.min.js');
        includeScript(_0x3d81xcf, this['realSetupGif']['bind'](this, _0x3d81x5))
    } else {
        this['realSetupGif'](_0x3d81x5)
    }
};
WebGLView['prototype']['realSetupGif'] = function(_0x3d81x5) {
    let _0x3d81xae = null;
    this['videoOpts']['fpsUpdate'] = false;
    this['videoOpts']['w'] = this['videoOpts']['h'] = null;
    if (this['video'] && this['video']['tagName'] === 'canvas') {
        _0x3d81xae = this['video'];
        try {
            document['body']['removeChild'](_0x3d81xae)
        } catch (err) {}
    } else {
        _0x3d81xae = document['createElement']('canvas')
    };
    gifler(_0x3d81x5)['frames'](_0x3d81xae, this['updateGifTexture']['bind'](this), true);
    this['video'] = _0x3d81xae;
    if (this['videoOpts']['debug']) {
        document['body']['appendChild'](_0x3d81xae)
    }
};
WebGLView['prototype']['updateGifTexture'] = function(_0x3d81x99, _0x3d81xd0) {
    if (this['videoOpts']['skinify']) {
        if (this['videoOpts']['w'] === null) {
            this['videoOpts']['w'] = this['video']['width'];
            this['videoOpts']['h'] = this['video']['height'];
            _0x3d81x99['canvas']['width'] = 64 * 9;
            _0x3d81x99['canvas']['height'] = 64
        };
        let _0x3d81xd1 = [9, 8, 1, 2, 3, 4, 5, 6, 7];
        for (var _0x3d81x13 = 0; _0x3d81x13 < 9; ++_0x3d81x13) {
            _0x3d81x99['globalCompositeOperation'] = 'source-over';
            let _0x3d81xd2 = 64 / this['videoOpts']['w'],
                _0x3d81xd3 = 64 / this['videoOpts']['h'],
                _0x3d81x19 = _0x3d81xd0['x'] * _0x3d81xd2,
                _0x3d81x18 = _0x3d81xd0['y'] * _0x3d81xd3,
                _0x3d81x6 = _0x3d81xd0['width'] * _0x3d81xd2,
                _0x3d81xaa = _0x3d81xd0['height'] * _0x3d81xd3;
            _0x3d81x99['drawImage'](_0x3d81xd0['buffer'], _0x3d81x13 * 64 + _0x3d81x19, _0x3d81x18, _0x3d81x6, _0x3d81xaa);
            if (this['videoOpts']['colorize']) {
                let _0x3d81xc0 = this['g']['colorsV3'][_0x3d81xd1[_0x3d81x13]],
                    _0x3d81xd4 = this['videoOpts']['colorAlpha'] || 0.7;
                _0x3d81x99['globalCompositeOperation'] = 'source-atop';
                _0x3d81x99['fillStyle'] = 'rgba(' + _0x3d81xc0[0] * 255 + ', ' + _0x3d81xc0[1] * 255 + ', ' + _0x3d81xc0[2] * 255 + ', ' + _0x3d81xd4 + ')';
                _0x3d81x99['fillRect'](_0x3d81x13 * 64, 0, 64, 64)
            }
        }
    } else {
        _0x3d81x99['drawImage'](_0x3d81xd0['buffer'], _0x3d81xd0['x'], _0x3d81xd0['y'], _0x3d81xd0['width'], _0x3d81xd0['height'])
    };
    this['updateTextureFromElem'](0, this['video'], this['video']['width'], this['video']['height']);
    this['videoSkin'] = true
};
WebGLView['prototype']['setupVideo'] = function(_0x3d81x5, _0x3d81xce) {
    this['videoOpts'] = _0x3d81xce;
    this['videoOpts']['fpsUpdate'] = true;
    this['videoSkin'] = false;
    const _0x3d81xd5 = document['createElement']('video');
    var _0x3d81xd6 = false,
        _0x3d81xd7 = false,
        _0x3d81x25 = this,
        _0x3d81xd8 = function() {
            _0x3d81xd6 = true;
            _0x3d81x25['videoSkin'] = (_0x3d81xd6 && _0x3d81xd7)
        },
        _0x3d81xd9 = function() {
            _0x3d81x25['videoSkin'] = _0x3d81xd6 = _0x3d81xd7 = false
        },
        _0x3d81xda = function() {
            _0x3d81xd7 = true;
            _0x3d81x25['videoSkin'] = (_0x3d81xd6 && _0x3d81xd7)
        };
    _0x3d81xd5['crossOrigin'] = 'anonymous';
    _0x3d81xd5['loop'] = _0x3d81xd5['autoplay'] = true;
    _0x3d81xd5['muted'] = this['videoOpts']['sound'] !== true;
    _0x3d81xd5['addEventListener']('playing', _0x3d81xd8, true);
    _0x3d81xd5['addEventListener']('timeupdate', _0x3d81xda, true);
    _0x3d81xd5['addEventListener']('waiting', _0x3d81xd9, true);
    _0x3d81xd5['src'] = _0x3d81x5;
    _0x3d81xd5['play']();
    this['video'] = _0x3d81xd5
};
WebGLView['prototype']['updateTexture'] = function(_0x3d81x7) {
    if (!this['videoSkin']) {
        return
    };
    this['updateTextureFromElem'](_0x3d81x7, this['video'], this['video']['videoWidth'], this['video']['videoHeight'])
};
WebGLView['prototype']['updateTextureFromElem'] = function(_0x3d81x7, _0x3d81x86, _0x3d81x6, _0x3d81xaa) {
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['ctxs']['length']; ++_0x3d81x13) {
        var _0x3d81xb0 = this['ctxs'][_0x3d81x13]['gl'],
            _0x3d81xbe = this['ctxs'][_0x3d81x13]['textureInfos'][_0x3d81x7];
        _0x3d81xbe['width'] = _0x3d81x6;
        _0x3d81xbe['height'] = _0x3d81xaa;
        if (this['ctxs'][_0x3d81x13]['boundTexture'] !== _0x3d81xbe['texture']) {
            _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81xbe['texture']);
            this['ctxs'][_0x3d81x13]['boundTexture'] = _0x3d81xbe['texture']
        };
        _0x3d81xb0['texImage2D'](_0x3d81xb0.TEXTURE_2D, 0, _0x3d81xb0.RGBA, _0x3d81xb0.RGBA, _0x3d81xb0.UNSIGNED_BYTE, _0x3d81x86)
    }
};
WebGLView['prototype']['createFastFont'] = function() {
    return new FastFont()
};

function FastFont() {
    this['fontSize'] = 16;
    this['sdfs'] = {
        "\x30": {
            "\x78": 0,
            "\x79": 0
        },
        "\x31": {
            "\x78": 20,
            "\x79": 0
        },
        "\x32": {
            "\x78": 40,
            "\x79": 0
        },
        "\x33": {
            "\x78": 60,
            "\x79": 0
        },
        "\x34": {
            "\x78": 80,
            "\x79": 0
        },
        "\x35": {
            "\x78": 100,
            "\x79": 0
        },
        "\x36": {
            "\x78": 120,
            "\x79": 0
        },
        "\x37": {
            "\x78": 140,
            "\x79": 0
        },
        "\x38": {
            "\x78": 160,
            "\x79": 0
        },
        "\x39": {
            "\x78": 180,
            "\x79": 0
        },
        "\x28": {
            "\x78": 200,
            "\x79": 0,
            "\x77": 7
        },
        "\x29": {
            "\x78": 220,
            "\x79": 0,
            "\x77": 7
        },
        "\x2F": {
            "\x78": 240,
            "\x79": 0,
            "\x77": 10
        },
        "\x3F": {
            "\x78": 260,
            "\x79": 0
        },
        "\x2D": {
            "\x78": 280,
            "\x79": 0
        },
        "\x2B": {
            "\x78": 300,
            "\x79": 0
        },
        "\u221E": {
            "\x78": 320,
            "\x79": 0
        },
        "\x2E": {
            "\x78": 340,
            "\x79": 0,
            "\x77": 7
        },
        "\x3A": {
            "\x78": 360,
            "\x79": 0,
            "\x77": 7
        },
        "\x2C": {
            "\x78": 380,
            "\x79": 0,
            "\x77": 7
        },
        "\x20": {
            "\x78": 400,
            "\x79": 0,
            "\x77": 4
        }
    };
    this['shaders'] = {
        vertex: 'attribute vec2 a_pos;     attribute vec2 a_texcoord;     uniform mat4 u_matrix;     uniform vec2 u_texsize;     varying vec2 v_texcoord;     void main() {     gl_Position = u_matrix * vec4(a_pos.xy, 0, 1);     v_texcoord = a_texcoord / u_texsize;     }',
        fragment: 'precision mediump float;     uniform sampler2D u_texture;     uniform vec4 u_color;     uniform float u_buffer;     uniform float u_gamma;     varying vec2 v_texcoord;     void main() {     float dist = texture2D(u_texture, v_texcoord).r;     float alpha = smoothstep(u_buffer - u_gamma, u_buffer + u_gamma, dist);     gl_FragColor = vec4(u_color.rgb, alpha * u_color.a);     }'
    };
    var _0x3d81x99 = this['ctx'] = {};
    this['canvas'] = _0x3d81x99['elem'] = document['getElementById']('glstats');
    var _0x3d81xb0 = _0x3d81x99['gl'] = WebGLUtils['getWebGLcontext'](_0x3d81x99['elem']);
    _0x3d81x99['program'] = WebGLUtils['createProgram'](_0x3d81x99['gl'], this['shaders']);
    _0x3d81xb0['useProgram'](_0x3d81x99['program']);
    _0x3d81xb0['clearColor'](0, 0, 0, 0);
    this['resizeCanvas']();
    WebGLUtils['registerContextAttrUnifs'](_0x3d81xb0, _0x3d81x99);
    _0x3d81xb0['enableVertexAttribArray'](_0x3d81x99['a_pos']);
    _0x3d81xb0['enableVertexAttribArray'](_0x3d81x99['a_texcoord']);
    _0x3d81x99['m4'] = new Float32Array(16);
    this['MAX_STR_LEN'] = 11;
    this['verElem'] = new Float32Array(12 * this['MAX_STR_LEN']);
    this['texElem'] = new Float32Array(12 * this['MAX_STR_LEN']);
    _0x3d81xb0['blendFuncSeparate'](_0x3d81xb0.SRC_ALPHA, _0x3d81xb0.ONE_MINUS_SRC_ALPHA, _0x3d81xb0.ONE, _0x3d81xb0.ONE);
    _0x3d81xb0['enable'](_0x3d81xb0.BLEND);
    _0x3d81x99['textureInfo'] = {
        texture: _0x3d81xb0['createTexture']()
    };
    _0x3d81x99['vertexBuffer'] = _0x3d81xb0['createBuffer']();
    _0x3d81x99['textureBuffer'] = _0x3d81xb0['createBuffer']();
    this['ready'] = false;
    this['scale'] = 16;
    this['gamma'] = 1.4;
    this['availableLines'] = 0;
    this['defaultGlyphW'] = 0.75;
    this['glParamsSet'] = false
}
FastFont['prototype']['init'] = function(_0x3d81xdc) {
    var _0x3d81x25 = this;
    var _0x3d81xc1 = new Image();
    _0x3d81xc1['addEventListener']('load', function() {
        var _0x3d81xb0 = _0x3d81x25['ctx']['gl'];
        var _0x3d81xbe = _0x3d81x25['ctx']['textureInfo'];
        _0x3d81xbe['width'] = _0x3d81xc1['width'];
        _0x3d81xbe['height'] = _0x3d81xc1['height'];
        _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81x25['ctx']['textureInfo']['texture']);
        _0x3d81xb0['texImage2D'](_0x3d81xb0.TEXTURE_2D, 0, _0x3d81xb0.RGBA, _0x3d81xb0.RGBA, _0x3d81xb0.UNSIGNED_BYTE, _0x3d81xc1);
        _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MAG_FILTER, _0x3d81xb0.LINEAR);
        _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_MIN_FILTER, _0x3d81xb0.LINEAR);
        _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_WRAP_S, _0x3d81xb0.CLAMP_TO_EDGE);
        _0x3d81xb0['texParameteri'](_0x3d81xb0.TEXTURE_2D, _0x3d81xb0.TEXTURE_WRAP_T, _0x3d81xb0.CLAMP_TO_EDGE);
        _0x3d81xb0['uniform2f'](_0x3d81x25['ctx']['u_texsize'], _0x3d81xc1['width'], _0x3d81xc1['height']);
        _0x3d81x25['ready'] = true;
        _0x3d81xdc()
    });
    _0x3d81xc1['crossOrigin'] = 'anonymous';
    _0x3d81xc1['src'] = CDN_URL('/res/img/sdf2.png')
};
FastFont['prototype']['resizeCanvas'] = function() {
    if (this['canvas']['height'] < this['canvas']['clientHeight']) {
        this['canvas']['height'] = this['canvas']['clientHeight'];
        this['ctx']['gl']['viewport'](0, 0, this['canvas']['width'], this['canvas']['height'])
    }
};
FastFont['prototype']['drawText'] = function(_0x3d81xac, _0x3d81x36) {
    var _0x3d81x99 = this['ctx'],
        _0x3d81xb0 = _0x3d81x99['gl'],
        _0x3d81xdd = this['fontSize'],
        _0x3d81xde = _0x3d81xdd / 8,
        _0x3d81xa6 = _0x3d81xdd + _0x3d81xde * 2,
        _0x3d81xa7 = _0x3d81xdd + _0x3d81xde * 2,
        _0x3d81xdf = 0,
        _0x3d81xe0 = _0x3d81xdd / 2 + _0x3d81xde,
        _0x3d81xe1 = _0x3d81x36 / _0x3d81xdd,
        _0x3d81xe2 = {
            x: 0,
            y: _0x3d81xa7 / 2 - _0x3d81xde
        },
        _0x3d81xe3 = 0;
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xac['length']; _0x3d81x13++) {
        var _0x3d81xe4 = this['sdfs'][_0x3d81xac[_0x3d81x13]];
        if (!_0x3d81xe4) {
            _0x3d81xe4 = this['sdfs']['?']
        };
        if (_0x3d81x13 === this['MAX_STR_LEN']) {
            _0x3d81xac = _0x3d81xac['substring'](0, this.MAX_STR_LEN);
            break
        };
        var _0x3d81xe5 = _0x3d81xe4['x'],
            _0x3d81xe6 = _0x3d81xe4['y'],
            _0x3d81xe7 = _0x3d81xe4['w'] || _0x3d81xdd * this['defaultGlyphW'];
        this['verElem']['set']([_0x3d81xe2['x'] + ((_0x3d81xdf - _0x3d81xde) * _0x3d81xe1), _0x3d81xe2['y'] - _0x3d81xe0 * _0x3d81xe1, _0x3d81xe2['x'] + ((_0x3d81xdf - _0x3d81xde + _0x3d81xa6) * _0x3d81xe1), _0x3d81xe2['y'] - _0x3d81xe0 * _0x3d81xe1, _0x3d81xe2['x'] + ((_0x3d81xdf - _0x3d81xde) * _0x3d81xe1), _0x3d81xe2['y'] + (_0x3d81xa7 - _0x3d81xe0) * _0x3d81xe1, _0x3d81xe2['x'] + ((_0x3d81xdf - _0x3d81xde + _0x3d81xa6) * _0x3d81xe1), _0x3d81xe2['y'] - _0x3d81xe0 * _0x3d81xe1, _0x3d81xe2['x'] + ((_0x3d81xdf - _0x3d81xde) * _0x3d81xe1), _0x3d81xe2['y'] + (_0x3d81xa7 - _0x3d81xe0) * _0x3d81xe1, _0x3d81xe2['x'] + ((_0x3d81xdf - _0x3d81xde + _0x3d81xa6) * _0x3d81xe1), _0x3d81xe2['y'] + (_0x3d81xa7 - _0x3d81xe0) * _0x3d81xe1], _0x3d81xe3);
        this['texElem']['set']([_0x3d81xe5, _0x3d81xe6, _0x3d81xe5 + _0x3d81xa6, _0x3d81xe6, _0x3d81xe5, _0x3d81xe6 + _0x3d81xa7, _0x3d81xe5 + _0x3d81xa6, _0x3d81xe6, _0x3d81xe5, _0x3d81xe6 + _0x3d81xa7, _0x3d81xe5 + _0x3d81xa6, _0x3d81xe6 + _0x3d81xa7], _0x3d81xe3);
        _0x3d81xe3 += 12;
        _0x3d81xe2['x'] = _0x3d81xe2['x'] + _0x3d81xe7 * _0x3d81xe1
    };
    _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['vertexBuffer']);
    _0x3d81xb0['bufferData'](_0x3d81xb0.ARRAY_BUFFER, this['verElem'], _0x3d81xb0.STATIC_DRAW);
    _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['textureBuffer']);
    _0x3d81xb0['bufferData'](_0x3d81xb0.ARRAY_BUFFER, this['texElem'], _0x3d81xb0.STATIC_DRAW);
    _0x3d81x99['vertexBuffer']['numItems'] = _0x3d81x99['textureBuffer']['numItems'] = 6 * _0x3d81xac['length']
};
FastFont['prototype']['renderLines'] = function(_0x3d81x72) {
    this['resizeCanvas']();
    var _0x3d81xb0 = this['ctx']['gl'],
        _0x3d81xaa = this['ctx']['elem']['height'];
    _0x3d81xb0['clear'](_0x3d81xb0.COLOR_BUFFER_BIT);
    if (_0x3d81x72['length'] > this['availableLines']) {
        this['availableLines'] = _0x3d81x72['length']
    };
    var _0x3d81xab = _0x3d81xaa / this['availableLines'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x72['length']; ++_0x3d81x13) {
        this['draw'](_0x3d81x72[_0x3d81x13]['value'].toString(), 0, _0x3d81xab * _0x3d81x13)
    }
};
FastFont['prototype']['draw'] = function(_0x3d81xe8, _0x3d81xc8, _0x3d81xc9) {
    if (!this['ready']) {
        return
    };
    var _0x3d81xb0 = this['ctx']['gl'],
        _0x3d81x99 = this['ctx'];
    this['drawText'](_0x3d81xe8, this['scale']);
    Matrix['orthographic'](0, _0x3d81x99['elem']['width'], _0x3d81x99['elem']['height'], 0, -1, 1, _0x3d81x99['m4']);
    Matrix['translate'](_0x3d81x99['m4'], _0x3d81xc8, _0x3d81xc9, 0, _0x3d81x99['m4']);
    _0x3d81xb0['uniformMatrix4fv'](_0x3d81x99['u_matrix'], false, _0x3d81x99['m4']);
    if (!this['glParamsSet']) {
        this['glParamsSet'] = true;
        _0x3d81xb0['activeTexture'](_0x3d81xb0.TEXTURE0);
        _0x3d81xb0['bindTexture'](_0x3d81xb0.TEXTURE_2D, _0x3d81x99['textureInfo']['texture']);
        _0x3d81xb0['uniform1i'](_0x3d81x99['u_texture'], 0);
        _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['vertexBuffer']);
        _0x3d81xb0['vertexAttribPointer'](_0x3d81x99['a_pos'], 2, _0x3d81xb0.FLOAT, false, 0, 0);
        _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x99['textureBuffer']);
        _0x3d81xb0['vertexAttribPointer'](_0x3d81x99['a_texcoord'], 2, _0x3d81xb0.FLOAT, false, 0, 0);
        _0x3d81xb0['uniform4fv'](_0x3d81x99['u_color'], [128 / 255, 128 / 255, 128 / 255, 1]);
        _0x3d81xb0['uniform1f'](_0x3d81x99['u_buffer'], 0.75);
        _0x3d81xb0['uniform1f'](_0x3d81x99['u_gamma'], this['gamma'] * 1.4142 / this['scale'])
    };
    _0x3d81xb0['drawArrays'](_0x3d81xb0.TRIANGLES, 0, _0x3d81x99['vertexBuffer']['numItems'])
};

function GameCore(_0x3d81xea) {
    this['ISGAME'] = _0x3d81xea;
    this['randomizer'] = null;
    this['matrix'] = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
    this['deadline'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this['blockSets'] = getBlockSets();
    this['softDropSpeeds'] = [{
        id: 0,
        time: 0.05,
        steps: 0
    }, {
        id: 1,
        time: 0.008,
        steps: 0
    }, {
        id: 2,
        time: 0,
        steps: 1
    }, {
        id: 3,
        time: 0,
        steps: 2
    }, {
        id: 4,
        time: 0,
        steps: 20
    }];
    this['blockIds'] = {
        "\x5A": 6,
        "\x4C": 3,
        "\x4F": 1,
        "\x53": 5,
        "\x49": 0,
        "\x4A": 4,
        "\x54": 2
    };
    this['queue'] = [new Block(0), new Block(0), new Block(0), new Block(0), new Block(0)];
    this['queueLength'] = this['queue']['length'];
    this['gamedata'] = {
        lines: 0,
        singles: 0,
        doubles: 0,
        triples: 0,
        tetrises: 0,
        maxCombo: 0,
        linesSent: 0,
        linesReceived: 0,
        PCs: 0,
        lastPC: 0,
        TSD: 0,
        TSD20: 0,
        B2B: 0,
        attack: 0,
        score: 0,
        holds: 0,
        garbageCleared: 0,
        wasted: 0,
        tpieces: 0,
        tspins: 0
    };
    this['skins'] = [{
        id: 0,
        name: 'None',
        data: ''
    }, {
        id: 1,
        name: 'Default',
        data: '/res/b1.png',
        w: 32
    }, {
        id: 2,
        name: 'Pixel',
        data: '/res/b2.png?v2',
        w: 32
    }, {
        id: 3,
        name: 'Glass',
        data: '/res/b3.png?v2',
        w: 32
    }, {
        id: 4,
        name: 'Gradient',
        data: '/res/b4.png?v2',
        w: 32
    }, {
        id: 5,
        name: 'Nullpomino4',
        data: '/res/b5.png',
        w: 32
    }, {
        id: 6,
        name: 'Invisible',
        data: ''
    }, {
        id: 7,
        name: 'Mono',
        data: ''
    }, {
        id: 8,
        name: 'Eyebites',
        data: '/res/b8.png',
        w: 32
    }, {
        id: 9,
        name: 'Retro',
        data: '/res/b9.png',
        w: 32
    }, {
        id: 10,
        name: 'Color Cubes',
        data: '/res/b10.png',
        w: 32
    }, {
        id: 11,
        name: 'Simple',
        data: '/res/b11.png',
        w: 32
    }, {
        id: 12,
        name: 'Color Blocks',
        data: '/res/b12.png',
        w: 32
    }, {
        id: 13,
        name: 'Critter',
        data: '/res/b13.png',
        w: 32
    }];
    this['customSkinPath'] = null;
    this['spinPossible'] = false;
    this['spinMiniPossible'] = false;
    this['tspinMiniPossible'] = false;
    this['isBack2Back'] = false;
    this['wasBack2Back'] = false;
    this['isInvisibleSkin'] = false;
    this['monochromeSkin'] = false;
    this['cids'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    this['coffset'] = [0, 2, 3, 4, 5, 6, 7, 8, 1, 0];
    this['colors'] = ['black', '#D70F37', '#E35B02', '#E39F02', '#59B101', '#0F9BD7', '#2141C6', '#AF298A', '#999999', '#6A6A6A', 'black', 'white'];
    this['colorsV3'] = [
        [0, 0, 0],
        [215, 15, 55],
        [227, 91, 2],
        [227, 159, 2],
        [89, 177, 1],
        [15, 155, 215],
        [33, 65, 198],
        [175, 41, 138],
        [153, 153, 153],
        [106, 106, 106],
        [0, 0, 0],
        [255, 255, 255]
    ];
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['colorsV3']['length']; ++_0x3d81x13) {
        for (var _0x3d81x70 = 0; _0x3d81x70 < 3; ++_0x3d81x70) {
            this['colorsV3'][_0x3d81x13][_0x3d81x70] /= 255
        }
    };
    this['NullCol'] = ['black', '#E4203E', '#E47E30', '#E3CF3C', '#1DE03D', '#00C9DF', '#0042DC', '#9E2CDC', '#999999', '#585858', 'black', 'white'];
    this['multipleNames'] = ['Single', 'Double', 'Triple', 'Quadruple', 'Multiple'];
    this['excludedBlocksAS'] = [];
    this['Items'] = new Items(this);
    this['R'] = this['DEF'] = {
        clearDelay: 0,
        rnd: 0,
        showPreviews: 5,
        holdEnabled: true,
        baseBlockSet: 0,
        gravityLvl: 1,
        lockDelay: [500, 5000, 20000],
        mess: 0,
        gapW: 1,
        gInv: false,
        gDelay: 500,
        gblock: 0,
        tsdOnly: false,
        allSpin: 0,
        speedLimit: 0,
        scoreMult: 1,
        clearLines: true,
        sfx: true,
        vsfx: true,
        solidAttack: false,
        ext: 0,
        sgProfile: [0, 3]
    };
    this['initRandomizer'](this['R']['rnd'])
}
GameCore['prototype']['randomizerFactory'] = function(_0x3d81x7, _0x3d81xeb) {
    let _0x3d81xec = this['blockSets'][this['R']['baseBlockSet']]['blocks']['length'];
    let _0x3d81xed = null;
    let _0x3d81xee = _0x3d81xeb || this['blockRNG'];
    switch (_0x3d81x7) {
        case 0:
            _0x3d81xed = new Bag(_0x3d81xee, _0x3d81xec, 1);
            break;
        case 1:
            _0x3d81xed = new Bag(_0x3d81xee, _0x3d81xec, 2);
            break;
        case 2:
            _0x3d81xed = new Classic(_0x3d81xee, _0x3d81xec);
            break;
        case 3:
            _0x3d81xed = new OneBlock(_0x3d81xee, _0x3d81xec, 1, null);
            break;
        case 4:
            _0x3d81xed = new OneBlock(_0x3d81xee, _0x3d81xec, 2, null);
            break;
        case 5:
            _0x3d81xed = new OneBlock(_0x3d81xee, _0x3d81xec, _0x3d81xec, 1);
            break;
        case 6:
            _0x3d81xed = new OneBlock(_0x3d81xee, _0x3d81xec, _0x3d81xec * 2, 2);
            break;
        case 7:
            _0x3d81xed = new C2Sim(_0x3d81xee, _0x3d81xec);
            break;
        case 8:
            _0x3d81xed = new Repeated(new Bag(_0x3d81xee, _0x3d81xec, 1), 7);
            break;
        case 9:
            _0x3d81xed = new BsBlock(new Bag(_0x3d81xee, _0x3d81xec, 1), [2, 4]);
            break;
        case 10:
            _0x3d81xed = new BigBlockRand(new Bag(_0x3d81xee, _0x3d81xec, 1), 2);
            break;
        case 11:
            _0x3d81xed = new ConstBlock(2, 0);
            break;
        case 12:
            _0x3d81xed = new ConstBlock(0, 5);
            break;
        default:
            _0x3d81xed = new Bag(_0x3d81xee, _0x3d81xec, 1)
    };
    return _0x3d81xed
};
GameCore['prototype']['initRandomizer'] = function(_0x3d81x97) {
    this['randomizer'] = this['randomizerFactory'](_0x3d81x97);
    if (this['ISGAME'] && this['Replay']) {
        if (_0x3d81x97 !== 0) {
            this['Replay']['config']['rnd'] = _0x3d81x97
        } else {
            if ('rnd' in this['Replay']['config']) {
                delete this['Replay']['config']['rnd']
            }
        }
    }
};
GameCore['prototype']['getRandomizerBlock'] = function(_0x3d81x4f) {
    _0x3d81x4f = _0x3d81x4f || this['randomizer'];
    let _0x3d81x54 = _0x3d81x4f['getBlock']();
    if (_0x3d81x54['set'] === 0) {
        _0x3d81x54['set'] = this['R']['baseBlockSet']
    } else {
        if (_0x3d81x54['set'] === -1) {
            _0x3d81x54['set'] = 0
        }
    };
    if (this['temporaryBlockSet'] !== null) {
        _0x3d81x54['set'] = this['temporaryBlockSet'];
        this['temporaryBlockSet'] = null
    };
    return _0x3d81x54
};
GameCore['prototype']['getBlockFromQueue'] = function() {
    if (this['queue']['length'] === 0) {
        if (this['ISGAME'] && this['pmode'] === 9) {
            this['Caption']['gameWarning'](i18n['noBlocks'], i18n['noBlocks2'])
        };
        this.GameOver();
        return this['activeBlock']
    };
    var _0x3d81xef = this['queue']['splice'](0, 1)[0];
    this['refillQueue']();
    this['updateQueueBox']();
    return _0x3d81xef
};
GameCore['prototype']['checkIntersection'] = function(_0x3d81x6b, _0x3d81x6c, _0x3d81xf0) {
    _0x3d81xf0 = (_0x3d81xf0 === null) ? this['activeBlock']['rot'] : _0x3d81xf0;
    let _0x3d81x34 = this['blockSets'][this['activeBlock']['set']],
        _0x3d81x35 = _0x3d81x34['blocks'][this['activeBlock']['id']]['blocks'],
        _0x3d81x36 = _0x3d81x34['blocks'][this['activeBlock']['id']]['blocks'][_0x3d81xf0]['length'];
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
            if (_0x3d81x35[_0x3d81xf0][_0x3d81x18][_0x3d81x19] > 0) {
                if ((_0x3d81x6c + _0x3d81x18) >= 20) {
                    return true
                };
                if ((_0x3d81x6b + _0x3d81x19) < 0 || (_0x3d81x6b + _0x3d81x19) >= 10) {
                    return true
                };
                if ((_0x3d81x6c + _0x3d81x18) >= 0 && this['matrix'][_0x3d81x6c + _0x3d81x18][_0x3d81x6b + _0x3d81x19] > 0) {
                    return true
                }
            }
        }
    };
    return false
};
GameCore['prototype']['setCurrentPieceToDefaultPos'] = function() {
    let _0x3d81x35 = this['blockSets'][this['activeBlock']['set']]['blocks'][this['activeBlock']['id']],
        _0x3d81x36 = _0x3d81x35['blocks'][0]['length'];
    this['activeBlock']['rot'] = 0;
    this['activeBlock']['pos']['x'] = _0x3d81x35['spawn'][0];
    this['activeBlock']['pos']['y'] = _0x3d81x35['spawn'][1];
    if (this['activeBlock']['set'] === 0) {
        var _0x3d81xf1 = _0x3d81x35['blocks'][0][-this['activeBlock']['pos']['y']];
        if ((this['matrix'][0][3] && _0x3d81xf1[0]) || (this['matrix'][0][4] && _0x3d81xf1[1]) || (this['matrix'][0][5] && _0x3d81xf1[2]) || (this['matrix'][0][6] && _0x3d81xf1[3])) {
            this['activeBlock']['pos']['y']--
        }
    } else {
        while (this['checkIntersection'](this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y'], null)) {
            this['activeBlock']['pos']['y']--
        }
    };
    var _0x3d81xf2 = -(1 + this['activeBlock']['pos']['y']);
    if (_0x3d81xf2 >= 0 && _0x3d81xf2 < _0x3d81x36) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x36; ++_0x3d81x13) {
            if (_0x3d81x35['blocks'][this['activeBlock']['rot']][_0x3d81xf2][_0x3d81x13] && this['deadline'][this['activeBlock']['pos']['x'] + _0x3d81x13]) {
                this.GameOver();
                break
            }
        }
    }
};
GameCore['prototype']['centerColumnCheck'] = function(_0x3d81x19, _0x3d81x18) {
    let _0x3d81x7 = this['activeBlock']['id'],
        _0x3d81xf3 = false;
    if (_0x3d81x18 < 0) {
        return false
    };
    if (_0x3d81x7 === 3 || _0x3d81x7 === 4) {
        if (this['activeBlock']['rot'] === 0) {
            _0x3d81xf3 = (this['matrix'][_0x3d81x18][_0x3d81x19 + 1] > 0 || this['matrix'][_0x3d81x18 + 2][_0x3d81x19 + 1] > 0) && !(this['matrix'][_0x3d81x18 + 2][_0x3d81x19 + 1] > 0 && this['matrix'][_0x3d81x18][_0x3d81x19 + (2 * (_0x3d81x7 === 4) | 0)] > 0)
        } else {
            if (this['activeBlock']['rot'] === 3) {
                _0x3d81xf3 = this['matrix'][_0x3d81x18][_0x3d81x19 + 1] > 0 || this['matrix'][_0x3d81x18 + 1][_0x3d81x19 + 1] > 0
            }
        }
    } else {
        if (_0x3d81x7 === 2) {
            if (this['activeBlock']['rot'] === 0 || this['activeBlock']['rot'] === 2) {
                _0x3d81xf3 = this['matrix'][_0x3d81x18 + 1][_0x3d81x19] > 0
            }
        }
    };
    return _0x3d81xf3
};
GameCore['prototype']['rotateCurrentBlock'] = function(_0x3d81x3e) {
    this['finesse'] += (_0x3d81x3e === 2) ? 2 : 1;
    let _0x3d81x4 = (_0x3d81x3e === -1) ? '-1' : ((_0x3d81x3e === 1) ? '1' : '2'),
        _0x3d81xf4 = this['activeBlock']['rot'] + _0x3d81x3e;
    _0x3d81xf4 = (_0x3d81xf4 === -1) ? _0x3d81xf4 = 3 : _0x3d81xf4 % 4;
    let _0x3d81x35 = this['blockSets'][this['activeBlock']['set']]['blocks'][this['activeBlock']['id']],
        _0x3d81xf5 = _0x3d81x35['kicks'][this['activeBlock']['rot']][_0x3d81x4],
        _0x3d81xf6 = _0x3d81xf5['length'];
    if (this['activeBlock']['set'] === 3 && this['centerColumnCheck'](this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y'])) {
        return
    };
    for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xf6; _0x3d81x13++) {
        let _0x3d81xf7 = _0x3d81xf5[_0x3d81x13][0],
            _0x3d81xf8 = _0x3d81xf5[_0x3d81x13][1];
        if (!this['checkIntersection'](this['activeBlock']['pos']['x'] + _0x3d81xf7, this['activeBlock']['pos']['y'] - _0x3d81xf8, _0x3d81xf4)) {
            this['spinPossible'] = true;
            if (_0x3d81x35['id'] === 2) {
                this['tspinType'] = _0x3d81xf4 * 10 + _0x3d81x13
            } else {
                if (_0x3d81x35['id'] === 50) {
                    if (_0x3d81x3e === 2) {
                        return
                    };
                    if (_0x3d81xf4 === 3 && _0x3d81x13 < 14) {
                        return
                    };
                    if (((_0x3d81xf4 === 1 && this['activeBlock']['rot'] === 0) || (_0x3d81xf4 === 2 && this['activeBlock']['rot'] === 1)) && _0x3d81x13 !== 3) {
                        return
                    };
                    if (((_0x3d81xf4 === 0 && this['activeBlock']['rot'] === 1) || (_0x3d81xf4 === 1 && this['activeBlock']['rot'] === 2)) && _0x3d81x13 !== 11) {
                        return
                    };
                    if (this['activeBlock']['rot'] === 3 && _0x3d81xf4 !== 0) {
                        return
                    }
                }
            };
            this['activeBlock']['rot'] = _0x3d81xf4;
            this['activeBlock']['pos']['x'] += _0x3d81xf7;
            this['activeBlock']['pos']['y'] -= _0x3d81xf8;
            if (_0x3d81xf8 > 0) {
                this['lockDelayActive'] = false;
                this['timer'] = 0
            };
            if (this['ISGAME']) {
                this['playSound']('rotate')
            };
            break
        }
    };
    this['updateGhostPiece'](true);
    this['redraw']()
};
GameCore['prototype']['addSolidGarbage'] = function() {
    if (this['solidHeight'] === 20) {
        return
    };
    let _0x3d81x78 = [9, 9, 9, 9, 9, 9, 9, 9, 9, 9];
    this['deadline'] = this['matrix'][0]['slice'](0);
    var _0x3d81x79 = this['matrix']['length'];
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x79; _0x3d81x18++) {
        if ((_0x3d81x79 - _0x3d81x18) > 1) {
            this['matrix'][_0x3d81x18] = this['matrix'][_0x3d81x18 + 1]['slice'](0)
        } else {
            this['matrix'][_0x3d81x18] = _0x3d81x78['slice'](0)
        }
    };
    this['solidHeight']++;
    if (this['ISGAME']) {
        this['Replay']['add'](new ReplayAction(this['Replay']['Action'].SGARBAGE_ADD, this['timestamp']()))
    }
};
GameCore['prototype']['gravityStep'] = function(_0x3d81xf9, _0x3d81x40) {
    if (!this['checkIntersection'](this['activeBlock']['pos']['x'], this['activeBlock']['pos']['y'] + 1, null)) {
        this['activeBlock']['pos']['y']++;
        this['spinPossible'] = false;
        if (_0x3d81xf9) {
            return 1 + this['gravityStep'](_0x3d81xf9 - 1, _0x3d81x40)
        } else {
            return 1
        }
    } else {
        if (this['ISGAME'] && !this['lockDelayActive']) {
            this['lockDelayActive'] = true;
            this['lockDelayActivated'] = _0x3d81x40;
            this['lastAction'] = _0x3d81x40;
            this['playSound']('land')
        };
        return 0
    }
};
GameCore['prototype']['holdBlock'] = function() {
    if (!this['holdUsedAlready'] && this['R']['holdEnabled']) {
        var _0x3d81x2e = null;
        if (this['ISGAME']) {
            _0x3d81x2e = this['timestamp']();
            this['Replay']['add'](new ReplayAction(this['Replay']['Action'].HOLD_BLOCK, _0x3d81x2e))
        };
        this['lockDelayActive'] = false;
        if (this['blockInHold'] === null) {
            this['blockInHold'] = this['activeBlock'];
            this['getNextBlock'](_0x3d81x2e)
        } else {
            var _0x3d81xfa = this['blockInHold'];
            this['blockInHold'] = this['activeBlock'];
            this['activeBlock'] = _0x3d81xfa;
            this['setCurrentPieceToDefaultPos']();
            if (this['ISGAME']) {
                this['lastGeneration'] = _0x3d81x2e;
                if (this['VSEenabled'] && this['VSFXset']['spawns']) {
                    this['playCurrentPieceSound']()
                }
            }
        };
        this['updateGhostPiece'](true);
        this['holdUsedAlready'] = true;
        this['totalKeyPresses']++;
        this['gamedata']['holds']++;
        if (this['GameStats']) {
            this['GameStats']['get']('HOLD')['set'](this['gamedata']['holds'])
        };
        if (this['ISGAME']) {
            this['checkAutoRepeat'](_0x3d81x2e, false);
            this['redrawHoldBox']();
            this['redraw']();
            this['playSound']('hold')
        } else {
            this['v']['onBlockHold']()
        }
    }
};
GameCore['prototype']['moveBlockToTheWall'] = function(_0x3d81x3e) {
    var _0x3d81xfb = 0;
    while (!this['checkIntersection'](this['activeBlock']['pos']['x'] + _0x3d81x3e, this['activeBlock']['pos']['y'], null)) {
        this['activeBlock']['pos']['x'] = this['activeBlock']['pos']['x'] + _0x3d81x3e;
        _0x3d81xfb += 1
    };
    if (_0x3d81xfb) {
        this['updateGhostPiece'](true);
        this['redraw']()
    };
    return _0x3d81xfb
};
GameCore['prototype']['updateGhostPiece'] = function(_0x3d81xfc) {
    if (!_0x3d81xfc && (!this['ghostEnabled'] || (this['ISGAME'] && (!this['v']['ghostEnabled'] || this['v']['redrawBlocked'])))) {
        return
    };
    for (var _0x3d81x6e = this['activeBlock']['pos']['y']; _0x3d81x6e <= 20; ++_0x3d81x6e) {
        if (this['checkIntersection'](this['activeBlock']['pos']['x'], _0x3d81x6e, null)) {
            this['ghostPiece']['pos']['x'] = this['activeBlock']['pos']['x'];
            this['ghostPiece']['pos']['y'] = (_0x3d81x6e - 1);
            break
        }
    }
};
GameCore['prototype']['checkTSpin'] = function(_0x3d81xfd) {
    let _0x3d81xfe = 0,
        _0x3d81xff = 0,
        _0x3d81xf4 = this['activeBlock']['rot'],
        _0x3d81x19 = this['activeBlock']['pos']['x'],
        _0x3d81x18 = this['activeBlock']['pos']['y'];
    if (_0x3d81xfd === 202) {
        if (this['activeBlock']['rot'] !== 2) {
            --_0x3d81x18
        };
        _0x3d81xf4 = ((this['activeBlock']['rot'] + 2) % 4)
    };
    if (_0x3d81x18 < -2) {
        return false
    };
    switch (_0x3d81xf4) {
        case 0:
            if (_0x3d81x18 >= -1) {
                _0x3d81xfe = (this['matrix'][_0x3d81x18 + 1][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 1][_0x3d81x19 + 2] > 0)
            } else {
                if (_0x3d81x18 === -2) {
                    _0x3d81xfe = (this['deadline'][_0x3d81x19] > 0) + (this['deadline'][_0x3d81x19 + 2] > 0)
                }
            };
            if (_0x3d81x18 === 17) {
                _0x3d81xff = 2
            } else {
                _0x3d81xff = (this['matrix'][_0x3d81x18 + 3][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19 + 2] > 0)
            };
            break;
        case 1:
            if (_0x3d81x19 === -1) {
                _0x3d81xff = 2
            };
            if (_0x3d81x18 >= -1) {
                _0x3d81xfe = (this['matrix'][_0x3d81x18 + 1][_0x3d81x19 + 2] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19 + 2] > 0);
                if (!_0x3d81xff) {
                    _0x3d81xff = (this['matrix'][_0x3d81x18 + 1][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19] > 0)
                }
            } else {
                if (_0x3d81x18 === -2) {
                    _0x3d81xfe = (this['deadline'][_0x3d81x19 + 2] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19 + 2] > 0);
                    if (!_0x3d81xff) {
                        _0x3d81xff = (this['deadline'][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19] > 0)
                    }
                }
            };
            break;
        case 2:
            if (_0x3d81x18 >= -1) {
                _0x3d81xff = (this['matrix'][_0x3d81x18 + 1][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 1][_0x3d81x19 + 2] > 0)
            } else {
                if (_0x3d81x18 === -2) {
                    _0x3d81xff = (this['deadline'][_0x3d81x19] > 0) + (this['deadline'][_0x3d81x19 + 2] > 0)
                }
            };
            if (_0x3d81x18 === 17) {
                _0x3d81xfe = 2
            } else {
                _0x3d81xfe = (this['matrix'][_0x3d81x18 + 3][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19 + 2] > 0)
            };
            break;
        case 3:
            if (_0x3d81x19 === 8) {
                _0x3d81xff = 2
            };
            if (_0x3d81x18 >= -1) {
                _0x3d81xfe = (this['matrix'][_0x3d81x18 + 1][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19] > 0);
                if (!_0x3d81xff) {
                    _0x3d81xff = (this['matrix'][_0x3d81x18 + 1][_0x3d81x19 + 2] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19 + 2] > 0)
                }
            } else {
                if (_0x3d81x18 === -2) {
                    _0x3d81xfe = (this['deadline'][_0x3d81x19] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19] > 0);
                    if (!_0x3d81xff) {
                        _0x3d81xff = (this['deadline'][_0x3d81x19 + 2] > 0) + (this['matrix'][_0x3d81x18 + 3][_0x3d81x19 + 2] > 0)
                    }
                }
            };
            break
    };
    this['spinPossible'] = (_0x3d81xfe === 2 && _0x3d81xff >= 1);
    this['spinMiniPossible'] = (_0x3d81xfe === 1 && _0x3d81xff === 2)
};
GameCore['prototype']['checkAllSpinImmobile'] = function() {
    let _0x3d81x19 = this['activeBlock']['pos']['x'],
        _0x3d81x18 = this['activeBlock']['pos']['y'];
    this['spinPossible'] = (this['checkIntersection'](_0x3d81x19 - 1, _0x3d81x18, null) && this['checkIntersection'](_0x3d81x19 + 1, _0x3d81x18, null) && this['checkIntersection'](_0x3d81x19, _0x3d81x18 - 1, null))
};
GameCore['prototype']['checkAllSpin'] = function(_0x3d81xfd) {
    let _0x3d81x35 = this['blockSets'][this['activeBlock']['set']];
    if (!_0x3d81x35['allspin']) {
        return
    };
    let _0x3d81x100 = _0x3d81x35['allspin'][_0x3d81xfd];
    if (!_0x3d81x100) {
        return
    };
    let _0x3d81x6b = this['activeBlock']['pos']['x'],
        _0x3d81x6c = this['activeBlock']['pos']['y'];
    let _0x3d81x101 = _0x3d81x100[this['activeBlock']['rot']][0],
        _0x3d81x102 = _0x3d81x100[this['activeBlock']['rot']][1],
        _0x3d81x25 = this;
    let _0x3d81x103 = function(_0x3d81x104) {
        let _0x3d81x12 = 0;
        for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x104['length']; _0x3d81x13 += 2) {
            let _0x3d81x19 = _0x3d81x6b + _0x3d81x104[_0x3d81x13],
                _0x3d81x18 = _0x3d81x6c + _0x3d81x104[_0x3d81x13 + 1];
            if (!(_0x3d81x19 < 0 || _0x3d81x19 >= 10 || _0x3d81x18 < 0 || _0x3d81x18 >= 20 || _0x3d81x25['matrix'][_0x3d81x18][_0x3d81x19] > 0)) {
                continue
            };
            ++_0x3d81x12
        };
        return _0x3d81x12
    };
    let _0x3d81x105 = _0x3d81x103(_0x3d81x101);
    if (_0x3d81x105 !== _0x3d81x101['length'] / 2) {
        return
    };
    let _0x3d81x106 = 0;
    if (Array['isArray'](_0x3d81x102[0])) {
        for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x102['length']; _0x3d81x13++) {
            if (_0x3d81x103(_0x3d81x102[_0x3d81x13])) {
                _0x3d81x106++
            }
        }
    } else {
        _0x3d81x106 = _0x3d81x103(_0x3d81x102)
    };
    this['spinPossible'] = true;
    if (_0x3d81x106 == 2) {} else {
        if (_0x3d81x106 == 1) {
            this['spinMiniPossible'] = true
        } else {
            return
        }
    }
};
GameCore['prototype']['checkLineClears'] = function(_0x3d81x40) {
    let _0x3d81x107 = 0,
        _0x3d81x108 = -1,
        _0x3d81x109 = 0,
        _0x3d81x10a = 0,
        _0x3d81x10b = false,
        _0x3d81x10c = 0,
        _0x3d81x10d = 0,
        _0x3d81x92 = [],
        _0x3d81x10e = null,
        _0x3d81x35 = this['blockSets'][this['activeBlock']['set']]['blocks'][this['activeBlock']['id']],
        _0x3d81x10f = '';
    this['wasBack2Back'] = this['isBack2Back'];
    this['spinMiniPossible'] = false;
    if (!this['ISGAME'] && this['pmode'] === 3) {
        _0x3d81x10d = this['countGarbageHeight']()
    };
    if (!this['R']['clearLines']) {
        this['comboCounter'] = -1;
        return
    };
    if (this['spinPossible']) {
        if (_0x3d81x35['id'] === 2 || _0x3d81x35['id'] === 202) {
            this['checkTSpin'](_0x3d81x35['id'])
        } else {
            if (this['R']['allSpin'] === 2) {
                this['spinPossible'] = false;
                this['checkAllSpin'](_0x3d81x35['id'])
            } else {
                if (this['R']['allSpin'] !== 1) {
                    this['spinPossible'] = false
                }
            }
        }
    };
    for (var _0x3d81x19 = 0; _0x3d81x19 < 10; ++_0x3d81x19) {
        if (this['deadline'][_0x3d81x19] !== 0) {
            ++_0x3d81x109
        } else {
            if (_0x3d81x109 > 0) {
                break
            }
        }
    };
    if (_0x3d81x109 === 10) {
        this['deadline'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        ++_0x3d81x107
    } else {
        _0x3d81x10c += _0x3d81x109
    };
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
        _0x3d81x109 = 0;
        _0x3d81x10a = 0;
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            let _0x3d81x110 = this['matrix'][_0x3d81x18][_0x3d81x19];
            if (_0x3d81x110 === 9) {
                break
            } else {
                if (_0x3d81x110 !== 0) {
                    _0x3d81x109++;
                    _0x3d81x10a |= _0x3d81x110
                } else {
                    if ((_0x3d81x10c + _0x3d81x109) > 0) {
                        break
                    }
                }
            }
        };
        if (_0x3d81x109 === 10) {
            if (this['R']['clearDelay']) {
                if (_0x3d81x10e === null) {
                    _0x3d81x10e = copyMatrix(this['matrix'])
                };
                _0x3d81x92['push'](_0x3d81x18)
            };
            if (this['matrix'][_0x3d81x18]['indexOf'](8) >= 0) {
                this['gamedata']['garbageCleared']++
            };
            for (var _0x3d81x13 = _0x3d81x18; _0x3d81x13 > 0; _0x3d81x13--) {
                this['matrix'][_0x3d81x13] = this['matrix'][_0x3d81x13 - 1]
            };
            this['matrix'][0] = this['deadline']['slice'](0);
            this['deadline'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            _0x3d81x109 = 0;
            _0x3d81x107++;
            _0x3d81x108 = _0x3d81x18;
            if (this['ISGAME'] && this['isPmode'](false) === 6 && this['MapManager']['mapData']['finish'] === this['MapManager']['FINISH_STANDARD']) {
                this['MapManager']['lineCleared'](_0x3d81x18)
            };
            if (_0x3d81x10a & 16) {
                _0x3d81x10b = true
            }
        };
        _0x3d81x10c += _0x3d81x109
    };
    if (_0x3d81x107 > 0) {
        this['gamedata']['lines'] += _0x3d81x107;
        if (this['GameStats']) {
            this['GameStats']['get']('LINES')['set'](this['gamedata']['lines'])
        };
        var _0x3d81x56 = 0,
            _0x3d81x111 = null;
        switch (_0x3d81x107) {
            case 1:
                this['gamedata']['singles']++;
                _0x3d81x56 = this['linesAttack'][1];
                if (this['spinPossible']) {
                    if (this['debug']) {
                        _0x3d81x10f = 'T-Spin Single'
                    };
                    _0x3d81x111 = this['Scoring']['A']['TSPIN_SINGLE'];
                    _0x3d81x56 = this['linesAttack'][7];
                    if (this['isBack2Back']) {
                        this['gamedata']['B2B'] += 1;
                        _0x3d81x56 += this['linesAttack'][10]
                    } else {
                        this['isBack2Back'] = true
                    }
                } else {
                    if (this['spinMiniPossible']) {
                        _0x3d81x111 = this['Scoring']['A']['TSPIN_MINI_SINGLE'];
                        if (this['isBack2Back']) {
                            this['gamedata']['B2B'] += 1
                        } else {
                            this['isBack2Back'] = true
                        };
                        _0x3d81x56 = this['linesAttack'][8];
                        if (this['debug']) {
                            _0x3d81x10f = 'T-Spin Mini Single'
                        }
                    } else {
                        this['isBack2Back'] = false;
                        _0x3d81x111 = this['Scoring']['A']['CLEAR1']
                    }
                };
                break;
            case 2:
                this['gamedata']['doubles']++;
                _0x3d81x56 = this['linesAttack'][2];
                if (this['spinPossible'] || this['spinMiniPossible']) {
                    this['gamedata']['TSD']++;
                    _0x3d81x56 = this['linesAttack'][5];
                    _0x3d81x111 = this['Scoring']['A']['TSPIN_DOUBLE'];
                    if (this['isBack2Back']) {
                        this['gamedata']['B2B'] += 1;
                        _0x3d81x56 += this['linesAttack'][10]
                    } else {
                        this['isBack2Back'] = true
                    };
                    if (this['debug']) {
                        _0x3d81x10f = 'T-Spin Double'
                    }
                } else {
                    this['isBack2Back'] = false;
                    _0x3d81x111 = this['Scoring']['A']['CLEAR2']
                };
                break;
            case 3:
                this['gamedata']['triples']++;
                _0x3d81x56 = this['linesAttack'][3];
                if ((this['spinPossible'] || this['spinMiniPossible']) && (_0x3d81x35['id'] === 2 || _0x3d81x35['id'] === 50)) {
                    _0x3d81x56 = this['linesAttack'][6];
                    _0x3d81x111 = this['Scoring']['A']['TSPIN_TRIPLE'];
                    if (this['isBack2Back']) {
                        this['gamedata']['B2B'] += 1;
                        _0x3d81x56 += this['linesAttack'][10]
                    } else {
                        this['isBack2Back'] = true
                    };
                    if (this['debug']) {
                        _0x3d81x10f = 'T-Spin Triple'
                    }
                } else {
                    this['isBack2Back'] = false;
                    _0x3d81x111 = this['Scoring']['A']['CLEAR3']
                };
                break;
            case 4:
                this['gamedata']['tetrises']++;
                _0x3d81x111 = this['Scoring']['A']['CLEAR4'];
                _0x3d81x56 = this['linesAttack'][4];
                if (this['isBack2Back']) {
                    this['gamedata']['B2B'] += 1;
                    _0x3d81x56 += this['linesAttack'][10]
                } else {
                    this['isBack2Back'] = true
                };
                if (this['debug']) {
                    _0x3d81x10f = 'Tetris'
                };
                break;
            default:
                this['gamedata']['tetrises']++;
                _0x3d81x111 = this['Scoring']['A']['CLEAR5'];
                _0x3d81x56 = this['linesAttack'][6];
                if (this['isBack2Back']) {
                    this['gamedata']['B2B'] += 1;
                    _0x3d81x56 += this['linesAttack'][10]
                } else {
                    this['isBack2Back'] = true
                };
                if (this['debug']) {
                    _0x3d81x10f = 'Multitris (' + _0x3d81x107 + ')'
                };
                break
        };
        if (this['R']['allSpin'] && this['spinPossible']) {
            if (this['excludedBlocksAS'] && this['excludedBlocksAS']['length'] && this['excludedBlocksAS']['indexOf'](_0x3d81x35['name']) !== -1) {
                _0x3d81x56 = 0;
                if (this['debug']) {
                    _0x3d81x10f = 'Ignored ' + _0x3d81x35['name'] + '-Spin'
                }
            } else {
                if (_0x3d81x35['id'] !== 2) {
                    _0x3d81x111 = 127;
                    if (this['R']['allSpin'] === 1) {
                        if (_0x3d81x107 >= 4) {
                            _0x3d81x56 = this['linesAttack'][6] + 1
                        } else {
                            if (_0x3d81x107 === 3) {
                                _0x3d81x56 = this['linesAttack'][6]
                            } else {
                                if (_0x3d81x107 === 2) {
                                    _0x3d81x56 = this['linesAttack'][5]
                                } else {
                                    _0x3d81x56 = this['linesAttack'][7]
                                }
                            }
                        }
                    } else {
                        _0x3d81x56 = this['spinMiniPossible'] ? Math['min'](2, _0x3d81x107) : Math['min'](5, 2 * _0x3d81x107)
                    };
                    if (this['wasBack2Back']) {
                        _0x3d81x56 += this['linesAttack'][10]
                    };
                    this['isBack2Back'] = true;
                    if (this['debug'] && this['ISGAME']) {
                        _0x3d81x10f = _0x3d81x35['name'] + '-Spin ' + ((_0x3d81x107 <= 4) ? this['multipleNames'][_0x3d81x107 - 1] : this['multipleNames'][4]);
                        if (this['spinMiniPossible']) {
                            _0x3d81x10f += ' Mini'
                        };
                        if (this['wasBack2Back']) {
                            _0x3d81x10f = 'B2B ' + _0x3d81x10f
                        }
                    }
                }
            }
        };
        if (_0x3d81x111 >= 8 && _0x3d81x111 <= 11) {
            this['gamedata']['wasted']--;
            this['gamedata']['tspins']++
        };
        this['score'](_0x3d81x111);
        let _0x3d81x112 = _0x3d81x111;
        if (_0x3d81x10c === 0) {
            this['gamedata']['PCs']++;
            _0x3d81x56 = this['linesAttack'][9];
            _0x3d81x112 = this['Scoring']['A']['PERFECT_CLEAR'];
            if (this['debug']) {
                _0x3d81x10f = 'Perfect Clear'
            };
            this['score'](_0x3d81x112);
            if (this['ISGAME'] && this['isPmode'](false) === 6 && this['MapManager']['mapData']['finish'] === this['MapManager']['FINISH_BY_PC']) {
                this['practiceModeCompleted'](_0x3d81x40)
            }
        };
        if (this['ISGAME'] && this['isPmode'](false) === 6 && this['MapManager']['mapData']['finish'] === this['MapManager']['FINISH_STANDARD'] && this['MapManager']['mapLines']['length'] === 0) {
            this['practiceModeCompleted'](_0x3d81x40)
        };
        this['fourWideFlag'] = this['ISGAME'] && this['Live']['noFourWide'] && ((this['fourWideFlag'] && this['comboCounter'] >= 0) || this['is4W'](_0x3d81x108));
        this['comboCounter']++;
        if (this['comboCounter'] > 0) {
            this['score'](this['Scoring']['A'].COMBO, this['comboCounter'])
        };
        if (this['comboCounter'] > this['gamedata']['maxCombo']) {
            this['gamedata']['maxCombo'] = this['comboCounter']
        };
        var _0x3d81x113 = this['getComboAttack'](this['comboCounter']);
        this['gamedata']['linesSent'] += _0x3d81x56 + _0x3d81x113;
        let _0x3d81x114 = {
            type: _0x3d81x112,
            b2b: this['wasBack2Back'],
            cmb: this['comboCounter']
        };
        if (_0x3d81x56 > 0 || _0x3d81x113 > 0) {
            if (this['GameStats']) {
                this['GameStats']['get']('ATTACK')['set'](this['gamedata']['linesSent'])
            }
        };
        if (this['ISGAME']) {
            let _0x3d81x115 = [_0x3d81x112, _0x3d81x111, (this['wasBack2Back'] && this['isBack2Back']), this['comboCounter']];
            this['playSound'](this['SFXset']['getClearSFX'](..._0x3d81x115), 1);
            if (this['VSEenabled']) {
                this['playSound'](this['VSFXset']['getClearSFX'](..._0x3d81x115), 2)
            };
            if (this['debug'] && _0x3d81x10f) {
                this['Live']['showInChat']('', _0x3d81x10f)
            }
        };
        if (this['isPmode'](false)) {
            this['gamedata']['attack'] = this['gamedata']['linesSent'];
            if (this['isPmode'](false) === 1) {
                if (this['linesRemaining'] >= _0x3d81x107) {
                    this['linesRemaining'] -= _0x3d81x107
                } else {
                    this['linesRemaining'] = 0
                };
                if (this['ISGAME']) {
                    this['lrem']['textContent'] = this['linesRemaining']
                }
            } else {
                if (this['isPmode'](false) === 3) {
                    let _0x3d81x116 = this['countGarbageHeight']();
                    if (this['ISGAME']) {
                        if (this['cheeseLevel'] > _0x3d81x116) {
                            var _0x3d81x5d = this['cheeseLevel'] - _0x3d81x116;
                            this['cheeseLevel'] = _0x3d81x116;
                            this['linesRemaining'] -= _0x3d81x5d;
                            if (this['linesRemaining'] > this['cheeseLevel'] && this['cheeseLevel'] < this['minCheeseHeight']) {
                                this['addGarbage'](1);
                                this['cheeseLevel'] += 1
                            }
                        };
                        this['setLrem'](this['linesRemaining'])
                    } else {
                        let _0x3d81x5d = _0x3d81x10d - _0x3d81x116;
                        this['linesRemaining'] -= _0x3d81x5d
                    }
                } else {
                    if (this['isPmode'](false) === 7) {
                        if (this['ISGAME'] && _0x3d81x111 !== this['Scoring']['A']['TSPIN_DOUBLE']) {
                            this['Caption']['gameWarning'](i18n['notTSD'], i18n['notTSDInfo']);
                            this['practiceModeCompleted']()
                        };
                        if (this['ISGAME']) {
                            this['lrem']['textContent'] = this['gamedata']['TSD']
                        };
                        if (this['gamedata']['TSD'] === 20) {
                            this['gamedata']['TSD20'] = Math['round'](this['clock'] * 1000)
                        }
                    } else {
                        if (this['ISGAME'] && this['isPmode'](false) === 8) {
                            if (_0x3d81x112 === this['Scoring']['A']['PERFECT_CLEAR']) {
                                this['gamedata']['lastPC'] = this['clock'];
                                this['lrem']['textContent'] = this['gamedata']['PCs'];
                                if (this['ISGAME']) {
                                    this['PCdata'] = {
                                        blocks: 0,
                                        lines: 0
                                    }
                                }
                            } else {
                                if (this['ISGAME']) {
                                    this['PCdata']['blocks']++;
                                    this['PCdata']['lines'] += _0x3d81x107;
                                    this['evalPCmodeEnd']()
                                }
                            }
                        } else {
                            if (this['ISGAME'] && this['isPmode'](false) === 9) {
                                let _0x3d81x117 = this['gamedata']['lines'] - _0x3d81x107;
                                for (let _0x3d81x13 = 1; _0x3d81x13 <= _0x3d81x107; ++_0x3d81x13) {
                                    this['ModeManager']['on'](this['ModeManager'].LINE, _0x3d81x117 + _0x3d81x13)
                                };
                                this['ModeManager']['on'](this['ModeManager'].LINECLEAR, _0x3d81x107);
                                if (_0x3d81x56 > 0) {
                                    this['blockOrSendAttack'](_0x3d81x56, _0x3d81x40)
                                };
                                if (_0x3d81x113 > 0) {
                                    this['blockOrSendAttack'](_0x3d81x113, _0x3d81x40)
                                }
                            }
                        }
                    }
                }
            };
            if (!this['linesRemaining'] && this['ISGAME']) {
                this['practiceModeCompleted']()
            }
        } else {
            if (this['ISGAME']) {
                if (this['fourWideFlag'] && _0x3d81x113 && this['Live']['noFourWide']) {
                    this['Caption']['gameWarning'](i18n['fwDetect'], i18n['fwDetectInfo']);
                    while (_0x3d81x113 > 0) {
                        this['addGarbage'](1);
                        --_0x3d81x113
                    }
                };
                let _0x3d81x118 = null,
                    _0x3d81x119 = null;
                if (_0x3d81x56 > 0) {
                    _0x3d81x118 = this['blockOrSendAttack'](_0x3d81x56, _0x3d81x40)
                };
                if (_0x3d81x113 > 0) {
                    _0x3d81x119 = this['blockOrSendAttack'](_0x3d81x113, _0x3d81x40)
                };
                if (_0x3d81x118 || _0x3d81x119) {
                    this['Live']['sendAttack'](_0x3d81x118, _0x3d81x119, _0x3d81x114)
                }
            }
        };
        if (this['ISGAME']) {
            if (_0x3d81x10b) {
                this['Items']['pickup']()
            };
            if (this['R']['clearDelay'] && !this['redrawBlocked']) {
                this['play'] = false;
                this['redrawBlocked'] = true;
                this['animator'] = new LineClearAnimator(_0x3d81x10e, _0x3d81x92, this)
            }
        } else {
            this['v']['onLinesCleared'](_0x3d81x56, _0x3d81x113, _0x3d81x114)
        }
    } else {
        this['comboCounter'] = -1;
        let _0x3d81x112 = null;
        if (this['spinPossible']) {
            _0x3d81x112 = this['Scoring']['A']['TSPIN'];
            if (this['debug'] && this['ISGAME']) {
                this['Live']['showInChat']('', _0x3d81x35['name'] + '-Spin')
            }
        } else {
            if (this['spinMiniPossible']) {
                _0x3d81x112 = this['Scoring']['A']['TSPIN_MINI'];
                if (this['debug'] && this['ISGAME']) {
                    this['Live']['showInChat']('', _0x3d81x35['name'] + '-Spin Mini')
                }
            }
        };
        if (_0x3d81x112) {
            this['score'](_0x3d81x112);
            if (this['ISGAME']) {
                let _0x3d81x115 = [_0x3d81x112, _0x3d81x112, false, -1];
                this['playSound'](this['SFXset']['getClearSFX'](..._0x3d81x115), 1);
                if (this['VSEenabled']) {
                    this['playSound'](this['VSFXset']['getClearSFX'](..._0x3d81x115), 2)
                }
            }
        };
        if (this['ISGAME'] && this['isPmode'](false) === 3) {
            var _0x3d81x11a = this['maxCheeseHeight'] - this['cheeseLevel'];
            if (_0x3d81x11a > 0) {
                var _0x3d81x11b = Math['min'](_0x3d81x11a, this['linesRemaining'] - this['cheeseLevel']);
                for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x11b; _0x3d81x13++) {
                    this['addGarbage'](1)
                };
                this['cheeseLevel'] += _0x3d81x11b
            }
        } else {
            if (this['ISGAME'] && this['isPmode'](false) === 8) {
                this['PCdata']['blocks']++;
                this['evalPCmodeEnd']()
            }
        }
    }
};
GameCore['prototype']['countGarbageHeight'] = function(_0x3d81x11c) {
    _0x3d81x11c = _0x3d81x11c || 20;
    var _0x3d81x10d = 0;
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x11c; _0x3d81x18++) {
        if (this['matrix'][19 - _0x3d81x18][0] === 8 || this['matrix'][19 - _0x3d81x18][1] === 8) {
            continue
        } else {
            _0x3d81x10d = _0x3d81x18;
            break
        }
    };
    return _0x3d81x10d
};
GameCore['prototype']['is4W'] = function(_0x3d81x108) {
    var _0x3d81x11d = 0,
        _0x3d81x11e = 3,
        _0x3d81x11f = 4;
    for (var _0x3d81x18 = _0x3d81x108; _0x3d81x18 >= 0; _0x3d81x18--) {
        var _0x3d81x120 = null,
            _0x3d81x121 = false;
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            if (this['matrix'][_0x3d81x18][_0x3d81x19] === 9) {
                continue
            };
            if (this['matrix'][_0x3d81x18][_0x3d81x19] === 0) {
                if (_0x3d81x121) {
                    _0x3d81x120 = -1;
                    _0x3d81x121 = false;
                    break
                };
                if (_0x3d81x120 === null) {
                    _0x3d81x120 = 1
                } else {
                    if (++_0x3d81x120 > _0x3d81x11f) {
                        _0x3d81x121 = false;
                        break
                    }
                }
            } else {
                if (_0x3d81x120 === _0x3d81x11f) {
                    _0x3d81x121 = true
                } else {
                    if (_0x3d81x120) {
                        _0x3d81x120 = -1;
                        _0x3d81x121 = false;
                        break
                    }
                }
            }
        };
        if (_0x3d81x120 === _0x3d81x11f || _0x3d81x121) {
            _0x3d81x11d++;
            if (_0x3d81x11d >= _0x3d81x11e) {
                return true
            }
        } else {
            _0x3d81x11d = 0;
            if (_0x3d81x18 <= 3) {
                return false
            }
        }
    };
    return false
};
GameCore['prototype']['placeBlock'] = function(_0x3d81x6b, _0x3d81x6c, _0x3d81x2e) {
    ++this['placedBlocks'];
    if (this['activeBlock']['id'] === 2) {
        this['gamedata']['wasted']++;
        this['gamedata']['tpieces']++
    };
    if (this['GameStats']) {
        this['GameStats']['get']('BLOCKS')['set'](this['placedBlocks'])
    };
    if (this['GameStats']) {
        this['GameStats']['get']('KPP')['set'](this['getKPP']())
    };
    if (this['ISGAME']) {
        this['savePlacementTime']();
        _0x3d81x2e = _0x3d81x2e || this['timestamp']()
    };
    let _0x3d81x122 = 0,
        _0x3d81x123 = 0,
        _0x3d81x124 = false,
        _0x3d81x35 = this['blockSets'][this['activeBlock']['set']]['blocks'][this['activeBlock']['id']],
        _0x3d81x36 = _0x3d81x35['blocks'][this['activeBlock']['rot']]['length'],
        _0x3d81x125 = 0;
    if (this['spinPossible'] && this['R']['allSpin'] === 1) {
        this['checkAllSpinImmobile']()
    };
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
            let _0x3d81x126 = _0x3d81x35['blocks'][this['activeBlock']['rot']][_0x3d81x18][_0x3d81x19];
            if (_0x3d81x126 > 0) {
                ++_0x3d81x123;
                _0x3d81x125 = (_0x3d81x126 === this['activeBlock']['item']) ? 16 : 0;
                if ((_0x3d81x6c + _0x3d81x18) >= 0 && (_0x3d81x6b + _0x3d81x19) >= 0) {
                    this['matrix'][_0x3d81x6c + _0x3d81x18][_0x3d81x6b + _0x3d81x19] = _0x3d81x35['color'] ^ _0x3d81x125;
                    if (_0x3d81x6c + _0x3d81x18 === 19 - this['solidHeight']) {
                        if (this['ISGAME'] && this['Live']['liveMode'] === 1 && !this['pmode']) {
                            this['Live']['raceCompleted']();
                            this['place'] = 1;
                            _0x3d81x124 = true
                        } else {
                            if (!this['ISGAME']) {
                                this['playing'] = false
                            }
                        }
                    }
                } else {
                    _0x3d81x122++;
                    if ((_0x3d81x6c + _0x3d81x18) === (-1)) {
                        if (this['deadline'][_0x3d81x6b + _0x3d81x19] === 0) {
                            this['deadline'][_0x3d81x6b + _0x3d81x19] = _0x3d81x35['color'] ^ _0x3d81x125
                        }
                    }
                }
            }
        }
    };
    if (_0x3d81x122 === _0x3d81x123) {
        this.GameOver()
    };
    if (this['play']) {
        this['holdUsedAlready'] = false;
        this['checkLineClears'](_0x3d81x2e);
        while (this['solidToAdd']) {
            this['addSolidGarbage']();
            this['solidToAdd']--
        };
        if (this['comboCounter'] === -1 || this['R']['gblock'] !== 0) {
            this['addGarbageFromQueue'](_0x3d81x2e)
        };
        this['getNextBlock'](_0x3d81x2e);
        if (_0x3d81x124) {
            this.GameOver()
        }
    } else {
        if (!this['ISGAME']) {
            this['holdUsedAlready'] = false;
            this['checkLineClears']();
            this['getNextBlock']();
            this['v']['onBlockLocked']()
        }
    };
    if (this['gamedata']['tpieces'] > 0) {
        if (this['GameStats']) {
            this['GameStats']['get']('WASTE')['set'](this['getWasted']())
        }
    }
};
GameCore['prototype']['getQueuePreview'] = function(_0x3d81x4f) {
    let _0x3d81x4c = [];
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['queueLength']; _0x3d81x13++) {
        _0x3d81x4c['push'](this['getRandomizerBlock'](_0x3d81x4f))
    };
    return _0x3d81x4c
};
GameCore['prototype']['generateQueue'] = function() {
    if (this['ISGAME']) {
        this['queue'] = []
    } else {
        this['queue']['splice'](0, this['queueLength'])
    };
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['queueLength']; _0x3d81x13++) {
        this['queue']['push'](this['getRandomizerBlock']())
    };
    if (!this['ISGAME']) {
        if (this['pmode'] === 1) {
            if (this['queue'][0]['id'] >= 5 && this['queue'][0]['set'] === 0) {
                var _0x3d81x127 = this['queue'][0];
                if (this['queue'][1]['id'] < 5) {
                    this['queue'][0] = this['queue'][1];
                    this['queue'][1] = _0x3d81x127
                } else {
                    this['queue'][0] = this['queue'][2];
                    this['queue'][2] = _0x3d81x127
                }
            }
        } else {
            if (this['r']['c']['v'] < 3.3 && this['pmode'] !== 2 && this['queue']['length'] >= 3) {
                if (this['queue'][0]['id'] >= 5 && this['queue'][1]['id'] >= 5) {
                    var _0x3d81x127 = this['queue'][0];
                    this['queue'][0] = this['queue'][2];
                    this['queue'][2] = _0x3d81x127
                }
            }
        }
    }
};
GameCore['prototype']['getAPM'] = function() {
    var _0x3d81x128 = this['clock'];
    if (!this['ISGAME']) {
        _0x3d81x128 /= 1000
    };
    return Math['round'](100 * this['gamedata']['attack'] / (_0x3d81x128 / 60)) / 100
};
GameCore['prototype']['getKPP'] = function() {
    var _0x3d81x129 = 0;
    if (this['placedBlocks']) {
        _0x3d81x129 = (this['totalKeyPresses'] + this['placedBlocks']) / this['placedBlocks']
    };
    return Math['round'](100 * (_0x3d81x129)) / 100
};
GameCore['prototype']['getVS'] = function() {
    var _0x3d81x128 = this['clock'];
    if (!this['ISGAME']) {
        _0x3d81x128 /= 1000
    };
    return Math['round']((10000 * (this['gamedata']['garbageCleared'] + this['gamedata']['attack'])) / _0x3d81x128) / 100
};
GameCore['prototype']['getWasted'] = function() {
    return Math['round'](100 * (this['gamedata']['wasted'] / this['gamedata']['tpieces'])) / 100
};
GameCore['prototype']['clearMatrix'] = function() {
    var _0x3d81x79 = this['matrix']['length'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x79; _0x3d81x13++) {
        var _0x3d81x12a = this['matrix'][_0x3d81x13]['length'];
        for (var _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x12a; _0x3d81x70++) {
            this['matrix'][_0x3d81x13][_0x3d81x70] = 0
        }
    }
};
GameCore['prototype']['updateQueueBox'] = function() {
    if (this['ISGAME'] && this['redrawBlocked']) {
        return
    } else {
        if (!this['ISGAME'] && (this['v']['redrawBlocked'] || !this['v']['QueueHoldEnabled'])) {
            return
        }
    };
    this['v']['clearQueueCanvas']();
    let _0x3d81x12b = 0;
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['R']['showPreviews']; _0x3d81x13++) {
        if (_0x3d81x13 >= this['queue']['length']) {
            if (this['pmode'] != 9) {
                break
            };
            if (this['ModeManager']['repeatQueue']) {
                this['ModeManager']['addStaticQueueToQueue']()
            } else {
                break
            }
        };
        var _0x3d81x54 = this['queue'][_0x3d81x13];
        var _0x3d81x34 = this['blockSets'][_0x3d81x54['set']]['previewAs'],
            _0x3d81x35 = _0x3d81x34['blocks'][_0x3d81x54['id']]['blocks'][0],
            _0x3d81x12c = _0x3d81x34['blocks'][_0x3d81x54['id']]['color'],
            _0x3d81x12d = (!_0x3d81x34['equidist']) ? _0x3d81x34['blocks'][_0x3d81x54['id']]['yp'] : [0, 3],
            _0x3d81x36 = _0x3d81x35['length'],
            _0x3d81x12e = (_0x3d81x34['blocks'][_0x3d81x54['id']]['xp']) ? _0x3d81x34['blocks'][_0x3d81x54['id']]['xp'] : [0, _0x3d81x36 - 1];
        for (var _0x3d81x18 = _0x3d81x12d[0]; _0x3d81x18 <= _0x3d81x12d[1]; _0x3d81x18++) {
            for (var _0x3d81x19 = _0x3d81x12e[0]; _0x3d81x19 <= _0x3d81x12e[1]; _0x3d81x19++) {
                if (_0x3d81x35[_0x3d81x18][_0x3d81x19] > 0) {
                    this['v']['drawBlockOnCanvas'](_0x3d81x19 - _0x3d81x12e[0], _0x3d81x18 - _0x3d81x12d[0] + _0x3d81x12b, _0x3d81x12c, this['v'].QUEUE);
                    if (_0x3d81x54['item'] && _0x3d81x35[_0x3d81x18][_0x3d81x19] === _0x3d81x54['item']) {
                        this['v']['drawBrickOverlayOnCanvas'](_0x3d81x19 - _0x3d81x12e[0], _0x3d81x18 - _0x3d81x12d[0] + _0x3d81x12b, this['v'].QUEUE)
                    }
                }
            }
        };
        if (_0x3d81x34['equidist']) {
            _0x3d81x12b += 3
        } else {
            _0x3d81x12b += _0x3d81x12d[1] - _0x3d81x12d[0] + 2
        }
    }
};
GameCore['prototype']['redrawHoldBox'] = function() {
    if (this['ISGAME'] && this['redrawBlocked']) {
        return
    };
    if (!this['ISGAME'] && (this['v']['redrawBlocked'] || !this['v']['QueueHoldEnabled'])) {
        return
    };
    this['v']['clearHoldCanvas']();
    if (this['blockInHold'] !== null) {
        var _0x3d81x34 = this['blockSets'][this['blockInHold']['set']]['previewAs'],
            _0x3d81x35 = _0x3d81x34['blocks'][this['blockInHold']['id']]['blocks'][0],
            _0x3d81x12c = _0x3d81x34['blocks'][this['blockInHold']['id']]['color'],
            _0x3d81x12d = (!_0x3d81x34['equidist']) ? _0x3d81x34['blocks'][this['blockInHold']['id']]['yp'] : [0, 3],
            _0x3d81x36 = _0x3d81x35['length'],
            _0x3d81x12e = (_0x3d81x34['blocks'][this['blockInHold']['id']]['xp']) ? _0x3d81x34['blocks'][this['blockInHold']['id']]['xp'] : [0, _0x3d81x36 - 1];
        for (var _0x3d81x18 = _0x3d81x12d[0]; _0x3d81x18 <= _0x3d81x12d[1]; _0x3d81x18++) {
            for (var _0x3d81x19 = _0x3d81x12e[0]; _0x3d81x19 <= _0x3d81x12e[1]; _0x3d81x19++) {
                if (_0x3d81x35[_0x3d81x18][_0x3d81x19] > 0) {
                    this['v']['drawBlockOnCanvas'](_0x3d81x19 - _0x3d81x12e[0], _0x3d81x18 - _0x3d81x12d[0], _0x3d81x12c, this['v'].HOLD);
                    if (this['blockInHold']['item'] && _0x3d81x35[_0x3d81x18][_0x3d81x19] === this['blockInHold']['item']) {
                        this['v']['drawBrickOverlayOnCanvas'](_0x3d81x19 - _0x3d81x12e[0], _0x3d81x18 - _0x3d81x12d[0], this['v'].HOLD)
                    }
                }
            }
        }
    }
};
GameCore['prototype']['resetGameData'] = function() {
    let _0x3d81x12f = Object['keys'](this['gamedata']);
    for (let _0x3d81x14 of _0x3d81x12f) {
        this['gamedata'][_0x3d81x14] = 0
    }
};
GameCore['prototype']['getGravityLevel'] = function(_0x3d81x7f) {
    if (_0x3d81x7f <= 0) {
        return [Number['MAX_SAFE_INTEGER'], 0]
    };
    if (_0x3d81x7f <= 18) {
        return [(18 - 1 * (_0x3d81x7f - 1)) / 20, 0]
    };
    if (_0x3d81x7f <= 24) {
        return [(0.9 - 0.1 * (_0x3d81x7f - 19)) / 20, 0]
    };
    if (_0x3d81x7f === 25) {
        return [0.03, 1]
    };
    if (_0x3d81x7f === 26) {
        return [0.02, 1]
    };
    if (_0x3d81x7f === 27) {
        return [0.1 / 3, 2]
    } else {
        return [0, 20]
    }
};
'undefined' !== typeof module && null != module && (module['exports'] = GameCore);

function Bag(_0x3d81xee, _0x3d81x131, _0x3d81x132) {
    this['RNG'] = _0x3d81xee;
    this['usebag'] = [];
    for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x131; _0x3d81x13++) {
        for (let _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x132; _0x3d81x70++) {
            this['usebag']['push'](_0x3d81x13)
        }
    };
    this['bag'] = this['usebag']['slice'](0)
}
Bag['prototype']['getBlock'] = function() {
    let _0x3d81xf = Math['floor'](this.RNG() * this['bag']['length']);
    let _0x3d81xed = this['bag']['splice'](_0x3d81xf, 1)[0];
    if (this['bag']['length'] === 0) {
        this['bag'] = this['usebag']['slice'](0)
    };
    return new Block(_0x3d81xed)
};

function Classic(_0x3d81xee, _0x3d81x131) {
    this['RNG'] = _0x3d81xee;
    this['n'] = _0x3d81x131
}
Classic['prototype']['getBlock'] = function() {
    let _0x3d81x7 = Math['floor'](this.RNG() * this['n']);
    return new Block(_0x3d81x7)
};

function OneBlock(_0x3d81xee, _0x3d81x131, _0x3d81x135, _0x3d81x136) {
    this['RNG'] = _0x3d81xee;
    this['n'] = _0x3d81x131;
    this['bag'] = [];
    if (!_0x3d81x136) {
        for (let _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x135; _0x3d81x70++) {
            let _0x3d81x7;
            do {
                _0x3d81x7 = Math['floor'](this.RNG() * this['n'])
            } while (this['bag']['indexOf'](_0x3d81x7) !== -1);;
            this['bag']['push'](_0x3d81x7)
        }
    } else {
        let _0x3d81x137 = new Bag(_0x3d81xee, _0x3d81x131, _0x3d81x136);
        for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x135; ++_0x3d81x13) {
            this['bag']['push'](_0x3d81x137['getBlock']()['id'])
        }
    };
    this['lastIndex'] = 0
}
OneBlock['prototype']['getBlock'] = function() {
    var _0x3d81x7 = this['bag'][0];
    if (this['bag']['length'] > 1) {
        _0x3d81x7 = this['bag'][this['lastIndex']++];
        this['lastIndex'] %= this['bag']['length']
    };
    return new Block(_0x3d81x7)
};

function C2Sim(_0x3d81xee, _0x3d81x131) {
    this['RNG'] = _0x3d81xee;
    this['n'] = _0x3d81x131;
    this['hist'] = [-1, -2]
}
C2Sim['prototype']['getRandomExcept'] = function(_0x3d81x84) {
    let _0x3d81x7 = Math['floor'](this.RNG() * (this['n'] - 1));
    if (_0x3d81x84 >= 0 && _0x3d81x7 >= _0x3d81x84) {
        ++_0x3d81x7
    };
    if (_0x3d81x7 === this['n']) {
        _0x3d81x7 = 0
    };
    return _0x3d81x7
};
C2Sim['prototype']['getBlock'] = function() {
    let _0x3d81x7 = 0;
    if (this['hist'][0] < 0) {
        _0x3d81x7 = Math['floor'](this.RNG() * this['n'])
    } else {
        if (this['hist'][0] === this['hist'][1] || this['hist'][1] < 0) {
            if (this.RNG() <= 0.028105) {
                _0x3d81x7 = this['hist'][0]
            } else {
                _0x3d81x7 = this['getRandomExcept'](this['hist'][0])
            }
        } else {
            let _0x3d81x139 = this.RNG();
            if (_0x3d81x139 <= 0.027055) {
                _0x3d81x7 = this['hist'][0]
            } else {
                if (_0x3d81x139 >= 0.882639) {
                    _0x3d81x7 = this['hist'][1]
                } else {
                    do {
                        _0x3d81x7 = this['getRandomExcept'](this['hist'][0])
                    } while (_0x3d81x7 === this['hist'][1]);
                }
            }
        }
    };
    this['hist'][1] = this['hist'][0];
    this['hist'][0] = _0x3d81x7;
    return new Block(_0x3d81x7)
};

function Repeated(_0x3d81x4f, _0x3d81x13b) {
    this['randomizer'] = _0x3d81x4f;
    this['n'] = _0x3d81x13b;
    this['block'] = null;
    this['i'] = 0;
    this['nextSegment']()
}
Repeated['prototype']['nextSegment'] = function(_0x3d81x84) {
    this['block'] = this['randomizer']['getBlock']();
    this['i'] = 1 + Math['floor'](this['randomizer'].RNG() * (this['n']))
};
Repeated['prototype']['getBlock'] = function(_0x3d81x84) {
    if (this['i'] === 0) {
        this['nextSegment']()
    };
    this['i']--;
    return this['block']
};

function BsBlock(_0x3d81x4f, _0x3d81x13d) {
    this['randomizer'] = _0x3d81x4f;
    this['bsArr'] = _0x3d81x13d;
    this['i'] = 0
}
BsBlock['prototype']['getBlock'] = function() {
    let _0x3d81x54 = this['randomizer']['getBlock']();
    this['i']++;
    if (this['i'] >= 21 && this['randomizer'].RNG() < 0.1) {
        _0x3d81x54['set'] = this['bsArr'][Math['floor'](this['randomizer'].RNG() * (this['bsArr']['length']))];
        this['i'] = 0;
        if (_0x3d81x54['set'] === 2) {
            _0x3d81x54['id'] = Math['floor'](this['randomizer'].RNG() * 7)
        };
        if (_0x3d81x54['set'] === 4) {
            _0x3d81x54['id'] = Math['floor'](this['randomizer'].RNG() * 18)
        }
    };
    return _0x3d81x54
};

function BigBlockRand(_0x3d81x4f, _0x3d81x13d) {
    this['randomizer'] = _0x3d81x4f;
    this['bsArr'] = _0x3d81x13d;
    this['i'] = 0;
    this['EXPECTED_BLOCKS'] = 500 * 10
}
BigBlockRand['prototype']['getBlock'] = function() {
    let _0x3d81x54 = this['randomizer']['getBlock']();
    this['i']++;
    if (this['randomizer'].RNG() < this['i'] / this['EXPECTED_BLOCKS']) {
        _0x3d81x54['set'] = 2;
        _0x3d81x54['id'] = Math['floor'](this['randomizer'].RNG() * 7)
    };
    return _0x3d81x54
};

function ConstBlock(_0x3d81x7, _0x3d81x140) {
    this['id'] = _0x3d81x7;
    this['set'] = _0x3d81x140
}
ConstBlock['prototype']['getBlock'] = function() {
    let _0x3d81x141 = new Block(this['id']);
    _0x3d81x141['set'] = (this['set'] === 0) ? -1 : this['set'];
    return _0x3d81x141
};
'use strict';

function Live(_0x3d81x139) {
    this['version'] = 'v1.39.4';
    this['serverScheme'] = conf_global['srvScheme'];
    this['tryProxy'] = conf_global['revProxy'];
    this['server'] = conf_global['srv'];
    this['port'] = conf_global['port'];
    this['authorized'] = (conf_global['name'] !== '');
    this['authReady'] = true;
    this['connected'] = false;
    this['sitout'] = false;
    this['socket'] = null;
    this['p'] = _0x3d81x139;
    this['cid'] = 0;
    this['conAttempts'] = 0;
    this['sTier'] = 0;
    this['isProxy'] = false;
    this['servers'] = null;
    this['serverId'] = null;
    this['joinRemote'] = null;
    this['createRoomRequest'] = null;
    this['connectionTimeout'] = null;
    this['clients'] = {};
    this['players'] = Array();
    this['bots'] = Array();
    this['authList'] = Array();
    this['places'] = {};
    this['notPlaying'] = [];
    this['rid'] = '';
    this['rc'] = 0;
    this['rcS'] = {};
    this['roomConfig'] = null;
    this['gid'] = '';
    this['lastGameId'] = '';
    this['currentTarget'] = 0;
    this['winnerCID'] = undefined;
    this['iAmHost'] = false;
    this['xbufferEnabled'] = false;
    this['xbuffer'] = {};
    this['urlPlayParamApplied'] = false;
    this['msgCount'] = 0;
    this['chatBox'] = document['getElementById']('ch1');
    this['friendsBox'] = document['getElementById']('ch2');
    this['chatArea'] = document['getElementById']('chatContent');
    this['friendsBtn'] = document['getElementById']('frLobby');
    this['chatInput'] = document['getElementById']('chatInput');
    this['chatInputArea'] = document['getElementById']('chatInputArea');
    this['chatButton'] = document['getElementById']('sendMsg');
    this['resetButton'] = document['getElementById']('res');
    this['resetProgress'] = document['createElement']('span');
    this['resetProgress']['classList']['add']('btn-progress');
    this['resetButton']['appendChild'](this['resetProgress']);
    this['resetProgress']['style']['width'] = '0%';
    this['teamOptions'] = document['getElementById']('team-options');
    this['tsArea'] = document['getElementById']('tsArea');
    this['myTeam'] = document['getElementById']('myTeam');
    this['resultsBox'] = document['getElementById']('resultsBox');
    this['resultsContent'] = document['getElementById']('resultsContent');
    this['moreResults'] = document['getElementById']('moreResults');
    this['moreData'] = document['getElementById']('moreData');
    this['saveLink'] = document['getElementById']('saveLink');
    this['moreVisible'] = false;
    this['statsSent'] = true;
    this['lobbyVisible'] = false;
    this['lobbyInfoShown'] = false;
    this['chatAtBottom'] = true;
    this['lobbyBox'] = document['getElementById']('lobbyBox');
    this['lobbyContent'] = document['getElementById']('lobbyContent');
    this['refreshLobbyButton'] = document['getElementById']('refreshLobby');
    this['RoomInfo'] = new RoomInfo(this);
    this['editRoomButton'] = document['getElementById']('editRoomButton');
    this['createRoomDialog'] = document['getElementById']('createRoom');
    this['roomNameInput'] = document['getElementById']('roomName');
    this['isPrivateInput'] = document['getElementById']('isPrivate');
    this['chatName'] = conf_global['name'];
    this['chatButton']['textContent'] = i18n['sendButton'];
    this['roomJoinTimes'] = {};
    this['LiveGameRunning'] = true;
    this['liveMode'] = 0;
    this['pingSent'] = 0;
    this['emoteAutocomplete'] = new ChatAutocomplete(this['chatInput'], this['chatInputArea'], ':', null);
    this['emoteAutocomplete']['minimalLengthForHint'] = 3;
    this['nameAutocomplete'] = new ChatAutocomplete(this['chatInput'], this['chatInputArea'], '@', function() {
            return arrayUnique(Object['values'](this['clients'])['map'](function(_0x3d81xba) {
                return _0x3d81xba['name']
            }))
        }
        ['bind'](this));
    this['nameAutocomplete']['prefixInSearch'] = false;
    let _0x3d81x143 = new _jstrisx();
    this['sessX'] = _0x3d81x143['get']();
    this['hostStartMode'] = false;
    this['noFourWide'] = 0;
    this['solidAfter'] = 0;
    this['liveSeed'] = '';
    this['liveMap'] = null;
    this['livePmodeTypes'] = [1, 1];
    this['team'] = null;
    this['teamSwitchDisabled'] = false;
    this['teamButtons'] = {};
    this['gdm'] = 0;
    this['gdms'] = ['targets', 'divide', 'toAll', 'toLeast', 'toMost', 'toSelf', 'random', 'roulette'];
    this['gDelay'] = 500;
    this['sgProfile'] = 0;
    this['playingOfflineWarningShown'] = false;
    this['loadMapForOpponents'] = false;
    this['Friends'] = new Friends(this);
    this['p']['Caption']['loading'](i18n['connecting']);
    this['connect']()
}
Live['prototype']['toggleLobbby'] = function(_0x3d81xfc) {
    if (typeof _0x3d81xfc === 'boolean') {
        this['lobbyVisible'] = !_0x3d81xfc
    };
    if (this['lobbyVisible']) {
        this['lobbyBox']['style']['display'] = 'none';
        this['lobbyVisible'] = false;
        this['p']['focusState'] = 0;
        this['RoomInfo']['onLobbyClosed']()
    } else {
        let _0x3d81x144 = (this['clients'][this['cid']] && this['clients'][this['cid']]['mod']);
        this['editRoomButton']['style']['display'] = (this['iAmHost'] || _0x3d81x144) ? null : 'none';
        this['editRoomButton']['classList']['add']('editBtn');
        if (_0x3d81x144) {
            if (!this['iAmHost']) {
                this['editRoomButton']['classList']['add']('modEditBtn');
                this['editRoomButton']['childNodes'][1]['nodeValue'] = ' ModEdit'
            } else {
                if (this['editRoomButton']['classList']['contains']('modEditBtn')) {
                    this['editRoomButton']['classList']['remove']('modEditBtn');
                    this['editRoomButton']['childNodes'][1]['nodeValue'] = ' Edit'
                }
            }
        };
        this['lobbyBox']['style']['display'] = 'block';
        this['lobbyVisible'] = true;
        this['refreshLobbby']();
        this['p']['focusState'] = 1
    }
};
Live['prototype']['initEmoteSelect'] = function(_0x3d81x145) {
    showElem(document['querySelector']('svg.emSel'));
    this['chatInput']['style']['paddingRight'] = '37px';
    var _0x3d81x146 = {
        "\x4A\x73\x74\x72\x69\x73": '/res/img/e/jstris.png',
        "\x73\x6D\x69\x6C\x65\x79\x73\x2D\x65\x6D\x6F\x74\x69\x6F\x6E": 'grinning_face',
        "\x70\x65\x6F\x70\x6C\x65\x2D\x62\x6F\x64\x79": 'waving_hand',
        "\x61\x6E\x69\x6D\x61\x6C\x73\x2D\x6E\x61\x74\x75\x72\x65": 'monkey_face',
        "\x66\x6F\x6F\x64\x2D\x64\x72\x69\x6E\x6B": 'red_apple',
        "\x74\x72\x61\x76\x65\x6C\x2D\x70\x6C\x61\x63\x65\x73": 'compass',
        "\x61\x63\x74\x69\x76\x69\x74\x69\x65\x73": 'joystick',
        "\x6F\x62\x6A\x65\x63\x74\x73": 'light_bulb',
        "\x73\x79\x6D\x62\x6F\x6C\x73": 'warning',
        "\x66\x6C\x61\x67\x73": 'flag_isle_of_man',
        "\x65\x78\x74\x72\x61\x73\x2D\x6F\x70\x65\x6E\x6D\x6F\x6A\x69": 'hacker_cat',
        "\x65\x78\x74\x72\x61\x73\x2D\x75\x6E\x69\x63\x6F\x64\x65": 'first_aid',
        "\x75\x73\x65\x72\x2D\x65\x6D\x6F\x74\x65\x73": '/res/img/e/u/erickmack.png'
    };
    this['EmoteSelect'] = new EmoteSelect(this['chatInput'], _0x3d81x145, this['chatArea'], document['querySelector']('svg.emSel'), '/res/oe/', _0x3d81x146)
};
Live['prototype']['toggleMore'] = function() {
    if (this['moreVisible']) {
        this['moreResults']['style']['display'] = 'none';
        this['moreVisible'] = false;
        this['p']['focusState'] = 0
    } else {
        this['safeSend']('{"t":15}');
        this['moreResults']['style']['display'] = 'block';
        this['moreVisible'] = true;
        this['p']['focusState'] = 1
    }
};
Live['prototype']['refreshLobbby'] = function() {
    var _0x3d81x147 = '{"t":10}';
    this['safeSend'](_0x3d81x147);
    this['refreshLobbyButton']['disabled'] = true;
    setTimeout(function() {
            this['refreshLobbyButton']['disabled'] = false
        }
        ['bind'](this), 2000)
};
Live['prototype']['saveRD'] = function() {
    var _0x3d81x148, _0x3d81x149, _0x3d81xf, _0x3d81x11 = {};
    var _0x3d81x14a = {
        "\x63\x6F\x6D\x62\x6F\x54\x61\x62\x6C\x65": {
            d: ',',
            t: 'i'
        },
        "\x61\x74\x74\x61\x63\x6B\x54\x61\x62\x6C\x65": {
            d: ',',
            t: 'i'
        },
        "\x6C\x6F\x63\x6B\x44\x65\x6C\x61\x79": {
            d: '/',
            t: 'i'
        },
        "\x73\x67\x50\x72\x6F\x66\x69\x6C\x65": {
            d: ',',
            t: 'f'
        }
    };
    _0x3d81x148 = document['getElementById']('moreSet2');
    var _0x3d81x14b = Array['prototype']['slice']['call'](_0x3d81x148['getElementsByTagName']('input'), 0);
    var _0x3d81x14c = Array['prototype']['slice']['call'](_0x3d81x148['getElementsByTagName']('select'), 0);
    _0x3d81x149 = _0x3d81x14b['concat'](_0x3d81x14c);
    for (_0x3d81xf = 0; _0x3d81xf < _0x3d81x149['length']; ++_0x3d81xf) {
        if (_0x3d81x149[_0x3d81xf]['id'] === 'attackMode') {
            continue
        };
        if (_0x3d81x149[_0x3d81xf]['type'] === 'radio') {
            continue
        };
        if (_0x3d81x149[_0x3d81xf]['type'] === 'hidden') {
            continue
        };
        if (_0x3d81x149[_0x3d81xf]['type'] === 'checkbox') {
            _0x3d81x11[_0x3d81x149[_0x3d81xf]['id']] = _0x3d81x149[_0x3d81xf]['checked']
        } else {
            var _0x3d81x30 = _0x3d81x149[_0x3d81xf]['value'];
            var _0x3d81x14d = /^\d+$/ ['test'](_0x3d81x30);
            _0x3d81x11[_0x3d81x149[_0x3d81xf]['id']] = (_0x3d81x14d) ? parseInt(_0x3d81x30) : _0x3d81x30;
            if (_0x3d81x149[_0x3d81xf]['id'] in _0x3d81x14a) {
                let _0x3d81x14e = _0x3d81x14a[_0x3d81x149[_0x3d81xf]['id']]['d'];
                let _0x3d81x14f = _0x3d81x14a[_0x3d81x149[_0x3d81xf]['id']]['t'];
                var _0x3d81x104 = _0x3d81x30['split'](_0x3d81x14e);
                var _0x3d81x150 = null;
                if ('i' === _0x3d81x14f) {
                    _0x3d81x150 = _0x3d81x104['map'](function(_0x3d81x19) {
                        return parseInt(_0x3d81x19)
                    })
                } else {
                    if ('f' === _0x3d81x14f) {
                        _0x3d81x150 = _0x3d81x104['map'](function(_0x3d81x19) {
                            return parseFloat(_0x3d81x19)
                        })
                    }
                };
                _0x3d81x11[_0x3d81x149[_0x3d81xf]['id']] = _0x3d81x150
            }
        }
    };
    var _0x3d81x151 = parseInt(document['querySelector']('input[name="allSpin"]:checked')['value']);
    if (_0x3d81x151) {
        _0x3d81x11['allSpin'] = _0x3d81x151
    };
    delete _0x3d81x11['hostStart'];
    delete _0x3d81x11['srvSel'];
    document['getElementById']('saveData')['style']['display'] = 'block';
    document['getElementById']('saveDataArea')['value'] = JSON['stringify'](_0x3d81x11);
    if (typeof ga === 'function') {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Preset',
            eventAction: 'export'
        })
    }
};
Live['prototype']['switchRDmode'] = function(_0x3d81xb) {
    var _0x3d81x19 = document['getElementsByName']('moreSel');
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x19['length']; _0x3d81x13++) {
        if (_0x3d81x19[_0x3d81x13]['value'] === _0x3d81xb.toString()) {
            _0x3d81x19[_0x3d81x13]['checked'] = true
        }
    };
    document['getElementById']('moreSet2')['style']['display'] = 'block';
    document['getElementById']('presetMode')['style']['display'] = 'none';
    document['getElementById']('saveRD')['style']['visibility'] = 'visible';
    this['createRoomDialog']['style']['height'] = null;
    if (_0x3d81xb === 0) {
        this['showClass']('adv', false);
        this['showClass']('simple', true)
    } else {
        if (_0x3d81xb === 1) {
            this['showClass']('simple', false);
            this['showClass']('adv', true);
            this['createRoomDialog']['style']['height'] = '660px'
        } else {
            if (_0x3d81xb === 2) {
                document['getElementById']('moreSet2')['style']['display'] = 'none';
                document['getElementById']('presetMode')['style']['display'] = 'block';
                document['getElementById']('saveRD')['style']['visibility'] = 'hidden';
                this['createRoomDialog']['style']['height'] = '493px'
            }
        }
    }
};
Live['prototype']['onPresetChange'] = function() {
    var _0x3d81x2d = document['getElementById']('presetSel');
    document['getElementById']('settingsDesc')['textContent'] = _0x3d81x2d['options'][_0x3d81x2d['selectedIndex']]['dataset']['desc']
};
Live['prototype']['useCustomPreset'] = function() {
    var _0x3d81x152 = prompt('Enter title or ID');
    if (_0x3d81x152 !== null) {
        var _0x3d81x153 = new XMLHttpRequest();
        var _0x3d81x25 = this;
        _0x3d81x153['addEventListener']('load', function() {
            var _0x3d81x12 = JSON['parse'](this['responseText']);
            var _0x3d81x154 = document['getElementById']('presetSel');
            addOption(_0x3d81x154, _0x3d81x12);
            _0x3d81x154['value'] = _0x3d81x12['id'];
            _0x3d81x25['onPresetChange']()
        });
        _0x3d81x153['open']('GET', '/code/getPresetData/' + _0x3d81x152);
        _0x3d81x153['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
        _0x3d81x153['send']()
    } else {
        return
    }
};
Live['prototype']['showRoomDialogForEdit'] = function() {
    this['showRoomDialog']();
    this['switchRDmode'](1);
    var _0x3d81x155 = function(_0x3d81x156) {
        var _0x3d81x157 = document['createElement']('textarea');
        _0x3d81x157['innerHTML'] = _0x3d81x156;
        return _0x3d81x157['value']
    };
    let _0x3d81x158 = this['roomConfig'];
    let _0x3d81x159 = document['getElementById']('create');
    _0x3d81x159['classList']['add']('editBtn');
    _0x3d81x159['childNodes'][1]['nodeValue'] = ' ' + i18n['applyCh'];
    document['getElementById']('gameMode')['disabled'] = true;
    document['getElementById']('srvSel')['disabled'] = true;
    hideElem(document['getElementById']('addJL'));
    this['roomNameInput']['value'] = _0x3d81x155(this['roomConfig']['n']);
    this['isPrivateInput']['checked'] = this['roomConfig']['p'];
    document['getElementById']('numPlayers')['value'] = (_0x3d81x158['max'] >= 1 && _0x3d81x158['max'] <= 7) ? _0x3d81x158['max'] : 28;
    document['getElementById']('gameMode')['value'] = (_0x3d81x158['pmode']) ? _0x3d81x158['pmode'][0] + 100 : _0x3d81x158['mode'];
    document['getElementById']('attackTable')['value'] = this['roomConfig']['at']['join'](',');
    document['getElementById']('comboTable')['value'] = this['roomConfig']['ct']['join'](',');
    document['getElementById']('sgProfile')['value'] = this['roomConfig']['sgpA']['join'](',');
    document['getElementById']('gdmSel')['value'] = _0x3d81x158['gdm'];
    document['getElementById']('gblockSel')['value'] = _0x3d81x158['gblock'];
    document['getElementById']('rndSel')['value'] = _0x3d81x158['rnd'];
    document['getElementById']('blocksSel')['value'] = _0x3d81x158['bbs'];
    document['getElementById']('prSel')['value'] = _0x3d81x158['pr'];
    document['getElementById']('garbageDelay')['value'] = _0x3d81x158['gDelay'];
    document['getElementById']('mess')['value'] = _0x3d81x158['mess'];
    document['getElementById']('gapW')['value'] = _0x3d81x158['gapW'];
    document['getElementById']('gInv')['checked'] = _0x3d81x158['gInv'];
    document['getElementById']('hasSolid')['checked'] = (_0x3d81x158['sg'] !== 0);
    document['getElementById']('solid')['value'] = _0x3d81x158['sg'];
    document['getElementById']('hasHold')['checked'] = _0x3d81x158['hold'];
    document['getElementById']('hostStart')['checked'] = _0x3d81x158['hostStart'];
    document['getElementById']('noFW')['checked'] = _0x3d81x158['noFW'];
    document['getElementById']('solidAtk')['checked'] = _0x3d81x158['sa'];
    document['getElementById']('as' + _0x3d81x158['as'])['checked'] = true;
    document['getElementById']('asEx')['value'] = _0x3d81x158['asEx'];
    document['getElementById']('clearDelay')['value'] = _0x3d81x158['cd'];
    document['getElementById']('speedLimit')['value'] = _0x3d81x158['sl'];
    document['getElementById']('gravityLvl')['value'] = _0x3d81x158['grav'];
    document['getElementById']('lockDelay')['value'] = _0x3d81x158['ld']['join']('/');
    document['getElementById']('srvSel')['value'] = this['serverId']
};
Live['prototype']['showRoomDialog'] = function() {
    this['clearRoomForm']();
    this['createRoomDialog']['style']['display'] = 'block';
    let _0x3d81x159 = document['getElementById']('create');
    _0x3d81x159['classList']['remove']('editBtn');
    _0x3d81x159['childNodes'][1]['nodeValue'] = ' ' + i18n['create'];
    document['getElementById']('gameMode')['disabled'] = false;
    document['getElementById']('srvSel')['disabled'] = false;
    document['getElementById']('addJL')['style']['display'] = 'inline';
    var _0x3d81x153 = new XMLHttpRequest();
    var _0x3d81x154 = document['getElementById']('presetSel');
    _0x3d81x154['innerHTML'] = '<option value="0" data-desc="The default settings">Default</option>';
    _0x3d81x154['value'] = '0';
    this['onPresetChange']();
    _0x3d81x153['addEventListener']('load', function() {
        var _0x3d81x104 = JSON['parse'](this['responseText']);
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x104['length']; _0x3d81x13++) {
            addOption(_0x3d81x154, _0x3d81x104[_0x3d81x13])
        };
        document['getElementById']('presetLoadState')['style']['visibility'] = 'hidden'
    });
    _0x3d81x153['open']('GET', '/code/presetList'); +
    _0x3d81x153['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
    _0x3d81x153['send']();
    document['getElementById']('presetLoadState')['style']['visibility'] = 'visible'
};
Live['prototype']['closeRoomDialog'] = function() {
    this['createRoomDialog']['style']['display'] = 'none';
    this['clearRoomForm']()
};
Live['prototype']['clearRoomForm'] = function() {
    this['switchRDmode'](2);
    this['roomNameInput']['value'] = ((this['chatName'] !== '') ? this['chatName'] : 'NoNamed') + '\'s room';
    this['isPrivateInput']['checked'] = false;
    document['getElementById']('createState')['style']['display'] = 'none';
    document['getElementById']('create')['disabled'] = false;
    document['getElementById']('saveData')['style']['display'] = 'none';
    document['getElementById']('numPlayers')['value'] = 28;
    document['getElementById']('gameMode')['value'] = 0;
    document['getElementById']('isPrivate')['checked'] = false;
    document['getElementById']('hasSolid')['checked'] = true;
    document['getElementById']('solid')['value'] = 120;
    this['setDefaultRule']();
    this['clearLimitsForm']()
};
Live['prototype']['clearLimitsForm'] = function() {
    let _0x3d81x15a = document['getElementById']('joinLimits'),
        _0x3d81x149 = Array['from'](_0x3d81x15a['getElementsByTagName']('input')),
        _0x3d81x13 = 0;
    for (let _0x3d81x15b of _0x3d81x149) {
        _0x3d81x15b['min'] = '0';
        _0x3d81x15b['step'] = '.1';
        _0x3d81x15b['autocomplete'] = 'off';
        _0x3d81x15b['placeholder'] = (_0x3d81x13 % 2 === 0) ? 'MIN' : 'MAX';
        _0x3d81x15b['value'] = '';
        ++_0x3d81x13
    }
};
Live['prototype']['getFilledLimits'] = function() {
    let _0x3d81x15a = document['getElementById']('joinLimits'),
        _0x3d81x149 = Array['from'](_0x3d81x15a['getElementsByTagName']('input')),
        _0x3d81x13 = -1,
        _0x3d81x15c = 0,
        _0x3d81x12 = [];
    for (let _0x3d81x15b of _0x3d81x149) {
        ++_0x3d81x13;
        if (_0x3d81x15b['value'] === '') {
            _0x3d81x12['push'](null);
            ++_0x3d81x15c;
            continue
        };
        let _0x3d81x30 = parseFloat(_0x3d81x15b['value']);
        if (isNaN(_0x3d81x30)) {
            return false
        };
        _0x3d81x12['push'](_0x3d81x30);
        if (_0x3d81x13 % 2 === 1 && _0x3d81x12[_0x3d81x13 - 1] !== null && _0x3d81x30 <= _0x3d81x12[_0x3d81x13 - 1]) {
            return false
        }
    };
    if (_0x3d81x15c === _0x3d81x12['length']) {
        return null
    };
    return _0x3d81x12
};
Live['prototype']['setDefaultRule'] = function() {
    document['getElementById']('attackMode')['value'] = 0;
    this['attackSelect']();
    document['getElementById']('solid')['disabled'] = !document['getElementById']('hasSolid')['checked'];
    document['getElementById']('hasHold')['checked'] = true;
    document['getElementById']('solid')['value'] = 120;
    document['getElementById']('clearDelay')['value'] = 0;
    document['getElementById']('speedLimit')['value'] = 0;
    document['getElementById']('rndSel')['value'] = '0';
    document['getElementById']('blocksSel')['value'] = '0';
    document['getElementById']('gravityLvl')['value'] = '1';
    document['getElementById']('lockDelay')['value'] = '500/5000/20000';
    document['getElementById']('sgProfile')['value'] = '0,3';
    document['getElementById']('prSel')['value'] = '5';
    document['getElementById']('gdmSel')['value'] = '3';
    document['getElementById']('gblockSel')['value'] = '0';
    document['getElementById']('garbageDelay')['value'] = '500';
    document['getElementById']('mess')['value'] = '0';
    document['getElementById']('gapW')['value'] = '1';
    document['getElementById']('gInv')['checked'] = false;
    document['getElementById']('hostStart')['checked'] = false;
    document['getElementById']('noFW')['checked'] = false;
    document['getElementById']('solidAtk')['checked'] = false;
    document['getElementById']('as0')['checked'] = true;
    document['getElementById']('asEx')['value'] = '';
    document['getElementById']('srvSel')['value'] = '0'
};
Live['prototype']['attackSelect'] = function() {
    var _0x3d81x102 = parseInt(document['getElementById']('attackMode')['value']);
    var _0x3d81x56 = '',
        _0x3d81x55 = '';
    switch (_0x3d81x102) {
        case 0:
            _0x3d81x56 = '0,0,1,2,4,4,6,2,0,10,1';
            _0x3d81x55 = '0,0,1,1,1,2,2,3,3,4,4,4,5';
            break;
        case 1:
            _0x3d81x56 = '0,0,1,2,4,4,6,2,1,9,1';
            _0x3d81x55 = '0,0,1,1,2,2,3,3,4,4,4,5,5';
            break;
        case 2:
            _0x3d81x56 = '0,0,1,2,4,1,2,0,0,10,1';
            _0x3d81x55 = '0,0,1,1,1,2,2,3,3,4,4,4,5';
            break
    };
    document['getElementById']('attackTable')['value'] = _0x3d81x56;
    document['getElementById']('comboTable')['value'] = _0x3d81x55
};
Live['prototype']['applyPreset'] = function(_0x3d81x15d) {
    for (var _0x3d81x15e in _0x3d81x15d) {
        switch (_0x3d81x15e) {
            case 'attackTable':
                document['getElementById']('attackTable')['value'] = _0x3d81x15d[_0x3d81x15e]['join'](',');
                break;
            case 'comboTable':
                document['getElementById']('comboTable')['value'] = _0x3d81x15d[_0x3d81x15e]['join'](',');
                break;
            case 'sgProfile':
                document['getElementById']('sgProfile')['value'] = _0x3d81x15d[_0x3d81x15e]['join'](',');
                break;
            case 'lockDelay':
                document['getElementById']('lockDelay')['value'] = _0x3d81x15d[_0x3d81x15e]['join']('/');
                break;
            case 'hasSolid':
                document['getElementById']('hasSolid')['checked'] = _0x3d81x15d[_0x3d81x15e];
                break;
            case 'hasHold':
                document['getElementById']('hasHold')['checked'] = _0x3d81x15d[_0x3d81x15e];
                break;
            case 'isPrivate':
                document['getElementById']('isPrivate')['checked'] = _0x3d81x15d[_0x3d81x15e];
                break;
            case 'allSpin':
                document['getElementById']('as' + _0x3d81x15d[_0x3d81x15e])['checked'] = true;
                break;
            default:
                document['getElementById'](_0x3d81x15e)['value'] = _0x3d81x15d[_0x3d81x15e];
                break
        }
    }
};
Live['prototype']['makeRoomWrapper'] = function() {
    var _0x3d81x15f = document['getElementById']('create')['classList']['contains']('editBtn');
    if (document['getElementById']('more_preset')['checked']) {
        var _0x3d81x30 = document['getElementById']('presetSel')['value'];
        if (_0x3d81x30 != '0') {
            document['getElementById']('createState')['style']['display'] = 'inline';
            document['getElementById']('create')['disabled'] = true;
            var _0x3d81x153 = new XMLHttpRequest();
            var _0x3d81x25 = this;
            _0x3d81x153['addEventListener']('load', function() {
                var _0x3d81x12 = JSON['parse'](this['responseText']);
                _0x3d81x25['setDefaultRule']();
                _0x3d81x25['applyPreset'](_0x3d81x12['data']);
                _0x3d81x25['makeRoom'](_0x3d81x15f)
            });
            _0x3d81x153['open']('GET', '/code/getPresetData/' + _0x3d81x30);
            _0x3d81x153['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
            _0x3d81x153['send']();
            if (typeof ga === 'function') {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Preset',
                    eventAction: 'load'
                })
            };
            return
        }
    };
    this['makeRoom'](_0x3d81x15f)
};
Live['prototype']['makeRoom'] = function(_0x3d81x15f) {
    var _0x3d81x160 = true,
        _0x3d81x161 = '';
    var _0x3d81x162 = this['roomNameInput']['value']['replace'](/"/g, '\"');
    var _0x3d81x158 = new Object();
    _0x3d81x158['t'] = 11;
    if (_0x3d81x15f) {
        _0x3d81x158['edit'] = _0x3d81x15f
    };
    if (document['getElementById']('more_simple')['checked']) {
        this['attackSelect']()
    };
    if (_0x3d81x162['length'] === 0) {
        _0x3d81x161 += i18n['rNameReq'] + '\x0A';
        _0x3d81x160 = false
    };
    _0x3d81x158['p'] = this['isPrivateInput']['checked'];
    _0x3d81x158['n'] = _0x3d81x162;
    var _0x3d81x2d = document['getElementById']('numPlayers');
    _0x3d81x158['pl'] = parseInt(_0x3d81x2d['options'][_0x3d81x2d['selectedIndex']]['value']);
    _0x3d81x2d = document['getElementById']('gameMode');
    _0x3d81x158['m'] = parseInt(_0x3d81x2d['options'][_0x3d81x2d['selectedIndex']]['value']);
    if (_0x3d81x158['m'] > 100) {
        _0x3d81x158['pm'] = _0x3d81x158['m'] - 100;
        _0x3d81x158['m'] = 3
    };
    var _0x3d81x163, _0x3d81x164;
    var _0x3d81x165 = '[' + document['getElementById']('attackTable')['value'] + ']';
    var _0x3d81x166 = '[' + document['getElementById']('comboTable')['value'] + ']';
    try {
        _0x3d81x163 = JSON['parse'](_0x3d81x165);
        _0x3d81x164 = JSON['parse'](_0x3d81x166);
        var _0x3d81x167 = function(_0x3d81x131) {
            return (Number(_0x3d81x131) === _0x3d81x131 && _0x3d81x131 % 1 === 0 && _0x3d81x131 >= 0 && _0x3d81x131 <= 255)
        };
        if (_0x3d81x163['length'] !== 11 || _0x3d81x164['length'] !== 13 || !_0x3d81x163['every'](_0x3d81x167) || !_0x3d81x164['every'](_0x3d81x167)) {
            throw 1
        }
    } catch (_0x3d81x2d) {
        _0x3d81x161 += 'Attack table or combo table has invalid format.\x0A';
        _0x3d81x160 = false
    };
    _0x3d81x158['at'] = _0x3d81x163;
    _0x3d81x158['ct'] = _0x3d81x164;
    _0x3d81x158['gdm'] = parseInt(document['getElementById']('gdmSel')['value']);
    _0x3d81x158['gblock'] = parseInt(document['getElementById']('gblockSel')['value']);
    _0x3d81x158['rnd'] = parseInt(document['getElementById']('rndSel')['value']);
    _0x3d81x158['bset'] = parseInt(document['getElementById']('blocksSel')['value']);
    _0x3d81x158['pr'] = parseInt(document['getElementById']('prSel')['value']);
    _0x3d81x158['gDelay'] = parseInt(document['getElementById']('garbageDelay')['value']);
    _0x3d81x158['mess'] = parseInt(document['getElementById']('mess')['value']);
    _0x3d81x158['gapW'] = parseInt(document['getElementById']('gapW')['value']);
    if (isNaN(_0x3d81x158['gDelay']) || isNaN(_0x3d81x158['mess']) || isNaN(_0x3d81x158['gapW'])) {
        _0x3d81x161 += 'gDelay, mess, gapW must be numeric values.\x0A';
        _0x3d81x160 = false
    };
    var _0x3d81x168 = document['getElementById']('asEx')['value'];
    if (_0x3d81x168['length']) {
        try {
            _0x3d81x168 = _0x3d81x168['toUpperCase']()['split'](',');
            var _0x3d81x167 = function(_0x3d81xac) {
                return (_0x3d81xac['length'] > 0 && _0x3d81xac['length'] <= 2 && /^[A-Z0-9]+$/ ['test'](_0x3d81xac))
            };
            if (_0x3d81x168['length'] > 6 || !_0x3d81x168['every'](_0x3d81x167)) {
                throw 1
            };
            _0x3d81x158['asEx'] = _0x3d81x168['join'](',')
        } catch (_0x3d81x2d) {
            _0x3d81x161 += 'Allspin piece exclusion list invalid. Enter e.g.: L,S,Z,J\x0A';
            _0x3d81x160 = false
        }
    };
    if (document['getElementById']('hasSolid')['checked']) {
        var _0x3d81x30 = parseInt(document['getElementById']('solid')['value']);
        if (!_0x3d81x30 || _0x3d81x30 < 0 || _0x3d81x30 > 600) {
            _0x3d81x161 += 'Time interval is invalid.\x0A';
            _0x3d81x160 = false
        } else {
            _0x3d81x158['sg'] = _0x3d81x30
        }
    } else {
        _0x3d81x158['sg'] = 0
    };
    _0x3d81x158['hold'] = document['getElementById']('hasHold')['checked'];
    _0x3d81x158['hostStart'] = document['getElementById']('hostStart')['checked'];
    _0x3d81x158['noFW'] = document['getElementById']('noFW')['checked'];
    _0x3d81x158['sa'] = document['getElementById']('solidAtk')['checked'];
    _0x3d81x158['gInv'] = document['getElementById']('gInv')['checked'];
    _0x3d81x158['as'] = parseInt(document['querySelector']('input[name="allSpin"]:checked')['value']);
    _0x3d81x158['srv'] = document['getElementById']('srvSel')['value'];
    if (_0x3d81x158['srv'] !== '0' && _0x3d81x158['srv']['charAt'](0) !== 'M' && !_0x3d81x158['p']) {
        _0x3d81x161 += 'Server selection is currently available only for Private rooms.\x0A';
        _0x3d81x160 = false
    };
    var _0x3d81x169 = parseInt(document['getElementById']('clearDelay')['value']);
    if (isNaN(_0x3d81x169) || _0x3d81x169 < 0 || _0x3d81x169 > 6000) {
        _0x3d81x161 += 'Clear delay is invalid.\x0A';
        _0x3d81x160 = false
    } else {
        _0x3d81x158['cd'] = _0x3d81x169
    };
    var _0x3d81x16a = parseFloat(document['getElementById']('speedLimit')['value']);
    if (isNaN(_0x3d81x16a) || _0x3d81x16a < 0 || _0x3d81x16a > 10) {
        _0x3d81x161 += 'Speed limit is invalid. Allowed (0-10PPS).\x0A';
        _0x3d81x160 = false
    } else {
        _0x3d81x158['sl'] = _0x3d81x16a
    };
    var _0x3d81x16b = parseInt(document['getElementById']('gravityLvl')['value']);
    if (isNaN(_0x3d81x16b) || _0x3d81x16b < 0 || _0x3d81x16b > 28) {
        _0x3d81x161 += 'Gravity LVL must be 0-28.\x0A';
        _0x3d81x160 = false
    } else {
        _0x3d81x158['grav'] = _0x3d81x16b
    };
    var _0x3d81x80 = document['getElementById']('lockDelay')['value']['split']('/', 3)['map'](function(_0x3d81x13) {
        return parseInt(_0x3d81x13, 10)
    });
    if (_0x3d81x80['length'] !== 3 || isNaN(_0x3d81x80[0]) || isNaN(_0x3d81x80[1]) || isNaN(_0x3d81x80[2]) || _0x3d81x80[0] < 0 || _0x3d81x80[1] < 0 || _0x3d81x80[2] < 0 || _0x3d81x80[0] > 20000000 || _0x3d81x80[1] > 20000000 || _0x3d81x80[2] > 20000000) {
        _0x3d81x161 += 'Lock delay value is invalid. Expected format: 500/5000/20000.\x0A';
        _0x3d81x160 = false
    } else {
        _0x3d81x158['ld'] = _0x3d81x80
    };
    var _0x3d81x16c = '[' + document['getElementById']('sgProfile')['value'] + ']';
    try {
        _0x3d81x16c = JSON['parse'](_0x3d81x16c);
        var _0x3d81x167 = function(_0x3d81x131) {
            return (Number(_0x3d81x131) === _0x3d81x131 && _0x3d81x131 >= 0 && _0x3d81x131 <= 999)
        };
        if (_0x3d81x16c['length'] < 1 || _0x3d81x16c['length'] > 30 || !_0x3d81x16c['every'](_0x3d81x167)) {
            throw 1
        };
        if (_0x3d81x16c[_0x3d81x16c['length'] - 1] <= 0) {
            _0x3d81x161 += 'Last sgProfile delay must be > 0.\x0A';
            throw 1
        }
    } catch (_0x3d81x2d) {
        _0x3d81x161 += 'Solid garbage profile has invalid format.\x0A';
        _0x3d81x160 = false
    };
    _0x3d81x158['sgpA'] = _0x3d81x16c;
    var _0x3d81x16d = this['getFilledLimits']();
    if (_0x3d81x16d === false) {
        _0x3d81x161 += 'All join limits must be numeric and MIN must be less than MAX.\x0A';
        _0x3d81x160 = false
    } else {
        if (_0x3d81x16d !== null) {
            _0x3d81x158['lim'] = _0x3d81x16d
        }
    };
    if (0 != document['getElementById']('r-ext')['value']) {
        _0x3d81x158['ext'] = parseInt(document['getElementById']('r-ext')['value'])
    };
    if (_0x3d81x160) {
        var _0x3d81x147 = JSON['stringify'](_0x3d81x158);
        this['safeSend'](_0x3d81x147);
        this['sendGameOverEvent']();
        this['closeRoomDialog']();
        this['toggleLobbby'](false);
        this['createRoomRequest'] = _0x3d81x158
    } else {
        alert(_0x3d81x161)
    };
    document['getElementById']('createState')['style']['display'] = 'none';
    document['getElementById']('create')['disabled'] = false
};
Live['prototype']['joinRoom'] = function(_0x3d81x16e) {
    this['sendGameOverEvent']();
    var _0x3d81x16f = _0x3d81x16e['replace'](/"/g, '\"');
    var _0x3d81x147 = '{"t":12, "id":"' + _0x3d81x16f + '"}';
    this['safeSend'](_0x3d81x147);
    this['closeRoomDialog']();
    this['toggleLobbby'](false);
    this['hideResults']();
    if (_0x3d81x16f == 5 && typeof ga === 'function') {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Team',
            eventAction: 'joinTeamRoom'
        })
    }
};
Live['prototype']['onOpen'] = function(_0x3d81x2d) {
    if (this['connectionTimeout']) {
        clearTimeout(this['connectionTimeout']);
        this['connectionTimeout'] = null
    };
    this['p']['Caption']['hide'](this['p']['Caption'].LOADING);
    this['socket']['onclose'] = this['onClose']['bind'](this);
    if (this['authorized'] && !this['joinRemote']) {
        this['authReady'] = false;
        this['p']['Caption']['loading'](i18n['signingIn']);
        var _0x3d81x153 = new XMLHttpRequest();
        var _0x3d81x25 = this;
        _0x3d81x153['onreadystatechange'] = function() {
            if (this['readyState'] === 4 && this['status'] === 200) {
                var _0x3d81x12 = JSON['parse'](this['responseText']);
                _0x3d81x25['authorize'](_0x3d81x12)
            }
        };
        _0x3d81x153['open']('GET', '/token', true);
        _0x3d81x153['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
        _0x3d81x153['send']()
    }
};
Live['prototype']['authorize'] = function(_0x3d81x12) {
    if (_0x3d81x12['hasOwnProperty']('token')) {
        _0x3d81x12['t'] = 19;
        _0x3d81x12['s'] = this['sessX'];
        this['safeSend'](JSON['stringify'](_0x3d81x12))
    }
};
Live['prototype']['getAuthorizedNameLink'] = function(_0x3d81x170, _0x3d81x97, _0x3d81x171) {
    var _0x3d81x172 = _0x3d81x170,
        _0x3d81x173 = '',
        _0x3d81x174 = '',
        _0x3d81x9a = null,
        _0x3d81x175 = '',
        _0x3d81x176 = [];
    if (_0x3d81x171) {
        if (_0x3d81x171['color']) {
            _0x3d81x9a = _0x3d81x171['color']
        }
    };
    if (_0x3d81x9a !== null) {
        _0x3d81x176['push']('color:' + _0x3d81x9a)
    };
    if (_0x3d81x171 && _0x3d81x171['bold']) {
        _0x3d81x176['push']('font-weight:bold')
    };
    if (_0x3d81x172['length'] >= 16) {
        _0x3d81x172 = _0x3d81x172['substr'](0, 14) + '&hellip;'
    };
    switch (_0x3d81x97) {
        case 100:
            _0x3d81x173 = '<img src="/res/crown.png" class="nameIcon">';
            _0x3d81x174 = 'Champion';
            break;
        case 101:
            _0x3d81x173 = '<img src="' + CDN_URL('/res/ield.png') + '" alt="Mod" class="nameIcon">';
            _0x3d81x174 = 'Moderator';
            break;
        default:
            if (_0x3d81x97 >= 110 && _0x3d81x97 <= 118) {
                let _0x3d81x177 = {
                    "\x31\x31\x30": 'gstr',
                    "\x31\x31\x31": 'jsT',
                    "\x31\x31\x32": 'blt',
                    "\x31\x31\x33": 'blo',
                    "\x31\x31\x34": 'bls',
                    "\x31\x31\x35": 'blz',
                    "\x31\x31\x36": 'bll',
                    "\x31\x31\x37": 'blj',
                    "\x31\x31\x38": 'bli'
                };
                _0x3d81x173 = '<img src="' + CDN_URL('/res/' + _0x3d81x177[_0x3d81x97] + '.png') + '" alt="D" class="nameIcon">';
                _0x3d81x174 = 'Jstris supporter'
            } else {
                if (_0x3d81x97 >= 1000 && _0x3d81x97 <= 2000) {
                    _0x3d81x97 -= 1000;
                    let _0x3d81x178 = String['fromCharCode'](((_0x3d81x97 & 992) >> 5) + 65);
                    _0x3d81x178 += String['fromCharCode']((_0x3d81x97 & 31) + 65);
                    _0x3d81x173 = '<img src="https://jstris.jezevec10.com/vendor/countries/flags/' + _0x3d81x178 + '.png" alt="' + _0x3d81x178 + '" class="nameIcon">';
                    _0x3d81x174 = 'Jstris supporter'
                } else {
                    if (_0x3d81x97 === 999 && _0x3d81x171 && _0x3d81x171['icon']) {
                        _0x3d81x173 = '<img src="' + CDN_URL('/res/oe/' + _0x3d81x171['icon'] + '.svg') + '" class="nameIcon_oe">';
                        _0x3d81x174 = 'Jstris supporter'
                    }
                }
            };
            break
    };
    if (_0x3d81x176['length']) {
        _0x3d81x175 += ' style="' + (_0x3d81x176['join'](';')) + '"'
    };
    _0x3d81x175 += (_0x3d81x174) ? ' title="' + _0x3d81x174 + '"' : '';
    return '<a href="/u/' + _0x3d81x170 + '" target="_blank"' + _0x3d81x175 + '>' + _0x3d81x173 + _0x3d81x172 + '</a>'
};
Live['prototype']['getName'] = function(_0x3d81x20, _0x3d81x179) {
    var _0x3d81x17a = (typeof _0x3d81x179 === 'undefined') ? true : _0x3d81x179;
    if (typeof this['clients'][_0x3d81x20] !== 'undefined' && this['clients'][_0x3d81x20]['name'] !== null) {
        if (_0x3d81x17a && (arrayContains(this['authList'], parseInt(_0x3d81x20)) || (this['cid'] == parseInt(_0x3d81x20) && this['authorized']))) {
            return this['getAuthorizedNameLink'](this['clients'][_0x3d81x20]['name'], this['clients'][_0x3d81x20]['type'], this['clients'][_0x3d81x20])
        };
        return this['clients'][_0x3d81x20]['name']
    } else {
        if (_0x3d81x20 === -1) {
            return ''
        } else {
            if (_0x3d81x20 === -2) {
                return '<span style=\'color:#00D928\'>' + i18n['newsUser'] + '</span>'
            } else {
                if (_0x3d81x20 === -3) {
                    return '<span style=\'color:#FF3700\'>' + i18n['serverUser'] + '</span>'
                } else {
                    if (_0x3d81x20 === -4) {
                        return '<span style=\'color:yellow\'>' + i18n['warning2'] + '</span>'
                    } else {
                        if (_0x3d81x20 === -5) {
                            return '<i class=\'glyphicon glyphicon-info-sign\'></i>'
                        } else {
                            return i18n['noNamed'] + _0x3d81x20.toString()
                        }
                    }
                }
            }
        }
    }
};
Live['prototype']['forgetRoomPlayers'] = function() {
    var _0x3d81x27 = null;
    if (typeof this['clients'][this['cid']] !== 'undefined') {
        _0x3d81x27 = this['clients'][this['cid']]
    };
    this['clients'] = {};
    this['players'] = Array();
    this['bots'] = Array();
    this['authList'] = Array();
    this['roomJoinTimes'] = {};
    if (_0x3d81x27 !== null) {
        this['clients'][this['cid']] = _0x3d81x27
    }
};
Live['prototype']['createPrivatePracticeRoom'] = function() {
    var _0x3d81x17b = this['getParameterByName']('play');
    if (!this['urlPlayParamApplied'] && !this['joinRemote'] && _0x3d81x17b != '' && !isNaN(parseInt(_0x3d81x17b))) {
        this['clearRoomForm']();
        this['roomNameInput']['value'] = 'The Private Room';
        this['isPrivateInput']['checked'] = true;
        this['makeRoom']()
    }
};
Live['prototype']['onCIDassigned'] = function() {
    if (this['lastDC'] === 4008) {
        this['lastDC'] = null;
        var _0x3d81x147 = JSON['stringify'](this['createRoomRequest']);
        this['safeSend'](_0x3d81x147);
        this['createRoomRequest'] = null
    } else {
        if (this['authReady']) {
            this['createPrivatePracticeRoom']()
        }
    };
    if (typeof ga === 'function') {
        var _0x3d81x97 = (this['isProxy']) ? 'proxy' : 'normal';
        ga('send', {
            hitType: 'event',
            eventCategory: 'Connect',
            eventAction: _0x3d81x97
        })
    };
    this['p']['Settings']['removeBanArtifact']()
};
Live['prototype']['readSpecs'] = function(_0x3d81x11, _0x3d81x17c) {
    var _0x3d81x17d = '';
    var _0x3d81x13 = 0;
    for (var _0x3d81x14 in _0x3d81x11['spec']) {
        var _0x3d81x20 = parseInt(_0x3d81x14);
        if (!isNaN(_0x3d81x20)) {
            if (typeof this['clients'][_0x3d81x20] === 'undefined') {
                this['clients'][_0x3d81x20] = new Client(_0x3d81x20)
            };
            if (_0x3d81x11['spec'][_0x3d81x14]['hasOwnProperty']('auth') && _0x3d81x11['spec'][_0x3d81x14]['auth'] === true) {
                this['clients'][_0x3d81x20]['auth'] = true;
                this['authList']['push'](_0x3d81x20)
            };
            if (_0x3d81x17c && typeof _0x3d81x11['spec'][_0x3d81x14]['n'] !== 'undefined') {
                this['clients'][_0x3d81x20]['name'] = _0x3d81x11['spec'][_0x3d81x14]['n']
            };
            if (_0x3d81x11['spec'][_0x3d81x14]['hasOwnProperty']('type')) {
                this['clients'][_0x3d81x20]['type'] = _0x3d81x11['spec'][_0x3d81x14]['type'];
                if (_0x3d81x11['spec'][_0x3d81x14]['hasOwnProperty']('icn')) {
                    this['clients'][_0x3d81x20]['icon'] = _0x3d81x11['spec'][_0x3d81x14]['icn']
                }
            };
            if (_0x3d81x11['spec'][_0x3d81x14]['hasOwnProperty']('col')) {
                this['clients'][_0x3d81x20]['color'] = _0x3d81x11['spec'][_0x3d81x14]['col']
            };
            if (_0x3d81x13 != 0) {
                _0x3d81x17d += ', '
            };
            _0x3d81x17d += this['getName'](_0x3d81x20);
            _0x3d81x13++
        }
    };
    this['showInChat']('', '<em>' + i18n['watching'] + ': ' + _0x3d81x17d + '</em>')
};
Live['prototype']['chatJoiningInfoEnabled'] = function(_0x3d81x20) {
    return (this['players']['length'] < 8 || (this['players']['length'] < 20 && arrayContains(this['authList'], _0x3d81x20)) || this['clients'][_0x3d81x20]['type'] >= 100)
};
Live['prototype']['showMessageAboutJoin'] = function(_0x3d81x20, _0x3d81x17e) {
    if (typeof this['clients'][_0x3d81x20] !== 'undefined' && this['clients'][_0x3d81x20]['name'] !== null) {
        if (this['chatJoiningInfoEnabled'](_0x3d81x20)) {
            var _0x3d81x147 = '<em>' + this['getName'](_0x3d81x20) + ' ';
            _0x3d81x147 += (_0x3d81x17e) ? i18n['userCame'] + '.</em>' : i18n['userJoined'] + '.</em>';
            this['showInChat']('', _0x3d81x147);
            if (this['p']['Settings']['soundEnabled']) {
                createjs['Sound']['play']('ding')
            };
            this['p']['pageTitle'](this['getName'](_0x3d81x20, false) + ' ' + i18n['joined'] + '!');
            setTimeout(function() {
                    this['p']['pageTitle']('Jstris')
                }
                ['bind'](this), 1500)
        }
    } else {
        this['roomJoinTimes'][_0x3d81x20] = [];
        this['roomJoinTimes'][_0x3d81x20][0] = this['p']['timestamp']();
        this['roomJoinTimes'][_0x3d81x20][1] = _0x3d81x17e
    }
};
Live['prototype']['updateConnectionInfo'] = function() {
    var _0x3d81x17f = this['isProxy'] ? ' [P]' : '';
    var _0x3d81x180 = '';
    if (!this['connected']) {
        _0x3d81x180 = (this['cid'] === 0) ? ' [F]' : ' [DC]'
    };
    var _0x3d81x181 = this['connected'] ? i18n['connected'] : i18n['notConnected'];
    var _0x3d81x182 = '';
    if (this['serverId']) {
        _0x3d81x182 = 's:' + this['serverId'] + ', '
    };
    this['p']['connectStatusElement']['innerHTML'] = _0x3d81x181 + _0x3d81x17f + _0x3d81x180 + ' (client ' + this['cid'] + '), ' + _0x3d81x182 + this['version']
};
Live['prototype']['handleResponse'] = function(_0x3d81x11) {
    switch (_0x3d81x11['t']) {
        case 1:
            this['cid'] = _0x3d81x11['cid'];
            this['serverId'] = _0x3d81x11['s'];
            if (typeof this['clients'][this['cid']] === 'undefined') {
                this['clients'][this['cid']] = new Client(this['cid'])
            };
            if (!this['authorized']) {
                if (_0x3d81x11['n']['i'] >= 0) {
                    this['p']['Settings']['setCookie']('nick', _0x3d81x11['n']['i'])
                };
                this['setChatName'](_0x3d81x11['n']['n'])
            };
            if (_0x3d81x11['hasOwnProperty']('r')) {
                this['servers'] = _0x3d81x11['r'];
                let _0x3d81x183 = document['getElementById']('srvSel');
                _0x3d81x183['innerHTML'] = '';
                let _0x3d81x184 = document['createElement']('option');
                _0x3d81x184['value'] = '0';
                _0x3d81x184['selected'] = true;
                _0x3d81x184['textContent'] = 'Default';
                _0x3d81x183['appendChild'](_0x3d81x184);
                for (let _0x3d81x185 in this['servers']) {
                    let _0x3d81x184 = document['createElement']('option');
                    _0x3d81x184['value'] = _0x3d81x185;
                    _0x3d81x184['textContent'] = this['servers'][_0x3d81x185]['n'];
                    _0x3d81x183['appendChild'](_0x3d81x184)
                }
            };
            this['connected'] = true;
            this['onCIDassigned']();
            this['updateConnectionInfo']();
            break;
        case 2:
            if (typeof this['clients'][_0x3d81x11['cid']] === 'undefined') {
                this['clients'][_0x3d81x11['cid']] = new Client(_0x3d81x11['cid'])
            };
            if (_0x3d81x11['hasOwnProperty']('type')) {
                this['clients'][_0x3d81x11['cid']]['type'] = _0x3d81x11['type'];
                if (_0x3d81x11['hasOwnProperty']('icn')) {
                    this['clients'][_0x3d81x11['cid']]['icon'] = _0x3d81x11['icn']
                }
            };
            if (_0x3d81x11['hasOwnProperty']('col')) {
                this['clients'][_0x3d81x11['cid']]['color'] = _0x3d81x11['col']
            };
            if (_0x3d81x11['hasOwnProperty']('team')) {
                this['clients'][_0x3d81x11['cid']]['team'] = _0x3d81x11['team']
            };
            if (typeof _0x3d81x11['n'] !== 'undefined') {
                this['clients'][_0x3d81x11['cid']]['name'] = _0x3d81x11['n']
            };
            if (_0x3d81x11['hasOwnProperty']('auth') && _0x3d81x11['auth'] === true) {
                this['authList']['push'](_0x3d81x11['cid']);
                this['clients'][_0x3d81x11['cid']]['auth'] = true
            };
            var _0x3d81x17e = _0x3d81x11['hasOwnProperty']('so') && _0x3d81x11['so'] === true;
            if (!_0x3d81x17e) {
                var _0x3d81x186 = this['getGameSlot'](_0x3d81x11['cid'], _0x3d81x11['rc'], _0x3d81x11['team']);
                this['p']['GS'].CID(_0x3d81x11['cid'])['setName'](this['getName'](_0x3d81x11['cid']));
                if (_0x3d81x11['hasOwnProperty']('bot') && _0x3d81x11['bot'] === true) {
                    this['bots']['push'](_0x3d81x11['cid'])
                };
                if (!_0x3d81x186 && (this['p']['Settings']['rescaleNow'] || !this['LiveGameRunning'])) {
                    this['p']['GS']['autoScale']()
                }
            };
            this['showMessageAboutJoin'](_0x3d81x11['cid'], _0x3d81x17e);
            break;
        case 3:
            if (_0x3d81x11['cid'] === this['cid'] && _0x3d81x11['sitout'] === 1) {
                if (!this['sitout']) {
                    this['spectatorMode'](1)
                };
                break
            };
            if (_0x3d81x11['sitout'] <= 1) {
                var _0x3d81xf;
                if (this['liveMode'] === 2 && this['clients'][_0x3d81x11['cid']]['hasOwnProperty']('team')) {
                    var _0x3d81x187 = this['clients'][_0x3d81x11['cid']]['team'];
                    _0x3d81xf = this['p']['GS']['teamMembers'][_0x3d81x187]['indexOf'](_0x3d81x11['cid']);
                    this['p']['GS']['teamMembers'][_0x3d81x187]['splice'](_0x3d81xf, 1)
                };
                _0x3d81xf = this['players']['indexOf'](_0x3d81x11['cid']);
                if (_0x3d81xf > -1) {
                    this['players']['splice'](_0x3d81xf, 1)
                };
                _0x3d81xf = this['bots']['indexOf'](_0x3d81x11['cid']);
                if (_0x3d81xf > -1) {
                    this['bots']['splice'](_0x3d81xf, 1)
                };
                this['p']['GS'].CID(_0x3d81x11['cid'])['vacantClear']();
                if (!this['p']['gameEnded'] && this['currentTarget'] === _0x3d81x11['cid']) {
                    this['changeTarget']()
                }
            } else {
                if (_0x3d81x11['sitout'] === 2 && this['chatJoiningInfoEnabled'](_0x3d81x11['cid'])) {
                    this['showInChat']('', '<em>' + i18n['spectator'] + ' ' + this['getName'](_0x3d81x11['cid']) + ' ' + i18n['hasLeft'] + '.</em>')
                }
            };
            if (_0x3d81x11['sitout'] === 1 && this['chatJoiningInfoEnabled'](_0x3d81x11['cid'])) {
                this['showInChat']('', '<em>' + this['getName'](_0x3d81x11['cid']) + ' ' + i18n['isSpectating'] + '.</em>')
            };
            if (_0x3d81x11['sitout'] === 0 || _0x3d81x11['sitout'] === 2) {
                delete this['authList'][_0x3d81x11['cid']];
                delete this['clients'][_0x3d81x11['cid']]
            };
            break;
        case 4:
            this['forgetRoomPlayers']();
            if (objSize(_0x3d81x11['spec']) > 0) {
                this['readSpecs'](_0x3d81x11, true)
            };
            if (objSize(_0x3d81x11['players']) > 0) {
                for (var _0x3d81x14 in _0x3d81x11['players']) {
                    var _0x3d81x20 = parseInt(_0x3d81x14);
                    if (!isNaN(_0x3d81x20)) {
                        this['getGameSlot'](_0x3d81x20, _0x3d81x11['players'][_0x3d81x14]['rc'], _0x3d81x11['players'][_0x3d81x14]['team']);
                        if (typeof this['clients'][_0x3d81x20] === 'undefined') {
                            this['clients'][_0x3d81x20] = new Client(_0x3d81x20)
                        };
                        if (typeof _0x3d81x11['players'][_0x3d81x14]['n'] !== 'undefined') {
                            this['clients'][_0x3d81x20]['name'] = _0x3d81x11['players'][_0x3d81x14]['n']
                        };
                        if (_0x3d81x11['players'][_0x3d81x14]['hasOwnProperty']('bot') && _0x3d81x11['players'][_0x3d81x14]['bot'] === true) {
                            this['bots']['push'](_0x3d81x20)
                        };
                        if (_0x3d81x11['players'][_0x3d81x14]['hasOwnProperty']('auth') && _0x3d81x11['players'][_0x3d81x14]['auth'] === true) {
                            this['clients'][_0x3d81x20]['auth'] = true;
                            this['authList']['push'](_0x3d81x20)
                        };
                        if (_0x3d81x11['players'][_0x3d81x14]['hasOwnProperty']('type')) {
                            this['clients'][_0x3d81x20]['type'] = _0x3d81x11['players'][_0x3d81x14]['type'];
                            if (_0x3d81x11['players'][_0x3d81x14]['hasOwnProperty']('icn')) {
                                this['clients'][_0x3d81x20]['icon'] = _0x3d81x11['players'][_0x3d81x14]['icn']
                            }
                        };
                        if (_0x3d81x11['players'][_0x3d81x14]['hasOwnProperty']('col')) {
                            this['clients'][_0x3d81x20]['color'] = _0x3d81x11['players'][_0x3d81x14]['col']
                        };
                        if (_0x3d81x11['players'][_0x3d81x14]['hasOwnProperty']('team')) {
                            this['clients'][_0x3d81x20]['team'] = _0x3d81x11['players'][_0x3d81x14]['team']
                        };
                        this['p']['GS'].CID(_0x3d81x20)['setName'](this['getName'](_0x3d81x20))
                    }
                };
                this['p']['play'] = false;
                this['onGameEnd']();
                this['LiveGameRunning'] = (_0x3d81x11['play'] == 'True') ? true : false;
                if (this['LiveGameRunning']) {
                    if (!this['sitout']) {
                        this['showInChat']('', '<em>' + i18n['waitNext2'] + '</em>')
                    };
                    this['p']['paintMatrixWithColor'](9);
                    this['p']['getPlace'](true, true)
                };
                this['setResetButton'](_0x3d81x11['ss'] || this['LiveGameRunning']);
                this['p']['GS']['autoScale']()
            } else {
                var _0x3d81x188 = 6;
                if (this['roomConfig'] && this['roomConfig']['hasOwnProperty']('max') && this['roomConfig']['max'] <= 7) {
                    _0x3d81x188 = Math['min'](1, this['roomConfig']['max'] - 1)
                };
                this['p']['GS']['setup'](_0x3d81x188);
                this['setResetButton'](false)
            };
            this['gid'] = _0x3d81x11['gid'];
            this['liveSeed'] = this['p']['getAdjustedLiveSeed'](_0x3d81x11['seed']);
            break;
        case 5:
            if (this['shouldWait'](_0x3d81x11)) {
                return
            };
            this['notPlaying'] = [];
            this['p']['transmitMode'] = _0x3d81x11['trM'];
            this['p']['GS']['extendedAvailable'] = (_0x3d81x11['trM'] === 1);
            if (this['p']['transmitMode'] === 0) {
                for (var _0x3d81x13 = 0; _0x3d81x13 < this['p']['GS']['shownSlots']; _0x3d81x13++) {
                    this['p']['GS']['slots'][_0x3d81x13]['v']['changeSkin'](this['p']['skinId'])
                }
            };
            this['p']['GS']['autoScale']();
            this['liveSeed'] = this['p']['getAdjustedLiveSeed'](_0x3d81x11['seed']);
            if (_0x3d81x11['hasOwnProperty']('map')) {
                this['liveMap'] = _0x3d81x11['map']['id'];
                this['showInChat']('Map', '<a target=\'_blank\' href=\'/map/' + this['liveMap'] + '\'>' + _0x3d81x11['map']['name'] + '</a> (D=' + _0x3d81x11['map']['diff'] + '%)')
            } else {
                this['liveMap'] = null
            };
            if (!this['sitout']) {
                this['p']['pmode'] = 0;
                this['p']['play'] = false;
                if (this['p']['starting']) {
                    clearInterval(this['p']['interval']);
                    this['p']['starting'] = false
                };
                this['p']['blockRNG'] = alea(this['liveSeed']);
                this['p']['RNG'] = alea(this['liveSeed']);
                this['p']['blockSeed'] = this['liveSeed'];
                this['p']['readyGo']();
                this['statsSent'] = false;
                this['setResetProgress'](0);
                this['setResetButton'](true);
                this['LiveGameRunning'] = true
            } else {
                if (this['liveMap']) {
                    this['p']['GS']['extendedAvailable'] = false
                };
                this['onReset']()
            };
            if (this['liveMode'] === 2 && typeof ga === 'function') {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Team',
                    eventAction: 'start'
                })
            };
            this['gid'] = _0x3d81x11['gid'];
            break;
        case 6:
            if (_0x3d81x11['hasOwnProperty']('e') && !this['hasOwnProperty']('chatErrShown')) {
                this['chatErrShown'] = true;
                var _0x3d81x189 = '<span style="color:#ff9100"><b>' + i18n['oops'] + '</b> ' + trans(i18n['chatNA'], {
                    chReq: 2
                }) + '</span>';
                _0x3d81x189 += ' <a href="/about/chat" target="_blank">' + i18n['leMore'] + '</a>';
                this['showInChat']('', _0x3d81x189);
                break
            };
            _0x3d81x11['m'] = _0x3d81x11['m']['replace'](/\\"/g, '"');
            if (this['authorized']) {
                _0x3d81x11['m'] = this['resolveMention'](_0x3d81x11)
            };
            if (_0x3d81x11['a']) {
                switch (_0x3d81x11['a']) {
                    case 1:
                        this['resetWinCounter']()
                }
            };
            let _0x3d81x18a = this['getChatLineClassesFor'](_0x3d81x11['cid']);
            if (_0x3d81x11['hasOwnProperty']('cl') && _0x3d81x11['cl'] !== null && _0x3d81x11['cl']['length']) {
                if (!_0x3d81x18a || _0x3d81x18a['length'] === 0) {
                    _0x3d81x18a = _0x3d81x11['cl']
                } else {
                    Array['prototype']['push']['apply'](_0x3d81x18a, _0x3d81x11['cl'])
                }
            };
            if (this['Friends']['friendsOpened'] && _0x3d81x11['cid'] !== this['cid'] && _0x3d81x11['cid'] > 0) {
                this['friendsBtn']['className'] = 'rchNew'
            };
            this['showInChat'](this['getName'](_0x3d81x11['cid']), _0x3d81x11['m'], _0x3d81x18a);
            break;
        case 7:
            if (_0x3d81x11['cid'] === this['cid']) {
                if (!this['LiveGameRunning']) {
                    break
                };
                if (this['liveMode'] === 3) {
                    this['p']['Caption']['liveRaceFinished']();
                    break
                };
                this['p']['paintMatrixWithColor'](9);
                this['p']['play'] = false;
                this['p']['lastSeen'] = null;
                this['onGameEnd']();
                this['statsSent'] = true;
                this['p']['getPlace'](true, true);
                this['p']['redraw']()
            } else {
                if (this['p']['GS']['cidSlots']['hasOwnProperty'](_0x3d81x11['cid'])) {
                    this['notPlaying']['push'](_0x3d81x11['cid']);
                    if (!this['p']['gameEnded'] && this['currentTarget'] === _0x3d81x11['cid']) {
                        this['changeTarget']()
                    };
                    var _0x3d81x18b = this['p']['GS'].CID(_0x3d81x11['cid']);
                    _0x3d81x18b['slotDiv']['classList']['add']('np');
                    if (_0x3d81x11['v']) {
                        this['places'][_0x3d81x11['cid']] = _0x3d81x11['p']
                    } else {
                        this['places'][_0x3d81x11['cid']] = 0
                    };
                    _0x3d81x18b['v']['afterRedraw']()
                }
            };
            break;
        case 8:
            if (typeof this['clients'][_0x3d81x11['cid']] === 'undefined') {
                this['clients'][_0x3d81x11['cid']] = new Client(_0x3d81x11['cid'])
            };
            this['clients'][_0x3d81x11['cid']]['name'] = _0x3d81x11['n'];
            if (_0x3d81x11['cid'] === this['cid'] && _0x3d81x11['hasOwnProperty']('ti')) {
                this['sTier'] = _0x3d81x11['ti']
            };
            var _0x3d81x25 = this;
            this['emoteAutocomplete']['onEmoteObjectReady'] = function(_0x3d81x145) {
                _0x3d81x25['initEmoteSelect'](_0x3d81x145)
            };
            this['emoteAutocomplete']['loadEmotesIndex'](this['version'] + '.t' + this['sTier']);
            if (this['sTier'] < 2 && this['p']['skinId'] >= 1000) {
                this['p']['changeSkin'](0)
            };
            if (_0x3d81x11['hasOwnProperty']('type')) {
                this['clients'][_0x3d81x11['cid']]['type'] = _0x3d81x11['type'];
                if (_0x3d81x11['hasOwnProperty']('icn')) {
                    this['clients'][_0x3d81x11['cid']]['icon'] = _0x3d81x11['icn']
                }
            };
            if (_0x3d81x11['hasOwnProperty']('col')) {
                this['clients'][_0x3d81x11['cid']]['color'] = _0x3d81x11['col']
            };
            if (_0x3d81x11['hasOwnProperty']('auth') && _0x3d81x11['auth'] === true) {
                this['clients'][_0x3d81x11['cid']]['auth'] = true;
                this['authList']['push'](parseInt(_0x3d81x11['cid']))
            };
            if (_0x3d81x11['cid'] in this['p']['GS']['cidSlots']) {
                this['p']['GS'].CID(_0x3d81x11['cid'])['setName'](this['getName'](_0x3d81x11['cid']))
            };
            if (typeof this['roomJoinTimes'][_0x3d81x11['cid']] !== 'undefined') {
                if (this['roomJoinTimes'][_0x3d81x11['cid']][0] >= this['p']['timestamp']() - 1000) {
                    this['showMessageAboutJoin'](_0x3d81x11['cid'], this['roomJoinTimes'][_0x3d81x11['cid']][1])
                };
                delete this['roomJoinTimes'][_0x3d81x11['cid']]
            };
            break;
        case 9:
            if (!this['statsSent']) {
                if (this['liveMode'] === 2) {
                    this['p']['place'] = 1;
                    this['p']['placePrinted'] = true
                } else {
                    if (this['liveMode'] !== 3) {
                        this['p']['getPlace'](false, true)
                    }
                };
                this['sendStats']();
                this['winnerCID'] = this['cid'];
                this['onGameEnd']();
                if (typeof ga === 'function') {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Game',
                        eventAction: 'victory'
                    })
                }
            } else {
                for (var _0x3d81x13 = 0; _0x3d81x13 < this['players']['length']; _0x3d81x13++) {
                    if (this['players'][_0x3d81x13] !== this['cid'] && this['notPlaying']['indexOf'](this['players'][_0x3d81x13]) === -1) {
                        this['winnerCID'] = this['players'][_0x3d81x13];
                        break
                    }
                }
            };
            this['places'][this['winnerCID']] = 1;
            if (this['winnerCID'] in this['p']['GS']['cidSlots']) {
                this['p']['GS'].CID(this['winnerCID'])['v']['afterRedraw']()
            };
            if (this['liveMode'] === 2) {
                this['displayTeamResults'](_0x3d81x11['r'])
            } else {
                this['displayResults'](_0x3d81x11['r']['res'], false)
            };
            this['lastGameId'] = _0x3d81x11['r']['gid'];
            this['saveLink']['href'] = '/games/' + this['lastGameId'];
            if (!this['sitout']) {
                this['setResetButton'](false);
                this['p']['redraw']()
            };
            this['LiveGameRunning'] = false;
            break;
        case 10:
            this['displayLobby'](_0x3d81x11['d']);
            break;
        case 12:
            this['rc'] = _0x3d81x11['rc'];
            if (this['rid'] !== '' && _0x3d81x11['re'] === 0) {
                this['chatBox']['textContent'] = '';
                if (this['hasOwnProperty']('chatErrShown')) {
                    delete this['chatErrShown']
                };
                this['showInChat']('', '<em>' + i18n['welcomeIn'] + ' ' + _0x3d81x11['n'] + '!</em>')
            };
            this['rid'] = _0x3d81x11['rid'];
            if (_0x3d81x11['hasOwnProperty']('so') && _0x3d81x11['so'] === 1) {
                this['spectatorMode'](1);
                this['showInChat']('', '<em>' + i18n['roomFull'] + '</em>')
            } else {
                if (this['sitout']) {
                    this['spectatorModeOff'](1)
                }
            };
            this['iAmHost'] = (_0x3d81x11['hasOwnProperty']('h') && _0x3d81x11['h'] === this['cid']);
            this['onRoomJoined'](_0x3d81x11['rid'], _0x3d81x11['p'], _0x3d81x11['n']);
            this['p']['GS']['extendedAvailable'] = false;
            this['liveMode'] = _0x3d81x11['conf']['mode'];
            for (var _0x3d81x18c in this['p']['GS']['teamMembers']) {
                delete this['p']['GS']['teamMembers'][_0x3d81x18c]
            };
            this['applyConfig'](_0x3d81x11);
            if (!this['sitout'] && _0x3d81x11['hasOwnProperty']('ss')) {
                this['setResetProgress'](_0x3d81x11['ss']);
                this['setResetButton'](true)
            } else {
                this['setResetProgress'](0)
            };
            break;
        case 13:
            if (this['p']['pmode'] || this['p']['gameEnded']) {
                return
            };
            this['p']['garbageQueue'](_0x3d81x11['a']);
            break;
        case 14:
            if (!_0x3d81x11['s']) {
                break
            };
            this['p']['Replay']['postLiveData'](_0x3d81x11['gid'], this['cid'], this);
            break;
        case 15:
            this['displayResults'](_0x3d81x11['r']['res'], true);
            break;
        case 16:
            if (objSize(_0x3d81x11['spec']) > 0) {
                this['readSpecs'](_0x3d81x11, false)
            } else {
                this['showInChat']('', '<em>' + i18n['noSpectators'] + '</em>')
            };
            break;
        case 17:
            if (!this['p']['gameEnded'] && !this['p']['pmode']) {
                this['p']['solidStartRaising']()
            };
            break;
        case 18:
            this['applyConfig'](_0x3d81x11);
            break;
        case 19:
            if (_0x3d81x11['id'] !== 0) {
                this['p']['Replay']['onSaved'] = this['onReplaySaved']['bind'](this);
                this['p']['Replay']['postData'](_0x3d81x11['id'], this)
            } else {
                this['p']['Replay']['uploadError'](this, 'REJECTED - ' + _0x3d81x11['err'])
            };
            break;
        case 20:
            if (_0x3d81x11['cid'] === this['cid']) {
                this['team'] = _0x3d81x11['team'];
                this['updateTeamData'](this['p']['GS']['teamData'])
            } else {
                if (typeof this['clients'][_0x3d81x11['cid']] !== 'undefined') {
                    var _0x3d81x18d = this['clients'][_0x3d81x11['cid']]['team'];
                    this['clients'][_0x3d81x11['cid']]['team'] = _0x3d81x11['team'];
                    var _0x3d81xf = this['p']['GS']['teamMembers'][_0x3d81x18d]['indexOf'](_0x3d81x11['cid']);
                    this['p']['GS']['teamMembers'][_0x3d81x18d]['splice'](_0x3d81xf, 1);
                    if (_0x3d81x11['team'] in this['p']['GS']['teamMembers']) {
                        this['p']['GS']['teamMembers'][_0x3d81x11['team']]['push'](_0x3d81x11['cid'])
                    };
                    this['p']['GS']['autoScale']()
                }
            };
            break;
        case 21:
            break;
        case 23:
            var _0x3d81x20 = this['rcS'][_0x3d81x11['rc']];
            if (!(_0x3d81x20 in this['p']['GS']['cidSlots'])) {
                break
            };
            var _0x3d81x18e = new Replayer(this['p']['GS'].CID(_0x3d81x20)['v']);
            _0x3d81x18e['debug'] = this['p']['debug'];
            this['clients'][_0x3d81x20]['rep'] = _0x3d81x18e;
            _0x3d81x11['v'] = 3.3;
            _0x3d81x11['se'] = 0;
            _0x3d81x11['m'] = 6553600;
            _0x3d81x11['seed'] = this['liveSeed'];
            _0x3d81x18e['r']['c'] = _0x3d81x11;
            _0x3d81x18e['initReplay']();
            if (this['loadMapForOpponents']) {
                _0x3d81x18e['loadMap'](this['p']['MapManager']['matrix'], this['p']['MapManager']['mapData']['queue']);
                _0x3d81x18e['pmode'] = 6
            };
            _0x3d81x18e['restart']();
            if (_0x3d81x11['bp']) {
                this['p']['GS'].CID(_0x3d81x20)['v']['customSkinPath'] = _0x3d81x11['bp']
            };
            if (!_0x3d81x11['bp'] && _0x3d81x11['bs'] >= 1000) {
                _0x3d81x11['bs'] = 0
            };
            this['p']['GS'].CID(_0x3d81x20)['v']['changeSkin'](_0x3d81x11['bs'], _0x3d81x11['mClr']);
            break;
        case 25:
            if (_0x3d81x11['cid'] in this['clients']) {
                this['clients'][_0x3d81x11['cid']]['rep'] = null;
                if (_0x3d81x11['cid'] in this['p']['GS']['cidSlots']) {
                    this['p']['GS'].CID(_0x3d81x11['cid'])['v']['onReady']()
                }
            };
            break;
        case 26:
            this['p']['Caption']['hide'](this['p']['Caption'].LOADING);
            this['authReady'] = true;
            this['createPrivatePracticeRoom']();
            if (_0x3d81x11['res']) {
                this['Friends']['friendsCount'] = (_0x3d81x11['hasOwnProperty']('f')) ? _0x3d81x11['f'] : 0;
                if (_0x3d81x11['hasOwnProperty']('mod') && _0x3d81x11['mod']) {
                    this['clients'][this['cid']]['mod'] = true
                };
                this['showInChat']('', i18n['signedAs'] + ' ' + this['getName'](this['cid']) + '.')
            } else {
                this['authorized'] = false;
                this['setChatName'](_0x3d81x11['n']['n']);
                this['p']['Caption']['loading'](i18n['loginFail'], 1);
                let _0x3d81x147 = trans(i18n['loginFail2'], {
                    name: '<b>' + _0x3d81x11['n']['n'] + '</b>'
                });
                this['chatMajorWarning'](_0x3d81x147, 'dc')
            };
            break;
        case 27:
            this['roomHost'] = _0x3d81x11['h'];
            if (_0x3d81x11['h'] === this['cid']) {
                this['showInChat']('', ((this['authorized']) ? '<span class="mention">@' + this['chatName'] + '</span> ' : '') + '<em>' + i18n['newHost'] + '</em>');
                this['iAmHost'] = true
            };
            break;
        case 28:
            this['RoomInfo']['acceptRoomDetail'](_0x3d81x11);
            break;
        case 29:
            this['p']['Replay']['getData']();
            var _0x3d81x18f = this['p']['Replay']['string'];
            if (_0x3d81x18f && this['p']['Replay']['config']['seed']['startsWith'](_0x3d81x11['gid'])) {
                _0x3d81x11['r'] = _0x3d81x18f
            } else {
                _0x3d81x11['r'] = 0
            };
            this['safeSend'](JSON['stringify'](_0x3d81x11));
            break;
        case 30:
            this['joinRemote'] = _0x3d81x11;
            break;
        case 31:
            if (!this['p']['Items']['avail']()) {
                return
            };
            if (_0x3d81x11['hasOwnProperty']('c')) {
                this['p']['Items']['loadConf'](_0x3d81x11['c'])
            };
            if (_0x3d81x11['hasOwnProperty']('s')) {
                this['p']['Items']['fs'] = true;
                this['p']['Items']['f'] = _0x3d81x11['s']
            };
            if (_0x3d81x11['hasOwnProperty']('a')) {
                this['p']['Items']['item'] = _0x3d81x11['a'];
                this['p']['Items']['use']()
            };
            break;
        case 32:
            let _0x3d81x190 = _0x3d81x11['j'];
            let _0x3d81x191 = _0x3d81x11['s'];
            this['Friends']['connect'](_0x3d81x191, _0x3d81x190);
            break;
        case 33:
            console['log'](_0x3d81x11);
            if (_0x3d81x11['hasOwnProperty']('e')) {
                if (_0x3d81x11['e'] === 1) {
                    this['showInChat']('<span style=\'color:green\'>Completed</span>', 'Not saved, you already completed this mode.')
                };
                return
            };
            const _0x3d81x192 = (_0x3d81x185) => {
                if (typeof _0x3d81x185 !== 'string') {
                    return ''
                };
                return _0x3d81x185['charAt'](0)['toUpperCase']() + _0x3d81x185['slice'](1)
            };
            if (!_0x3d81x11['hasOwnProperty']('f')) {
                let _0x3d81x12 = '<b>' + _0x3d81x11['r'][0]['toFixed'](2) + '</b> ' + _0x3d81x192(_0x3d81x11['r'][1]);
                if (_0x3d81x11['r'][3] != 'none') {
                    _0x3d81x12 += ', <b>' + _0x3d81x11['r'][2]['toFixed'](2) + '</b> ' + _0x3d81x192(_0x3d81x11['r'][3])
                };
                this['showInChat']('<span style=\'color:green\'>Saved</span>', '<a href="/usermodes/' + _0x3d81x11['mid'] + '" target="_blank">' + _0x3d81x11['n'] + '</a>: ' + _0x3d81x12)
            } else {
                this['showInChat']('<span style=\'color:red\'>Skipped</span>', '<a href="/usermodes/' + _0x3d81x11['mid'] + '" target="_blank">' + _0x3d81x11['n'] + '</a>')
            };
            if (_0x3d81x11['rr']) {
                let _0x3d81x5d = _0x3d81x11['rr']['newRating'] - _0x3d81x11['rr']['oldRating'];
                let _0x3d81x193 = ((_0x3d81x5d > 0) ? '+' : '') + Math['round'](_0x3d81x5d)['toFixed'](0);
                let _0x3d81x194 = _0x3d81x11['rr']['mapRating'];
                let _0x3d81x195 = '';
                if (_0x3d81x11['rr']['so'] === false) {
                    _0x3d81x195 = '. Used ' + _0x3d81x11['rr']['res'][0] + ' of ' + _0x3d81x11['rr']['res'][1] + ' restarts!'
                };
                let _0x3d81x196 = '<span style=\'color:' + ((_0x3d81x5d >= 0) ? '#00ff00' : '#ff7400') + '\'>' + _0x3d81x193 + '</span>';
                this['showInChat']('Rank', _0x3d81x196 + ', new: <b>' + _0x3d81x11['rr']['newRating']['toFixed'](0) + '</b>, quiz rating: ' + _0x3d81x194['toFixed'](0) + _0x3d81x195)
            };
            break;
        case 97:
            this['showModListOfRooms'](_0x3d81x11['data']);
            break;
        case 98:
            this['socket']['close']();
            break;
        case 99:
            this['showInChat']('', '<b>' + (this['p']['timestamp']() - this['pingSent']) + 'ms</b>');
            break;
        case 100:
            alert(i18n['oldVer'] + '\x0A If you continue, your game will be unstable.');
            this['chatBox']['textContent'] = '';
            this['showInChat']('<span style=\'color:yellow\'>' + i18n['warning2'] + '</span>', trans(i18n['oldVer2'], {
                key: '<b>CTRL+F5</b>'
            }) + '!!!');
            break;
        case 101:
            alert(i18n['badRoom']);
            this['joinRoom']('2');
            break;
        default:
            console['log']('Unknown')
    }
};
Live['prototype']['applyConfig'] = function(_0x3d81x11) {
    this['roomHost'] = _0x3d81x11['h'];
    this['roomConfig'] = _0x3d81x11['conf'];
    if (_0x3d81x11['hasOwnProperty']('edit')) {
        this['showInChat']('', '<b>' + i18n['stngsChanged'] + ' (' + this['getName'](_0x3d81x11['edit']) + ').</b>')
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('ct') && _0x3d81x11['conf']['hasOwnProperty']('at')) {
        this['p']['linesAttack'] = _0x3d81x11['conf']['at'];
        this['p']['comboAttack'] = _0x3d81x11['conf']['ct']
    } else {
        _0x3d81x11['conf']['at'] = this['p']['linesAttack'] = this['p']['linesAttackDef'];
        _0x3d81x11['conf']['ct'] = this['p']['comboAttack'] = this['p']['comboAttackDef']
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('asEx')) {
        this['p']['excludedBlocksAS'] = _0x3d81x11['conf']['asEx']['split'](',')
    } else {
        _0x3d81x11['conf']['asEx'] = this['p']['excludedBlocksAS'] = null
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('gh')) {
        this['p']['cheeseHeight'] = _0x3d81x11['conf']['gh']
    } else {
        _0x3d81x11['conf']['gh'] = this['p']['cheeseHeight'] = 10
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('sg')) {
        this['solidAfter'] = _0x3d81x11['conf']['sg']
    } else {
        this['solidAfter'] = _0x3d81x11['conf']['sg'] = 120
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('gdm')) {
        this['gdm'] = _0x3d81x11['conf']['gdm'];
        if (this['gdm'] !== 0) {
            this['p']['GS']['setTarget'](-1);
            this['currentTarget'] = 0
        }
    };
    this['p']['RulesetManager']['applyRule'](_0x3d81x11['conf'], this['p']['conf'][0]);
    if (_0x3d81x11['conf']['hasOwnProperty']('hostStart') && _0x3d81x11['conf']['hostStart']) {
        this['hostStartMode'] = true
    } else {
        this['hostStartMode'] = _0x3d81x11['conf']['hostStart'] = false
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('noFW') && _0x3d81x11['conf']['noFW']) {
        this['noFourWide'] = true
    } else {
        this['noFourWide'] = _0x3d81x11['conf']['noFW'] = false
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('bi')) {
        this['p']['bigChance'] = _0x3d81x11['conf']['bi'];
        if (_0x3d81x11['conf']['bi'] === 0) {
            this['p']['conf'][0]['baseBlockSet'] = 1
        } else {
            this['p']['conf'][0]['baseBlockSet'] = 0
        }
    } else {
        this['p']['bigChance'] = 100000000
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('xb')) {
        this['setXbuffer'](_0x3d81x11['conf']['xb'], false)
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('snr')) {
        this['p']['snapRate'] = _0x3d81x11['conf']['snr']
    } else {
        this['p']['snapRate'] = 1000
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('lsnr')) {
        this['p']['liveSnapRate'] = 100 * _0x3d81x11['conf']['lsnr']
    } else {
        this['p']['liveSnapRate'] = 100
    };
    if (_0x3d81x11['conf']['hasOwnProperty']('sgp')) {
        this['sgProfile'] = _0x3d81x11['conf']['sgp']
    } else {
        this['sgProfile'] = 0
    };
    if (this['liveMode'] === 2) {
        if (_0x3d81x11['hasOwnProperty']('team')) {
            this['team'] = _0x3d81x11['team']
        };
        if (_0x3d81x11['hasOwnProperty']('teams')) {
            this['p']['GS']['updateTeamNames'](_0x3d81x11['teams']);
            this['updateTeamData'](_0x3d81x11['teams'])
        }
    };
    if (this['liveMode'] === 3) {
        if (_0x3d81x11['conf']['hasOwnProperty']('pmode')) {
            this['livePmodeTypes'] = _0x3d81x11['conf']['pmode']
        }
    }
};
Live['prototype']['updateTeamData'] = function(_0x3d81x197) {
    var _0x3d81x198;
    for (_0x3d81x198 in _0x3d81x197) {
        var _0x3d81x199;
        if (!(_0x3d81x198 in this['teamButtons'])) {
            _0x3d81x199 = document['createElement']('button');
            this['teamButtons'][_0x3d81x198] = _0x3d81x199;
            _0x3d81x199['dataset']['team'] = _0x3d81x198;
            _0x3d81x199['classList']['add']('teamSelect');
            _0x3d81x199['addEventListener']('click', (function(_0x3d81x2d) {
                this['teamSwitch'](_0x3d81x2d['target']['dataset']['team'])
            })['bind'](this), false);
            this['tsArea']['appendChild'](_0x3d81x199)
        };
        _0x3d81x199 = this['teamButtons'][_0x3d81x198];
        _0x3d81x199['textContent'] = _0x3d81x197[_0x3d81x198]['name'];
        _0x3d81x199['style']['backgroundColor'] = _0x3d81x197[_0x3d81x198]['color'];
        if (_0x3d81x198 === this['team']) {
            _0x3d81x199['disabled'] = true;
            this['myTeam']['textContent'] = _0x3d81x197[_0x3d81x198]['name'];
            this['myTeam']['style']['backgroundColor'] = _0x3d81x197[_0x3d81x198]['color']
        } else {
            _0x3d81x199['disabled'] = false
        }
    }
};
Live['prototype']['teamSwitch'] = function(_0x3d81x19a) {
    if (this['teamSwitchDisabled']) {
        return false
    };
    this['teamSwitchDisabled'] = true;
    setTimeout(function() {
            this['teamSwitchDisabled'] = false
        }
        ['bind'](this), 1000);
    for (var _0x3d81x198 in this['teamButtons']) {
        if (_0x3d81x198 === _0x3d81x19a) {
            this['teamButtons'][_0x3d81x198]['disabled'] = true
        } else {
            this['teamButtons'][_0x3d81x198]['disabled'] = false
        }
    };
    var _0x3d81x147 = '{"t":20,"team":"' + _0x3d81x19a + '"}';
    this['safeSend'](_0x3d81x147)
};
Live['prototype']['tryPlayParam'] = function() {
    var _0x3d81x17b = this['getParameterByName']('play');
    if (_0x3d81x17b !== '' && !this['urlPlayParamApplied']) {
        if (!this['authReady']) {
            return true
        };
        var _0x3d81x19b = parseInt(_0x3d81x17b);
        this['showInChat']('', '<em>' + i18n['privateRoom'] + ' ' + i18n['restartInfo'] + '</em>');
        this['toggleLobbby'](false);
        this['spectatorMode'](2);
        let _0x3d81x19c = this['getParameterByName']('rule');
        if (_0x3d81x19c) {
            this['p']['RulesetManager']['ruleSetChange'](_0x3d81x19c)
        };
        this['p']['sprintMode'] = parseInt(this['getParameterByName']('mode'));
        this['p']['pmode'] = -1;
        this['p']['startPractice'](_0x3d81x19b, isNaN(this['p']['sprintMode']));
        this['urlPlayParamApplied'] = true;
        return true
    };
    return false
};
Live['prototype']['onRoomJoined'] = function(_0x3d81x16e, _0x3d81x139, _0x3d81x170) {
    if (_0x3d81x16e === '') {
        this['showInChat']('', '<em><b>Room was not found!</b></em>')
    } else {
        if (_0x3d81x16e === '.') {
            this['showInChat']('', '<em><b>Room is full, please find another one in the lobby!</b></em>')
        } else {
            this['msgCount'] = 0;
            this['onReset']();
            this['p']['GS']['resetAll']();
            this['rcS'] = {};
            this['players'] = Array();
            this['bots'] = Array();
            this['authList'] = Array();
            this['notPlaying'] = [];
            this['p']['GS']['reset']();
            this['p']['Items']['isPriv'] = _0x3d81x139;
            if (_0x3d81x139) {
                if (!this['tryPlayParam']() && _0x3d81x16e['length'] < 15) {
                    var _0x3d81x19d = 'https://jstris.jezevec10.com/join/' + _0x3d81x16e;
                    this['showInChat']('', '<em>' + i18n['joinLinkInfo'] + '</em><span class=\'joinLink\' onClick=\'selectText(this)\'>' + _0x3d81x19d + '</span>')
                }
            } else {
                this['urlPlayParamApplied'] = false
            };
            if (this['rid'] != '2') {
                if (typeof ga === 'function') {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Game',
                        eventAction: 'customRoomJoin'
                    })
                }
            };
            this['Friends']['sendStatus'](_0x3d81x16e, _0x3d81x139, _0x3d81x170)
        }
    }
};
Live['prototype']['displayLobby'] = function(_0x3d81x12) {
    var _0x3d81x19e = [0, 'Sprint', 0, 'Cheese', 'Surv.', 0, 'Maps'];
    var _0x3d81x19f = this['rid'];
    var _0x3d81x1a0 = function(_0x3d81x12, _0x3d81xb) {
        _0x3d81xb = _0x3d81xb || 0;
        var _0x3d81x1a1 = '';
        var _0x3d81x1a2 = _0x3d81x12['length'];
        var _0x3d81x1a3 = function(_0x3d81xa5, _0x3d81x141) {
                return _0x3d81x141['g'] - _0x3d81xa5['g']
            },
            _0x3d81x1a4 = function(_0x3d81xa5, _0x3d81x141) {
                if (null == _0x3d81x141['w']) {
                    _0x3d81x141['w'] = 0
                };
                if (null == _0x3d81xa5['w']) {
                    _0x3d81xa5['w'] = 0
                };
                var _0x3d81x131 = _0x3d81xa5['w'] - _0x3d81x141['w'];
                if (_0x3d81x131 !== 0) {
                    return _0x3d81x131
                };
                return _0x3d81x141['g'] - _0x3d81xa5['g']
            },
            _0x3d81x1a5 = function(_0x3d81xa5, _0x3d81x141) {
                var _0x3d81xa5 = _0x3d81x141['tr'] - _0x3d81xa5['tr'];
                return (_0x3d81xa5 !== 0) ? _0x3d81xa5 : _0x3d81x141['g'] - _0x3d81xa5['g']
            };
        var _0x3d81x1a6 = _0x3d81x1a3;
        if (_0x3d81xb === 1) {
            _0x3d81x1a6 = _0x3d81x1a4
        } else {
            if (_0x3d81xb === 2) {
                _0x3d81x1a6 = _0x3d81x1a5
            }
        };
        _0x3d81x12['sort'](_0x3d81x1a6);
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x1a2; _0x3d81x13++) {
            var _0x3d81x1a7 = '',
                _0x3d81x90 = '',
                _0x3d81x1a8 = _0x3d81x12[_0x3d81x13]['c'].toString();
            if (_0x3d81x12[_0x3d81x13]['s'] > 0) {
                _0x3d81x1a7 = '<span class="plusSpec">+' + _0x3d81x12[_0x3d81x13]['s'] + '</span>'
            };
            var _0x3d81x1a9 = '',
                _0x3d81x1aa = '',
                _0x3d81x1ab = '';
            if (_0x3d81x12[_0x3d81x13]['w']) {
                _0x3d81x1ab = getSVG('s-customw', 'dark', 'loIc')
            };
            if (_0x3d81x12[_0x3d81x13]['mo'] === 0) {
                if (_0x3d81x12[_0x3d81x13]['l']) {
                    _0x3d81x1ab += getSVG('s-unlocked', 'dark', 'loIc')
                } else {
                    if (_0x3d81xb === 3) {
                        _0x3d81x1ab += getSVG('s-locked', 'dark', 'loIc')
                    }
                }
            };
            if (_0x3d81x12[_0x3d81x13]['mo'] > 0) {
                let _0x3d81x1ac, _0x3d81x1ad = [0, 'Cheese', 'Team'];
                switch (_0x3d81x12[_0x3d81x13]['mo']) {
                    case 3:
                        _0x3d81x1ac = _0x3d81x19e[_0x3d81x12[_0x3d81x13]['pm']];
                        break;
                    default:
                        _0x3d81x1ac = _0x3d81x1ad[_0x3d81x12[_0x3d81x13]['mo']]
                };
                _0x3d81x1a9 = '<div class="modeCol"><span class="gmTag">' + _0x3d81x1ac + '</span>' + _0x3d81x1ab + '</div>'
            } else {
                if (_0x3d81x12[_0x3d81x13]['sl'] >= 0.1 && (!_0x3d81x1ab || _0x3d81x12[_0x3d81x13]['id']['length'] === 1)) {
                    _0x3d81x1a9 = '<div class="modeCol">' + getSVG('s-speedlimit', 'dark', 'slIc') + '<span class="slVal">' + _0x3d81x12[_0x3d81x13]['sl']['toFixed'](0) + '</span><span class="ppsTag">PPS</span></div>'
                } else {
                    if (_0x3d81x1ab) {
                        _0x3d81x1a9 = '<div class="modeCol">' + _0x3d81x1ab + '</div>'
                    } else {}
                }
            };
            if (_0x3d81x12[_0x3d81x13]['m'] < 24) {
                _0x3d81x90 = '/' + _0x3d81x12[_0x3d81x13]['m']
            };
            if (_0x3d81x12[_0x3d81x13]['c'] === 0 && _0x3d81x12[_0x3d81x13]['s'] > 0) {
                _0x3d81x1a8 = ''
            };
            var _0x3d81x1ae = '',
                _0x3d81x1af = '';
            if (_0x3d81x19f === _0x3d81x12[_0x3d81x13]['id']) {
                _0x3d81x1af = ' myRoom'
            } else {
                _0x3d81x1ae = 'onclick="window.joinRoom(\'' + _0x3d81x12[_0x3d81x13]['id'] + '\')"'
            };
            _0x3d81x1a1 += '<tr data-id=' + _0x3d81x12[_0x3d81x13]['id'] + ' class="lobbyRow' + _0x3d81x1af + '"' + _0x3d81x1ae + '><td ' + _0x3d81x1aa + '>' + _0x3d81x12[_0x3d81x13]['n'] + _0x3d81x1a9 + '</td><td class=\'gamesCol\'>' + _0x3d81x12[_0x3d81x13]['g'] + '</td><td class=\'plCol\'>' + _0x3d81x1a8 + _0x3d81x1a7 + '</td><td class=\'pLimit\'>' + _0x3d81x90 + '</td></tr>'
        };
        return _0x3d81x1a1
    };
    var _0x3d81x1b0 = document['getElementById']('lobbyTable');
    _0x3d81x1b0['getElementsByTagName']('tbody')[0]['innerHTML'] = _0x3d81x1a0(_0x3d81x12['s'], 0);
    if (!_0x3d81x12['c']['length']) {
        hideElem(document['getElementById']('rLSep'))
    } else {
        showElem(document['getElementById']('rLSep'))
    };
    var _0x3d81x1b1 = document['getElementById']('customTable');
    _0x3d81x1b1['innerHTML'] = _0x3d81x1a0(_0x3d81x12['c'], 1);
    if (!_0x3d81x12['o']['length']) {
        hideElem(document['getElementById']('ovLSep'))
    } else {
        showElem(document['getElementById']('ovLSep'))
    };
    var _0x3d81x1b1 = document['getElementById']('ovfTable');
    _0x3d81x1b1['innerHTML'] = _0x3d81x1a0(_0x3d81x12['o'], 2);
    if (!_0x3d81x12['l'] || !_0x3d81x12['l']['length']) {
        hideElem(document['getElementById']('loLSep'))
    } else {
        showElem(document['getElementById']('loLSep'))
    };
    var _0x3d81x1b1 = document['getElementById']('loTable');
    _0x3d81x1b1['innerHTML'] = _0x3d81x1a0(_0x3d81x12['l'], 3);
    this['RoomInfo']['onLobbyRefresh']()
};
Live['prototype']['resetWinCounter'] = function() {
    for (let _0x3d81x13 = 0; _0x3d81x13 < this['p']['GS']['slots']['length']; _0x3d81x13++) {
        this['p']['GS']['slots'][_0x3d81x13]['stats']['winCounter']['textContent'] = '0'
    }
};
Live['prototype']['displayResults'] = function(_0x3d81x12, _0x3d81x1b2) {
    this['resultsBox']['style']['display'] = 'block';
    var _0x3d81x1b3 = '';
    var _0x3d81x1a2 = _0x3d81x12['length'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x1a2; _0x3d81x13++) {
        _0x3d81x12[_0x3d81x13]['forfeit'] = (_0x3d81x12[_0x3d81x13]['t'] < 0) ? true : false
    };
    _0x3d81x12['sort'](function(_0x3d81xa5, _0x3d81x141) {
        return parseFloat(_0x3d81x141['t']) - parseFloat(_0x3d81xa5['t'])
    });
    if (!_0x3d81x1b2) {
        _0x3d81x1b3 += '<table class="tstripes" width="100%"><tr><td></td><td><b>' + i18n['name'] + '</b></td><td><b>' + i18n['wins'] + '</b></td><td><b>' + i18n['time'] + '</b></td><td><b>' + i18n['received'] + '</b></td><td><b>' + i18n['sent'] + '</b></td></tr>';
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x1a2; _0x3d81x13++) {
            if (!_0x3d81x12[_0x3d81x13]['forfeit']) {
                _0x3d81x1b3 += '<tr><td><b>' + (_0x3d81x13 + 1) + '.</b></td><td>' + this['getName'](_0x3d81x12[_0x3d81x13]['c']) + '</td><td>' + _0x3d81x12[_0x3d81x13]['w'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['t'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['r'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['l'] + '</td></tr>'
            } else {
                _0x3d81x1b3 += '<tr><td><b>-</b></td><td>' + this['getName'](_0x3d81x12[_0x3d81x13]['c']) + '</td><td>' + _0x3d81x12[_0x3d81x13]['w'] + '</td><td>&infin;</td><td>' + _0x3d81x12[_0x3d81x13]['r'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['l'] + '</td></tr>'
            };
            let _0x3d81x18b = this['p']['GS']['cidSlots'][_0x3d81x12[_0x3d81x13]['c']];
            if (typeof _0x3d81x18b !== 'undefined') {
                this['p']['GS']['slots'][_0x3d81x18b]['stats']['winCounter']['textContent'] = _0x3d81x12[_0x3d81x13]['w']
            }
        };
        _0x3d81x1b3 += '</table>';
        this['resultsContent']['innerHTML'] = _0x3d81x1b3;
        this['p']['GS']['resultsShown'] = true;
        this['p']['GS']['resizeElements']()
    } else {
        _0x3d81x1b3 += '<table class="tstripes" width="100%"><tr><td></td><td width="100"><b>' + i18n['name'] + '</b></td><td><b>' + i18n['wins'] + '</b></td><td><b>' + i18n['time'] + '</b></td><td><b>' + i18n['rec'] + '</b></td><td><b>' + i18n['sent'] + '</b></td>                        <td><b>B2B</b></td><td><b>B2Bpm</b></td><td><b>' + i18n['blocks'] + '</b></td><td><b>APM</b></td><td><b>SPM</b></td><td><b>PPS</b></td><td><b>' + i18n['ren'] + '</b></td><td><b>' + i18n['rep'] + '</b></td></tr>';
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x1a2; _0x3d81x13++) {
            if (!_0x3d81x12[_0x3d81x13]['forfeit']) {
                var _0x3d81x1b4 = Math['round'](100 * _0x3d81x12[_0x3d81x13]['l'] / (_0x3d81x12[_0x3d81x13]['t'] / 60)) / 100;
                var _0x3d81x1b5 = Math['round'](100 * _0x3d81x12[_0x3d81x13]['a'] / (_0x3d81x12[_0x3d81x13]['t'] / 60)) / 100;
                var _0x3d81x1b6 = Math['round'](100 * _0x3d81x12[_0x3d81x13]['p'] / _0x3d81x12[_0x3d81x13]['t']) / 100;
                var _0x3d81x1b7 = Math['round'](100 * _0x3d81x12[_0x3d81x13]['B2B'] / (_0x3d81x12[_0x3d81x13]['t'] / 60)) / 100;
                var _0x3d81x1b8 = '-';
                if (_0x3d81x12[_0x3d81x13]['hasOwnProperty']('rep') && _0x3d81x12[_0x3d81x13]['rep'] !== 0) {
                    _0x3d81x1b8 = '<a href="/replay/live/' + _0x3d81x12[_0x3d81x13]['rep'] + '" target="_blank"><img height="16" src="' + CDN_URL('/res/play.png') + '"></a>'
                };
                _0x3d81x1b3 += '<tr><td><b>' + (_0x3d81x13 + 1) + '.</b></td><td>' + this['getName'](_0x3d81x12[_0x3d81x13]['c']) + '</td><td>' + _0x3d81x12[_0x3d81x13]['w'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['t'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['r'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['l'] + '</td>                    <td>' + _0x3d81x12[_0x3d81x13]['B2B'] + '</td><td>' + _0x3d81x1b7 + '</td><td>' + _0x3d81x12[_0x3d81x13]['p'] + '</td>                    <td>' + _0x3d81x1b5 + '</td><td>' + _0x3d81x1b4 + '</td><td>' + _0x3d81x1b6 + '</td><td>' + _0x3d81x12[_0x3d81x13]['mc'] + '</td><td>' + _0x3d81x1b8 + '</td></tr>'
            } else {
                _0x3d81x1b3 += '<tr><td><b>-</b></td><td>' + this['getName'](_0x3d81x12[_0x3d81x13]['c']) + '</td><td>' + _0x3d81x12[_0x3d81x13]['w'] + '</td><td>&infin;</td><td>' + _0x3d81x12[_0x3d81x13]['r'] + '</td><td>' + _0x3d81x12[_0x3d81x13]['l'] + '</td>                    <td>' + _0x3d81x12[_0x3d81x13]['B2B'] + '</td><td>-</td><td>-</td>                    <td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>'
            }
        };
        _0x3d81x1b3 += '</table>';
        this['moreData']['innerHTML'] = _0x3d81x1b3
    }
};
Live['prototype']['displayTeamResults'] = function(_0x3d81x11) {
    this['resultsBox']['style']['display'] = 'block';
    var _0x3d81x1b3 = '';
    var _0x3d81x12 = _0x3d81x11['res'];
    var _0x3d81x1a2 = _0x3d81x12['length'];
    var _0x3d81x25 = this;
    var _0x3d81x1b9 = function(_0x3d81x187) {
        var _0x3d81x131 = _0x3d81x25['p']['GS']['teamData'][_0x3d81x187]['name'];
        var _0x3d81xc0 = _0x3d81x25['p']['GS']['teamData'][_0x3d81x187]['color'];
        return '<span class="teamResTag" style="background-color:' + _0x3d81xc0 + ';">' + _0x3d81x131 + '</span>'
    };
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x1a2; _0x3d81x13++) {
        _0x3d81x12[_0x3d81x13]['forfeit'] = (_0x3d81x12[_0x3d81x13]['t'] < 0) ? true : false
    };
    _0x3d81x11['teams']['sort'](function(_0x3d81xa5, _0x3d81x141) {
        return _0x3d81xa5['pl'] - _0x3d81x141['pl']
    });
    _0x3d81x1b3 += '<table width="100%"><tr><td></td><td><b>' + i18n['name'] + '</b></td><td><b>' + i18n['wins'] + '</b></td><td><b>' + i18n['time'] + '</b></td><td><b>' + i18n['received'] + '</b></td><td><b>' + i18n['sent'] + '</b></td></tr>';
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x11['teams']['length']; _0x3d81x13++) {
        _0x3d81x1b3 += '<tr class=\'spaceBefore\'><td><b>' + (_0x3d81x13 + 1) + '.</b></td><td>' + _0x3d81x1b9(_0x3d81x11['teams'][_0x3d81x13]['team']) + '</td><td>' + _0x3d81x11['teams'][_0x3d81x13]['w'] + '</td><td>' + _0x3d81x11['teams'][_0x3d81x13]['t'] + '</td><td>' + _0x3d81x11['teams'][_0x3d81x13]['r'] + '</td><td>' + _0x3d81x11['teams'][_0x3d81x13]['l'] + '</td></tr>';
        var _0x3d81x1ba = [];
        for (var _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x1a2; _0x3d81x70++) {
            if (_0x3d81x12[_0x3d81x70]['team'] === _0x3d81x11['teams'][_0x3d81x13]['team']) {
                _0x3d81x1ba['push'](_0x3d81x12[_0x3d81x70])
            }
        };
        _0x3d81x1ba['sort'](function(_0x3d81xa5, _0x3d81x141) {
            var _0x3d81x128 = parseFloat(_0x3d81x141['t']) - parseFloat(_0x3d81xa5['t']);
            return _0x3d81x128 === 0 ? _0x3d81x141['l'] - _0x3d81xa5['l'] : _0x3d81x128
        });
        for (var _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x1ba['length']; _0x3d81x70++) {
            if (!_0x3d81x1ba[_0x3d81x70]['forfeit']) {
                _0x3d81x1b3 += '<tr><td><b></b></td><td>' + this['getName'](_0x3d81x1ba[_0x3d81x70]['c']) + '</td><td>' + _0x3d81x1ba[_0x3d81x70]['w'] + '</td><td>' + _0x3d81x1ba[_0x3d81x70]['t'] + '</td><td>' + _0x3d81x1ba[_0x3d81x70]['r'] + '</td><td>' + _0x3d81x1ba[_0x3d81x70]['l'] + '</td></tr>'
            } else {
                _0x3d81x1b3 += '<tr><td><b></b></td><td>' + this['getName'](_0x3d81x1ba[_0x3d81x70]['c']) + '</td><td>' + _0x3d81x1ba[_0x3d81x70]['w'] + '</td><td>&infin;</td><td>' + _0x3d81x1ba[_0x3d81x70]['r'] + '</td><td>' + _0x3d81x1ba[_0x3d81x70]['l'] + '</td></tr>'
            }
        }
    };
    _0x3d81x1b3 += '</table>';
    this['resultsContent']['innerHTML'] = _0x3d81x1b3;
    this['p']['GS']['resultsShown'] = true;
    this['p']['GS']['resizeElements']()
};
Live['prototype']['resolveMention'] = function(_0x3d81x11) {
    var _0x3d81x104 = _0x3d81x11['m']['split'](' '),
        _0x3d81x1bb = '@' + this['chatName'];
    var _0x3d81x1bc = false;
    for (var _0x3d81x13 in _0x3d81x104) {
        if (_0x3d81x104[_0x3d81x13] === _0x3d81x1bb) {
            _0x3d81x104[_0x3d81x13] = '<span class="mention">' + _0x3d81x1bb + '</span>';
            _0x3d81x1bc = true;
            break
        }
    };
    if (!_0x3d81x1bc) {
        return _0x3d81x11['m']
    } else {
        return _0x3d81x104['join'](' ')
    }
};
Live['prototype']['chatMajorWarning'] = function(_0x3d81xac, _0x3d81x18a, _0x3d81xce) {
    if (typeof _0x3d81xce === 'undefined') {
        _0x3d81xce = {
            closable: true
        }
    };
    if (this['Friends']['friendsOpened']) {
        this['Friends']['openFriends']()
    };
    var _0x3d81x1bd = document['createElement']('div');
    _0x3d81x1bd['classList']['add']('warnBox');
    if (typeof _0x3d81x18a !== 'undefined') {
        _0x3d81x1bd['classList']['add'](_0x3d81x18a)
    };
    _0x3d81x1bd['innerHTML'] = _0x3d81xac;
    if (_0x3d81xce['closable']) {
        var _0x3d81x1be = document['createElement']('a');
        _0x3d81x1be['innerHTML'] = '<img src="/res/darkClose.png" alt="CLOSE">';
        _0x3d81x1be['href'] = 'javascript:void(0)';
        _0x3d81x1be['classList']['add']('warnClose');
        _0x3d81x1be['addEventListener']('click', function(_0x3d81x2d) {
            _0x3d81x2d['target']['parentNode']['parentNode']['remove']()
        });
        _0x3d81x1bd['appendChild'](_0x3d81x1be)
    };
    this['showInChat']('', _0x3d81x1bd)
};
Live['prototype']['getChatLineClassesFor'] = function(_0x3d81x20) {
    if (_0x3d81x20 === -5) {
        return ['infoChl']
    };
    return null
};
Live['prototype']['showInChat'] = function(_0x3d81x170, _0x3d81x147, _0x3d81x18a) {
    var _0x3d81x1bf = (_0x3d81x170 === '') ? '' : '<b>' + _0x3d81x170 + '</b>: ';
    var _0x3d81x1c0 = (_0x3d81x170 === '') ? 'srv' : '';
    var _0x3d81x1bd = document['createElement']('div');
    _0x3d81x1bd['classList']['add']('chl');
    if (typeof _0x3d81x18a === 'object' && _0x3d81x18a !== null) {
        _0x3d81x1bd['classList']['add'](_0x3d81x18a)
    };
    if (_0x3d81x1c0) {
        _0x3d81x1bd['classList']['add'](_0x3d81x1c0)
    };
    if (_0x3d81x147 instanceof HTMLDivElement) {
        _0x3d81x1bd['appendChild'](_0x3d81x147)
    } else {
        _0x3d81x1bd['innerHTML'] = _0x3d81x1bf + _0x3d81x147
    };
    this['chatBox']['appendChild'](_0x3d81x1bd);
    this['clearOldChatIfNeeded']();
    if (!this['Friends']['friendsOpened']) {
        this['scrollOnMessage']()
    }
};
Live['prototype']['scrollOnMessage'] = function(_0x3d81x1c1) {
    if (this['chatAtBottom'] || _0x3d81x1c1) {
        var _0x3d81x25 = this;
        setTimeout(function() {
            _0x3d81x25['chatArea']['scrollTop'] = _0x3d81x25['chatArea']['scrollHeight']
        }, 0)
    } else {
        if (this['scrollDownChatBtn']) {
            this['scrollDownChatBtn']['style']['backgroundColor'] = 'yellow'
        }
    }
};
Live['prototype']['clearOldChatIfNeeded'] = function() {
    ++this['msgCount'];
    if (this['msgCount'] % 10 === 0) {
        var _0x3d81x1c2 = this['chatBox']['children'];
        if (_0x3d81x1c2['length'] > 120) {
            for (var _0x3d81x13 = 0; _0x3d81x13 < 10; _0x3d81x13++) {
                this['chatBox']['removeChild'](_0x3d81x1c2[_0x3d81x13])
            }
        }
    }
};
Live['prototype']['showInLobbyChat'] = function(_0x3d81x11) {
    var _0x3d81x170, _0x3d81x18a = '';
    if (!_0x3d81x11['hasOwnProperty']('n')) {
        _0x3d81x170 = (_0x3d81x11['hasOwnProperty']('cid') && _0x3d81x11['cid'] < 0) ? ('<b>' + this['getName'](_0x3d81x11['cid']) + ': </b>') : '';
        if (_0x3d81x11['cid'] === -5) {
            _0x3d81x170 = this['getName'](_0x3d81x11['cid']);
            _0x3d81x18a += ' infoChl'
        }
    } else {
        if (_0x3d81x11['hasOwnProperty']('d') && _0x3d81x11['d'] === 1) {
            _0x3d81x170 = '<b><a class="relUser" target="_blank" href="https://discord.gg/RcNFCZC"><img src="' + CDN_URL('/res/svg/disW.svg') + '"> ' + _0x3d81x11['n'] + '</a>: </b>'
        } else {
            _0x3d81x170 = '<b><a href="/u/' + _0x3d81x11['n'] + '" target="_blank">' + _0x3d81x11['n'] + '</a>: </b>';
            if (_0x3d81x11['n'] === 'Jstris') {
                _0x3d81x170 = '<b><a href="/about" target="_blank">' + _0x3d81x11['n'] + '</a>: </b>'
            }
        }
    };
    var _0x3d81x147 = _0x3d81x11['m'];
    this['chatBoxLobby']['innerHTML'] = this['chatBoxLobby']['innerHTML'] + '<div class=\'chl' + _0x3d81x18a + '\'>' + _0x3d81x170 + _0x3d81x147 + '</div>';
    this['chatArea']['scrollTop'] = this['chatArea']['scrollHeight']
};
Live['prototype']['getGameSlot'] = function(_0x3d81x20, _0x3d81x1c3, _0x3d81x198) {
    if (this['p']['GS']['getSlot'](_0x3d81x20, _0x3d81x198)) {
        this['players']['push'](_0x3d81x20);
        this['rcS'][_0x3d81x1c3] = _0x3d81x20;
        return true
    };
    return false
};
Live['prototype']['onMessage'] = function(_0x3d81x2d) {
    if (typeof _0x3d81x2d['data'] === 'string') {
        if (this['port'] === '9002') {
            console['log']('Text message received: ' + _0x3d81x2d['data'])
        };
        this['handleResponse'](JSON['parse'](_0x3d81x2d['data']))
    } else {
        var _0x3d81x104 = new Uint8Array(_0x3d81x2d['data']);
        var _0x3d81x38, _0x3d81x1c4 = '',
            _0x3d81x1c5, _0x3d81x1c6, _0x3d81x1c7, _0x3d81x1c8;
        if (_0x3d81x104[0] === 1) {
            _0x3d81x1c5 = _0x3d81x104[1];
            _0x3d81x1c6 = _0x3d81x104[2];
            _0x3d81x1c8 = _0x3d81x104[3];
            _0x3d81x1c7 = _0x3d81x104['length'];
            for (var _0x3d81x13 = 4; _0x3d81x13 < _0x3d81x1c7; _0x3d81x13++) {
                var _0x3d81x1c9 = _0x3d81x104[_0x3d81x13].toString(2);
                if (_0x3d81x1c9['length'] < 8) {
                    var _0x3d81x1ca = 8 - _0x3d81x1c9['length'];
                    for (var _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x1ca; _0x3d81x70++) {
                        _0x3d81x1c9 = '0' + _0x3d81x1c9
                    }
                };
                _0x3d81x1c4 = _0x3d81x1c4 + _0x3d81x1c9
            };
            _0x3d81x38 = this['parseBinaryMatrix'](_0x3d81x1c4, _0x3d81x1c8);
            this['updateLiveMatrixViaSnapshot'](_0x3d81x1c5, _0x3d81x1c6, _0x3d81x38)
        } else {
            if (_0x3d81x104[0] === 2) {
                _0x3d81x1c5 = _0x3d81x104[1];
                _0x3d81x1c6 = _0x3d81x104[2];
                _0x3d81x1c7 = _0x3d81x104['length'];
                _0x3d81x38 = Array();
                var _0x3d81x1cb = Array();
                for (var _0x3d81x13 = 3; _0x3d81x13 < _0x3d81x1c7; _0x3d81x13++) {
                    var _0x3d81x1c9 = _0x3d81x104[_0x3d81x13];
                    _0x3d81x1cb['push'](_0x3d81x1c9 & 15);
                    _0x3d81x1cb['push']((_0x3d81x1c9 & (15 << 4)) >> 4);
                    if (_0x3d81x1cb['length'] === 10) {
                        _0x3d81x38['push'](_0x3d81x1cb);
                        _0x3d81x1cb = Array()
                    }
                };
                this['updateLiveMatrixViaSnapshot'](_0x3d81x1c5, _0x3d81x1c6, _0x3d81x38)
            } else {
                if (_0x3d81x104[0] === 4) {
                    this['decodeActionsAndPlay'](_0x3d81x104, true)
                } else {
                    if (_0x3d81x104[0] === 5) {
                        this['decodeActionsAndPlay'](_0x3d81x104, false)
                    } else {
                        if (_0x3d81x104[0] === 99) {
                            this['safeSend'](_0x3d81x2d['data']);
                            return
                        }
                    }
                }
            }
        }
    }
};
Live['prototype']['decodeActionsAndPlay'] = function(_0x3d81x18f, _0x3d81x1cc) {
    if (!this['p']['GS']['extendedAvailable']) {
        return
    };
    var _0x3d81x1cd = new Replay();
    var _0x3d81x1ce = new ReplayStream();
    var _0x3d81x1cf = [];
    _0x3d81x1ce['data'] = _0x3d81x18f;
    _0x3d81x1ce['wordSize'] = 8;
    _0x3d81x1ce['byte'] = 2;
    while (true) {
        var _0x3d81x1d0 = {};
        _0x3d81x1d0['t'] = (_0x3d81x1cc) ? _0x3d81x1ce['pullBits'](12) : 0;
        _0x3d81x1d0['a'] = _0x3d81x1ce['pullBits'](4);
        if (_0x3d81x1d0['t'] === null || _0x3d81x1d0['a'] === null) {
            break
        };
        if (_0x3d81x1d0['a'] === _0x3d81x1cd['Action']['GARBAGE_ADD']) {
            _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](5), _0x3d81x1ce['pullBits'](4)]
        };
        if (_0x3d81x1d0['a'] === _0x3d81x1cd['Action']['REDBAR_SET']) {
            _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](5)]
        };
        if (_0x3d81x1d0['a'] === _0x3d81x1cd['Action']['ARR_MOVE']) {
            _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](1)]
        };
        if (_0x3d81x1d0['a'] === _0x3d81x1cd['Action']['AUX']) {
            _0x3d81x1d0['aux'] = _0x3d81x1ce['pullBits'](4);
            if (_0x3d81x1d0['aux'] === _0x3d81x1cd['AUX']['AFK']) {} else {
                if (_0x3d81x1d0['aux'] === _0x3d81x1cd['AUX']['BLOCK_SET']) {
                    _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](1), _0x3d81x1ce['pullBits'](4)]
                } else {
                    if (_0x3d81x1d0['aux'] === _0x3d81x1cd['AUX']['MOVE_TO']) {
                        _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](4) - 3, _0x3d81x1ce['pullBits'](5) - 12]
                    } else {
                        if (_0x3d81x1d0['aux'] === _0x3d81x1cd['AUX']['RANDOMIZER']) {
                            _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](1), _0x3d81x1ce['pullBits'](5)]
                        } else {
                            if (_0x3d81x1d0['aux'] === _0x3d81x1cd['AUX']['MATRIX_MOD']) {
                                _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](4), _0x3d81x1ce['pullBits'](5)]
                            } else {
                                if (_0x3d81x1d0['aux'] === _0x3d81x1cd['AUX']['WIDE_GARBAGE_ADD']) {
                                    _0x3d81x1d0['d'] = [_0x3d81x1ce['pullBits'](5), _0x3d81x1ce['pullBits'](4), _0x3d81x1ce['pullBits'](3), _0x3d81x1ce['pullBits'](1)]
                                }
                            }
                        }
                    }
                }
            }
        };
        _0x3d81x1cf['push'](_0x3d81x1d0)
    };
    var _0x3d81x20 = this['rcS'][_0x3d81x18f[1]];
    if (_0x3d81x20 in this['p']['GS']['cidSlots'] && this['clients'][_0x3d81x20]['rep']) {
        this['clients'][_0x3d81x20]['rep']['playLive'](_0x3d81x1cf)
    }
};
Live['prototype']['updateLiveMatrixViaSnapshot'] = function(_0x3d81x1c5, _0x3d81x1c6, _0x3d81x38) {
    var _0x3d81x20 = this['rcS'][_0x3d81x1c5];
    if (this['p']['GS']['cidSlots']['hasOwnProperty'](_0x3d81x20)) {
        if (this['xbufferEnabled']) {
            this['xbuffer'][_0x3d81x20] = [_0x3d81x38, _0x3d81x1c6]
        } else {
            this['p']['updateLiveMatrix'](this['p']['GS']['cidSlots'][_0x3d81x20], _0x3d81x38, _0x3d81x1c6)
        }
    }
};
Live['prototype']['onClose'] = function(_0x3d81x2d) {
    this['p']['Caption']['hide'](this['p']['Caption'].LOADING);
    this['connected'] = false;
    this['socket'] = null;
    if (_0x3d81x2d['code'] === 4001) {
        var _0x3d81xac = '<span style=\'font-weight:normal\'>' + i18n['connLimit'] + ' (discord.gg/RcNFCZC).</span>';
        this['chatMajorWarning'](_0x3d81xac, 'criticalErr', {
            closable: false
        });
        this['chatMajorWarning']('<b>Disconnected</b> - ' + i18n['RLreach'] + ' (<em>' + _0x3d81x2d['reason'] + '</em>)', 'dc');
        this['p']['connectStatusElement']['innerHTML'] += ' | ' + _0x3d81x2d['reason'];
        return
    } else {
        if (_0x3d81x2d['code'] === 4002) {
            this['chatMajorWarning'](i18n['idleDC'], 'dc');
            return
        } else {
            if (_0x3d81x2d['code'] === 4003 || _0x3d81x2d['code'] === 4004) {
                this['conAttempts'] = 1000;
                this['chatArea']['style']['backgroundColor'] = 'red';
                this['toggleMorePractice'](true, true);
                showElem(this['p']['practiceMenu']);
                showElem(this['p']['rInfoBox']);
                if (_0x3d81x2d['code'] === 4003) {
                    this['p']['Settings']['setBanArtifact'](_0x3d81x2d['reason']);
                    this['chatMajorWarning'](i18n['ban1'], 'criticalErr', {
                        closable: false
                    });
                    this['p']['connectStatusElement']['innerHTML'] += ' | ' + _0x3d81x2d['reason'] + 'PE'
                } else {
                    this['chatMajorWarning'](i18n['ban2'], 'criticalErr', {
                        closable: false
                    });
                    this['p']['connectStatusElement']['innerHTML'] += ' | Account BANNED'
                };
                return
            } else {
                if (_0x3d81x2d['code'] === 4005) {
                    alert('Authentication failure!');
                    return
                } else {
                    if (_0x3d81x2d['code'] === 4006) {
                        this['chatMajorWarning']('OLD VERSION - refresh to update your client!', 'criticalErr')
                    } else {
                        if (_0x3d81x2d['code'] === 4007 || _0x3d81x2d['code'] === 4008) {
                            this['p']['Caption']['loading']('Switching servers');
                            this['conAttempts'] = 0;
                            if (_0x3d81x2d['code'] === 4008) {
                                this['lastDC'] = _0x3d81x2d['code']
                            };
                            if (this['p']['debug']) {
                                this['chatMajorWarning']('CHANGING SERVER', 'criticalErr')
                            };
                            this['updateConnectionInfo']();
                            setTimeout(this['changeServer']['bind'](this), 50);
                            return
                        } else {
                            if (_0x3d81x2d['code'] === 1006 && this['joinRemote']) {
                                console['log']('Server switch request with code 1006?');
                                let _0x3d81x5d = this['p']['timestamp']() - this['joinRemote']['t'];
                                console['log'](_0x3d81x5d);
                                console['log'](this['joinRemote']);
                                if (_0x3d81x5d < 3000) {
                                    this['updateConnectionInfo']();
                                    setTimeout(this['changeServer']['bind'](this), 50);
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    if (this['cid'] === 0 && this['conAttempts'] < 2) {
        this['useProxy']();
        this['connect']();
        return
    } else {
        if (this['cid'] === 0) {
            this['isProxy'] = false
        }
    };
    var _0x3d81xac = '<span class=\'wFirstLine\'><span class=\'wTitle\'>' + i18n['connLost']['toUpperCase']() + '</span></span>';
    _0x3d81xac += '<p>' + trans(i18n['ncGS'], {
        refr: '<a href="javascript:location.reload(true);">' + i18n['refr'] + '</a>'
    }) + '</p>';
    this['chatMajorWarning'](_0x3d81xac, 'dc');
    this['updateConnectionInfo']();
    if (this['p']['play'] === false && this['p']['lastSeen'] === null) {
        this['toggleMorePractice'](true, true)
    };
    if (!(this['p']['play'] && this['p']['isPmode'](true))) {
        showElem(this['p']['practiceMenu']);
        showElem(this['p']['rInfoBox']);
        hideElem(this['p']['sprintInfo'])
    };
    this['p']['GS']['gsDiv']['textContent'] = '';
    this['p']['GS']['setup'](8);
    this['p']['Caption']['hide']();
    this['p']['changeSkin'](0)
};
Live['prototype']['useProxy'] = function() {
    if (!this['tryProxy']) {
        return
    };
    this['port'] = '';
    this['server'] = this['server'] + '/ws/';
    this['isProxy'] = true;
    console['log']('1st connection attempt failed, reconnecting...')
};
Live['prototype']['changeServer'] = function() {
    let _0x3d81x1d1 = this['joinRemote']['srvId'],
        _0x3d81x1d2 = this['joinRemote']['sess'];
    let _0x3d81x182 = this['servers'][_0x3d81x1d1];
    var _0x3d81x1d3 = (_0x3d81x182['p']['length'] > 1) ? (':' + _0x3d81x182['p']) : '';
    this['socket'] = new WebSocket(_0x3d81x182['s'] + '://' + _0x3d81x182['h'] + _0x3d81x1d3 + '?v=' + this['version'] + '&sess=' + _0x3d81x1d2);
    this['socket']['binaryType'] = 'arraybuffer';
    var _0x3d81x25 = this;
    this['socket']['onopen'] = function(_0x3d81x2d) {
        if (_0x3d81x2d['target']['readyState'] === 1) {
            _0x3d81x25['onOpen'](_0x3d81x2d)
        }
    };
    this['socket']['onmessage'] = this['onMessage']['bind'](this);
    var _0x3d81x1d4 = function(_0x3d81x2d) {
        if (_0x3d81x25['conAttempts'] > 10) {
            _0x3d81x25['onClose'](_0x3d81x2d);
            return
        };
        setTimeout(function() {
            _0x3d81x25['conAttempts']++;
            _0x3d81x25['changeServer']()
        }, 1000)
    };
    this['socket']['onerror'] = function(_0x3d81x2d) {
        _0x3d81x2d['target']['onclose'] = function() {};
        if (_0x3d81x2d['target']['readyState'] === 0 || _0x3d81x2d['target']['readyState'] === 1) {
            _0x3d81x2d['target']['close']()
        };
        _0x3d81x2d['code'] = 1006;
        _0x3d81x1d4(_0x3d81x2d)
    };
    this['socket']['onclose'] = function(_0x3d81x2d) {
        if (_0x3d81x2d['code'] === 1006) {
            _0x3d81x1d4(_0x3d81x2d)
        } else {
            _0x3d81x25['onClose'](_0x3d81x2d)
        }
    }
};
Live['prototype']['connect'] = function() {
    ++this['conAttempts'];
    var _0x3d81x127;
    var _0x3d81x1d5 = '';
    var _0x3d81x1d6 = (this['getParameterByName']('play')) ? '&join=0' : '';
    var _0x3d81x158 = this['getParameterByName']('join');
    if (!_0x3d81x158) {
        let _0x3d81x1d7 = this['getParameterByName']('teamflow');
        if (_0x3d81x1d7 && _0x3d81x1d7['length'] == 36) {
            let _0x3d81x1d8 = this['getParameterByName']('pass');
            if (_0x3d81x1d8) {
                _0x3d81x1d5 += '&pass=' + _0x3d81x1d8
            };
            _0x3d81x158 = 'tf-' + _0x3d81x1d7
        }
    };
    if (_0x3d81x158 !== '') {
        _0x3d81x1d6 = '&join=' + _0x3d81x158
    };
    var _0x3d81x1d9 = (!this['authorized']) ? '&guest=1' : '';
    var _0x3d81x1da = (!this['authorized']) ? '&gSess=' + this['sessX'] : '';
    var _0x3d81x1db = (!this['authorized'] && !isNaN(_0x3d81x127 = parseInt(this['p']['Settings']['getCookie']('nick')))) ? '&nt=' + _0x3d81x127 : '';
    var _0x3d81x1dc = (this['p']['Settings']['getBanArtifact']() !== null) ? '&room=' + this['p']['Settings']['getBanArtifact']() : '';
    if (window['WebSocket']) {
        var _0x3d81x1d3 = (this['port']['length'] > 1) ? (':' + this['port']) : '';
        this['socket'] = new WebSocket(this['serverScheme'] + '://' + this['server'] + _0x3d81x1d3 + '?v=' + this['version'] + _0x3d81x1d6 + _0x3d81x1d9 + _0x3d81x1db + _0x3d81x1dc + _0x3d81x1da + _0x3d81x1d5);
        this['socket']['binaryType'] = 'arraybuffer';
        this['socket']['onopen'] = this['onOpen']['bind'](this);
        this['socket']['onmessage'] = this['onMessage']['bind'](this);
        this['socket']['onclose'] = this['onClose']['bind'](this);
        var _0x3d81x25 = this;
        this['connectionTimeout'] = setTimeout(function() {
            var _0x3d81x1dd = _0x3d81x25['socket'];
            if (_0x3d81x1dd !== null && _0x3d81x1dd['readyState'] === 0 && _0x3d81x25['conAttempts'] < 2) {
                _0x3d81x1dd['onclose'] = _0x3d81x1dd['onmessage'] = function() {};
                _0x3d81x1dd['onopen'] = function() {
                    _0x3d81x1dd['close']()
                };
                _0x3d81x1dd['close']();
                _0x3d81x25['socket'] = _0x3d81x1dd = null;
                _0x3d81x25['useProxy']();
                _0x3d81x25['connect']()
            }
        }, 1500)
    } else {
        alert('This browser does not support websockets, we can\'t connect you :(')
    }
};
Live['prototype']['parseBinaryMatrix'] = function(_0x3d81x1c4, _0x3d81x1de) {
    var _0x3d81x38 = Array();
    var _0x3d81x1cb = Array();
    var _0x3d81xf1 = _0x3d81x1c4['length'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xf1; _0x3d81x13++) {
        _0x3d81x1cb['push'](parseInt(_0x3d81x1c4['charAt'](_0x3d81x13)));
        if (_0x3d81x1cb['length'] === 10) {
            if (_0x3d81x38['length'] < 20 - _0x3d81x1de) {
                _0x3d81x38['push'](_0x3d81x1cb);
                _0x3d81x1cb = Array()
            } else {
                while (_0x3d81x1de > 0) {
                    _0x3d81x38['push']([9, 9, 9, 9, 9, 9, 9, 9, 9, 9]);
                    _0x3d81x1de--
                };
                break
            }
        }
    };
    return _0x3d81x38
};
Live['prototype']['sendReplayConfig'] = function() {
    var _0x3d81x70 = {
        "\x74": 23
    };
    _0x3d81x70['rc'] = this['rc'];
    _0x3d81x70['softDropId'] = this['p']['Replay']['config']['softDropId'];
    _0x3d81x70['bs'] = this['p']['Replay']['config']['bs'];
    if (this['p']['Replay']['config']['bbs']) {
        _0x3d81x70['bbs'] = this['p']['Replay']['config']['bbs']
    };
    if (this['p']['Replay']['config']['rnd']) {
        _0x3d81x70['rnd'] = this['p']['Replay']['config']['rnd']
    };
    if (_0x3d81x70['bs'] === 7) {
        _0x3d81x70['mClr'] = this['p']['Replay']['config']['mClr']
    };
    if (_0x3d81x70['bs'] > 1000) {
        _0x3d81x70['bp'] = this['p']['Replay']['config']['bp']
    };
    var _0x3d81x1df = JSON['stringify'](_0x3d81x70);
    this['safeSend'](_0x3d81x1df)
};
Live['prototype']['sendRepFragment'] = function(_0x3d81x1cf, _0x3d81x67) {
    var _0x3d81x1ce = new ReplayStream();
    _0x3d81x1ce['wordSize'] = 8;
    var _0x3d81x1cd = new Replay();
    _0x3d81x1ce['pushBits']((_0x3d81x67) ? 4 : 5, 8);
    _0x3d81x1ce['pushBits'](this['rc'], 8);
    var _0x3d81x73, _0x3d81x8f = _0x3d81x1cf['length'];
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x8f; _0x3d81x13++) {
        var _0x3d81x73 = _0x3d81x1cf[_0x3d81x13]['t'];
        if (_0x3d81x67) {
            _0x3d81x1ce['pushBits'](_0x3d81x73 >>> 6, 6);
            _0x3d81x1ce['pushBits'](_0x3d81x73 & 63, 6)
        };
        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['a'], 4);
        if (typeof _0x3d81x1cd['AuxBits'][_0x3d81x1cf[_0x3d81x13]['a']] !== 'undefined') {
            var _0x3d81x104 = _0x3d81x1cd['AuxBits'][_0x3d81x1cf[_0x3d81x13]['a']];
            for (var _0x3d81x1e0 = 0; _0x3d81x1e0 < _0x3d81x104['length']; _0x3d81x1e0++) {
                _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][_0x3d81x1e0], _0x3d81x104[_0x3d81x1e0])
            };
            if (_0x3d81x1cf[_0x3d81x13]['a'] === _0x3d81x1cd['Action']['AUX']) {
                if (_0x3d81x1cf[_0x3d81x13]['d'][0] === _0x3d81x1cd['AUX']['AFK']) {} else {
                    if (_0x3d81x1cf[_0x3d81x13]['d'][0] === _0x3d81x1cd['AUX']['BLOCK_SET']) {
                        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][1], 1);
                        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][2], 4)
                    } else {
                        if (_0x3d81x1cf[_0x3d81x13]['d'][0] === _0x3d81x1cd['AUX']['MOVE_TO']) {
                            _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][1] + 3, 4);
                            _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][2] + 12, 5)
                        } else {
                            if (_0x3d81x1cf[_0x3d81x13]['d'][0] === _0x3d81x1cd['AUX']['RANDOMIZER']) {
                                _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][1], 1);
                                _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][2], 5)
                            } else {
                                if (_0x3d81x1cf[_0x3d81x13]['d'][0] === _0x3d81x1cd['AUX']['MATRIX_MOD']) {
                                    _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][1], 4);
                                    _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][2], 5)
                                } else {
                                    if (_0x3d81x1cf[_0x3d81x13]['d'][0] === _0x3d81x1cd['AUX']['WIDE_GARBAGE_ADD']) {
                                        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][1], 5);
                                        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][2], 4);
                                        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][3], 3);
                                        _0x3d81x1ce['pushBits'](_0x3d81x1cf[_0x3d81x13]['d'][4], 1)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    if (!_0x3d81x67) {
        var _0x3d81x1e1 = 8 - _0x3d81x1ce['bitpos'];
        if (_0x3d81x1e1 >= 4) {
            _0x3d81x1ce['pushBits'](_0x3d81x1cd['Action'].AUX, 4)
        }
    };
    var _0x3d81x1e2 = new Uint8Array(_0x3d81x1ce['data']);
    this['safeSend'](_0x3d81x1e2['buffer'])
};
Live['prototype']['sendSnapshot'] = function(_0x3d81x38) {
    var _0x3d81x1e3 = true;
    if (this['connected'] === true && !this['p']['isPmode'](true)) {
        if (_0x3d81x1e3) {
            var _0x3d81xde = new ArrayBuffer(103);
            var _0x3d81x104 = new Uint8Array(_0x3d81xde);
            _0x3d81x104[0] = 2;
            _0x3d81x104[1] = this['rc'];
            _0x3d81x104[2] = this['p']['redBar'];
            var _0x3d81x1e4 = 0;
            var _0x3d81xf = 3,
                _0x3d81x141 = 0;
            for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
                for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
                    _0x3d81x141 |= (_0x3d81x38[_0x3d81x18][_0x3d81x19] & 15) << ((_0x3d81x1e4++) * 4);
                    if (_0x3d81x1e4 == 2) {
                        _0x3d81x104[_0x3d81xf] = _0x3d81x141;
                        _0x3d81x141 = 0;
                        _0x3d81x1e4 = 0;
                        _0x3d81xf++
                    }
                }
            };
            this['safeSend'](_0x3d81xde)
        } else {
            var _0x3d81xde = new ArrayBuffer(28);
            var _0x3d81x104 = new Uint8Array(_0x3d81xde);
            var _0x3d81xf = 3,
                _0x3d81x141 = 0,
                _0x3d81x1e5 = 7,
                _0x3d81x79 = _0x3d81x38['length'];
            _0x3d81x104[0] = 1;
            _0x3d81x104[1] = this['rc'];
            _0x3d81x104[2] = this['p']['redBar'];
            for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x79; _0x3d81x18++) {
                var _0x3d81x1e6 = _0x3d81x38[_0x3d81x18]['length'];
                for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x1e6; _0x3d81x19++) {
                    if (_0x3d81x38[_0x3d81x18][_0x3d81x19]) {
                        _0x3d81x141 |= (1 << _0x3d81x1e5)
                    };
                    if (--_0x3d81x1e5 == -1) {
                        _0x3d81x104[_0x3d81xf] = _0x3d81x141;
                        _0x3d81x141 = 0;
                        _0x3d81x1e5 = 7;
                        _0x3d81xf++
                    }
                }
            };
            this['safeSend'](_0x3d81xde)
        }
    }
};
Live['prototype']['safeSend'] = function(_0x3d81x147) {
    if (this['socket'] && this['socket']['readyState'] === this['socket']['OPEN']) {
        this['socket']['send'](_0x3d81x147);
        return true
    } else {
        return false
    }
};
Live['prototype']['sendStats'] = function() {
    if (this['statsSent']) {
        return
    };
    var _0x3d81x1e7 = {
        pcs: this['p']['placedBlocks'],
        apm: this['p']['getAPM'](),
        mc: this['p']['gamedata']['maxCombo'],
        B2B: this['p']['gamedata']['B2B'],
        a: this['p']['gamedata']['attack']
    };
    var _0x3d81x70 = JSON['stringify'](_0x3d81x1e7);
    this['p']['Replay']['config']['gameEnd'] = this['p']['timestamp']();
    var _0x3d81x1e8 = this['p']['Replay']['getData']()['substr'](0, 5);
    var _0x3d81x147 = '{"t":14,"d":' + _0x3d81x70 + ',"rep":"' + _0x3d81x1e8 + '"}';
    this['safeSend'](_0x3d81x147);
    this['statsSent'] = true
};
Live['prototype']['sendRestartEvent'] = function() {
    var _0x3d81x147 = '{"t":5}';
    this['safeSend'](_0x3d81x147);
    this['p']['focusState'] = 0;
    this['p']['canvas']['focus']();
    if (typeof ga === 'function') {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Game',
            eventAction: 'restart'
        })
    }
};
Live['prototype']['sendGameOverEvent'] = function() {
    var _0x3d81x147 = '{"t":7}';
    this['safeSend'](_0x3d81x147);
    if (!this['statsSent']) {
        this['sendStats']()
    };
    this['notPlaying']['push'](this['cid'])
};
Live['prototype']['showTargetOnSlot'] = function(_0x3d81x37) {
    if (!(_0x3d81x37 >= 0)) {
        _0x3d81x37 = -1
    };
    this['p']['GS']['setTarget'](_0x3d81x37)
};
Live['prototype']['hideResults'] = function() {
    this['resultsBox']['style']['display'] = 'none';
    this['resultsContent']['textContent'] = '';
    this['p']['GS']['resultsShown'] = false;
    this['p']['GS']['resizeElements']()
};
Live['prototype']['beforeReset'] = function() {
    if (!this['p']['isTabFocused']) {
        var _0x3d81x86 = document['getElementById']('favicon');
        var _0x3d81x139 = this['p'],
            _0x3d81x1e9 = document['title'],
            _0x3d81x13 = 0,
            _0x3d81x1ea = setInterval(function() {
                ++_0x3d81x13;
                _0x3d81x86['href'] = '/res/favicon' + ((_0x3d81x13 % 2 === 0) ? '2' : '') + '.ico';
                if (_0x3d81x13 % 10 > 7) {
                    document['title'] = _0x3d81x1e9
                } else {
                    document['title'] = '\u26A0 New game starting! \u26A0'
                };
                if (_0x3d81x13 === 101 || _0x3d81x139['isTabFocused']) {
                    clearInterval(_0x3d81x1ea);
                    document['title'] = _0x3d81x1e9;
                    _0x3d81x86['href'] = '/res/favicon.ico'
                }
            }, 100)
    }
};
Live['prototype']['onReset'] = function() {
    this['winnerCID'] = undefined;
    this['places'] = {};
    this['p']['GS']['reset']();
    this['hideResults']();
    this['changeTarget']()
};
Live['prototype']['changeTarget'] = function() {
    if (this['sitout'] || this['liveMode'] == 1 || this['gdm'] !== 0) {
        return false
    };
    var _0x3d81x1eb = [];
    var _0x3d81x1ec = 0;
    var _0x3d81x1ed = this['players']['length'];
    var _0x3d81x1ee = false;
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['bots']['length']; _0x3d81x13++) {
        if (arrayContains(this['notPlaying'], this['bots'][_0x3d81x13]) === false) {
            _0x3d81x1eb['push'](this['bots'][_0x3d81x13])
        }
    };
    if (!_0x3d81x1ee || _0x3d81x1eb['length'] === 0) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x1ed; _0x3d81x13++) {
            if (arrayContains(this['notPlaying'], this['players'][_0x3d81x13]) === false && arrayContains(this['bots'], this['players'][_0x3d81x13]) === false) {
                _0x3d81x1eb['push'](this['players'][_0x3d81x13])
            }
        }
    };
    if (_0x3d81x1eb['length'] > 0) {
        var _0x3d81xf = _0x3d81x1eb['indexOf'](this['currentTarget']);
        if (_0x3d81xf == -1) {
            _0x3d81xf = 0
        };
        if (typeof _0x3d81x1eb[_0x3d81xf + 1] !== 'undefined') {
            _0x3d81x1ec = _0x3d81x1eb[_0x3d81xf + 1]
        } else {
            _0x3d81x1ec = _0x3d81x1eb[0]
        };
        this['currentTarget'] = _0x3d81x1ec
    } else {
        this['currentTarget'] = 0
    };
    this['showTargetOnSlot'](this['p']['GS']['cidSlots'][_0x3d81x1ec])
};
Live['prototype']['sendAttack'] = function(_0x3d81x118, _0x3d81x119, _0x3d81x114) {
    if (this['gdm'] === 0 && this['currentTarget'] === 0) {
        return
    };
    var _0x3d81x20 = parseInt(this['currentTarget']);
    var _0x3d81x1ef = (this['gdm'] === 0) ? 11 : 7;
    var _0x3d81x19 = new Uint8Array(_0x3d81x1ef);
    _0x3d81x19[0] = 13;
    _0x3d81x19[1] = (!_0x3d81x118 || _0x3d81x118 > 255) ? 0 : _0x3d81x118;
    _0x3d81x19[2] = _0x3d81x114['type'] | (_0x3d81x114['b2b'] ? 128 : 0);
    _0x3d81x19[3] = (!_0x3d81x119 || _0x3d81x119 > 255) ? 0 : _0x3d81x119;
    _0x3d81x19[4] = _0x3d81x114['cmb'];
    _0x3d81x19[5] = (this['gid'] & 65280) >> 8;
    _0x3d81x19[6] = (this['gid'] & 255) >> 0;
    if (this['gdm'] === 0) {
        _0x3d81x19[7] = (_0x3d81x20 & -16777216) >> 24;
        _0x3d81x19[8] = (_0x3d81x20 & 16711680) >> 16;
        _0x3d81x19[9] = (_0x3d81x20 & 65280) >> 8;
        _0x3d81x19[10] = (_0x3d81x20 & 255) >> 0
    };
    this['safeSend'](_0x3d81x19['buffer'])
};
Live['prototype']['sendAttackOld'] = function(_0x3d81x56) {
    if (this['gdm'] === 0 && this['currentTarget'] === 0) {
        return
    };
    var _0x3d81x20 = parseInt(this['currentTarget']);
    var _0x3d81x147 = '{"t":13, "a":' + _0x3d81x56 + ', "cid":' + _0x3d81x20 + ', "g":' + this['gid'] + '}';
    this['safeSend'](_0x3d81x147)
};
Live['prototype']['sendAttackToSlot'] = function(_0x3d81x37, _0x3d81x56) {
    var _0x3d81x147 = '{"t":13, "a":' + _0x3d81x56 + ', "cid":' + this['p']['GS']['slots'][_0x3d81x37]['cid'] + ', "g":' + this['gid'] + '}';
    this['safeSend'](_0x3d81x147)
};
Live['prototype']['setChatName'] = function(_0x3d81x1f0) {
    if (!this['authorized']) {
        var _0x3d81x1f1 = 15;
        var _0x3d81x1f2 = _0x3d81x1f0['substr'](0, _0x3d81x1f1);
        this['chatName'] = stringEscape(_0x3d81x1f2)
    };
    this['clients'][this['cid']]['name'] = this['chatName'];
    this['chatInput']['placeholder'] = '';
    this['showInChat']('', '<em>' + i18n['welcome'] + ' ' + this['chatName'] + '!</em>');
    this['showInChat']('', '<em>' + i18n['typeHelp'] + '</em>')
};
Live['prototype']['spectatorMode'] = function(_0x3d81xb) {
    if (arguments['length'] === 0) {
        _0x3d81xb = 0
    };
    if (_0x3d81xb !== 1) {
        var _0x3d81x147 = '{"t":16, "mode":0}';
        this['safeSend'](_0x3d81x147)
    };
    if (_0x3d81xb !== 2) {
        this['showInChat']('', '<em>' + i18n['specModeInfo'] + '</em>')
    };
    this['sitout'] = true;
    this['setResetButton'](true);
    this['p']['paintMatrixWithColor'](9);
    this['p']['play'] = false;
    this['onGameEnd']();
    this['statsSent'] = true;
    this['p']['GS']['setTarget'](-1);
    this['p']['v']['clearHoldCanvas']();
    this['p']['v']['clearQueueCanvas']();
    this['p']['clearMatrix']();
    this['p']['redBar'] = 0;
    this['p']['activeBlock']['pos']['y'] = 30;
    this['p']['focusState'] = 0;
    this['p']['redraw']();
    this['p']['Caption']['hide']();
    this['p']['Caption']['spectatorMode']();
    hideElem(this['teamOptions'])
};
Live['prototype']['spectatorModeOff'] = function(_0x3d81xb) {
    if (arguments['length'] === 0) {
        _0x3d81xb = 0
    };
    if (_0x3d81xb === 0) {
        var _0x3d81x147 = '{"t":16, "mode":0}';
        this['safeSend'](_0x3d81x147)
    };
    if (this['p']['GS']['isFullscreen']) {
        this['p']['GS']['fullScreen'](false)
    };
    this['sitout'] = false;
    this['setResetButton'](false);
    this['showInChat']('', '<em>You left the spectator mode.</em>');
    this['p']['Caption']['hide']()
};
Live['prototype']['onGameEnd'] = function() {
    hideElem(this['p']['sprintInfo']);
    hideElem(this['p']['teamInfo']);
    showElem(this['p']['rInfoBox']);
    showElem(this['p']['practiceMenu']);
    if (this['liveMode'] === 2) {
        showElem(this['teamOptions'])
    }
};
Live['prototype']['onReplaySaved'] = function(_0x3d81x11) {
    if (_0x3d81x11['rep']) {
        this['replayInfoToChat'](_0x3d81x11['id'], _0x3d81x11['gm'], _0x3d81x11['m'])
    };
    if (_0x3d81x11['pb'] && !this['p']['play'] && !this['p']['starting']) {
        if (typeof _0x3d81x11['pb'] === 'object') {
            let _0x3d81x1f3 = this['p']['RulesetManager']['fullModeName'](_0x3d81x11['gm'], _0x3d81x11['m']);
            _0x3d81x11['pb']['modeTitle'] = _0x3d81x1f3['replace']('/ /g', '&nbsp;') + ' by&nbsp;' + this['chatName']
        };
        this['p']['Caption']['newPB'](_0x3d81x11['pb'])
    }
};
Live['prototype']['setResetButton'] = function(_0x3d81x141) {
    if (this['hostStartMode']) {
        this['resetButton']['disabled'] = false
    } else {
        this['resetButton']['disabled'] = _0x3d81x141
    }
};
Live['prototype']['replayInfoToChat'] = function(_0x3d81x7, _0x3d81x1f4, _0x3d81x1f5) {
    if (!this['authorized']) {
        this['showInChat']('', i18n['replayAvailable'] + ': <a target="_blank" href="/replay/' + _0x3d81x7 + '">/replay/' + _0x3d81x7 + '</a>.')
    } else {
        var _0x3d81x17a = this['getLeaderboardLink'](_0x3d81x1f4, _0x3d81x1f5) + '&display=4&id=' + _0x3d81x7;
        var _0x3d81x1f6 = (_0x3d81x1f4 === 5) ? 'my scores' : 'my times';
        if (_0x3d81x1f4 === 7) {
            _0x3d81x1f6 = 'my TSD games'
        } else {
            if (_0x3d81x1f4 === 8) {
                _0x3d81x1f6 = 'my PC games'
            }
        };
        this['showInChat']('', i18n['replay'] + ': <a target="_blank" href="/replay/' + _0x3d81x7 + '">/replay/' + _0x3d81x7 + '</a>. View in <a href="' + _0x3d81x17a + '" target="_blank">' + _0x3d81x1f6 + '</a>.')
    }
};
Live['prototype']['getLeaderboardLink'] = function(_0x3d81x1f4, _0x3d81x1f5) {
    var _0x3d81x12 = '',
        _0x3d81x1f7 = '';
    if (_0x3d81x1f5 >= 10 && _0x3d81x1f4 != 6) {
        var _0x3d81x19c = Math['floor'](_0x3d81x1f5 / 10),
            _0x3d81x1f8 = this['p']['RulesetManager']['RULESETS'][_0x3d81x19c];
        if (_0x3d81x19c === 0 || !_0x3d81x1f8) {
            _0x3d81x1f7 = ''
        } else {
            _0x3d81x1f7 = '&rule=' + _0x3d81x1f8['key'];
            _0x3d81x1f5 %= 10
        }
    };
    switch (_0x3d81x1f4) {
        case 1:
            var _0x3d81x72 = this['p']['sprintModes'][_0x3d81x1f5] + 'L';
            _0x3d81x12 = '/sprint?lines=' + _0x3d81x72;
            break;
        case 3:
            var _0x3d81x72 = this['p']['cheeseModes'][_0x3d81x1f5] + 'L';
            _0x3d81x12 = '/cheese?lines=' + _0x3d81x72;
            break;
        case 4:
            var _0x3d81x1ad = ['1+g%2Fs'];
            var _0x3d81xb = _0x3d81x1ad[_0x3d81x1f5] + 'L';
            _0x3d81x12 = '/survival?lines=' + _0x3d81xb;
            break;
        case 5:
            _0x3d81x12 = '/ultra?lines=2+minutes';
            break;
        case 6:
            _0x3d81x12 = '/map/' + _0x3d81x1f5 + '?ref=game';
            break;
        case 7:
            _0x3d81x12 = '/20TSD?ref=game';
            break;
        case 8:
            _0x3d81x12 = '/PC-mode?ref=game';
            break
    };
    _0x3d81x12 += _0x3d81x1f7;
    return _0x3d81x12
};
Live['prototype']['sendPracticeModeStarting'] = function() {
    if (this['socket']) {
        var _0x3d81x15d = new Object();
        _0x3d81x15d['t'] = 22;
        _0x3d81x15d['s'] = this['p']['Replay']['config']['seed'];
        this['safeSend'](JSON['stringify'](_0x3d81x15d))
    }
};
Live['prototype']['sendGameModeResult'] = function(_0x3d81x153) {
    if (_0x3d81x153['mode'] === -1) {
        return
    };
    var _0x3d81x73 = _0x3d81x153['getGameTime']();
    var _0x3d81x1f9 = _0x3d81x153['getData']();
    var _0x3d81x1fa = (true || this['authorized']) ? '' : '&guests=1';
    var _0x3d81x1fb = sprintTimeFormat(_0x3d81x73, 3);
    var _0x3d81x1f7 = '',
        _0x3d81x1fc = '';
    if (_0x3d81x153['config']['r'] === 0 && this['p']['pmode'] === 1 && ((this['p']['sprintMode'] === 1 && this['p']['gamedata']['TSD'] === 20 && this['p']['gamedata']['lines'] === 40) || (this['p']['sprintMode'] === 2 && this['p']['gamedata']['TSD'] === 10 && this['p']['gamedata']['lines'] === 20))) {
        _0x3d81x153['config']['m'] = 0x00070000 | 1;
        this['p']['pmode'] = 7;
        this['p']['sprintMode'] = 1;
        if (this['p']['gamedata']['TSD'] === 20) {
            this['p']['gamedata']['TSD20'] = Math['round'](_0x3d81x73 * 1000)
        };
        _0x3d81x1f9 = _0x3d81x153['getData']()
    };
    if (_0x3d81x153['config']['r'] > 0) {
        let _0x3d81x1f8 = this['p']['RulesetManager']['RULESETS'][_0x3d81x153['config']['r']];
        _0x3d81x1f7 = '&rule=' + _0x3d81x1f8['key'];
        if (_0x3d81x1f8['name']) {
            _0x3d81x1fc = _0x3d81x1f8['name'] + ' '
        }
    };
    var _0x3d81x15d = new Object();
    if (this['p']['pmode'] === 1) {
        var _0x3d81x72 = this['p']['sprintModes'][this['p']['sprintMode']] + 'L';
        this['showInChat']('', i18n['sprint'] + ' ' + i18n['gameTime'] + ': <b>' + _0x3d81x1fb + ' s</b>. ' + i18n['see'] + ' <a href=\'/sprint?lines=' + _0x3d81x72 + _0x3d81x1fa + _0x3d81x1f7 + '\' target=\'_blank\'>' + _0x3d81x1fc + _0x3d81x72 + ' ' + i18n['leaderboard'] + '</a>.')
    } else {
        if (this['p']['pmode'] === 3) {
            var _0x3d81x72 = this['p']['cheeseModes'][this['p']['sprintMode']] + 'L';
            this['showInChat']('', i18n['cheese'] + ' ' + i18n['gameTime'] + ': <b>' + _0x3d81x1fb + ' s</b>. ' + i18n['see'] + ' <a href=\'/cheese?lines=' + _0x3d81x72 + _0x3d81x1fa + _0x3d81x1f7 + '\' target=\'_blank\'>' + _0x3d81x1fc + _0x3d81x72 + ' ' + i18n['leaderboard'] + '</a>.')
        } else {
            if (this['p']['pmode'] === 4) {
                _0x3d81x1f7 = '?' + _0x3d81x1f7['substr'](1);
                _0x3d81x73 = _0x3d81x153['getGameTime'](false);
                this['showInChat']('', i18n['survival'] + ' ' + i18n['gameTime'] + ': <b>' + _0x3d81x1fb + ' s</b>. ' + i18n['see'] + ' <a href=\'/survival' + _0x3d81x1f7 + '\' target=\'_blank\'>' + _0x3d81x1fc + i18n['leaderboard'] + '</a>.')
            } else {
                if (this['p']['pmode'] === 5) {
                    _0x3d81x1f7 = '?' + _0x3d81x1f7['substr'](1);
                    _0x3d81x15d['pts'] = this['p']['gamedata']['score'];
                    this['showInChat']('', i18n['ultra'] + ' Score: <b>' + this['p']['gamedata']['score'] + '</b>. ' + i18n['see'] + ' <a href=\'/ultra' + _0x3d81x1f7 + '\' target=\'_blank\'>' + _0x3d81x1fc + i18n['leaderboard'] + '</a>.')
                } else {
                    if (this['p']['pmode'] === 6) {
                        var _0x3d81x170 = this['p']['MapManager']['mapData']['name'];
                        this['showInChat']('', '<b>"' + _0x3d81x170 + '"</b> map ' + i18n['gameTime'] + ': <b>' + _0x3d81x1fb + ' s</b>. ' + i18n['see'] + ' <a href=\'/map/' + this['p']['MapManager']['mapId'] + '\' target=\'_blank\'>the map ' + i18n['leaderboard'] + '</a>.');
                        if (this['p']['MapManager']['mapData']['state'] !== this['p']['MapManager']['STATE_PUBLISHED']) {
                            this['showInChat']('', i18n['nsUnpub']);
                            return
                        }
                    } else {
                        if (this['p']['pmode'] === 7) {
                            _0x3d81x1f7 = '?' + _0x3d81x1f7['substr'](1);
                            _0x3d81x15d['pts'] = this['p']['gamedata']['TSD'];
                            _0x3d81x15d['t20'] = this['p']['gamedata']['TSD20'];
                            this['showInChat']('', '20TSD Result: <b>' + this['p']['gamedata']['TSD'] + ' TSDs, ' + _0x3d81x1fb + ' s</b>. ' + i18n['see'] + ' <a href=\'/20TSD' + _0x3d81x1f7 + '\' target=\'_blank\'>' + _0x3d81x1fc + i18n['leaderboard'] + '</a>.');
                            if (this['p']['gamedata']['TSD'] < 1) {
                                this['showInChat']('', i18n['nsTspins']);
                                return
                            }
                        } else {
                            if (this['p']['pmode'] === 8) {
                                _0x3d81x1f7 = '?' + _0x3d81x1f7['substr'](1);
                                _0x3d81x15d['pts'] = this['p']['gamedata']['PCs'];
                                this['p']['placedBlocks'] -= this['p']['PCdata']['blocks'];
                                if (_0x3d81x15d['pts'] > 0) {
                                    _0x3d81x15d['tt'] = _0x3d81x73;
                                    _0x3d81x73 = this['p']['gamedata']['lastPC'];
                                    _0x3d81x1fb = sprintTimeFormat(_0x3d81x73, 3)
                                };
                                this['showInChat']('', i18n['PCmode'] + ' Result: <b>' + this['p']['gamedata']['PCs'] + ' PCs, ' + _0x3d81x1fb + ' s</b>. ' + i18n['see'] + ' <a href=\'/PC-mode' + _0x3d81x1f7 + '\' target=\'_blank\'>' + _0x3d81x1fc + i18n['leaderboard'] + '</a>.');
                                if (this['p']['gamedata']['PCs'] < 2) {
                                    this['showInChat']('', i18n['nsLowPC']);
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    _0x3d81x15d['t'] = 17;
    _0x3d81x15d['gm'] = this['p']['pmode'];
    _0x3d81x15d['m'] = this['p']['sprintMode'];
    _0x3d81x15d['time'] = _0x3d81x73;
    _0x3d81x15d['f'] = this['p']['totalFinesse'];
    _0x3d81x15d['bl'] = this['p']['placedBlocks'];
    _0x3d81x15d['s'] = _0x3d81x153['config']['seed'];
    _0x3d81x15d['r'] = _0x3d81x153['config']['r'];
    if (_0x3d81x153['config']['err']) {
        _0x3d81x15d['err'] = _0x3d81x153['config']['err']
    };
    if (this['socket']) {
        _0x3d81x15d['rep'] = _0x3d81x1f9;
        this['safeSend'](JSON['stringify'](_0x3d81x15d))
    } else {
        _0x3d81x153['config']['offline'] = _0x3d81x15d;
        _0x3d81x15d['rep'] = _0x3d81x1f9 = _0x3d81x153['getData']();
        this['p']['Replay']['uploadError'](this, 'OFFLINE')
    }
};
Live['prototype']['raceCompleted'] = function() {
    var _0x3d81x15d = new Object();
    _0x3d81x15d['t'] = 18;
    this['safeSend'](JSON['stringify'](_0x3d81x15d))
};
Live['prototype']['setXbuffer'] = function(_0x3d81x13, _0x3d81x1fd) {
    if (_0x3d81x13 >= 1 && _0x3d81x13 <= 6) {
        this['p']['xbuffMask'] = (Math['pow'](2, _0x3d81x13) - 1) >>> 0;
        this['xbufferEnabled'] = true;
        if (_0x3d81x1fd) {
            this['showInChat']('', 'Xbuffer enabled at lvl' + _0x3d81x13 + ' (fm:' + this['p']['xbuffMask'] + ').')
        }
    } else {
        this['xbufferEnabled'] = false;
        this['p']['xbuffMask'] = 1;
        if (_0x3d81x1fd) {
            this['showInChat']('', 'Xbuffer disabled.')
        }
    }
};
Live['prototype']['shouldWait'] = function(_0x3d81x11) {
    if (_0x3d81x11['hasOwnProperty']('w')) {
        if (_0x3d81x11['w'] === 0) {
            this['setResetButton'](false);
            return true
        };
        if (!this['sitout']) {
            this['setResetProgress'](_0x3d81x11['w'])
        };
        this['setResetButton'](true);
        return true
    };
    return false
};
Live['prototype']['setResetProgress'] = function(_0x3d81x1fe) {
    this['resetProgress']['style']['width'] = '0%';
    this['resetProgress']['style']['transitionDuration'] = '0s';
    if (_0x3d81x1fe > 0) {
        var _0x3d81x25 = this;
        setTimeout(function() {
            _0x3d81x25['resetProgress']['style']['width'] = '99.9%';
            _0x3d81x25['resetProgress']['style']['transitionDuration'] = _0x3d81x1fe + 's'
        }, 10)
    }
};
Live['prototype']['onChatScroll'] = function(_0x3d81x1ff) {
    if (this['Friends']['friendsOpened']) {
        return
    };
    let _0x3d81x200 = this['chatAtBottom'];
    this['chatAtBottom'] = (this['chatArea']['scrollTop'] + this['chatArea']['clientHeight'] >= this['chatArea']['scrollHeight'] - 42);
    if (!this['chatAtBottom'] && _0x3d81x200) {
        let _0x3d81x199 = document['createElement']('button');
        this['scrollDownChatBtn'] = _0x3d81x199;
        _0x3d81x199['innerHTML'] = document['getElementById']('tc-hd')['innerHTML'];
        _0x3d81x199['classList']['add']('chatScrolllBtn');
        var _0x3d81x25 = this;
        _0x3d81x199['addEventListener']('click', function() {
            _0x3d81x25['chatAtBottom'] = true;
            _0x3d81x25['chatArea']['scrollTop'] = _0x3d81x25['chatArea']['scrollHeight']
        });
        this['chatArea']['appendChild'](_0x3d81x199)
    } else {
        if (this['chatAtBottom'] && this['scrollDownChatBtn']) {
            this['removeScrollButton']()
        }
    }
};
Live['prototype']['removeScrollButton'] = function() {
    try {
        this['chatArea']['removeChild'](this['scrollDownChatBtn'])
    } catch (err) {}
};
Live['prototype']['sendChat'] = function(_0x3d81x147) {
    var _0x3d81x1f0 = (typeof _0x3d81x147 !== 'string') ? this['chatInput']['value']['replace'](/"/g, '\"') : _0x3d81x147;
    this['p']['inactiveGamesCount'] = 0;
    if (this['authorized'] && typeof this['clients'][this['cid']] !== 'undefined' && this['clients'][this['cid']]['name'] === 'jez') {
        if (_0x3d81x1f0['substring'](0, 2) === '.a') {
            var _0x3d81x7 = parseInt(_0x3d81x1f0['substring'](2, _0x3d81x1f0['length']));
            _0x3d81x1f0 = '/js - {"t":31,"a":' + _0x3d81x7 + '}'
        } else {
            if (_0x3d81x1f0['substring'](0, 2) === '.s') {
                var _0x3d81x7 = parseInt(_0x3d81x1f0['substring'](2, _0x3d81x1f0['length']));
                _0x3d81x1f0 = '/js - {"t":31,"s":' + _0x3d81x7 + '}'
            }
        }
    };
    if (_0x3d81x1f0['length'] > 0) {
        if (_0x3d81x1f0 === '/clearCookies') {
            this['p']['Settings']['clearAllCookies']();
            this['showInChat']('', '<em>All saved settings cleared.</em>')
        } else {
            if (_0x3d81x1f0 === '/last' || _0x3d81x1f0 === '/result' || _0x3d81x1f0 === '/results' || _0x3d81x1f0 === '/stats') {
                if (!this['lastGameId']) {
                    this['showInChat']('', 'There is no last game to show.')
                } else {
                    this['showInChat']('', i18n['lastGame'] + ' <a target="_blank" href="/games/' + this['lastGameId'] + '">/games/' + this['lastGameId'] + '</a>')
                }
            } else {
                if (_0x3d81x1f0 === '/sitout' || _0x3d81x1f0 === '/spectate' || _0x3d81x1f0 === '/spec') {
                    if (!this['sitout']) {
                        this['spectatorMode']()
                    } else {
                        this['showInChat']('', '<em>' + i18n['aSpec'] + '</em>')
                    }
                } else {
                    if (_0x3d81x1f0 === '/play') {
                        if (this['sitout']) {
                            this['spectatorModeOff']()
                        } else {
                            this['showInChat']('', '<em>' + i18n['aPlay'] + '</em>')
                        }
                    } else {
                        if (_0x3d81x1f0 === '/speclist' || _0x3d81x1f0 === '/watching' || _0x3d81x1f0 === '/spectators') {
                            var _0x3d81x147 = '{"t":16, "mode":1}';
                            this['safeSend'](_0x3d81x147)
                        } else {
                            if (_0x3d81x1f0 === '/clear') {
                                this['chatBox']['textContent'] = ''
                            } else {
                                if (_0x3d81x1f0 === '/fps') {
                                    this['p']['toggleStats'](0)
                                } else {
                                    if (_0x3d81x1f0 === '/realfps' || _0x3d81x1f0 === '/fps2') {
                                        this['p']['toggleStats'](1)
                                    } else {
                                        if (_0x3d81x1f0 === '/debug') {
                                            this['showInChat']('', '<em>Debug output activated.</em>');
                                            this['p']['debug'] = true
                                        } else {
                                            if (_0x3d81x1f0 === '/DAS') {
                                                this['p']['DASdebug'] = !this['p']['DASdebug'];
                                                this['p']['toggleStats'](0);
                                                if (this['p']['stats']) {
                                                    this['p']['stats']['showPanel'](3)
                                                }
                                            } else {
                                                if (_0x3d81x1f0 === '/version') {
                                                    this['showInChat']('', this['version'])
                                                } else {
                                                    if (_0x3d81x1f0 === '/link' || _0x3d81x1f0 === '/url' || _0x3d81x1f0 === '/URL') {
                                                        var _0x3d81x19d = 'https://jstris.jezevec10.com/join/' + this['rid'];
                                                        this['showInChat']('', '<span class=\'joinLink\' onClick=\'selectText(this)\'>' + _0x3d81x19d + '</span>')
                                                    } else {
                                                        if (_0x3d81x1f0 === '/config') {
                                                            var _0x3d81x1ad = ['Standard', 'Cheese', 'Team', 'LiveRace'];
                                                            var _0x3d81xac = '<b>' + i18n['roomSettings'] + '</b><br>';
                                                            var _0x3d81x201 = this['p']['conf'][0];
                                                            _0x3d81xac += i18n['attack'] + ': ' + JSON['stringify'](this['p']['linesAttack'])['substr'](1)['slice'](0, -1) + '<br>' + i18n['combo'] + ': ' + JSON['stringify'](this['p']['comboAttack'])['substr'](1)['slice'](0, -1) + '<br>' + i18n['solid'] + ': ' + ((!this['solidAfter']) ? 'Off' : ('On - ' + this['solidAfter'] + ' sec')) + '<br>' + i18n['clear'] + ': ' + ((!this['p']['R']['clearDelay']) ? 'Off' : ('On - ' + this['p']['R']['clearDelay'] + ' ms')) + '<br>' + i18n['mode'] + ': ' + _0x3d81x1ad[this['liveMode']] + '<br>' + i18n['garbage'] + ': ' + this['gdms'][this['gdm']] + '<br>' + i18n['garbageDelay'] + ': ' + this['p']['R']['gDelay'] + ' ms<br>' + i18n['messiness'] + ': ' + this['p']['R']['mess'] + '%';
                                                            this['showInChat']('', _0x3d81xac)
                                                        } else {
                                                            if (_0x3d81x1f0['substring'](0, 4) === '/cid') {
                                                                this['showInChat']('', this['p']['GS']['slots'][parseInt(_0x3d81x1f0['split'](' ')[1])]['cid'])
                                                            } else {
                                                                if (_0x3d81x1f0 === '/ping') {
                                                                    var _0x3d81x147 = '{"t":99}';
                                                                    this['pingSent'] = this['p']['timestamp']();
                                                                    this['safeSend'](_0x3d81x147)
                                                                } else {
                                                                    if (_0x3d81x1f0 === '/dc') {
                                                                        this['socket']['close']()
                                                                    } else {
                                                                        if (_0x3d81x1f0 === '/clients') {
                                                                            for (var _0x3d81x20 in this['clients']) {
                                                                                this['showInChat'](_0x3d81x20, this['clients'][_0x3d81x20]['name'])
                                                                            }
                                                                        } else {
                                                                            if (_0x3d81x1f0 === '/memory') {
                                                                                if (window['performance'] && window['performance']['memory']) {
                                                                                    this['showInChat']('Memory', Math['round']((window['performance']['memory']['usedJSHeapSize'] * 1000) / (1024 * 1024)) / 1000 + ' MiB')
                                                                                } else {
                                                                                    this['showInChat']('', 'Available only in Chrome browser.')
                                                                                }
                                                                            } else {
                                                                                if (_0x3d81x1f0['substring'](0, 5) === '/zoom') {
                                                                                    this['p']['GS']['setZoom'](parseInt(_0x3d81x1f0['split'](' ')[1]))
                                                                                } else {
                                                                                    if (_0x3d81x1f0['substring'](0, 6) === '/setup') {
                                                                                        this['p']['GS']['setup'](parseInt(_0x3d81x1f0['split'](' ')[1]))
                                                                                    } else {
                                                                                        if (_0x3d81x1f0['substring'](0, 7) === '/tsetup') {
                                                                                            this['p']['GS']['tsetup']([parseInt(_0x3d81x1f0['split'](' ')[1]), parseInt(_0x3d81x1f0['split'](' ')[2])])
                                                                                        } else {
                                                                                            if (_0x3d81x1f0 === '/fe') {
                                                                                                this['p']['GS']['forceExtended'] = !this['p']['GS']['forceExtended'];
                                                                                                this['p']['GS']['autoScale']()
                                                                                            } else {
                                                                                                if (_0x3d81x1f0 === '/fullscreen' || _0x3d81x1f0 === '/fs') {
                                                                                                    this['p']['GS']['fullScreen'](!this['p']['GS']['isFullscreen'])
                                                                                                } else {
                                                                                                    if (_0x3d81x1f0 === '/rescale' || _0x3d81x1f0 === '/autoscale') {
                                                                                                        this['p']['GS']['autoScale']()
                                                                                                    } else {
                                                                                                        if (_0x3d81x1f0 === '/replay') {
                                                                                                            this['p']['Replay']['getData']();
                                                                                                            this['p']['Replay']['uploadError'](this, 'REQUESTED_BY_USER')
                                                                                                        } else {
                                                                                                            if (_0x3d81x1f0 === '/host') {
                                                                                                                if (this['hasOwnProperty']('roomHost') && this['roomHost'] && this['clients'][this['roomHost']]) {
                                                                                                                    this['showInChat']('', 'Room host: ' + this['clients'][this['roomHost']]['name'])
                                                                                                                } else {
                                                                                                                    this['showInChat']('', 'No room host is set.')
                                                                                                                }
                                                                                                            } else {
                                                                                                                if (_0x3d81x1f0['substring'](0, 8) === '/xbuffer') {
                                                                                                                    this['setXbuffer'](parseInt(_0x3d81x1f0['split'](' ')[1]), true)
                                                                                                                } else {
                                                                                                                    if (!this['socket']) {
                                                                                                                        this['showInChat']('', 'You are offline! Only possible to use offline commands.')
                                                                                                                    } else {
                                                                                                                        if (this['Friends']['isFriendChat']()) {
                                                                                                                            this['Friends']['sendChat'](_0x3d81x1f0)
                                                                                                                        } else {
                                                                                                                            var _0x3d81x11 = {
                                                                                                                                t: 6,
                                                                                                                                m: _0x3d81x1f0
                                                                                                                            };
                                                                                                                            this['safeSend'](JSON['stringify'](_0x3d81x11))
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    this['chatInput']['value'] = ''
};
Live['prototype']['showModListOfRooms'] = function(_0x3d81x11) {
    var _0x3d81x202 = function(_0x3d81x11) {
        var _0x3d81x12 = '';
        if (_0x3d81x11['auth']) {
            _0x3d81x12 = '<a href="/u/' + _0x3d81x11['name'] + '" target="_blank">' + _0x3d81x11['name'] + '</a>'
        } else {
            _0x3d81x12 += (_0x3d81x11['name'] === null) ? '<em>NoNamed</em>' : _0x3d81x11['name'];
            _0x3d81x12 += ' <span style=\'color:grey\'>(guest)</span>'
        };
        if (_0x3d81x11['issitout']) {
            _0x3d81x12 += ' <span style=\'color:grey\'>(spec)</span>'
        };
        return _0x3d81x12
    };
    var _0x3d81x86 = document['createElement']('div');
    _0x3d81x86['classList']['add']('modWindow');
    var _0x3d81x203 = document['createElement']('div');
    _0x3d81x203['classList']['add']('container');
    _0x3d81x86['appendChild'](_0x3d81x203);
    var _0x3d81x1be = document['createElement']('a');
    _0x3d81x1be['href'] = 'javascript:void(0)';
    _0x3d81x1be['classList']['add']('admBtn', 'closeBtn');
    _0x3d81x1be['addEventListener']('click', function(_0x3d81x2d) {
        _0x3d81x2d['target']['parentNode']['remove']()
    }, false);
    _0x3d81x86['appendChild'](_0x3d81x1be);
    var _0x3d81x204 = document['createElement']('a');
    _0x3d81x204['textContent'] = 'REFRESH';
    _0x3d81x204['href'] = 'javascript:void(0)';
    _0x3d81x204['style']['left'] = '60px';
    _0x3d81x204['classList']['add']('admBtn');
    var _0x3d81x25 = this;
    _0x3d81x204['addEventListener']('click', function(_0x3d81x2d) {
        _0x3d81x25['sendChat']('/admin');
        _0x3d81x2d['target']['parentNode']['remove']()
    });
    _0x3d81x86['appendChild'](_0x3d81x204);
    document['body']['appendChild'](_0x3d81x86);
    _0x3d81x203['innerHTML'] = '<h1>Active rooms</h1>';
    var _0x3d81x205 = document['createElement']('ul');
    _0x3d81x205['classList']['add']('admList');
    Object['keys'](_0x3d81x11)['forEach'](function(_0x3d81x14) {
        var _0x3d81x206 = document['createElement']('li');
        var _0x3d81x207 = [],
            _0x3d81x208 = false;
        var _0x3d81x209 = (_0x3d81x11[_0x3d81x14]['settings']['p']) ? '<span style=\'color:yellow\'>(P)</span>' : '';
        var _0x3d81x156 = '<b>' + _0x3d81x14 + '</b> ' + _0x3d81x209 + ' - ' + _0x3d81x11[_0x3d81x14]['settings']['n'];
        _0x3d81x156 += '<ul>';
        for (var _0x3d81x20 in _0x3d81x11[_0x3d81x14]['clients']) {
            var _0x3d81x171 = _0x3d81x11[_0x3d81x14]['clients'][_0x3d81x20];
            _0x3d81x171['cid'] = _0x3d81x20;
            _0x3d81x207['push'](_0x3d81x171)
        };
        _0x3d81x207['sort'](function(_0x3d81xa5, _0x3d81x141) {
            return _0x3d81xa5['issitout'] - _0x3d81x141['issitout']
        });
        _0x3d81x207['forEach'](function(_0x3d81x20a) {
            if (_0x3d81x20a['issitout'] && !_0x3d81x208) {
                _0x3d81x208 = true;
                _0x3d81x156 += '<hr style="margin:0">'
            };
            _0x3d81x156 += '<li>' + _0x3d81x20a['cid'] + ' - ' + _0x3d81x202(_0x3d81x20a) + ' <span style="float:right">[<a href="javascript:void(0)" class="dcMLink" data-cid="' + _0x3d81x20a['cid'] + '">D/C</a> ' + '| <a href="javascript:void(0)" style="color:red" class="banMLink" data-cid="' + _0x3d81x20a['cid'] + '">IP-BAN</a>]</span></li>'
        });
        _0x3d81x156 += '</ul>';
        _0x3d81x206['innerHTML'] = _0x3d81x156;
        _0x3d81x205['appendChild'](_0x3d81x206)
    });
    _0x3d81x203['appendChild'](_0x3d81x205);
    var _0x3d81x19 = document['getElementsByClassName']('dcMLink'),
        _0x3d81x25 = this;
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x19['length']; _0x3d81x13++) {
        _0x3d81x19[_0x3d81x13]['addEventListener']('click', function() {
            var _0x3d81x20b = '/kickByDC ' + this['dataset']['cid'];
            _0x3d81x25['socket']['send']('{"t":6,"m":"' + _0x3d81x20b + '","cid":' + _0x3d81x25['cid'] + '}')
        })
    };
    var _0x3d81x19 = document['getElementsByClassName']('banMLink'),
        _0x3d81x25 = this;
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x19['length']; _0x3d81x13++) {
        _0x3d81x19[_0x3d81x13]['addEventListener']('click', function() {
            var _0x3d81x20c = this['dataset']['cid'];
            if (!confirm('Execute BAN CID=' + _0x3d81x20c + ' ?')) {
                return
            };
            var _0x3d81x20d = new XMLHttpRequest();
            _0x3d81x20d['onreadystatechange'] = function() {
                if (_0x3d81x20d['readyState'] === 4 && _0x3d81x20d['status'] === 200) {
                    var _0x3d81x20b = '/resetIP';
                    _0x3d81x25['socket']['send']('{"t":6,"m":"' + _0x3d81x20b + '","cid":' + _0x3d81x25['cid'] + '}');
                    var _0x3d81x20b = '/kickByDC ' + _0x3d81x20c;
                    _0x3d81x25['socket']['send']('{"t":6,"m":"' + _0x3d81x20b + '","cid":' + _0x3d81x25['cid'] + '}')
                }
            };
            _0x3d81x20d['open']('GET', '/admin/ban/cid/' + _0x3d81x20c, true);
            _0x3d81x20d['send'](null)
        })
    }
};
Live['prototype']['showOfflineWarning'] = function() {
    this['playingOfflineWarningShown'] = true;
    this['chatBox']['textContent'] = '';
    this['showInChat']('<span style=\'color:yellow\'>' + i18n['warning2'] + '</span>', 'You are playing offline!<br>Try reloading Jstris online, otherwise:');
    this['showInChat']('', '&#8226; Your games won\'t be stored in the leaderboards.');
    this['showInChat']('', '&#8226; Your replay won\'t be uploaded, but you can save it.');
    this['showInChat']('', '&#8226; Your game version <b>' + this['version'] + '</b> might be outdated.');
    this['showInChat']('', '&#8226; Multiplayer games and chat are unavailable.')
};
Live['prototype']['toggleMorePractice'] = function(_0x3d81xfc, _0x3d81x20e) {
    _0x3d81xfc = _0x3d81xfc || false;
    _0x3d81x20e = _0x3d81x20e || false;
    let _0x3d81x20f = (document['getElementById']('practice-menu-big')['style']['display'] === 'block');
    if (_0x3d81xfc) {
        _0x3d81x20f = !_0x3d81x20e
    };
    document['getElementById']('practice-menu-big')['style']['display'] = _0x3d81x20f ? 'none' : 'block';
    document['getElementById']('more-practice')['textContent'] = _0x3d81x20f ? i18n['showMore'] : i18n['showLess']
};
Live['prototype']['getParameterByName'] = function(_0x3d81x170) {
    _0x3d81x170 = _0x3d81x170['replace'](/[\[]/, '\[')['replace'](/[\]]/, '\]');
    var _0x3d81x210 = new RegExp('[\?&]' + _0x3d81x170 + '=([^&#]*)'),
        _0x3d81x211 = _0x3d81x210['exec'](location['search']);
    return _0x3d81x211 === null ? '' : decodeURIComponent(_0x3d81x211[1]['replace'](/\+/g, ' '))
};
Live['prototype']['showClass'] = function(_0x3d81x212, _0x3d81x213) {
    _0x3d81x213 = (typeof _0x3d81x213 === 'undefined') ? true : _0x3d81x213;
    var _0x3d81x214 = document['getElementsByClassName'](_0x3d81x212);
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x214['length']; _0x3d81x13++) {
        _0x3d81x214[_0x3d81x13]['style']['display'] = (!_0x3d81x213) ? 'none' : 'table-row'
    }
};

function Client(_0x3d81x20) {
    this['cid'] = _0x3d81x20;
    this['type'] = 0;
    this['rep'] = null;
    this['name'] = null;
    this['mod'] = false;
    this['auth'] = false;
    this['color'] = null;
    this['icon'] = null
}

function RoomInfo(_0x3d81x217) {
    this['l'] = _0x3d81x217;
    this['roomDetailBox'] = this['timeoutRequestDetail'] = this['timeoutRoomDetail'] = null;
    this['rdParts'] = {};
    this['roomDetails'] = {};
    this['ON_OFF'] = [i18n['off'], i18n['on']];
    this['CONF_NAMES'] = {
        "\x61\x74": 'Attack table',
        "\x63\x74": 'Combo table',
        "\x6C\x64": 'Lock delay',
        "\x63\x64": {
            n: 'Clear delay',
            u: 'ms'
        },
        "\x67\x64\x6D": {
            n: 'G-distrib.',
            v: [null, 'divide', 'toAll', null, 'toMost', 'toSelf', 'random', 'roulette']
        },
        "\x67\x62\x6C\x6F\x63\x6B": {
            n: 'G-blocking',
            v: ['full', 'limited', 'none', 'instant']
        },
        "\x72\x6E\x64": {
            n: 'Randomizer',
            v: ['7bag', '14bag', 'Classic', '1Block', '2Block', '1x7bag', '1x14bag', 'C2Sim', '7b-RR', 'BSb-7b', 'BB-7b']
        },
        "\x70\x72": 'Previews',
        "\x68\x6F\x6C\x64": 'Hold',
        "\x62\x62\x73": {
            n: 'Blocks',
            v: [null, 'Big', null, 'ARS', 'Penta', 'M123', 'All28', 'C2RS', 'OSpin']
        },
        "\x67\x72\x61\x76": 'Gravity',
        "\x6D\x65\x73\x73": {
            n: 'Messiness',
            u: '%'
        },
        "\x67\x44\x65\x6C\x61\x79": {
            n: 'G-delay',
            u: 'ms'
        },
        "\x68\x6F\x73\x74\x53\x74\x61\x72\x74": 'HostStart',
        "\x67\x49\x6E\x76": 'G-invert',
        "\x67\x61\x70\x57": 'G-gap',
        "\x6E\x6F\x46\x57": 'noFW',
        "\x73\x61": 'SolidAtk',
        "\x61\x73": {
            n: 'AllSpin',
            v: [i18n['off'], i18n['on'] + ' - Im.', i18n['on'] + ' - 4P']
        },
        "\x61\x73\x45\x78": 'AS-Excl.',
        "\x73\x67\x70": {
            n: 'Solid',
            v: ['0', '1', '2', 'Custom speed']
        }
    };
    this['LIMIT_NAMES'] = {
        "\x61\x70\x6D": {
            n: i18n['APM'],
            u: ''
        },
        "\x73\x75\x62": {
            n: i18n['sprint'],
            u: i18n['s']
        },
        "\x67\x74": {
            n: i18n['gTimeShort'],
            u: i18n['hrs']
        }
    }
}
RoomInfo['prototype']['onLobbyRefresh'] = function() {
    var _0x3d81x218 = Array['from'](this['l']['lobbyBox']['getElementsByClassName']('lobbyRow'));
    for (let _0x3d81x219 of _0x3d81x218) {
        _0x3d81x219['addEventListener']('mouseenter', this['openRoomDetails']['bind'](this));
        _0x3d81x219['addEventListener']('mouseleave', this['closeRoomDetails']['bind'](this))
    };
    this['roomDetails'] = {}
};
RoomInfo['prototype']['onLobbyClosed'] = function() {
    if (this['roomDetailBox'] !== null) {
        this['roomDetailBox']['parentElement']['removeChild'](this['roomDetailBox']);
        this['roomDetailBox'] = null
    };
    this['timeoutRoomDetail'] = null;
    this['rdParts'] = {};
    this['roomDetails'] = {}
};
RoomInfo['prototype']['detailBoxEntered'] = function(_0x3d81x2d) {
    if (this['timeoutRoomDetail']) {
        clearTimeout(this['timeoutRoomDetail']);
        this['timeoutRoomDetail'] = null
    }
};
RoomInfo['prototype']['detailBoxLeft'] = function(_0x3d81x2d) {
    hideElem(this['roomDetailBox']);
    this['timeoutRoomDetail'] = null;
    if (this['timeoutRequestDetail']) {
        clearTimeout(this['timeoutRequestDetail'])
    }
};
RoomInfo['prototype']['createElement'] = function(_0x3d81x97, _0x3d81x18a, _0x3d81x21a) {
    var _0x3d81x8c = document['createElement'](_0x3d81x97);
    for (var _0x3d81x13 in _0x3d81x18a) {
        _0x3d81x8c['classList']['add'](_0x3d81x18a[_0x3d81x13])
    };
    if (_0x3d81x21a) {
        _0x3d81x21a['appendChild'](_0x3d81x8c)
    };
    return _0x3d81x8c
};
RoomInfo['prototype']['requestRoomDetail'] = function(_0x3d81x16e) {
    this['l']['safeSend']('{"t":28,"r":"' + _0x3d81x16e + '"}')
};
RoomInfo['prototype']['acceptRoomDetail'] = function(_0x3d81x11) {
    if (!this['l']['lobbyVisible']) {
        return
    };
    this['roomDetails'][_0x3d81x11['r']] = _0x3d81x11;
    this['displayRoomDetail'](_0x3d81x11['r'])
};
RoomInfo['prototype']['displayRoomDetail'] = function(_0x3d81x16e) {
    if (this['roomDetailBox']['dataset']['id'] !== _0x3d81x16e || this['roomDetailBox']['style']['display'] === 'none') {
        return
    };
    var _0x3d81x11 = this['roomDetails'][_0x3d81x16e];
    hideElem(this['rdParts']['spinner']);
    this['displayPlayers'](_0x3d81x11);
    this['displayConfig'](_0x3d81x11);
    this['displayLimit'](_0x3d81x11)
};
RoomInfo['prototype']['displayLimit'] = function(_0x3d81x11) {
    if (_0x3d81x11['l'] === null) {
        hideElem(this['rdParts']['limit']);
        return
    };
    let _0x3d81x90 = this['rdParts']['limit'];
    let _0x3d81x12 = _0x3d81x11['l']['r'],
        _0x3d81x21b = _0x3d81x11['l']['l'],
        _0x3d81x1e7 = _0x3d81x11['l']['s'] || {};
    this['rdParts']['limit']['style']['display'] = 'flex';
    let _0x3d81x21c = this['createElement']('div', ['rdLimitInf'], null);
    if (_0x3d81x12) {
        _0x3d81x90['classList']['add']('rdOK');
        _0x3d81x90['classList']['remove']('rdF');
        _0x3d81x90['innerHTML'] = getSVG('s-unlocked', 'dark', 'lIcn');
        _0x3d81x21c['innerHTML'] = '<h1>' + i18n['joinPossible'] + '</h1>'
    } else {
        _0x3d81x90['classList']['add']('rdF');
        _0x3d81x90['classList']['remove']('rdOK');
        _0x3d81x90['innerHTML'] = getSVG('s-locked', 'dark', 'lIcn');
        _0x3d81x21c['innerHTML'] = '<h1>' + i18n['notEligible'] + '</h1>'
    };
    let _0x3d81x21d = function(_0x3d81x104) {
        let _0x3d81x217 = _0x3d81x104[0],
            _0x3d81x153 = _0x3d81x104[1];
        if (!_0x3d81x217) {
            _0x3d81x217 = '0'
        };
        if (!_0x3d81x153) {
            _0x3d81x153 = '&infin;'
        };
        return '\u27E8' + _0x3d81x217 + ',' + _0x3d81x153 + '\u27E9'
    };
    let _0x3d81x21e = function(_0x3d81x30, _0x3d81x21f) {
        if (!_0x3d81x30) {
            return 'None'
        } else {
            return _0x3d81x30 + ' ' + _0x3d81x21f
        }
    };
    let _0x3d81x167 = function(_0x3d81x30, _0x3d81x104) {
        let _0x3d81x217 = _0x3d81x104[0],
            _0x3d81x153 = _0x3d81x104[1],
            _0x3d81x220 = '\u2713',
            _0x3d81x221 = '\u2717';
        if (!_0x3d81x30) {
            return _0x3d81x221
        };
        if ((!_0x3d81x217 && !_0x3d81x153) || (!_0x3d81x217 && _0x3d81x30 <= _0x3d81x153) || (!_0x3d81x153 && _0x3d81x30 >= _0x3d81x217)) {
            return _0x3d81x220
        } else {
            return (_0x3d81x30 <= _0x3d81x153 && _0x3d81x30 >= _0x3d81x217) ? _0x3d81x220 : _0x3d81x221
        }
    };
    let _0x3d81xac = '<dl>';
    for (let _0x3d81x14 in _0x3d81x21b) {
        _0x3d81xac += '<dt>' + this['LIMIT_NAMES'][_0x3d81x14]['n'] + ': ' + _0x3d81x21d(_0x3d81x21b[_0x3d81x14]) + '</dt>';
        let _0x3d81x222 = _0x3d81x167(_0x3d81x1e7[_0x3d81x14], _0x3d81x21b[_0x3d81x14]) + ' ' + _0x3d81x21e(_0x3d81x1e7[_0x3d81x14], this['LIMIT_NAMES'][_0x3d81x14]['u']);
        _0x3d81xac += '<dd>' + _0x3d81x222 + '</dd>'
    };
    _0x3d81xac += '</dl>';
    _0x3d81x21c['innerHTML'] += _0x3d81xac;
    _0x3d81x90['appendChild'](_0x3d81x21c)
};
RoomInfo['prototype']['displayConfig'] = function(_0x3d81x11) {
    let _0x3d81x8b = 0,
        _0x3d81x8c = this['rdParts']['settingsContent'];
    while (_0x3d81x8c['firstChild']) {
        _0x3d81x8c['removeChild'](_0x3d81x8c['firstChild'])
    };
    for (let _0x3d81x14 in _0x3d81x11['s']) {
        if (!(_0x3d81x14 in this['CONF_NAMES'])) {
            continue
        };
        let _0x3d81x223 = (typeof this['CONF_NAMES'][_0x3d81x14] === 'object');
        let _0x3d81x170 = (_0x3d81x223) ? this['CONF_NAMES'][_0x3d81x14]['n'] : this['CONF_NAMES'][_0x3d81x14];
        let _0x3d81x224 = '';
        let _0x3d81x30 = '\u2022 ' + _0x3d81x170;
        if (typeof _0x3d81x11['s'][_0x3d81x14] === 'boolean') {
            _0x3d81x224 = this['ON_OFF'][_0x3d81x11['s'][_0x3d81x14] + 0]
        } else {
            if (Array['isArray'](_0x3d81x11['s'][_0x3d81x14])) {} else {
                if (_0x3d81x223 && 'v' in this['CONF_NAMES'][_0x3d81x14]) {
                    let _0x3d81x225 = this['CONF_NAMES'][_0x3d81x14]['v'][_0x3d81x11['s'][_0x3d81x14]];
                    if (_0x3d81x225 === null) {
                        continue
                    };
                    _0x3d81x225 = _0x3d81x225 || '?';
                    _0x3d81x224 = _0x3d81x225
                } else {
                    _0x3d81x224 = _0x3d81x11['s'][_0x3d81x14]
                };
                if (_0x3d81x223 && 'u' in this['CONF_NAMES'][_0x3d81x14]) {
                    _0x3d81x224 += this['CONF_NAMES'][_0x3d81x14]['u']
                }
            }
        };
        if (_0x3d81x224) {
            _0x3d81x30 += ': <span class=confVal>' + _0x3d81x224 + '</span>'
        };
        let _0x3d81x206 = this['createElement']('div', ['rdItem'], _0x3d81x8c);
        _0x3d81x206['innerHTML'] = _0x3d81x30;
        ++_0x3d81x8b
    };
    if (_0x3d81x8b) {
        showElem(this['rdParts']['settings'])
    }
};
RoomInfo['prototype']['displayPlayers'] = function(_0x3d81x11) {
    this['rdParts']['content']['style']['display'] = 'flex';
    var _0x3d81x226 = 22;
    var _0x3d81x227 = 21;
    var _0x3d81x228 = 0;
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x11['p']['p']['length']; ++_0x3d81x13) {
        if (!_0x3d81x11['p']['p'][_0x3d81x13]['hasOwnProperty']('ti')) {
            _0x3d81x11['p']['p'][_0x3d81x13]['ti'] = 0
        }
    };
    _0x3d81x11['p']['p']['sort'](function(_0x3d81xa5, _0x3d81x141) {
        if (_0x3d81xa5['type'] && !_0x3d81x141['type']) {
            return -1
        } else {
            if (!_0x3d81xa5['type'] && _0x3d81x141['type']) {
                return 1
            } else {
                if (_0x3d81xa5['type'] && _0x3d81x141['type'] && _0x3d81xa5['ti'] != _0x3d81x141['ti']) {
                    return (_0x3d81xa5['ti'] > _0x3d81x141['ti']) ? -1 : 1
                }
            }
        };
        return _0x3d81xa5['n']['localeCompare'](_0x3d81x141['n'])
    });
    var _0x3d81x229 = '',
        _0x3d81x26 = null;
    for (var _0x3d81x22a of _0x3d81x11['p']['p']) {
        if (_0x3d81x26 !== null && _0x3d81x26 === _0x3d81x22a['n']) {
            continue
        };
        let _0x3d81x97 = _0x3d81x22a['type'] || 0;
        let _0x3d81x22b = {
            color: _0x3d81x22a['col'] || null,
            icon: _0x3d81x22a['icn'] || null,
            bold: (_0x3d81x22a['ti'] >= 2)
        };
        _0x3d81x229 += this['l']['getAuthorizedNameLink'](_0x3d81x22a['n'], _0x3d81x97, _0x3d81x22b);
        ++_0x3d81x228;
        _0x3d81x26 = _0x3d81x22a['n'];
        if (_0x3d81x228 >= _0x3d81x227) {
            break
        }
    };
    if (_0x3d81x11['p']['c'] > _0x3d81x227) {
        _0x3d81x229 += '<span class=pInfo>+' + trans(i18n['cntMore'], {
            cnt: _0x3d81x11['p']['c'] - _0x3d81x227
        }) + '</span>';
        ++_0x3d81x228
    } else {
        if (_0x3d81x11['p']['g']) {
            _0x3d81x229 += '<span class=pInfo>+' + trans(i18n['cntGuests'], {
                cnt: _0x3d81x11['p']['g']
            }) + '</span>';
            ++_0x3d81x228
        } else {
            if (_0x3d81x11['p']['c'] + _0x3d81x11['p']['s'] === 0) {
                _0x3d81x229 += '<span class=pInfo>' + i18n['noPlayers'] + '</span>'
            }
        }
    };
    if (_0x3d81x11['p']['s'] && _0x3d81x228 < _0x3d81x226) {
        _0x3d81x229 += '<span class=pInfo>+' + trans(i18n['cntSpec'], {
            cnt: _0x3d81x11['p']['s']
        }) + '</span>';
        ++_0x3d81x228
    };
    this['rdParts']['content']['innerHTML'] = _0x3d81x229
};
RoomInfo['prototype']['openRoomDetails'] = function(_0x3d81x2d) {
    var _0x3d81x1bd, _0x3d81x16e = _0x3d81x2d['target']['dataset']['id'],
        _0x3d81x170 = _0x3d81x2d['target']['firstElementChild']['childNodes'][0]['nodeValue'];
    var _0x3d81x22c = function(_0x3d81x22d, _0x3d81x22e) {
        var _0x3d81x22f = _0x3d81x22e['getBoundingClientRect']();
        var _0x3d81x18 = _0x3d81x22d['clientY'] - _0x3d81x22f['top'];
        return _0x3d81x18
    };
    if (!this['roomDetailBox']) {
        _0x3d81x1bd = this['roomDetailBox'] = this['createElement']('div', ['rdW'], this['l']['lobbyBox']);
        let _0x3d81x230 = this['rdParts']['detail'] = this['createElement']('div', ['roomDetail'], _0x3d81x1bd);
        this['rdParts']['title'] = this['createElement']('div', ['rdTitle'], _0x3d81x230);
        this['rdParts']['spinner'] = this['createElement']('div', ['rdSpinner'], _0x3d81x230);
        this['rdParts']['content'] = this['createElement']('div', ['rdContent'], _0x3d81x230);
        this['rdParts']['settings'] = this['createElement']('div', [], _0x3d81x230);
        this['rdParts']['settingsTitle'] = this['createElement']('div', ['rdTitle', 'rdSub'], this['rdParts']['settings']);
        this['rdParts']['settingsContent'] = this['createElement']('div', ['rdContent', 'rdConf'], this['rdParts']['settings']);
        this['rdParts']['limit'] = this['createElement']('div', ['rdLimit'], _0x3d81x1bd);;;
        this['rdParts']['settingsTitle']['innerHTML'] = i18n['stngsCustom'];
        var _0x3d81x231 = this['createElement']('img', [], this['rdParts']['spinner']);
        _0x3d81x231['src'] = CDN_URL('/res/svg/spinWhite.svg');
        _0x3d81x231['style']['width'] = '40px';
        _0x3d81x1bd['addEventListener']('mouseenter', this['detailBoxEntered']['bind'](this));
        _0x3d81x1bd['addEventListener']('mouseleave', this['detailBoxLeft']['bind'](this))
    } else {
        _0x3d81x1bd = this['roomDetailBox'];
        showElem(this['roomDetailBox']);
        this['detailBoxEntered']()
    };
    showElem(this['rdParts']['spinner']);
    hideElem(this['rdParts']['content']);
    hideElem(this['rdParts']['settings']);
    hideElem(this['rdParts']['limit']);
    let _0x3d81x18 = _0x3d81x22c(_0x3d81x2d, this['l']['lobbyBox']);
    let _0x3d81x232 = Math['min'](255, Math['max'](-15, (_0x3d81x18 - _0x3d81x1bd['clientHeight'] / 2)));
    _0x3d81x1bd['style']['top'] = _0x3d81x232 + 'px';
    _0x3d81x1bd['dataset']['id'] = _0x3d81x16e;
    this['rdParts']['title']['innerHTML'] = _0x3d81x170;
    if (this['timeoutRequestDetail']) {
        clearTimeout(this['timeoutRequestDetail'])
    };
    if (this['roomDetails'][_0x3d81x16e]) {
        this['displayRoomDetail'](_0x3d81x16e)
    } else {
        this['timeoutRequestDetail'] = setTimeout(this['requestRoomDetail']['bind'](this, _0x3d81x16e), 30)
    }
};
RoomInfo['prototype']['closeRoomDetails'] = function(_0x3d81x2d) {
    if (this['roomDetailBox']) {
        this['timeoutRoomDetail'] = setTimeout(this['detailBoxLeft']['bind'](this), 200)
    }
};
'use strict';

function Settings(_0x3d81x139) {
    this['p'] = _0x3d81x139;
    this['box'] = document['getElementById']('settingsBox');
    this['touchEnabledBox'] = document['getElementById']('touch');
    this['soundEnabledBox'] = document['getElementById']('esound');
    this['DMsoundBox'] = document['getElementById']('DMsound');
    this['SEEnabledBox'] = document['getElementById']('SE');
    this['VSEEnabledBox'] = document['getElementById']('vSE');
    this['sfxSelect'] = document['getElementById']('sfxSelect');
    this['vsfxSelect'] = document['getElementById']('vsfxSelect');
    this['SEStartEnabledBox'] = document['getElementById']('SEstart');
    this['SErotateEnabledBox'] = document['getElementById']('SErot');
    this['SEFaultEnabledBox'] = document['getElementById']('SEfault');
    this['monoColorInp'] = document['getElementById']('monoColor');
    this['statOptId'] = document['getElementById']('statOptId');
    this['settingBoxes'] = Array();
    this['settingBoxes'][1] = document['getElementById']('DAS');
    this['settingBoxes'][2] = document['getElementById']('ARR');
    this['settingsSaveBtn'] = document['getElementById']('settingsSave');
    this['settingsSaveBtn']['onclick'] = this['closeSettings']['bind'](this);
    this['settingsResetBtn'] = document['getElementById']('settingsReset');
    this['settingsResetBtn']['onclick'] = this['resetSettings']['bind'](this);
    this['mainDiv'] = document['getElementById']('main');
    this['moreSkins'] = null;
    this['skinsLoaded'] = false;
    this['inputBoxes'] = Array();
    for (var _0x3d81x13 = 1; _0x3d81x13 <= 10; _0x3d81x13++) {
        this['inputBoxes'][_0x3d81x13] = document['getElementById']('input' + _0x3d81x13);
        this['inputBoxes'][_0x3d81x13]['onkeydown'] = this['onControlKeySet']['bind'](this, _0x3d81x13)
    };
    this['webGlStartFailed'] = false;
    this['BAN_ARTIFACT_KEY'] = 'room3';
    this['mc'] = new Hammer(this['mainDiv']);
    this['mc']['set']({
        enable: false,
        touchAction: 'auto'
    });
    this['preventZoomHandler'] = this['preventZoom'];
    this['touchActuallyUsed'] = false;
    this['monochromePicker'] = this['registerColorPicker']();
    this['gamepadFound'] = false;
    this['statGameModeSelect'] = document['getElementById']('statGameModeSelect');
    this['statGameModeSelect']['onchange'] = this['onStatGameModeChange']['bind'](this);
    this['statCheckboxes'] = Array();
    for (var _0x3d81x13 = 1; _0x3d81x13 <= document['getElementsByClassName']('statCheckbox')['length']; _0x3d81x13++) {
        this['statCheckboxes'][_0x3d81x13] = document['getElementById']('stat' + _0x3d81x13);
        this['statCheckboxes'][_0x3d81x13]['onchange'] = this['onStatCheckboxChange']['bind'](this, _0x3d81x13)
    }
}
Settings['prototype']['init'] = function() {
    this['applyDefaults']();
    this['tryToLoadControlsFromCookie']();
    this['cookiePrefOnly']();
    this['clearRealCookies']()
};
Settings['prototype']['applyDefaults'] = function() {
    this['controls'] = [37, 39, 40, 32, 90, 38, 67, 65, 115, 113];
    this['soundEnabled'] = false;
    this['DMsound'] = true;
    this['ml'] = 37;
    this['mr'] = 39;
    this['sd'] = 40;
    this['hd'] = 32;
    this['rl'] = 90;
    this['rr'] = 38;
    this['hk'] = 67;
    this['dr'] = 65;
    this['DAS'] = 133;
    this['ARR'] = 10;
    this['touchControlsEnabled'] = false;
    this['touchControls'] = false;
    this['gridMode'] = 1;
    this['SFXsetID'] = 2;
    this['VSFXsetID'] = 2;
    this['restartSprintOnFF'] = false;
    this['rescaleNow'] = false;
    this['DAScancel'] = true;
    this['defaultMonochrome'] = '#5c5c5c';
    this['p']['DASmethod'] = 1;
    this['shownStats'] = [345, 745, 745, 745, 745, 746, 745, 745, 745, 741, 69]
};
Settings['prototype']['openSettings'] = function() {
    this['box']['style']['display'] = 'block';
    for (var _0x3d81x13 = 1; _0x3d81x13 < this['inputBoxes']['length']; _0x3d81x13++) {
        this['inputBoxes'][_0x3d81x13]['value'] = this['getKeyName'](this['controls'][_0x3d81x13 - 1])
    };
    this['settingBoxes'][1]['value'] = this['DAS'];
    this['settingBoxes'][2]['value'] = this['ARR'];
    this['touchEnabledBox']['checked'] = this['touchControlsEnabled'];
    this['soundEnabledBox']['checked'] = this['soundEnabled'];
    this['DMsoundBox']['checked'] = this['DMsound'];
    this['SEEnabledBox']['checked'] = this['p']['SEenabled'];
    this['VSEEnabledBox']['checked'] = this['p']['VSEenabled'];
    this['SEStartEnabledBox']['checked'] = this['p']['SEStartEnabled'];
    this['SEFaultEnabledBox']['checked'] = this['p']['SEFaultEnabled'];
    this['SErotateEnabledBox']['checked'] = this['p']['SErotate'];
    document['getElementById']('rescaleNow')['checked'] = this['rescaleNow'];
    document['getElementById']('cancelDAS')['checked'] = this['DAScancel'];
    document['getElementById']('dasMethod')['checked'] = (this['p']['DASmethod'] === 0);
    document['getElementById']('wGL')['checked'] = (this['p']['v']['NAME'] === 'webGL');
    document['getElementById']('sd' + this['p']['softDropId'])['checked'] = true;
    document['getElementById']('gr' + this['gridMode'])['checked'] = true;
    document['getElementById']('ghost')['checked'] = this['p']['ghostEnabled'];
    document['getElementById']('sd' + this['p']['softDropId'])['checked'] = true;
    document['getElementById']('mLay')['checked'] = (this['p']['Mobile'] && this['p']['Mobile']['isMobile']);
    this['statGameModeSelect']['value'] = 0;
    this['onStatGameModeChange']();
    var _0x3d81x234 = (this['p']['monochromeSkin']) ? 7 : this['p']['skinId'];
    try {
        document['getElementById']('bs' + _0x3d81x234)['checked'] = true
    } catch (e) {};
    this['sfxSelect']['value'] = this['SFXsetID'].toString();
    this['vsfxSelect']['value'] = this['VSFXsetID'].toString();
    this['volumeChange'](Math['round'](createjs['Sound']['volume'] * 100));
    soundCredits({
        srcElement: this['sfxSelect'],
        set: this['p']['SFXset']
    });
    soundCredits({
        srcElement: this['vsfxSelect'],
        set: this['p']['VSFXset']
    });
    var _0x3d81xc0 = (this['p']['monochromeSkin']) ? this['p']['monochromeSkin'] : this['defaultMonochrome'];
    this['monoColorInp']['value'] = this['monoColorInp']['style']['backgroundColor'] = _0x3d81xc0;
    this['monochromePicker']['set'](_0x3d81xc0);
    if (this['p']['Live']['sTier'] >= 2 && !this['skinsLoaded']) {
        this['loadMoreSkins']();
        hideElem(document['getElementById']('mSkInf-s'))
    }
};
Settings['prototype']['addSkins'] = function() {
    if (this['moreSkins'] === null) {
        return
    };
    let _0x3d81x12 = '';
    for (let _0x3d81x235 of this['moreSkins']) {
        let _0x3d81x5 = CDN_URL('/res/b/' + _0x3d81x235['p'] + '.png');
        _0x3d81x12 += '<input type="radio" name="bSkin" data-p="' + _0x3d81x235['p'] + '" value="' + _0x3d81x235['i'] + '" id="bs' + _0x3d81x235['i'] + '"><label for="bs' + _0x3d81x235['i'] + '"> ';
        _0x3d81x12 += '<img height="20" src="' + _0x3d81x5 + '"><span class="skinAuth">by ' + _0x3d81x235['a'] + '</span></label><br>'
    };
    let _0x3d81x8c = document['getElementById']('moreSkins');
    _0x3d81x8c['innerHTML'] = _0x3d81x12;
    if (this['p']['skinId'] >= 1000) {
        var _0x3d81x234 = (this['p']['monochromeSkin']) ? 7 : this['p']['skinId'];
        try {
            document['getElementById']('bs' + _0x3d81x234)['checked'] = true
        } catch (e) {}
    }
};
Settings['prototype']['loadMoreSkins'] = function() {
    this['skinsLoaded'] = true;
    var _0x3d81x236 = new XMLHttpRequest();
    var _0x3d81x5 = '/code/skins?v2';
    _0x3d81x236['timeout'] = 10000;
    _0x3d81x236['open']('GET', _0x3d81x5, true);
    _0x3d81x236['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
    _0x3d81x236['setRequestHeader']('Content-type', 'application/x-www-form-urlencoded');
    var _0x3d81x8c = document['getElementById']('moreSkins');
    _0x3d81x8c['innerHTML'] = '<img src="/res/svg/spinWhite.svg" style="height:20px"> Loading more skins...';
    var _0x3d81x237 = function() {
        this['skinsLoaded'] = false;
        _0x3d81x8c['innerHTML'] = 'Could not load skin data, try refreshing the page or reopening the Settings.'
    };
    try {
        _0x3d81x236['send']()
    } catch (err) {
        _0x3d81x237()
    };
    var _0x3d81x25 = this;
    _0x3d81x236['ontimeout'] = _0x3d81x236['onerror'] = _0x3d81x236['onabort'] = function() {
        _0x3d81x237()
    };
    _0x3d81x236['onload'] = function() {
        if (_0x3d81x236['status'] === 200) {
            _0x3d81x25['moreSkins'] = JSON['parse'](_0x3d81x236['responseText']);
            _0x3d81x25['addSkins']()
        } else {
            _0x3d81x237()
        }
    }
};
Settings['prototype']['reloadCanvases'] = function() {
    var _0x3d81x238 = function(_0x3d81xae) {
        var _0x3d81x239 = _0x3d81xae['cloneNode'](false);
        _0x3d81xae['parentNode']['replaceChild'](_0x3d81x239, _0x3d81xae);
        return _0x3d81x239
    };
    this['p']['canvas'] = _0x3d81x238(this['p']['canvas']);
    this['p']['holdCanvas'] = _0x3d81x238(this['p']['holdCanvas']);
    this['p']['queueCanvas'] = _0x3d81x238(this['p']['queueCanvas']);
    _0x3d81x238(document['getElementById']('glstats'))
};
Settings['prototype']['closeSettings'] = function() {
    let _0x3d81x23a = false,
        _0x3d81x23b = false;
    var _0x3d81x23c = parseInt(this['settingBoxes'][1]['value']);
    this['setDAS'](_0x3d81x23c);
    var _0x3d81x23d = parseInt(this['settingBoxes'][2]['value']);
    this['setARR'](_0x3d81x23d);
    this['soundEnabled'] = this['soundEnabledBox']['checked'];
    this['DMsound'] = this['DMsoundBox']['checked'];
    this['p']['SEenabled'] = this['SEEnabledBox']['checked'];
    if (this['p']['VSEenabled'] != this['VSEEnabledBox']['checked']) {
        _0x3d81x23b = true;
        this['p']['VSEenabled'] = this['VSEEnabledBox']['checked']
    };
    this['p']['SEStartEnabled'] = this['SEStartEnabledBox']['checked'];
    this['p']['SEFaultEnabled'] = this['SEFaultEnabledBox']['checked'];
    if (this['p']['SErotate'] != this['SErotateEnabledBox']['checked']) {
        _0x3d81x23b = true
    };
    this['p']['SErotate'] = this['SErotateEnabledBox']['checked'];
    if (this['soundEnabled']) {
        this['setCookie']('eSE', '1')
    } else {
        this['setCookie']('eSE', '0')
    };
    if (this['DMsound']) {
        this['setCookie']('eDM', '1')
    } else {
        this['setCookie']('eDM', '0')
    };
    if (this['p']['SEenabled']) {
        this['setCookie']('SE', '1')
    } else {
        this['setCookie']('SE', '0')
    };
    if (this['p']['SEStartEnabled']) {
        this['setCookie']('SEstart', '1')
    } else {
        this['setCookie']('SEstart', '0')
    };
    if (this['p']['SErotate']) {
        this['setCookie']('SErot', '1')
    } else {
        this['setCookie']('SErot', '0')
    };
    if (this['p']['VSEenabled']) {
        this['setCookie']('VSE', '1')
    } else {
        this['setCookie']('VSE', '0')
    };
    var _0x3d81x23e = document['querySelector']('input[name="bSkin"]:checked');
    var _0x3d81x23f = (_0x3d81x23e) ? parseInt(_0x3d81x23e['value']) : 0;
    var _0x3d81x240 = _0x3d81x23f;
    if (_0x3d81x23f >= 1000) {
        this['p']['customSkinPath'] = _0x3d81x23e['dataset']['p']
    } else {
        if (_0x3d81x23f === 6) {
            _0x3d81x23f = _0x3d81x240 = _0x3d81x240 = 0;
            this['p']['isInvisibleSkin'] = true
        } else {
            this['p']['isInvisibleSkin'] = false
        }
    };
    if (_0x3d81x23f === 7) {
        _0x3d81x240 = 0;
        _0x3d81x23f = 7;
        this['p']['monochromeSkin'] = this['defaultMonochrome'] = document['getElementById']('monoColor')['value']
    } else {
        this['p']['monochromeSkin'] = false
    };
    this['p']['changeSkin'](_0x3d81x240);
    this['setCookie']('skinId', _0x3d81x23f);
    var _0x3d81x241 = parseInt(document['querySelector']('input[name="sds"]:checked')['value']);
    var _0x3d81x242 = (this['p']['softDropId'] !== _0x3d81x241);
    this['p']['softDropId'] = _0x3d81x241;
    this['setCookie']('SD', _0x3d81x241);
    this['touchControlsEnabled'] = this['touchControls'] = this['touchEnabledBox']['checked'];
    this['mc']['set']({
        enable: this['touchControls'],
        touchAction: this['touchActionVal']()
    });
    if (this['touchControlsEnabled']) {
        this['setCookie']('TouchC', '1')
    } else {
        this['setCookie']('TouchC', '0')
    };
    this['restartSprintOnFF'] = document['getElementById']('ffRestart')['checked'];
    var _0x3d81x243 = parseInt(this['sfxSelect']['value']);
    if (this['SFXsetID'] !== _0x3d81x243) {
        this['SFXsetID'] = _0x3d81x243;
        this['setCookie']('SFXset', this.SFXsetID);
        _0x3d81x23b = true
    };
    _0x3d81x243 = parseInt(this['vsfxSelect']['value']);
    if (this['VSFXsetID'] !== _0x3d81x243) {
        this['VSFXsetID'] = _0x3d81x243;
        this['setCookie']('VSFXset', this.VSFXsetID);
        _0x3d81x23b = true
    };
    if (_0x3d81x23b) {
        this['p']['initSFX']()
    };
    var _0x3d81x3c = Math['round'](createjs['Sound']['volume'] * 100);
    this['setCookie']('SEvol', _0x3d81x3c);
    this['rescaleNow'] = document['getElementById']('rescaleNow')['checked'];
    if (this['rescaleNow']) {
        this['setCookie']('rescale', '1')
    } else {
        this['removeCookie']('rescale')
    };
    this['DAScancel'] = document['getElementById']('cancelDAS')['checked'];
    this['p']['DASmethod'] = (document['getElementById']('dasMethod')['checked']) ? 0 : 1;
    if (this['p']['DASmethod'] === 0) {
        this['setCookie']('dasMethod', '0')
    } else {
        this['removeCookie']('dasMethod')
    };
    this['p']['GameStats']['reorder']();
    this['setCookie']('shownStats', this['shownStats']);
    if (this['p']['Live']['authorized']) {
        this['sendSettings']()
    };
    var _0x3d81x244 = this['p']['v'];
    if (document['getElementById']('wGL')['checked'] && this['p']['v']['NAME'] !== 'webGL') {
        this['startWebGL'](true)
    } else {
        if (!document['getElementById']('wGL')['checked'] && this['p']['v']['NAME'] !== '2d') {
            this['startCtx2D']()
        }
    };
    if (_0x3d81x244 !== this['p']['v']) {
        this['p']['GameStats']['setView'](this['p']['v'])
    };
    let _0x3d81x245 = document['getElementById']('mLay')['checked'];
    if (this['p']['Mobile'] && this['p']['Mobile']['isMobile'] !== _0x3d81x245) {
        let _0x3d81x246 = this['p']['Mobile']['isMobileDetect']();
        if (_0x3d81x245 !== _0x3d81x246) {
            this['setCookie']('mobile', _0x3d81x245)
        } else {
            this['removeCookie']('mobile')
        };
        _0x3d81x23a = true
    };
    this['box']['style']['display'] = 'none';
    if (this['p']['play']) {
        this['p']['Replay']['config']['sc'] = 1;
        if (_0x3d81x242) {
            this['p']['Replay']['mode'] = -1;
            this['p']['Live']['showInChat']('', '<em>(SD) ' + i18n['settingsChanged'] + '</em>');
            this['p']['transmitMode'] = 0;
            if (this['p']['Live']['socket']) {
                this['p']['Live']['safeSend']('{"t":25}')
            }
        }
    };
    if (_0x3d81x23a) {
        location['reload']()
    }
};
Settings['prototype']['onStatGameModeChange'] = function(_0x3d81x247) {
    _0x3d81x247 = (typeof _0x3d81x247 === 'undefined') ? 0 : _0x3d81x247['target']['value'];
    for (var _0x3d81x13 = 1; _0x3d81x13 < this['statCheckboxes']['length']; _0x3d81x13++) {
        this['statCheckboxes'][_0x3d81x13]['checked'] = this['shownStats'][_0x3d81x247] & (1 << (_0x3d81x13 - 1))
    };
    this['updateStatsOptionsId']()
};
Settings['prototype']['onStatCheckboxChange'] = function(_0x3d81x13) {
    this['shownStats'][parseInt(this['statGameModeSelect']['value'])] ^= (1 << (_0x3d81x13 - 1));
    this['updateStatsOptionsId']()
};
Settings['prototype']['updateStatsOptionsId'] = function() {
    this['statOptId']['textContent'] = this['shownStats'][parseInt(this['statGameModeSelect']['value'])]
};
Settings['prototype']['startCtx2D'] = function() {
    this['reloadCanvases']();
    this['p']['v'] = new Ctx2DView(this['p']);
    this['p']['v']['initRenderer']();
    this['p']['redraw']();
    this['p']['redrawHoldBox']();
    this['p']['updateQueueBox']()
};
Settings['prototype']['startWebGL'] = function(_0x3d81x248) {
    if (_0x3d81x248) {
        this['reloadCanvases']()
    };
    var _0x3d81x249 = new WebGLView(this['p']);
    if (_0x3d81x249['isAvailable']()) {
        this['p']['v'] = _0x3d81x249;
        this['p']['v']['initRenderer']();
        this['p']['redraw']();
        this['p']['redrawHoldBox']();
        this['p']['updateQueueBox']()
    } else {
        this['webGlStartFailed'] = true;
        console['error']('No WebGL support! Option has been disabled.');
        document['getElementById']('wGL')['checked'] = false;
        this['startCtx2D']()
    }
};
Settings['prototype']['setBanArtifact'] = function(_0x3d81x7) {
    var _0x3d81x24a = 0;
    if (typeof _0x3d81x7 === 'string') {
        var _0x3d81x12 = _0x3d81x7['match'](/\d+/g);
        if (_0x3d81x12) {
            _0x3d81x24a = parseInt(_0x3d81x12[0])
        }
    } else {
        _0x3d81x24a = _0x3d81x7
    };
    localStorage['setItem'](self.BAN_ARTIFACT_KEY, _0x3d81x24a)
};
Settings['prototype']['getBanArtifact'] = function() {
    localStorage['removeItem']('room2');
    return localStorage['getItem'](self.BAN_ARTIFACT_KEY)
};
Settings['prototype']['removeBanArtifact'] = function() {
    return localStorage['removeItem'](self.BAN_ARTIFACT_KEY)
};
Settings['prototype']['resetSettings'] = function() {
    let _0x3d81x24b = (localStorage['getItem'](self.BAN_ARTIFACT_KEY) !== null);
    localStorage['clear']();
    this['applyDefaults']();
    this['openSettings']();
    if (_0x3d81x24b) {
        this['setBanArtifact']()
    };
    if (this['p']['Mobile']['isMobile']) {
        location['reload']()
    }
};
Settings['prototype']['setDAS'] = function(_0x3d81x23c) {
    if (_0x3d81x23c > 0 && _0x3d81x23c < 5000) {
        this['DAS'] = _0x3d81x23c;
        this['setCookie']('DAS', _0x3d81x23c)
    } else {
        alert(i18n['invalidDAS'])
    }
};
Settings['prototype']['setARR'] = function(_0x3d81x23d) {
    if (_0x3d81x23d >= 0 && _0x3d81x23d < 5000) {
        this['ARR'] = _0x3d81x23d;
        this['setCookie']('ARR', _0x3d81x23d)
    } else {
        alert('ARR value is invalid.')
    }
};
Settings['prototype']['handleKeyDown'] = function(_0x3d81x24c, _0x3d81x7) {
    document['getElementById']('kc' + _0x3d81x7)['innerHTML'] = _0x3d81x24c;
    this['inputBoxes'][_0x3d81x7]['value'] = this['getKeyName'](_0x3d81x24c);
    this['setControlKey'](_0x3d81x7, _0x3d81x24c);
    this['setCookie']('k' + _0x3d81x7, _0x3d81x24c)
};
Settings['prototype']['setControlKey'] = function(_0x3d81x7, _0x3d81x24c) {
    this['controls'][_0x3d81x7 - 1] = _0x3d81x24c;
    switch (_0x3d81x7) {
        case 1:
            this['ml'] = _0x3d81x24c;
            break;
        case 2:
            this['mr'] = _0x3d81x24c;
            break;
        case 3:
            this['sd'] = _0x3d81x24c;
            break;
        case 4:
            this['hd'] = _0x3d81x24c;
            break;
        case 5:
            this['rl'] = _0x3d81x24c;
            break;
        case 6:
            this['rr'] = _0x3d81x24c;
            break;
        case 7:
            this['hk'] = _0x3d81x24c;
            break;
        case 8:
            this['dr'] = _0x3d81x24c;
            break;
        case 9:
            break;
        case 10:
            break;
        default:
            console['log']('unknown')
    }
};
Settings['prototype']['loadFromJson'] = function(_0x3d81x24d) {
    for (var _0x3d81x13 = 0; _0x3d81x13 < 10; _0x3d81x13++) {
        this['setControlKey'](_0x3d81x13 + 1, _0x3d81x24d['k' + _0x3d81x13])
    };
    this['setDAS'](_0x3d81x24d.DAS);
    this['setARR'](_0x3d81x24d.ARR);
    this['p']['SEenabled'] = _0x3d81x24d['SE'];
    this['p']['SEFaultEnabled'] = _0x3d81x24d['SEf'];
    this['p']['SEStartEnabled'] = _0x3d81x24d['SEs'];
    if (_0x3d81x24d['bSp']) {
        this['p']['customSkinPath'] = _0x3d81x24d['bSp']
    };
    this['p']['changeSkin'](_0x3d81x24d['bSk']);
    this['soundEnabled'] = _0x3d81x24d['eso'];
    this['p']['ghostEnabled'] = _0x3d81x24d['gho'];
    this['setGrid'](_0x3d81x24d['grs']);
    this['rescaleNow'] = _0x3d81x24d['res'];
    this['p']['softDropId'] = _0x3d81x24d['sds'];
    this['touchControlsEnabled'] = _0x3d81x24d['tou'];
    this['DAScancel'] = _0x3d81x24d['DASca'];
    this['volumeChange'](_0x3d81x24d['vol']);
    this['SFXsetID'] = (_0x3d81x24d['SE'] > 0) ? _0x3d81x24d['SE'] - 1 : 0;
    if (_0x3d81x24d['mClr']) {
        this['defaultMonochrome'] = _0x3d81x24d['mClr'];
        if (_0x3d81x24d['bSk'] === 7) {
            this['p']['monochromeSkin'] = _0x3d81x24d['mClr']
        }
    };
    if (_0x3d81x24d['wGL'] && this['p']['v']['NAME'] !== 'webGL' && !this['webGlStartFailed']) {
        this['startWebGL']()
    } else {
        if (!_0x3d81x24d['wGL'] && this['p']['v']['NAME'] !== '2d') {
            this['startCtx2D']()
        }
    }
};
Settings['prototype']['cookiePrefOnly'] = function() {
    var _0x3d81x225 = 0;
    _0x3d81x225 = this['getCookie']('SErot');
    if (_0x3d81x225 === '1') {
        this['p']['SErotate'] = true
    } else {
        this['p']['SErotate'] = false
    };
    _0x3d81x225 = this['getCookie']('VSE');
    if (_0x3d81x225 === '1') {
        this['p']['VSEenabled'] = true
    } else {
        this['p']['VSEenabled'] = false
    };
    _0x3d81x225 = parseInt(this['getCookie']('VSFXset'));
    if (!isNaN(_0x3d81x225)) {
        this['VSFXsetID'] = _0x3d81x225
    };
    _0x3d81x225 = parseInt(this['getCookie']('dasMethod'));
    this['p']['DASmethod'] = ((_0x3d81x225 === 0) ? 0 : 1);
    _0x3d81x225 = this['getCookie']('shownStats');
    if (_0x3d81x225 != null) {
        this['shownStats'] = _0x3d81x225['split'](',')['map']((_0x3d81x19) => {
            return parseInt(_0x3d81x19)
        })
    }
};
Settings['prototype']['tryToLoadControlsFromCookie'] = function() {
    if (conf_global['name'] !== '' && typeof sts !== 'undefined') {
        this['loadFromJson'](sts);
        return true
    };
    var _0x3d81x225 = 0;
    for (var _0x3d81x13 = 1; _0x3d81x13 <= 10; _0x3d81x13++) {
        _0x3d81x225 = parseInt(this['getCookie']('k' + _0x3d81x13));
        if (!isNaN(_0x3d81x225)) {
            this['setControlKey'](_0x3d81x13, _0x3d81x225)
        }
    };
    _0x3d81x225 = parseInt(this['getCookie']('DAS'));
    if (!isNaN(_0x3d81x225)) {
        this['setDAS'](_0x3d81x225)
    };
    _0x3d81x225 = parseInt(this['getCookie']('ARR'));
    if (!isNaN(_0x3d81x225)) {
        this['setARR'](_0x3d81x225)
    };
    _0x3d81x225 = parseInt(this['getCookie']('skinId'));
    if (!isNaN(_0x3d81x225)) {
        this['p']['changeSkin'](parseInt(_0x3d81x225))
    };
    _0x3d81x225 = this['getCookie']('SE');
    if (_0x3d81x225 === '0') {
        this['p']['SEenabled'] = false
    } else {
        this['p']['SEenabled'] = true
    };
    _0x3d81x225 = this['getCookie']('SEstart');
    if (_0x3d81x225 === '0') {
        this['p']['SEStartEnabled'] = false
    } else {
        this['p']['SEStartEnabled'] = true
    };
    _0x3d81x225 = this['getCookie']('eSE');
    if (_0x3d81x225 === '0' || _0x3d81x225 === null) {
        this['soundEnabled'] = false
    } else {
        this['soundEnabled'] = true
    };
    _0x3d81x225 = this['getCookie']('eDM');
    if (_0x3d81x225 === '1' || _0x3d81x225 === null) {
        this['DMsound'] = true
    } else {
        this['DMsound'] = false
    };
    _0x3d81x225 = parseInt(this['getCookie']('SD'));
    if (!isNaN(_0x3d81x225)) {
        this['p']['softDropId'] = _0x3d81x225
    };
    _0x3d81x225 = this['getCookie']('TouchC');
    this['touchControlsEnabled'] = (_0x3d81x225 === '1');
    _0x3d81x225 = parseInt(this['getCookie']('SFXset'));
    if (!isNaN(_0x3d81x225)) {
        this['SFXsetID'] = _0x3d81x225
    };
    _0x3d81x225 = parseInt(this['getCookie']('SEvol'));
    if (!isNaN(_0x3d81x225) && _0x3d81x225 >= 0 && _0x3d81x225 <= 100) {
        this['volumeChange'](parseInt(_0x3d81x225))
    };
    _0x3d81x225 = this['getCookie']('rescale');
    this['rescaleNow'] = !(_0x3d81x225 === null || _0x3d81x225 === '0');
    this['setGrid'](1)
};
Settings['prototype']['sendSettings'] = function() {
    var _0x3d81x148, _0x3d81x149, _0x3d81xf, _0x3d81x11 = {};
    _0x3d81x148 = document['getElementById']('settingsBox');
    var _0x3d81x14b = Array['prototype']['slice']['call'](_0x3d81x148['getElementsByTagName']('input'), 0);
    var _0x3d81x14c = Array['prototype']['slice']['call'](_0x3d81x148['getElementsByTagName']('select'), 0);
    var _0x3d81x24e = function(_0x3d81x24f) {
        if (_0x3d81x24f === 'cancelDAS') {
            return 'DASca'
        };
        return null
    };
    _0x3d81x149 = _0x3d81x14b['concat'](_0x3d81x14c);
    for (_0x3d81xf = 0; _0x3d81xf < _0x3d81x149['length']; ++_0x3d81xf) {
        if (_0x3d81x149[_0x3d81xf]['id']['substr'](0, 5) === 'input' || _0x3d81x149[_0x3d81xf]['id'] === 'ffRestart' || _0x3d81x149[_0x3d81xf]['id']['substr'](0, 4) === 'stat') {
            continue
        };
        if (_0x3d81x149[_0x3d81xf]['type'] === 'radio') {
            if (_0x3d81x149[_0x3d81xf]['checked']) {
                let _0x3d81x131 = _0x3d81x149[_0x3d81xf]['name']['substr'](0, 3);
                _0x3d81x11[_0x3d81x131] = _0x3d81x149[_0x3d81xf]['value'];
                if (_0x3d81x131 == 'bSk' && _0x3d81x149[_0x3d81xf]['dataset']['p']) {
                    _0x3d81x11['bSp'] = _0x3d81x149[_0x3d81xf]['dataset']['p']
                }
            }
        } else {
            if (_0x3d81x149[_0x3d81xf]['type'] === 'checkbox') {
                var _0x3d81x170 = _0x3d81x24e(_0x3d81x149[_0x3d81xf]['id']) ? _0x3d81x24e(_0x3d81x149[_0x3d81xf]['id']) : _0x3d81x149[_0x3d81xf]['id']['substr'](0, 3);
                _0x3d81x11[_0x3d81x170] = _0x3d81x149[_0x3d81xf]['checked']
            } else {
                _0x3d81x11[_0x3d81x149[_0x3d81xf]['id']['substr'](0, 3)] = _0x3d81x149[_0x3d81xf]['value'];
                if (_0x3d81x149[_0x3d81xf]['id'] === this['sfxSelect']['id'] && typeof _0x3d81x11['SE'] !== 'undefined' && _0x3d81x11['SE']) {
                    _0x3d81x11['SE'] += parseInt(_0x3d81x149[_0x3d81xf]['value'])
                }
            }
        }
    };
    _0x3d81x11['key'] = this['controls'];
    _0x3d81x11['stats'] = this['shownStats'];
    this['postSettings'](JSON['stringify'](_0x3d81x11))
};
Settings['prototype']['postSettings'] = function(_0x3d81xac) {
    var _0x3d81x250 = document['head']['querySelector']('[name=csrf-token]')['content'];
    var _0x3d81x236 = new XMLHttpRequest();
    var _0x3d81x5 = '/code/settings';
    var _0x3d81x251 = 'd=' + encodeURIComponent(_0x3d81xac);
    _0x3d81x236['timeout'] = 10000;
    _0x3d81x236['open']('POST', _0x3d81x5, true);
    _0x3d81x236['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
    _0x3d81x236['setRequestHeader']('X-CSRF-TOKEN', _0x3d81x250);
    _0x3d81x236['setRequestHeader']('Content-type', 'application/x-www-form-urlencoded');
    _0x3d81x236['send'](_0x3d81x251);
    _0x3d81x236['onload'] = function() {
        if (_0x3d81x236['status'] === 200) {}
    }
};
Settings['prototype']['onGameStart'] = function() {
    if (this['touchControlsEnabled']) {
        this['setTouchControls'](1)
    }
};
Settings['prototype']['onGameEnd'] = function() {
    if (this['touchControlsEnabled']) {
        this['setTouchControls'](0)
    }
};
Settings['prototype']['volumeChange'] = function(_0x3d81x30) {
    document['getElementById']('vol-control')['value'] = _0x3d81x30;
    document['getElementById']('vol-value')['innerHTML'] = _0x3d81x30 + '%';
    createjs['Sound']['volume'] = _0x3d81x30 / 100
};
Settings['prototype']['touchActionVal'] = function() {
    return (!this['touchControls']) ? 'auto' : 'none'
};
Settings['prototype']['touchUsed'] = function() {
    this['touchActuallyUsed'] = true;
    if (!this['p']['Mobile']['isMobile']) {
        this['mainDiv']['style']['border'] = '1px solid #0062ca';
        showElem(document['getElementById']('tc-area-desc'))
    }
};
Settings['prototype']['setTouchControls'] = function(_0x3d81x24) {
    if (_0x3d81x24 === 0) {
        this['touchControls'] = false;
        this['mc']['set']({
            enable: this['touchControls'],
            touchAction: this['touchActionVal']()
        });
        this['mainDiv']['removeEventListener']('touchstart', this['preventZoomHandler']);
        if (this['touchActuallyUsed']) {
            this['mainDiv']['style']['border'] = 'none';
            hideElem(document['getElementById']('tc-area-desc'))
        }
    } else {
        if (!this['touchControlsEnabled']) {
            return false
        };
        this['touchControls'] = true;
        this['mc']['set']({
            enable: this['touchControls'],
            touchAction: this['touchActionVal']()
        });
        this['mainDiv']['addEventListener']('touchstart', this['preventZoomHandler']);
        if (this['touchActuallyUsed']) {
            this['touchUsed']()
        }
    }
};
Settings['prototype']['setCookie'] = function(_0x3d81x252, _0x3d81x253) {
    localStorage['setItem'](_0x3d81x252, _0x3d81x253)
};
Settings['prototype']['getCookie'] = function(_0x3d81x252) {
    return localStorage['getItem'](_0x3d81x252)
};
Settings['prototype']['removeCookie'] = function(_0x3d81x170) {
    localStorage['removeItem'](_0x3d81x170)
};
Settings['prototype']['onControlKeySet'] = function(_0x3d81x7, _0x3d81x2d) {
    this['handleKeyDown'](_0x3d81x2d['keyCode'], _0x3d81x7);
    _0x3d81x2d['preventDefault']()
};
Settings['prototype']['setGrid'] = function(_0x3d81x254) {
    this['gridMode'] = _0x3d81x254;
    this['p']['drawBgGrid'](this['gridMode'])
};
Settings['prototype']['nullpoDAS'] = function() {
    var _0x3d81x30 = parseInt(prompt(i18n['enterNullDAS']));
    if (!isNaN(_0x3d81x30) && _0x3d81x30 >= 0 && _0x3d81x30 <= 1000) {
        var _0x3d81x255 = Math['round']((1000 / 60) * _0x3d81x30);
        var _0x3d81x153 = confirm(i18n['suggestedIs'] + ' ' + _0x3d81x255 + '. ' + i18n['applyConfirm']);
        if (_0x3d81x153 === true) {
            this['settingBoxes'][1]['value'] = _0x3d81x255
        }
    }
};
Settings['prototype']['setupSwipeControl'] = function() {
    var _0x3d81x93 = this['p'];
    var _0x3d81x2d = {
        keyCode: 0,
        repeat: false,
        preventDefault: function() {},
        stopPropagation: function() {}
    };
    var _0x3d81xba = this['controls'];
    var _0x3d81xfb = 0,
        _0x3d81x256 = 0,
        _0x3d81x257 = false,
        _0x3d81x23d = 25;
    var _0x3d81x25 = this;
    var _0x3d81x258 = _0x3d81x93['block_size'];
    var _0x3d81x259 = 0.15;
    this['mc']['get']('pan')['set']({
        direction: Hammer['DIRECTION_ALL'],
        threshold: 10
    });
    this['mc']['on']('panstart', function(_0x3d81x1ff) {
        if (!_0x3d81x25['touchActuallyUsed']) {
            _0x3d81x25['touchUsed']()
        };
        _0x3d81xfb = 0;
        _0x3d81x257 = false;
        _0x3d81x256 = true;
        _0x3d81x23d = (_0x3d81x25['ARR'] <= 25) ? 25 : _0x3d81x25['ARR']
    });
    this['mc']['on']('pan', function(_0x3d81x1ff) {
        if (_0x3d81x1ff['direction'] === Hammer['DIRECTION_DOWN']) {
            if (_0x3d81x1ff['distance'] < _0x3d81x93['block_size']) {
                _0x3d81x258 = _0x3d81x93['block_size']
            };
            if (_0x3d81x1ff['velocityY'] < _0x3d81x259 && _0x3d81x1ff['distance'] > _0x3d81x258) {
                _0x3d81x257 = true;
                _0x3d81x258 += _0x3d81x93['block_size'];
                _0x3d81x93['gravityStep'](0);
                _0x3d81x93['Replay']['add'](new ReplayAction(_0x3d81x93['Replay']['Action'].GRAVITY_STEP, _0x3d81x93['timestamp']()));
                _0x3d81x93['redraw']()
            }
        } else {
            if (_0x3d81x1ff['direction'] === Hammer['DIRECTION_LEFT']) {
                if (_0x3d81x1ff['deltaX'] <= (_0x3d81xfb - 1) * _0x3d81x23d) {
                    _0x3d81xfb--;
                    if (_0x3d81x256) {
                        _0x3d81xfb--;
                        _0x3d81x256 = false
                    };
                    if (_0x3d81x93['moveCurrentBlock'](-1, false)) {
                        _0x3d81x93['Replay']['add'](new ReplayAction(_0x3d81x93['Replay']['Action'].MOVE_LEFT, _0x3d81x93['timestamp']()))
                    }
                }
            } else {
                if (_0x3d81x1ff['direction'] === Hammer['DIRECTION_RIGHT']) {
                    if (_0x3d81x1ff['deltaX'] >= (_0x3d81xfb + 1) * _0x3d81x23d) {
                        _0x3d81xfb++;
                        if (_0x3d81x256) {
                            _0x3d81xfb++;
                            _0x3d81x256 = false
                        };
                        if (_0x3d81x93['moveCurrentBlock'](1, false)) {
                            _0x3d81x93['Replay']['add'](new ReplayAction(_0x3d81x93['Replay']['Action'].MOVE_RIGHT, _0x3d81x93['timestamp']()))
                        }
                    }
                }
            }
        }
    });
    this['mc']['on']('panend', function(_0x3d81x1ff) {
        _0x3d81x258 = _0x3d81x93['block_size'];
        _0x3d81x93['gameStep'] = 0.9;
        _0x3d81x93['timer'] = 0;
        if (_0x3d81x1ff['direction'] === Hammer['DIRECTION_DOWN']) {
            if (!_0x3d81x257 && _0x3d81x1ff['deltaY'] >= 40 && Math['abs'](_0x3d81x1ff['overallVelocityY']) >= _0x3d81x259) {
                _0x3d81x2d['keyCode'] = _0x3d81xba[3];
                _0x3d81x93['keyInput2'](_0x3d81x2d);
                _0x3d81x93['keyInput3'](_0x3d81x2d)
            }
        } else {
            if (_0x3d81x1ff['direction'] === Hammer['DIRECTION_UP']) {
                _0x3d81x2d['keyCode'] = _0x3d81xba[6];
                _0x3d81x93['keyInput2'](_0x3d81x2d);
                _0x3d81x93['keyInput3'](_0x3d81x2d)
            }
        }
    });
    this['mc']['on']('panleft', function(_0x3d81x1ff) {
        if (Math['abs'](_0x3d81x1ff['overallVelocityX']) > 0.8 && Math['abs'](_0x3d81x1ff['deltaX']) > 4 * _0x3d81x23d) {
            if (_0x3d81x93['moveBlockToTheWall'](-1)) {
                _0x3d81x93['Replay']['add'](new ReplayAction(_0x3d81x93['Replay']['Action'].DAS_LEFT, _0x3d81x93['timestamp']()))
            };
            return
        }
    });
    this['mc']['on']('panright', function(_0x3d81x1ff) {
        if (Math['abs'](_0x3d81x1ff['overallVelocityX']) > 0.8 && Math['abs'](_0x3d81x1ff['deltaX']) > 4 * _0x3d81x23d) {
            if (_0x3d81x93['moveBlockToTheWall'](1)) {
                _0x3d81x93['Replay']['add'](new ReplayAction(_0x3d81x93['Replay']['Action'].DAS_RIGHT, _0x3d81x93['timestamp']()))
            };
            return
        }
    });
    this['mc']['on']('tap', function(_0x3d81x1ff) {
        var _0x3d81x153 = _0x3d81x93['canvas']['getBoundingClientRect']();
        var _0x3d81x19 = _0x3d81x1ff['center']['x'] - _0x3d81x153['left'];
        var _0x3d81x18 = _0x3d81x1ff['center']['y'] - _0x3d81x153['top'];
        var _0x3d81x25a = _0x3d81x93['canvas']['offsetWidth'] / 2;
        if (_0x3d81x19 > _0x3d81x25a) {
            _0x3d81x2d['keyCode'] = _0x3d81xba[5];
            _0x3d81x93['keyInput2'](_0x3d81x2d);
            _0x3d81x93['keyInput3'](_0x3d81x2d)
        } else {
            _0x3d81x2d['keyCode'] = _0x3d81xba[4];
            _0x3d81x93['keyInput2'](_0x3d81x2d);
            _0x3d81x93['keyInput3'](_0x3d81x2d)
        }
    })
};
Settings['prototype']['clearAllCookies'] = function() {
    var _0x3d81x25b = document['cookie']['split'](';');
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x25b['length']; _0x3d81x13++) {
        var _0x3d81x25c = _0x3d81x25b[_0x3d81x13];
        var _0x3d81x25d = _0x3d81x25c['indexOf']('=');
        var _0x3d81x170 = _0x3d81x25d > -1 ? _0x3d81x25c['substr'](0, _0x3d81x25d) : _0x3d81x25c;
        document['cookie'] = _0x3d81x170 + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT'
    }
};
Settings['prototype']['clearRealCookies'] = function() {
    var _0x3d81x25e = '; ' + document['cookie'];
    var _0x3d81x25f = _0x3d81x25e['split']('; DAS=');
    if (_0x3d81x25f['length'] === 2) {
        this['clearAllCookies']()
    }
};
Settings['prototype']['preventZoom'] = function(_0x3d81x2d) {
    var _0x3d81x260 = _0x3d81x2d['timeStamp'];
    var _0x3d81x261 = _0x3d81x2d['currentTarget']['dataset']['lastTouch'] || _0x3d81x260;
    var _0x3d81x262 = _0x3d81x260 - _0x3d81x261;
    var _0x3d81x263 = _0x3d81x2d['touches']['length'];
    _0x3d81x2d['currentTarget']['dataset']['lastTouch'] = _0x3d81x260;
    if (!_0x3d81x262 || _0x3d81x262 > 500 || _0x3d81x263 > 1) {
        return
    };
    _0x3d81x2d['preventDefault']();
    _0x3d81x2d['target']['click']()
};

function soundCredits(_0x3d81x2d) {
    var _0x3d81x7 = _0x3d81x2d['srcElement']['id'],
        _0x3d81x265, _0x3d81x266, _0x3d81x267;
    if (_0x3d81x7 == 'sfxSelect') {
        _0x3d81x265 = 'soundDesc';
        _0x3d81x266 = 'audioCred';
        _0x3d81x267 = SFXsets
    } else {
        _0x3d81x265 = 'soundDesc2';
        _0x3d81x266 = 'audioCred2';
        _0x3d81x267 = VSFXsets
    };
    var _0x3d81x86 = document['getElementById'](_0x3d81x265),
        _0x3d81x268 = document['getElementById'](_0x3d81x266),
        _0x3d81x269 = _0x3d81x267[parseInt(_0x3d81x2d['srcElement']['value'])]['data'];
    var _0x3d81x26a = new _0x3d81x269;
    if (_0x3d81x26a['author']) {
        _0x3d81x86['style']['display'] = 'table-row';
        _0x3d81x268['innerHTML'] = _0x3d81x26a['author']
    } else {
        hideElem(_0x3d81x86)
    }
}
Settings['prototype']['registerColorPicker'] = function() {
    var _0x3d81x26b = new CP(document['querySelector']('.colorPicker'));
    _0x3d81x26b['on']('change', function(_0x3d81x9a) {
        this['source']['value'] = '#' + _0x3d81x9a;
        this['source']['style']['backgroundColor'] = '#' + _0x3d81x9a
    });
    _0x3d81x26b['on']('enter', function(_0x3d81x9a) {
        _0x3d81x26b['set'](this['source']['value']);
        document['getElementById']('bs7')['checked'] = true
    });
    return _0x3d81x26b
};
Settings['prototype']['initGamePad'] = function(_0x3d81x2d) {
    var _0x3d81x26c = _0x3d81x2d['gamepad'];
    if (this['gamepadFound'] && _0x3d81x26c['buttons']['length'] > this['gamepadButtons']['length']) {
        this['gamepadButtons'] = []
    };
    if (_0x3d81x26c['buttons']['length'] > 0 && !this['gamepadFound']) {
        this['gamepadButtons'] = [];
        for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x26c['buttons']['length']; _0x3d81x13++) {
            this['gamepadButtons']['push']({
                pressed: false,
                index: _0x3d81x13
            })
        };
        this['gamepadFound'] = true
    };
    console['log']('Gamepad connected at index %d: %s. %d buttons, %d axes.', _0x3d81x2d['gamepad']['index'], _0x3d81x2d['gamepad']['id'], _0x3d81x2d['gamepad']['buttons']['length'], _0x3d81x2d['gamepad']['axes']['length'])
};
Settings['prototype']['removeGamePad'] = function(_0x3d81x2d) {
    this['gamepadFound'] = false;
    this['gamepadButtons'] = [];
    console['log']('Lost connection with the gamepad.')
};
Settings['prototype']['processGamepad'] = function() {
    try {
        var _0x3d81x26d = navigator['getGamepads'] ? navigator['getGamepads']() : (navigator['webkitGetGamepads'] ? navigator['webkitGetGamepads'] : []);
        if (!_0x3d81x26d) {
            return
        };
        var _0x3d81x26c = null;
        for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x26d['length']; _0x3d81x13++) {
            if (_0x3d81x26d[_0x3d81x13] && _0x3d81x26d[_0x3d81x13]['buttons']['length'] > 0) {
                _0x3d81x26c = _0x3d81x26d[_0x3d81x13];
                let _0x3d81x26e = false;
                for (let _0x3d81x70 = 0; _0x3d81x70 < _0x3d81x26c['buttons']['length']; _0x3d81x70++) {
                    let _0x3d81x26f = _0x3d81x26c['buttons'][_0x3d81x70];
                    if (_0x3d81x26f['pressed'] && !this['gamepadButtons'][_0x3d81x70]['pressed']) {
                        this['gamepadButtons'][_0x3d81x70]['pressed'] = true;
                        let _0x3d81x270 = new KeyboardEvent('keydown', {
                            keyCode: (230 + _0x3d81x70),
                            bubbles: true
                        });
                        document['activeElement']['dispatchEvent'](_0x3d81x270);
                        _0x3d81x26e = true
                    } else {
                        if (!_0x3d81x26f['pressed'] && this['gamepadButtons'][_0x3d81x70]['pressed']) {
                            this['gamepadButtons'][_0x3d81x70]['pressed'] = false;
                            let _0x3d81x270 = new KeyboardEvent('keyup', {
                                keyCode: (230 + _0x3d81x70),
                                bubbles: true
                            });
                            document['activeElement']['dispatchEvent'](_0x3d81x270);
                            _0x3d81x26e = true
                        }
                    }
                };
                if (_0x3d81x26e) {
                    break
                }
            }
        }
    } catch (error) {
        console['error'](error)
    }
};
Settings['prototype']['getKeyName'] = function(_0x3d81x271) {
    if (_0x3d81x271 >= 230 && _0x3d81x271 <= 300) {
        let _0x3d81x272 = _0x3d81x271 - 230;
        let _0x3d81xe4 = String['fromCharCode'](65 + _0x3d81x272);
        return 'GPAD_' + _0x3d81xe4
    };
    var _0x3d81x273 = {
        0: '\',
        8: 'backspace',
        9: 'tab',
        12: 'num',
        13: 'enter',
        16: 'shift',
        17: 'ctrl',
        18: 'alt',
        19: 'pause',
        20: 'caps',
        27: 'esc',
        32: 'space',
        33: 'pageup',
        34: 'pagedown',
        35: 'end',
        36: 'home',
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        44: 'print',
        45: 'insert',
        46: 'delete',
        48: '0',
        49: '1',
        50: '2',
        51: '3',
        52: '4',
        53: '5',
        54: '6',
        55: '7',
        56: '8',
        57: '9',
        65: 'a',
        66: 'b',
        67: 'c',
        68: 'd',
        69: 'e',
        70: 'f',
        71: 'g',
        72: 'h',
        73: 'i',
        74: 'j',
        75: 'k',
        76: 'l',
        77: 'm',
        78: 'n',
        79: 'o',
        80: 'p',
        81: 'q',
        82: 'r',
        83: 's',
        84: 't',
        85: 'u',
        86: 'v',
        87: 'w',
        88: 'x',
        89: 'y',
        90: 'z',
        91: 'cmd',
        92: 'cmd',
        93: 'cmd',
        96: 'num_0',
        97: 'num_1',
        98: 'num_2',
        99: 'num_3',
        100: 'num_4',
        101: 'num_5',
        102: 'num_6',
        103: 'num_7',
        104: 'num_8',
        105: 'num_9',
        106: 'num_multiply',
        107: 'num_add',
        108: 'num_enter',
        109: 'num_subtract',
        110: 'num_decimal',
        111: 'num_divide',
        112: 'f1',
        113: 'f2',
        114: 'f3',
        115: 'f4',
        116: 'f5',
        117: 'f6',
        118: 'f7',
        119: 'f8',
        120: 'f9',
        121: 'f10',
        122: 'f11',
        123: 'f12',
        124: 'print',
        144: 'num',
        145: 'scroll',
        186: ';',
        187: '=',
        188: ',',
        189: '-',
        190: '.',
        191: '/',
        192: '`',
        219: '[',
        220: '\',
        221: ']',
        222: '\'',
        223: '`',
        224: 'cmd',
        225: 'alt',
        57392: 'ctrl',
        63289: 'num',
        59: ';',
        61: '-',
        173: '='
    };
    if (typeof _0x3d81x273[_0x3d81x271] != 'undefined') {
        return _0x3d81x273[_0x3d81x271]
    } else {
        return 'KeyCode ' + _0x3d81x271
    }
};
'use strict';

function GameSlots(_0x3d81x139) {
    this['p'] = _0x3d81x139;
    this['gsDiv'] = document['getElementById']('gameSlots');
    this['chatBox'] = document['getElementById']('chatBox');
    this['chatExp'] = document['getElementById']('chatExpand');
    this['resultsBox'] = document['getElementById']('resultsBox');
    this['lobbyBox'] = document['getElementById']('lobbyBox');
    this['w'] = this['gsDiv']['offsetWidth'];
    this['h'] = this['gsDiv']['offsetHeight'];
    this['slotHeight'] = this['matrixHeight'] = 0;
    this['slotWidth'] = this['matrixWidth'] = 0;
    this['zoom'] = 1.0;
    this['isFullscreen'] = false;
    this['forceExtended'] = false;
    this['slotStats'] = false;
    document['getElementById']('fsSlots')['checked'] = this['isFullscreen'];
    document['getElementById']('hqSlots')['checked'] = this['forceExtended'];
    document['getElementById']('zoomControl')['value'] = 100;
    this['cidSlots'] = {};
    this['nameFontSize'] = 15;
    this['nameHeight'] = 18;
    this['redBarWidth'] = 5;
    this['slots'] = [];
    this['targetSlotId'] = -1;
    this['shownSlots'] = 0;
    this['extendedView'] = [1, 2, 4];
    this['isExtended'] = false;
    this['extendedAvailable'] = true;
    this['skinOverride'] = false;
    this['baseSize'] = {
        playersW: 450,
        playersH: 450,
        gameFrame: 1091
    };
    this['rowCount'] = 0;
    this['liveBlockSize'] = 10;
    this['holdQueueBlockSize'] = 10;
    this['blocksY'] = 20;
    this['chatExpanded'] = false;
    this['resultsShown'] = false;
    this['setup'](6);
    this['teamTags'] = [];
    this['tagHeight'] = 20;
    this['teamData'] = null;
    this['teamMembers'] = {}
}
GameSlots['prototype']['setZoom'] = function(_0x3d81x275) {
    this['zoom'] = (_0x3d81x275 / 100);
    this['w'] = this['baseSize']['playersW'] * this['zoom'];
    this['h'] = this['baseSize']['playersH'] * ((this['zoom'] - 1) / 2 + 1);
    var _0x3d81x276 = -(this['w'] - this['baseSize']['playersW']) / 2;
    document['getElementById']('players')['style']['width'] = this['w'] + 'px';
    document['getElementById']('main')['style']['marginLeft'] = _0x3d81x276 + 'px';
    document['getElementById']('gameFrame')['style']['width'] = this['baseSize']['gameFrame'] + ((this['w'] - this['baseSize']['playersW'] + _0x3d81x276)) + 'px';
    this['autoScale']()
};
GameSlots['prototype']['fullScreen'] = function(_0x3d81x277) {
    this['isFullscreen'] = _0x3d81x277;
    document['getElementById']('zoomControl')['disabled'] = _0x3d81x277;
    if (_0x3d81x277) {
        hideElem(document['getElementById']('main'));
        document['getElementById']('players')['style']['width'] = '100%';
        document['getElementById']('gameFrame')['style']['width'] = '100%';
        var _0x3d81x278 = document['getElementById']('gc');
        _0x3d81x278['style']['width'] = '95%';
        this['w'] = this['gsDiv']['offsetWidth'];
        this['h'] = Math['min'](this['w'] / 2, _0x3d81x278['offsetHeight'] * 1.05);
        this['zoom'] = Math['max'](1, (this['h'] / 450));
        this['autoScale']()
    } else {
        showElem(document['getElementById']('main'));
        document['getElementById']('players')['style']['width'] = null;
        document['getElementById']('gameFrame')['style']['width'] = null;
        document['getElementById']('gc')['style']['width'] = null;
        this['setZoom'](parseInt(document['getElementById']('zoomControl')['value']))
    }
};
GameSlots['prototype']['numRows'] = function(_0x3d81x279) {
    if (_0x3d81x279 <= 3) {
        return 1
    } else {
        if (_0x3d81x279 <= 10) {
            return 2
        } else {
            if (_0x3d81x279 <= 39) {
                return 3
            } else {
                return 4
            }
        }
    }
};
GameSlots['prototype']['nmob'] = function(_0x3d81x131) {
    return this['blocksY'] * (Math['round'](_0x3d81x131 / this['blocksY']))
};
GameSlots['prototype']['swapSlot'] = function(_0x3d81x70, _0x3d81x13) {
    this['cidSlots'][this['slots'][_0x3d81x13]['cid']] = _0x3d81x70;
    this['cidSlots'][this['slots'][_0x3d81x70]['cid']] = _0x3d81x13;
    var _0x3d81x27a = this['slots'][_0x3d81x13]['cid'];
    var _0x3d81x27b = this['slots'][_0x3d81x13]['name'];
    this['slots'][_0x3d81x13]['cid'] = this['slots'][_0x3d81x70]['cid'];
    this['slots'][_0x3d81x13]['name'] = this['slots'][_0x3d81x70]['name'];
    this['slots'][_0x3d81x70]['cid'] = _0x3d81x27a;
    this['slots'][_0x3d81x70]['name'] = _0x3d81x27b
};
GameSlots['prototype']['mvSlot'] = function(_0x3d81x70, _0x3d81x13) {
    this['slots'][_0x3d81x13]['cid'] = this['slots'][_0x3d81x70]['cid'];
    this['slots'][_0x3d81x13]['name']['innerHTML'] = this['slots'][_0x3d81x70]['name']['innerHTML'];
    this['slots'][_0x3d81x70]['vacantClear']();
    this['cidSlots'][this['slots'][_0x3d81x13]['cid']] = _0x3d81x13
};
GameSlots['prototype']['shrink'] = function() {
    var _0x3d81x27c = 0;
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['slots']['length']; _0x3d81x13++) {
        if (this['slots'][_0x3d81x13]['cid'] === -1) {
            var _0x3d81x70 = _0x3d81x13 + 1;
            while (_0x3d81x70 < this['slots']['length'] && this['slots'][_0x3d81x70]['cid'] === -1) {
                _0x3d81x70++
            };
            if (_0x3d81x70 === this['slots']['length']) {
                break
            } else {
                if (this['slots'][_0x3d81x70]['cid'] !== -1) {
                    this['mvSlot'](_0x3d81x70, _0x3d81x13);
                    _0x3d81x27c++
                }
            }
        } else {
            _0x3d81x27c++
        }
    };
    return _0x3d81x27c
};
GameSlots['prototype']['teamOfSlot'] = function(_0x3d81x13) {
    return this['p']['Live']['clients'][this['slots'][_0x3d81x13]['cid']]['team']
};
GameSlots['prototype']['teamOfCid'] = function(_0x3d81x20) {
    return this['p']['Live']['clients'][_0x3d81x20]['team']
};
GameSlots['prototype']['autoScale'] = function() {
    if (this['p']['Live']['liveMode'] === 2) {
        this['shrink']();
        var _0x3d81xa5, _0x3d81x141, _0x3d81x13, _0x3d81x70;
        for (_0x3d81x13 = 0; _0x3d81x13 < this['slots']['length'] - 1; _0x3d81x13++) {
            var _0x3d81x27d = 0;
            for (_0x3d81x70 = 0; _0x3d81x70 < this['slots']['length'] - _0x3d81x13 - 1; _0x3d81x70++) {
                if (this['slots'][_0x3d81x70]['cid'] === -1 || this['slots'][_0x3d81x70 + 1]['cid'] === -1) {
                    break
                };
                _0x3d81xa5 = parseInt(this['teamOfSlot'](_0x3d81x70));
                _0x3d81x141 = parseInt(this['teamOfSlot'](_0x3d81x70 + 1));
                if (_0x3d81xa5 > _0x3d81x141) {
                    this['swapSlot'](_0x3d81x70, _0x3d81x70 + 1);
                    _0x3d81x27d++
                }
            };
            if (!_0x3d81x27d) {
                break
            }
        };
        this['tsetup']([this['teamMembers']['0']['length'], this['teamMembers']['1']['length']])
    } else {
        this['hideTags']();
        var _0x3d81x27c = this['shrink']();
        if (!_0x3d81x27c) {
            _0x3d81x27c = 1
        };
        this['setup'](_0x3d81x27c)
    }
};
GameSlots['prototype']['hideTags'] = function() {
    for (let _0x3d81x27e of this['teamTags']) {
        if (_0x3d81x27e) {
            _0x3d81x27e['style']['display'] = 'none'
        }
    }
};
GameSlots['prototype']['updateTeamNames'] = function(_0x3d81x197) {
    this['teamData'] = _0x3d81x197;
    for (var _0x3d81x198 in _0x3d81x197) {
        if (!this['teamMembers']['hasOwnProperty'](_0x3d81x198)) {
            this['teamMembers'][_0x3d81x198] = []
        }
    };
    this['autoScale']()
};
GameSlots['prototype']['initTeamTag'] = function(_0x3d81x187, _0x3d81x19, _0x3d81x18, _0x3d81x6) {
    var _0x3d81x27e = null;
    if (typeof this['teamTags'][_0x3d81x187] !== 'undefined') {
        _0x3d81x27e = this['teamTags'][_0x3d81x187];
        _0x3d81x27e['style']['display'] = 'block'
    } else {
        _0x3d81x27e = this['teamTags'][_0x3d81x187] = document['createElement']('div');
        this['gsDiv']['appendChild'](_0x3d81x27e)
    };
    var _0x3d81x73 = _0x3d81x187.toString();
    _0x3d81x27e['textContent'] = this['teamData'][_0x3d81x73]['name'];
    _0x3d81x27e['classList']['add']('teamTag');
    _0x3d81x27e['style']['minWidth'] = _0x3d81x6 + 2 + 'px';
    _0x3d81x27e['style']['height'] = this['tagHeight'] + 'px';
    _0x3d81x27e['style']['left'] = (_0x3d81x19) + 'px';
    _0x3d81x27e['style']['top'] = (_0x3d81x18) + 'px';
    if (_0x3d81x6 + 2 < 170) {
        _0x3d81x27e['style']['minWidth'] = '170px';
        _0x3d81x27e['style']['left'] = (_0x3d81x19 - (170 - (_0x3d81x6 + 2)) / 2) + 'px'
    };
    _0x3d81x27e['style']['backgroundColor'] = this['teamData'][_0x3d81x73]['color']
};
GameSlots['prototype']['tsetup'] = function(_0x3d81x27f) {
    var _0x3d81x280 = Math['max']['apply'](null, _0x3d81x27f);
    var _0x3d81x281 = this['h'] / 2;
    var _0x3d81x282 = 0;
    this['isExtended'] = false;
    this['nameFontSize'] = 15;
    this['nameHeight'] = 18;
    var _0x3d81x283 = _0x3d81x280;
    var _0x3d81x284 = _0x3d81x281;
    var _0x3d81x285 = (_0x3d81x283 === 2) ? 30 : 60;
    var _0x3d81x286 = 15;
    var _0x3d81x287 = (_0x3d81x283 === 1) ? 0 : (_0x3d81x285 / (_0x3d81x283 - 1));
    var _0x3d81x288 = this['tagHeight'] + 2;
    this['slotHeight'] = this['nmob'](_0x3d81x284 - this['nameHeight'] - _0x3d81x286);
    this['redBarWidth'] = Math['ceil'](this['slotHeight'] / 55) + 1;
    this['slotWidth'] = this['slotHeight'] / 2 + this['redBarWidth'];
    var _0x3d81x289 = this['slotWidth'] * _0x3d81x283 + (_0x3d81x283 - 1) * _0x3d81x287;
    if (_0x3d81x289 > this['w']) {
        this['slotWidth'] = Math['floor'](this['w'] / _0x3d81x283) - _0x3d81x287;
        this['slotHeight'] = this['nmob']((this['slotWidth'] - this['redBarWidth']) * 2);
        this['redBarWidth'] = Math['ceil'](this['slotHeight'] / 55) + 1;
        this['slotWidth'] = this['slotHeight'] / 2 + this['redBarWidth'];
        _0x3d81x289 = this['slotWidth'] * _0x3d81x283 + (_0x3d81x283 - 1) * _0x3d81x287
    };
    this['liveBlockSize'] = (this['slotHeight'] / 20);
    var _0x3d81x28a = (this['slotHeight'] + this['nameHeight'] + _0x3d81x286 + _0x3d81x288);
    this['matrixHeight'] = this['slotHeight'];
    this['matrixWidth'] = this['slotWidth'];
    for (var _0x3d81x187 = 0; _0x3d81x187 < _0x3d81x27f['length']; _0x3d81x187++) {
        var _0x3d81x283 = _0x3d81x27f[_0x3d81x187];
        _0x3d81x289 = this['slotWidth'] * _0x3d81x283 + (_0x3d81x283 - 1) * _0x3d81x287;
        var _0x3d81x28b = Math['floor']((this['w'] - _0x3d81x289) / 2);
        if (_0x3d81x283 > 0) {
            this['initTeamTag'](_0x3d81x187, _0x3d81x28b, _0x3d81x28a * _0x3d81x187, _0x3d81x289)
        };
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x283; _0x3d81x13++) {
            var _0x3d81x19 = _0x3d81x28b + _0x3d81x13 * (this['slotWidth'] + _0x3d81x287);
            var _0x3d81x18 = _0x3d81x28a * _0x3d81x187 + _0x3d81x288;
            if (_0x3d81x282 >= this['slots']['length']) {
                this['slots'][_0x3d81x282] = new Slot(_0x3d81x282, _0x3d81x19, _0x3d81x18, this)
            } else {
                this['slots'][_0x3d81x282]['x'] = _0x3d81x19;
                this['slots'][_0x3d81x282]['y'] = _0x3d81x18;
                this['slots'][_0x3d81x282]['init']()
            };
            _0x3d81x282++
        }
    };
    this['shownSlots'] = _0x3d81x282;
    while (_0x3d81x282 < this['slots']['length']) {
        this['slots'][_0x3d81x282]['hide']();
        _0x3d81x282++
    };
    this['realHeight'] = _0x3d81x28a * _0x3d81x27f['length'] - _0x3d81x286;
    this['resizeElements']()
};
GameSlots['prototype']['setup'] = function(_0x3d81x279, _0x3d81x28c) {
    if (typeof _0x3d81x28c === 'undefined') {
        _0x3d81x28c = false
    };
    if (!this['isFullscreen'] && this['zoom'] < 1.5 && _0x3d81x279 < 50) {
        this['rowCount'] = this['numRows'](_0x3d81x279)
    } else {
        if (!_0x3d81x28c) {
            var _0x3d81x83 = [0, 3],
                _0x3d81x28d = Math['ceil'](_0x3d81x279 / 10) + 1;
            for (var _0x3d81x13 = 1; _0x3d81x13 <= _0x3d81x28d; _0x3d81x13++) {
                this['rowCount'] = _0x3d81x13;
                this['setup'](_0x3d81x279, true);
                if (this['slotWidth'] > _0x3d81x83[0]) {
                    _0x3d81x83[0] = this['slotWidth'];
                    _0x3d81x83[1] = _0x3d81x13
                }
            };
            this['rowCount'] = _0x3d81x83[1]
        }
    };
    var _0x3d81x283 = Math['ceil'](_0x3d81x279 / this['rowCount']);
    var _0x3d81x284 = Math['floor'](this['h'] / this['rowCount']);
    var _0x3d81x285 = (_0x3d81x283 === 2) ? 30 : 60;
    this['isExtended'] = (this['extendedAvailable'] && (this['forceExtended'] || this['extendedView']['indexOf'](_0x3d81x279) !== -1));
    if (this['isExtended']) {
        if (this['rowCount'] === 1) {
            _0x3d81x285 = (_0x3d81x283 === 2) ? 30 : 30
        } else {
            if (this['rowCount'] === 2) {
                var _0x3d81x28e = [0, 0, 90, 65];
                _0x3d81x285 = (_0x3d81x283 < _0x3d81x28e['length']) ? _0x3d81x28e[_0x3d81x283] : 60
            }
        }
    };
    var _0x3d81x286 = (this['rowCount'] === 1) ? 0 : 10;
    var _0x3d81x287 = (_0x3d81x283 === 1) ? 0 : (_0x3d81x285 / (_0x3d81x283 - 1));
    var _0x3d81x28f = (this['slotStats']) ? 3 : 1;
    if (this['rowCount'] >= 3) {
        this['nameFontSize'] = 10;
        this['nameHeight'] = 12
    } else {
        this['nameFontSize'] = 15;
        this['nameHeight'] = 18
    };
    this['nameFontSize'] = Math['ceil'](this['nameFontSize'] * Math['max'](1, this['zoom'] * 0.80));
    this['nameHeight'] = Math['ceil'](this['nameHeight'] * Math['max'](1, this['zoom'] * 0.80));
    _0x3d81x287 *= this['zoom'];
    this['matrixHeight'] = this['nmob'](_0x3d81x284 - this['nameHeight'] * _0x3d81x28f - _0x3d81x286);
    this['redBarWidth'] = Math['ceil'](this['matrixHeight'] / 55) + 1;
    this['matrixWidth'] = this['matrixHeight'] / 2 + this['redBarWidth'];
    this['slotWidth'] = (this['isExtended']) ? this['matrixWidth'] * 1.7413 : this['matrixWidth'];
    var _0x3d81x289 = this['slotWidth'] * _0x3d81x283 + (_0x3d81x283 - 1) * _0x3d81x287;
    if (_0x3d81x289 > this['w']) {
        this['slotWidth'] = Math['floor']((this['w'] - (_0x3d81x283 - 1) * _0x3d81x287) / _0x3d81x283);
        this['matrixWidth'] = (this['isExtended']) ? this['slotWidth'] / 1.7413 : this['slotWidth'];
        this['matrixHeight'] = this['nmob']((this['matrixWidth'] - this['redBarWidth']) * 2);
        this['redBarWidth'] = Math['ceil'](this['matrixHeight'] / 55) + 1;
        this['matrixWidth'] = this['matrixHeight'] / 2 + this['redBarWidth'];
        this['slotWidth'] = (this['isExtended']) ? this['matrixWidth'] * 1.7413 : this['matrixWidth'];
        _0x3d81x289 = this['slotWidth'] * _0x3d81x283 + (_0x3d81x283 - 1) * _0x3d81x287
    };
    this['slotHeight'] = this['matrixHeight'] + this['nameHeight'] * _0x3d81x28f;
    this['realHeight'] = this['slotHeight'] * this['rowCount'] + _0x3d81x286 * (this['rowCount'] - 1);
    this['liveBlockSize'] = (this['matrixHeight'] / 20);
    this['holdQueueBlockSize'] = (this['isExtended']) ? this['liveBlockSize'] * 0.8 : 0;
    var _0x3d81x28b = Math['floor']((this['w'] - _0x3d81x289) / 2);
    if (_0x3d81x28c) {
        return
    };
    var _0x3d81x70 = 0;
    for (var _0x3d81x219 = 0; _0x3d81x219 < this['rowCount']; _0x3d81x219++) {
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x283; _0x3d81x13++) {
            var _0x3d81x19 = _0x3d81x28b + _0x3d81x13 * (this['slotWidth'] + _0x3d81x287);
            var _0x3d81x18 = _0x3d81x219 * (this['slotHeight'] + _0x3d81x286);
            if (_0x3d81x70 >= this['slots']['length']) {
                this['slots'][_0x3d81x70] = new Slot(_0x3d81x70, _0x3d81x19, _0x3d81x18, this)
            } else {
                this['slots'][_0x3d81x70]['x'] = _0x3d81x19;
                this['slots'][_0x3d81x70]['y'] = _0x3d81x18;
                this['slots'][_0x3d81x70]['init']()
            };
            _0x3d81x70++
        }
    };
    this['shownSlots'] = _0x3d81x70;
    while (_0x3d81x70 < this['slots']['length']) {
        this['slots'][_0x3d81x70]['hide']();
        _0x3d81x70++
    };
    this['resizeElements']()
};
GameSlots['prototype']['chatMaxH'] = function() {
    var _0x3d81x13, _0x3d81x290 = 57;
    for (_0x3d81x13 = 600; _0x3d81x13 <= 2000; _0x3d81x13 += 50) {
        if (_0x3d81x13 - this['realHeight'] >= _0x3d81x290) {
            return _0x3d81x13
        }
    };
    return _0x3d81x13
};
GameSlots['prototype']['resizeElements'] = function() {
    var _0x3d81x291 = this['resultsShown'];
    if (this['rowCount'] > 1 || this['shownSlots'] === 1) {
        _0x3d81x291 = false
    };
    var _0x3d81x292 = this['realHeight'] + ((_0x3d81x291) ? this['resultsBox']['offsetHeight'] : 0);
    this['gsDiv']['style']['height'] = _0x3d81x292 + 'px';
    this['lobbyBox']['style']['height'] = Math['max'](_0x3d81x292 + 7, 500) + 'px';
    this['resultsBox']['style']['maxHeight'] = (_0x3d81x292 - 5 + 14) + 'px';
    if (_0x3d81x291) {
        if (this['shownSlots'] === 1) {
            this['resultsBox']['style']['maxHeight'] = '140px'
        } else {
            if (this['shownSlots'] === 2) {
                this['resultsBox']['style']['maxHeight'] = '180px'
            } else {
                this['resultsBox']['style']['maxHeight'] = '240px'
            }
        }
    };
    if (!this['chatExpanded']) {
        this['chatBox']['style']['height'] = (this['chatMaxH']() - _0x3d81x292) + 'px';
        this['chatBox']['scrollTop'] = this['chatBox']['scrollHeight']
    }
};
GameSlots['prototype']['resetAll'] = function() {
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['slots']['length']; _0x3d81x13++) {
        this['slots'][_0x3d81x13]['vacantClear']()
    };
    this['cidSlots'] = {}
};
GameSlots['prototype']['reset'] = function() {
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['slots']['length']; _0x3d81x13++) {
        this['slots'][_0x3d81x13]['clear']();
        this['slots'][_0x3d81x13]['slotDiv']['classList']['remove']('np')
    }
};
GameSlots['prototype']['setTarget'] = function(_0x3d81x7) {
    if (this['targetSlotId'] === _0x3d81x7) {
        return
    } else {
        if (this['targetSlotId'] !== -1) {
            this['slots'][this['targetSlotId']]['setIsTargetted'](false)
        }
    };
    this['targetSlotId'] = _0x3d81x7;
    if (_0x3d81x7 !== -1) {
        this['slots'][_0x3d81x7]['setIsTargetted'](true)
    }
};
GameSlots['prototype']['CID'] = function(_0x3d81x20) {
    return this['slots'][this['cidSlots'][_0x3d81x20]]
};
GameSlots['prototype']['getSlot'] = function(_0x3d81x20, _0x3d81x198) {
    var _0x3d81x13, _0x3d81x187 = null;
    _0x3d81x187 = (typeof _0x3d81x198 === 'undefined') ? null : _0x3d81x198;
    if (_0x3d81x187 !== null && this['teamMembers'][_0x3d81x198]['indexOf'](_0x3d81x20) === -1) {
        this['teamMembers'][_0x3d81x198]['push'](_0x3d81x20)
    };
    for (_0x3d81x13 = 0; _0x3d81x13 < this['slots']['length']; _0x3d81x13++) {
        if (this['slots'][_0x3d81x13]['cid'] === -1) {
            this['slots'][_0x3d81x13]['cid'] = _0x3d81x20;
            this['cidSlots'][_0x3d81x20] = _0x3d81x13;
            return true
        }
    };
    this['slots'][_0x3d81x13] = new Slot(_0x3d81x13, 0, 0, this);
    this['slots'][_0x3d81x13]['cid'] = _0x3d81x20;
    this['slots'][_0x3d81x13]['hide']();
    this['cidSlots'][_0x3d81x20] = _0x3d81x13;
    return true
};
GameSlots['prototype']['chatExpand'] = function() {
    this['chatExpanded'] = !this['chatExpanded'];
    if (this['chatExpanded']) {
        this['gsDiv']['style']['display'] = 'none';
        this['chatBox']['style']['height'] = (this['chatMaxH']()) + 'px';
        this['chatBox']['scrollTop'] = this['chatBox']['scrollHeight'];
        this['chatExp']['setAttribute']('data-original-title', 'Minify')
    } else {
        this['gsDiv']['style']['display'] = 'block';
        this['resizeElements']();
        this['chatExp']['setAttribute']('data-original-title', 'Expand')
    };
    this['chatExp']['classList']['toggle']('exUp');
    this['chatExp']['classList']['toggle']('exDown');
    if (this['chatExp']['hasAttribute']('aria-describedby')) {
        let _0x3d81x293 = this['chatExp']['getAttribute']('aria-describedby');
        hideElem(document['getElementById'](_0x3d81x293))
    }
};

function Slot(_0x3d81x7, _0x3d81x19, _0x3d81x18, _0x3d81x9d) {
    this['gs'] = _0x3d81x9d;
    this['id'] = _0x3d81x7;
    this['cid'] = -1;
    this['x'] = _0x3d81x19;
    this['y'] = _0x3d81x18;
    this['pCan'] = document['createElement']('canvas');
    this['bgCan'] = document['createElement']('canvas');
    this['holdCan'] = document['createElement']('canvas');
    this['queueCan'] = document['createElement']('canvas');
    this['pCan']['classList']['add']('layer', 'mainLayer');
    this['bgCan']['classList']['add']('layer', 'bgLayer');
    this['holdCan']['classList']['add']('layer', 'mp-holdCan');
    this['queueCan']['classList']['add']('layer', 'mp-queueCan');
    this['name'] = document['createElement']('span');
    this['slotDiv'] = document['createElement']('div');
    this['stageDiv'] = document['createElement']('div');
    this['stats'] = new SlotStats(this, this['gs']);
    var _0x3d81x295 = {
        "\x6D\x61\x69\x6E": this['pCan'],
        "\x62\x67": this['bgCan'],
        "\x68\x6F\x6C\x64": this['holdCan'],
        "\x71\x75\x65\x75\x65": this['queueCan']
    };
    this['v'] = new SlotView(_0x3d81x295);
    this['v']['g'] = this['gs']['p'];
    this['v']['slot'] = this;
    this['init']()
}
Slot['prototype']['init'] = function() {
    this['slotDiv']['className'] = 'slot';
    this['slotDiv']['style']['left'] = this['x'] + 'px';
    this['slotDiv']['style']['top'] = this['y'] + 'px';
    this['stageDiv']['style']['position'] = 'relative';
    this['name']['style']['width'] = (this['gs']['matrixWidth'] + 2) + 'px';
    this['name']['style']['height'] = this['gs']['nameHeight'] + 'px';
    this['name']['style']['fontSize'] = this['gs']['nameFontSize'] + 'px';
    this['pCan']['width'] = this['bgCan']['width'] = this['gs']['matrixWidth'];
    this['pCan']['height'] = this['bgCan']['height'] = this['gs']['matrixHeight'];
    this['queueCan']['width'] = this['holdCan']['width'] = this['gs']['holdQueueBlockSize'] * 4;
    this['holdCan']['height'] = this['gs']['holdQueueBlockSize'] * 4;
    this['queueCan']['height'] = this['gs']['holdQueueBlockSize'] * 15;
    this['pCan']['style']['top'] = this['bgCan']['style']['top'] = this['holdCan']['style']['top'] = this['queueCan']['style']['top'] = this['gs']['nameHeight'] + 'px';
    this['holdCan']['style']['left'] = '0px';
    var _0x3d81x296 = this['gs']['holdQueueBlockSize'] * 0.8;
    var _0x3d81x297 = this['gs']['holdQueueBlockSize'] * 4 + _0x3d81x296;
    this['name']['style']['left'] = _0x3d81x297 + 'px';
    this['pCan']['style']['left'] = this['bgCan']['style']['left'] = _0x3d81x297 + 'px';
    this['queueCan']['style']['left'] = _0x3d81x297 + this['pCan']['width'] + _0x3d81x296 + 'px';
    if (this['gs']['slotStats'] && this['gs']['matrixWidth'] >= 50) {
        this['stats']['init']();
        this['stats']['statsDiv']['style']['left'] = _0x3d81x297 + 'px';
        this['slotDiv']['appendChild'](this['stats']['statsDiv']);
        let _0x3d81x298 = this['stats']['statsDiv']['childNodes'][0]['clientWidth'] * 1.10;
        let _0x3d81x299 = (_0x3d81x298 * 2 < this['gs']['matrixWidth'] * 0.85) || (_0x3d81x298 > this['gs']['matrixWidth'] * 0.6);
        this['stats']['winCounter']['style']['display'] = _0x3d81x299 ? null : 'none'
    } else {
        this['stats']['disable']()
    };
    this['slotDiv']['appendChild'](this['name']);
    this['slotDiv']['appendChild'](this['stageDiv']);
    this['stageDiv']['appendChild'](this['bgCan']);
    this['stageDiv']['appendChild'](this['pCan']);
    this['stageDiv']['appendChild'](this['holdCan']);
    this['stageDiv']['appendChild'](this['queueCan']);
    this['slotDiv']['style']['display'] = 'block';
    this['gs']['gsDiv']['appendChild'](this['slotDiv']);
    this['v']['onResized']()
};
Slot['prototype']['clear'] = function() {
    this['v']['clearMainCanvas']();
    this['v']['clearHoldCanvas']();
    this['v']['clearQueueCanvas']()
};
Slot['prototype']['vacantClear'] = function() {
    delete this['gs']['cidSlots'][this['cid']];
    this['cid'] = -1;
    this['clear']();
    this['setName']('');
    this['slotDiv']['classList']['remove']('np');
    this['stats']['winCounter']['style']['display'] = 'none';
    this['stats']['winCounter']['textContent'] = '0'
};
Slot['prototype']['hide'] = function() {
    this['slotDiv']['style']['display'] = 'none'
};
Slot['prototype']['setName'] = function(_0x3d81xac) {
    this['name']['innerHTML'] = _0x3d81xac
};
Slot['prototype']['setIsTargetted'] = function(_0x3d81x30) {
    if (_0x3d81x30) {
        this['slotDiv']['classList']['add']('target')
    } else {
        this['slotDiv']['classList']['remove']('target')
    }
};

function SlotStats(_0x3d81x18b, _0x3d81x9d) {
    this['slot'] = _0x3d81x18b;
    this['gs'] = _0x3d81x9d;
    this['statsDiv'] = document['createElement']('div');
    this['statsDiv']['classList']['add']('stat', 'unsel');
    this['pps'] = document['createElement']('span');
    this['apm'] = document['createElement']('span');
    this['ppsTitle'] = document['createElement']('span');
    this['apmTitle'] = document['createElement']('span');
    var _0x3d81x29b = document['createElement']('div');
    _0x3d81x29b['classList']['add']('statLine');
    var _0x3d81x29c = document['createElement']('div');
    _0x3d81x29c['classList']['add']('statLine');
    this['winCounter'] = document['createElement']('span');
    this['winCounter']['classList']['add']('wins');
    this['apmTitle']['classList']['add']('ti');
    this['ppsTitle']['classList']['add']('ti');
    this['winCounter']['classList']['add']('ti');
    this['ppsTitle']['textContent'] = 'PPS';
    this['apmTitle']['textContent'] = 'APM';
    this['winCounter']['textContent'] = '0';
    this['pps']['textContent'] = this['apm']['textContent'] = '0.00';
    _0x3d81x29b['appendChild'](this['ppsTitle']);
    _0x3d81x29b['appendChild'](this['pps']);
    _0x3d81x29c['appendChild'](this['apmTitle']);
    _0x3d81x29c['appendChild'](this['apm']);
    this['statsDiv']['appendChild'](_0x3d81x29b);
    this['statsDiv']['appendChild'](_0x3d81x29c);
    this['statsDiv']['appendChild'](this['winCounter'])
}
SlotStats['prototype']['init'] = function() {
    this['statsDiv']['style']['display'] = 'block';
    this['statsDiv']['style']['top'] = (this['gs']['nameHeight'] + this['gs']['matrixHeight']) + 'px';
    this['statsDiv']['style']['height'] = this['gs']['nameHeight'] + 'px';
    var _0x3d81x29d = Math['min'](this['gs']['nameFontSize'], Math['floor'](this['gs']['matrixWidth'] / 8));
    this['statsDiv']['style']['fontSize'] = _0x3d81x29d + 'px';
    let _0x3d81x272 = this['gs']['redBarWidth'] * 2;
    this['statsDiv']['style']['width'] = this['gs']['matrixWidth'] + _0x3d81x272 + 'px';
    this['winCounter']['style']['marginRight'] = _0x3d81x272 - 2 + 'px'
};
SlotStats['prototype']['disable'] = function() {
    this['statsDiv']['style']['display'] = 'none'
};
SlotStats['prototype']['update'] = function(_0x3d81x1b6, _0x3d81x1b4) {
    this['pps']['textContent'] = _0x3d81x1b6['toFixed'](2);
    this['apm']['textContent'] = _0x3d81x1b4['toFixed'](2)
};

function SlotView(_0x3d81x295) {
    this['g'] = null;
    this['slot'] = null;
    this['MAIN'] = 0;
    this['HOLD'] = 1;
    this['QUEUE'] = 2;
    this['canvas'] = _0x3d81x295['main'];
    this['ctx'] = this['canvas']['getContext']('2d');
    this['bgCanvas'] = _0x3d81x295['bg'];
    this['bgctx'] = this['bgCanvas']['getContext']('2d');
    this['holdCanvas'] = _0x3d81x295['hold'];
    this['hctx'] = this['holdCanvas']['getContext']('2d');
    this['queueCanvas'] = _0x3d81x295['queue'];
    this['qctx'] = this['queueCanvas']['getContext']('2d');
    this['block_size'] = 24;
    this['holdQueueBlockSize'] = 24;
    this['drawScale'] = 1;
    this['QueueHoldEnabled'] = false;
    this['SEenabled'] = false;
    this['replaySEset'] = 0;
    this['tex'] = new Image();
    this['skinId'] = 0;
    this['ghostSkinId'] = 0;
    this['skinWidth'] = 32;
    this['redrawBlocked'] = false;
    this['ghostEnabled'] = true
}
SlotView['prototype']['changeSkin'] = function(_0x3d81x7, _0x3d81x29f) {
    if (_0x3d81x7 === 7 && (typeof _0x3d81x29f !== 'undefined')) {
        this['g']['monochromeSkin'] = _0x3d81x29f;
        _0x3d81x7 = 0
    } else {
        this['g']['monochromeSkin'] = false
    };
    if (_0x3d81x7 < 1000 && (typeof this['g']['skins'][_0x3d81x7] === 'undefined' || !this['g']['skins'][_0x3d81x7]['data'])) {
        _0x3d81x7 = 0
    };
    if (_0x3d81x7 >= 1000 && !this['customSkinPath']) {
        console['log']('Skin unavailable - ' + _0x3d81x7);
        _0x3d81x7 = 0
    };
    if (this['slot']['gs']['skinOverride']) {
        _0x3d81x7 = 1;
        this['g']['skins'][1] = this['slot']['gs']['skinOverride']
    };
    this['skinId'] = _0x3d81x7;
    if (_0x3d81x7 > 0) {
        let _0x3d81x5;
        if (_0x3d81x7 >= 1000) {
            _0x3d81x5 = '/res/b/' + this['customSkinPath'] + '.png';
            this['g']['skins'][_0x3d81x7] = {
                id: _0x3d81x7,
                data: _0x3d81x5,
                w: 32
            };
            this['skinWidth'] = 32
        } else {
            _0x3d81x5 = this['g']['skins'][_0x3d81x7]['data'];
            try {
                this['skinWidth'] = this['g']['skins'][_0x3d81x7]['w']
            } catch (e) {
                this['skinWidth'] = 32
            }
        };
        this['tex']['src'] = _0x3d81x5
    }
};
SlotView['prototype']['loadResources'] = function() {};
SlotView['prototype']['drawBgGrid'] = function() {
    var _0x3d81x2a0 = this['slot']['gs']['matrixWidth'] - this['slot']['gs']['redBarWidth'] + 1;
    this['bgctx']['clearRect'](0, 0, this['canvas']['width'], this['canvas']['height']);
    this['bgctx']['beginPath']();
    this['bgctx']['lineWidth'] = 1;
    for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
        this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
        this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
    };
    for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
        this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
        this['bgctx']['lineTo'](_0x3d81x2a0, _0x3d81x13 * this['block_size'] + 0.5)
    };
    this['bgctx']['strokeStyle'] = (this['slot']['gs']['shownSlots'] > 6) ? '#0c0c0c' : '#101010';
    this['bgctx']['stroke']();
    this['bgctx']['beginPath']();
    for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
        for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
            this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5);
            this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5)
        }
    };
    for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
        for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
            this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] + 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
            this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] + 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
        }
    };
    this['bgctx']['strokeStyle'] = (this['slot']['gs']['shownSlots'] > 6) ? '#1c1c1c' : '#202020';
    this['bgctx']['stroke']();
    this['bgctx']['beginPath']();
    this['bgctx']['strokeStyle'] = '#393939';
    if (this['slot']['gs']['rowCount'] === 1) {
        this['bgctx']['lineWidth'] = 2;
        this['bgctx']['strokeRect'](1, 1, _0x3d81x2a0 - 1, this['canvas']['height'] - 2)
    } else {
        this['bgctx']['lineWidth'] = 1;
        this['bgctx']['strokeStyle'] = '#1c1c1c';
        this['drawLine'](this['bgctx'], _0x3d81x2a0 - 0.5, 0, _0x3d81x2a0 - 0.5, this['slot']['gs']['matrixHeight'])
    }
};
SlotView['prototype']['drawGhostAndCurrent'] = function() {
    var _0x3d81x34 = this['g']['blockSets'][this['g']['activeBlock']['set']],
        _0x3d81x35 = (_0x3d81x34['scale'] === 1) ? _0x3d81x34['blocks'][this['g']['activeBlock']['id']]['blocks'][this['g']['activeBlock']['rot']] : _0x3d81x34['previewAs']['blocks'][this['g']['activeBlock']['id']]['blocks'][this['g']['activeBlock']['rot']],
        _0x3d81x36 = _0x3d81x35['length'];
    this['drawScale'] = _0x3d81x34['scale'];
    if (this['ghostEnabled']) {
        for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
            for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
                if (_0x3d81x35[_0x3d81x18][_0x3d81x19] > 0) {
                    this['drawGhostBlock'](this['g']['ghostPiece']['pos']['x'] + _0x3d81x19 * this['drawScale'], this['g']['ghostPiece']['pos']['y'] + _0x3d81x18 * this['drawScale'], _0x3d81x34['blocks'][this['g']['activeBlock']['id']]['color'])
                }
            }
        }
    };
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x36; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x36; _0x3d81x19++) {
            if (_0x3d81x35[_0x3d81x18][_0x3d81x19] > 0) {
                this['drawBlock'](this['g']['activeBlock']['pos']['x'] + _0x3d81x19 * this['drawScale'], this['g']['activeBlock']['pos']['y'] + _0x3d81x18 * this['drawScale'], _0x3d81x34['blocks'][this['g']['activeBlock']['id']]['color'])
            }
        }
    };
    this['drawScale'] = 1
};
SlotView['prototype']['redraw'] = function() {
    if (this['redrawBlocked'] || !this['g']) {
        return
    };
    this['clearMainCanvas']();
    if (!this['g']['isInvisibleSkin']) {
        for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
            for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
                this['drawBlock'](_0x3d81x19, _0x3d81x18, this['g']['matrix'][_0x3d81x18][_0x3d81x19])
            }
        }
    };
    this['drawGhostAndCurrent']();
    var _0x3d81x2a1 = this['slot']['gs']['matrixWidth'] - this['slot']['gs']['redBarWidth'] + 1;
    if (this['g']['redBar'] > 0) {
        this['drawRectangle'](this['ctx'], _0x3d81x2a1, (20 - this['g']['redBar']) * this['block_size'], this['slot']['gs']['redBarWidth'], this['g']['redBar'] * this['block_size'], '#FF270F')
    };
    this['afterRedraw']()
};
SlotView['prototype']['clearMainCanvas'] = function() {
    this['ctx']['clearRect'](0, 0, this['canvas']['width'], this['canvas']['height'])
};
SlotView['prototype']['clearHoldCanvas'] = function() {
    this['hctx']['clearRect'](0, 0, this['holdCanvas']['width'], this['holdCanvas']['height'])
};
SlotView['prototype']['clearQueueCanvas'] = function() {
    this['qctx']['clearRect'](0, 0, this['queueCanvas']['width'], this['queueCanvas']['height'])
};
SlotView['prototype']['drawBlockOnCanvas'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97, _0x3d81x98, _0x3d81x123) {
    _0x3d81x123 = _0x3d81x123 || this['holdQueueBlockSize'];
    var _0x3d81x99 = null;
    if (_0x3d81x98 === this['MAIN']) {
        _0x3d81x99 = this['ctx']
    } else {
        if (_0x3d81x98 === this['HOLD']) {
            _0x3d81x99 = this['hctx']
        } else {
            _0x3d81x99 = this['qctx']
        }
    };
    if (this['skinId'] === 0) {
        var _0x3d81x9a = (this['g']['monochromeSkin'] && _0x3d81x97 <= 7) ? this['g']['monochromeSkin'] : this['g']['colors'][_0x3d81x97];
        this['drawRectangle'](_0x3d81x99, _0x3d81x19 * _0x3d81x123, _0x3d81x18 * _0x3d81x123, _0x3d81x123, _0x3d81x123, _0x3d81x9a)
    } else {
        let _0x3d81x6 = this['skinWidth'];
        _0x3d81x99['drawImage'](this['tex'], this['g']['coffset'][_0x3d81x97] * _0x3d81x6, 0, _0x3d81x6, _0x3d81x6, _0x3d81x19 * _0x3d81x123, _0x3d81x18 * _0x3d81x123, _0x3d81x123, _0x3d81x123)
    }
};
SlotView['prototype']['drawBlock'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97) {
    if (_0x3d81x97 && _0x3d81x19 >= 0 && _0x3d81x18 >= 0 && _0x3d81x19 < 10 && _0x3d81x18 < 20) {
        var _0x3d81x9c = this['drawScale'] * this['block_size'];
        if (this['skinId']) {
            let _0x3d81x6 = this['skinWidth'];
            this['ctx']['drawImage'](this['tex'], this['g']['coffset'][_0x3d81x97] * _0x3d81x6, 0, _0x3d81x6, _0x3d81x6, _0x3d81x19 * this['block_size'], _0x3d81x18 * this['block_size'], _0x3d81x9c, _0x3d81x9c)
        } else {
            var _0x3d81x9a = (this['g']['monochromeSkin'] && _0x3d81x97 <= 7) ? this['g']['monochromeSkin'] : this['g']['colors'][_0x3d81x97];
            this['drawRectangle'](this['ctx'], _0x3d81x19 * this['block_size'], _0x3d81x18 * this['block_size'], _0x3d81x9c, _0x3d81x9c, _0x3d81x9a)
        }
    }
};
SlotView['prototype']['drawGhostBlock'] = function(_0x3d81x19, _0x3d81x18, _0x3d81x97) {
    if (_0x3d81x19 >= 0 && _0x3d81x18 >= 0 && _0x3d81x19 < 10 && _0x3d81x18 < 20) {
        var _0x3d81x9c = this['drawScale'] * this['block_size'];
        if (this['ghostSkinId'] === 0) {
            this['ctx']['globalAlpha'] = 0.5;
            if (this['skinId'] > 0) {
                let _0x3d81x6 = this['skinWidth'];
                this['ctx']['drawImage'](this['tex'], this['g']['coffset'][_0x3d81x97] * _0x3d81x6, 0, _0x3d81x6, _0x3d81x6, _0x3d81x19 * this['block_size'], _0x3d81x18 * this['block_size'], _0x3d81x9c, _0x3d81x9c)
            } else {
                this['drawBlock'](_0x3d81x19, _0x3d81x18, _0x3d81x97)
            };
            this['ctx']['globalAlpha'] = 1
        } else {
            var _0x3d81x9d = this['ghostSkins'][this['ghostSkinId']];
            this['ctx']['drawImage'](this['ghostTex'], (this['g']['coffset'][_0x3d81x97] - 2) * _0x3d81x9d['w'], 0, _0x3d81x9d['w'], _0x3d81x9d['w'], _0x3d81x19 * this['block_size'], _0x3d81x18 * this['block_size'], _0x3d81x9c, _0x3d81x9c)
        }
    }
};
SlotView['prototype']['drawRectangle'] = function(_0x3d81x99, _0x3d81x6b, _0x3d81x6c, _0x3d81x9e, _0x3d81x9f, _0x3d81x9a) {
    _0x3d81x99['beginPath']();
    _0x3d81x99['rect'](_0x3d81x6b, _0x3d81x6c, _0x3d81x9e, _0x3d81x9f);
    _0x3d81x99['fillStyle'] = _0x3d81x9a;
    _0x3d81x99['fill']()
};
SlotView['prototype']['drawLine'] = function(_0x3d81x99, _0x3d81xa0, _0x3d81xa1, _0x3d81xa2, _0x3d81xa3) {
    _0x3d81x99['beginPath']();
    _0x3d81x99['moveTo'](_0x3d81xa0, _0x3d81xa1);
    _0x3d81x99['lineTo'](_0x3d81xa2, _0x3d81xa3);
    _0x3d81x99['stroke']()
};
SlotView['prototype']['paintMatrixWithColor'] = function(_0x3d81x47) {
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; _0x3d81x18++) {
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; _0x3d81x19++) {
            if (this['g']['matrix'][_0x3d81x18][_0x3d81x19] > 0) {
                this['g']['matrix'][_0x3d81x18][_0x3d81x19] = _0x3d81x47
            }
        }
    }
};
SlotView['prototype']['updateLiveMatrix'] = function(_0x3d81x38, _0x3d81x39) {
    this['clearMainCanvas']();
    var _0x3d81x79 = _0x3d81x38['length'];
    for (var _0x3d81x18 = 0; _0x3d81x18 < _0x3d81x79; _0x3d81x18++) {
        var _0x3d81x1e6 = _0x3d81x38[_0x3d81x18]['length'];
        for (var _0x3d81x19 = 0; _0x3d81x19 < _0x3d81x1e6; _0x3d81x19++) {
            if (_0x3d81x38[_0x3d81x18][_0x3d81x19] > 0) {
                this['drawBlockOnCanvas'](_0x3d81x19, _0x3d81x18, _0x3d81x38[_0x3d81x18][_0x3d81x19], this.MAIN, this['slot']['gs']['liveBlockSize'])
            }
        }
    };
    var _0x3d81x2a1 = this['slot']['gs']['matrixWidth'] - this['slot']['gs']['redBarWidth'] + 1;
    this['drawLine'](this['ctx'], _0x3d81x2a1, 0, _0x3d81x2a1, this['slot']['gs']['matrixHeight']);
    this['drawRectangle'](this['ctx'], _0x3d81x2a1, 0, this['slot']['gs']['redBarWidth'], this['slot']['gs']['matrixHeight'], 'black');
    if (_0x3d81x39 > 0) {
        this['drawRectangle'](this['ctx'], _0x3d81x2a1, (20 - _0x3d81x39) * this['slot']['gs']['liveBlockSize'], this['slot']['gs']['redBarWidth'], _0x3d81x39 * this['slot']['gs']['liveBlockSize'], '#FF270F')
    };
    this['afterRedraw']()
};
SlotView['prototype']['updateTextBar'] = function() {
    if (this['slot']['gs']['slotStats']) {
        var _0x3d81x2a2 = this['slot']['gs']['p']['timestamp']() - this['restartedAt'],
            _0x3d81x1b6 = Math['round'](100 * this['g']['placedBlocks'] / (_0x3d81x2a2 / 1000)) / 100,
            _0x3d81x1b4 = Math['round'](100 * this['g']['gamedata']['linesSent'] / (_0x3d81x2a2 / 60000)) / 100;
        this['slot']['stats']['update'](_0x3d81x1b6, _0x3d81x1b4)
    }
};
SlotView['prototype']['onCreate'] = function() {};
SlotView['prototype']['onReady'] = function() {
    this['g']['updateQueueBox']();
    this['g']['redrawHoldBox']();
    this['clearMainCanvas']()
};
SlotView['prototype']['onRestart'] = function() {
    this['g']['redrawHoldBox']();
    this['redraw']();
    this['restartedAt'] = this['slot']['gs']['p']['timestamp']();
    if (this['slot']['gs']['slotStats']) {
        this['slot']['stats']['update'](0, 0)
    }
};
SlotView['prototype']['onBlockHold'] = function() {
    this['g']['redrawHoldBox']();
    this['redraw']()
};
SlotView['prototype']['onBlockMove'] = function(_0x3d81x2a3) {
    this['redraw']()
};
SlotView['prototype']['onFinesseChange'] = function() {};
SlotView['prototype']['onGameOver'] = function() {
    this['paintMatrixWithColor'](9)
};
SlotView['prototype']['onBlockLocked'] = function() {};
SlotView['prototype']['onLinesCleared'] = function() {};
SlotView['prototype']['onScoreChanged'] = function() {};
SlotView['prototype']['onResized'] = function() {
    this['block_size'] = this['slot']['gs']['liveBlockSize'];
    this['holdQueueBlockSize'] = this['slot']['gs']['holdQueueBlockSize'];
    this['drawBgGrid']();
    this['clearMainCanvas']();
    if (this['slot']['gs']['isExtended']) {
        this['QueueHoldEnabled'] = true;
        this['holdCanvas']['style']['display'] = 'block';
        this['queueCanvas']['style']['display'] = 'block'
    } else {
        this['QueueHoldEnabled'] = false;
        this['holdCanvas']['style']['display'] = 'none';
        this['queueCanvas']['style']['display'] = 'none'
    }
};
SlotView['prototype']['printTextBg'] = function() {
    this['ctx']['save']();
    this['ctx']['globalAlpha'] = 0.8;
    this['ctx']['beginPath']();
    var _0x3d81x2a4 = (this['slot']['gs']['shownSlots'] === 1) ? (this['slot']['gs']['matrixHeight'] * (7 / 20)) : (this['slot']['gs']['matrixHeight'] * (13 / 20));
    this['ctx']['rect'](0, Math['round'](_0x3d81x2a4), this['slot']['gs']['matrixWidth'], Math['round'](this['slot']['gs']['matrixHeight'] / 5.7));
    this['ctx']['fillStyle'] = '#090909';
    this['ctx']['fill']();
    this['ctx']['restore']()
};
SlotView['prototype']['printSlotPlace'] = function(_0x3d81x28) {
    var _0x3d81x4 = this['slot']['gs']['p']['getPlaceColor'](_0x3d81x28);
    this['printTextBg']();
    this['ctx']['fillStyle'] = _0x3d81x4['color'];
    var _0x3d81x2a5 = Math['round'](this['slot']['gs']['matrixHeight'] / 10);
    this['ctx']['font'] = 'bold ' + _0x3d81x2a5 + 'px "Exo 2"';
    this['ctx']['textAlign'] = 'center';
    var _0x3d81x2a4 = (this['slot']['gs']['shownSlots'] === 1) ? (this['slot']['gs']['matrixHeight'] * (94 / 200)) : (this['slot']['gs']['matrixHeight'] * (154 / 200));
    this['ctx']['fillText'](_0x3d81x4['str'], Math['round'](this['slot']['gs']['matrixWidth'] / 2), Math['round'](_0x3d81x2a4))
};
SlotView['prototype']['printSlotNotPlaying'] = function() {
    this['printTextBg']();
    this['ctx']['fillStyle'] = '#999999';
    var _0x3d81x2a5 = Math['round'](this['slot']['gs']['matrixHeight'] / 12);
    this['ctx']['font'] = 'bold ' + _0x3d81x2a5 + 'px "Exo 2"';
    this['ctx']['textAlign'] = 'center';
    var _0x3d81x2a4 = (this['slot']['gs']['shownSlots'] === 1) ? (this['slot']['gs']['matrixHeight'] * (95 / 200)) : (this['slot']['gs']['matrixHeight'] * (154 / 200));
    this['ctx']['fillText'](i18n['notPlaying'], Math['round'](this['slot']['gs']['matrixWidth'] / 2), Math['round'](_0x3d81x2a4))
};
SlotView['prototype']['afterRedraw'] = function() {
    var _0x3d81x2a6 = this['slot']['gs']['p']['Live']['places'];
    if (this['slot']['cid'] in _0x3d81x2a6) {
        if (_0x3d81x2a6[this['slot']['cid']]) {
            this['printSlotPlace'](_0x3d81x2a6[this['slot']['cid']])
        } else {
            this['printSlotNotPlaying']()
        }
    }
};

function StatsManager(_0x3d81x2a8) {
    this['stats'] = {};
    this['ordered'] = [];
    this['dirty'] = false;
    this['labelsElem'] = document['getElementById']('statLabels');
    this['shown'] = _0x3d81x2a8['g']['Settings']['shownStats'][_0x3d81x2a8['g']['isPmode'](false)];
    this['initDefault']();
    this['setView'](_0x3d81x2a8)
}
StatsManager['prototype']['setView'] = function(_0x3d81x2a9) {
    this['view'] = _0x3d81x2a9;
    this['ff'] = _0x3d81x2a9['createFastFont']();
    this['ff']['init'](this['render']['bind'](this))
};
StatsManager['prototype']['adjustToGameMode'] = function() {
    this['resetAll']();
    this['applyShownStats'](this['view']['g']['Settings']['shownStats'][this['view']['g']['isPmode'](false)]);
    this['render']()
};
StatsManager['prototype']['initDefault'] = function(_0x3d81x7) {
    this['addStat'](new StatLine('CLOCK', i18n['roundTime'], 0), false);
    this['addStat'](new StatLine('SCORE', i18n['score'], 10), false);
    this['addStat'](new StatLine('LINES', 'Lines', 15), false);
    this['addStat'](new StatLine('ATTACK', i18n['attack'], 20), false);
    this['addStat'](new StatLine('RECV', i18n['received'], 30), false);
    this['addStat'](new StatLine('FINESSE', i18n['finesse'], 40), false);
    this['addStat'](new StatLine('PPS', i18n.PPS, 50), false);
    this['addStat'](new StatLine('KPP', i18n.KPP, 60), false);
    this['addStat'](new StatLine('APM', i18n.APM, 70), false);
    this['addStat'](new StatLine('BLOCKS', '#', 80), false);
    this['addStat'](new StatLine('VS', 'VS', 90), false);
    this['addStat'](new StatLine('WASTE', 'Wasted', 100), false);
    this['addStat'](new StatLine('HOLD', 'Hold', 110), false);
    this['get']('CLOCK')['initialVal'] = '0.00';
    this['applyShownStats'](this['shown']);
    this['reorder']()
};
StatsManager['prototype']['applyShownStats'] = function(_0x3d81x2aa) {
    var _0x3d81x2ab = Object['keys'](this['stats']);
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x2ab['length']; _0x3d81x13++) {
        if (_0x3d81x2aa & (1 << _0x3d81x13)) {
            this['get'](_0x3d81x2ab[_0x3d81x13])['enable']()
        } else {
            this['get'](_0x3d81x2ab[_0x3d81x13])['disable']()
        }
    }
};
StatsManager['prototype']['get'] = function(_0x3d81x7) {
    return this['stats'][_0x3d81x7]
};
StatsManager['prototype']['render'] = function() {
    this['ff']['renderLines'](this['ordered']);
    this['dirty'] = false
};
StatsManager['prototype']['addStat'] = function(_0x3d81x2ac, _0x3d81x2ad) {
    if (_0x3d81x2ac['id'] in this['stats']) {
        return
    };
    this['stats'][_0x3d81x2ac['id']] = _0x3d81x2ac;
    _0x3d81x2ac['manager'] = this;
    _0x3d81x2ac['enabled'] = true;
    if (_0x3d81x2ad) {
        this['reorder']()
    }
};
StatsManager['prototype']['reorder'] = function() {
    var _0x3d81x2ae = this['ordered']['length'];
    this['ordered'] = Object['values'](this['stats']);
    this['ordered']['sort']((_0x3d81xa5, _0x3d81x141) => {
        if (!_0x3d81xa5['enabled']) {
            return 1
        };
        if (!_0x3d81x141['enabled']) {
            return -1
        };
        return (_0x3d81xa5['order'] > _0x3d81x141['order']) ? 1 : ((_0x3d81x141['order'] > _0x3d81xa5['order']) ? -1 : 0)
    });
    while (this['ordered']['length']) {
        if (!this['ordered'][this['ordered']['length'] - 1]['enabled']) {
            this['ordered']['pop']()
        } else {
            break
        }
    };
    while (this['labelsElem']['firstChild']) {
        this['labelsElem']['removeChild'](this['labelsElem']['firstChild'])
    };
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['ordered']['length']; ++_0x3d81x13) {
        if (this['ordered'][_0x3d81x13]['enabled']) {
            this['labelsElem']['appendChild'](this['ordered'][_0x3d81x13]['label'])
        }
    }
};
StatsManager['prototype']['resetAll'] = function() {
    for (var _0x3d81x14 in this['stats']) {
        this['stats'][_0x3d81x14]['reset']()
    };
    this['dirty'] = true
};

function StatLine(_0x3d81x7, _0x3d81x2b0, _0x3d81x2b1) {
    this['id'] = _0x3d81x7;
    this['order'] = _0x3d81x2b1;
    this['manager'] = null;
    this['enabled'] = false;
    this['label'] = document['createElement']('span');
    this['label']['textContent'] = _0x3d81x2b0;
    this['initialVal'] = this['value'] = '0';
    this['resets'] = true;
    this['locked'] = false
}
StatLine['prototype']['set'] = function(_0x3d81x30) {
    if (_0x3d81x30 === this['value']) {
        return this
    };
    this['value'] = _0x3d81x30;
    if (this['enabled']) {
        this['manager']['dirty'] = true
    };
    return this
};
StatLine['prototype']['enable'] = function() {
    this['enabled'] = true;
    this['label']['style']['display'] = null;
    return this
};
StatLine['prototype']['disable'] = function() {
    if (this['locked']) {
        return this
    };
    this['enabled'] = false;
    hideElem(this['label']);
    return this
};
StatLine['prototype']['reset'] = function() {
    if (!this['resets']) {
        return this
    };
    this['value'] = this['initialVal'];
    return this
};
StatLine['prototype']['setLock'] = function(_0x3d81x30) {
    this['locked'] = _0x3d81x30;
    return this
};
StatLine['prototype']['setOrder'] = function(_0x3d81x30) {
    this['order'] = _0x3d81x30;
    return this
};
'use strict';

function GameCaption(_0x3d81x22e) {
    this['parent'] = _0x3d81x22e;
    this['captions'] = {};
    this['SPECTATOR_MODE'] = 1;
    this['OUT_OF_FOCUS'] = 2;
    this['READY_GO'] = 3;
    this['GAME_PLACE'] = 4;
    this['SPEED_LIMIT'] = 5;
    this['MAP_LOADING'] = 6;
    this['NEW_PERSONAL_BEST'] = 7;
    this['LOADING'] = 8;
    this['RACE_FINISHED'] = 9;
    this['GAME_WARNING'] = 10;
    this['MODE_INFO'] = 11;
    this['MODE_COMPLETE'] = 12;
    this['PAUSED'] = 13;
    this['BUTTON'] = 14;
    this['speedTimout'] = null
}
GameCaption['prototype']['create'] = function() {
    var _0x3d81x2b3 = document['createElement']('div');
    this['parent']['appendChild'](_0x3d81x2b3);
    _0x3d81x2b3['style']['width'] = '242px';
    _0x3d81x2b3['classList']['add']('gCapt');
    return _0x3d81x2b3
};
GameCaption['prototype']['hide'] = function(_0x3d81x2b4) {
    if (typeof _0x3d81x2b4 === 'undefined') {
        for (var _0x3d81x14 in this['captions']) {
            this['captions'][_0x3d81x14]['style']['display'] = 'none'
        }
    } else {
        if (_0x3d81x2b4 in this['captions']) {
            this['captions'][_0x3d81x2b4]['style']['display'] = 'none'
        }
    }
};
GameCaption['prototype']['hideExcept'] = function(_0x3d81x7) {
    for (var _0x3d81x14 in this['captions']) {
        if (_0x3d81x14 == _0x3d81x7) {
            continue
        };
        this['captions'][_0x3d81x14]['style']['display'] = 'none'
    }
};
GameCaption['prototype']['spectatorMode'] = function() {
    this['hide']();
    if (this['SPECTATOR_MODE'] in this['captions']) {
        this['captions'][this['SPECTATOR_MODE']]['style']['display'] = 'block';
        return
    };
    var _0x3d81xba = this['captions'][this['SPECTATOR_MODE']] = this['create']();
    _0x3d81xba['style']['top'] = '288px';
    _0x3d81xba['style']['height'] = '73px';
    _0x3d81xba['style']['color'] = '#CBD600';
    var _0x3d81x174 = document['createElement']('div');
    var _0x3d81x2b5 = document['createElement']('div');
    _0x3d81x174['textContent'] = i18n['specMode'];
    _0x3d81x174['style']['fontSize'] = '22px';
    _0x3d81x174['style']['marginTop'] = '7px';
    _0x3d81x2b5['textContent'] = i18n['endSpec'];
    _0x3d81x2b5['style']['fontSize'] = '15px';
    _0x3d81x2b5['style']['marginTop'] = '7px';
    _0x3d81xba['appendChild'](_0x3d81x174);
    _0x3d81xba['appendChild'](_0x3d81x2b5)
};
GameCaption['prototype']['outOfFocus'] = function(_0x3d81xaf) {
    if (this['GAME_PLACE'] in this['captions'] && this['captions'][this['GAME_PLACE']]['style']['display'] === 'block') {
        return
    };
    if (this['OUT_OF_FOCUS'] in this['captions']) {
        this['captions'][this['OUT_OF_FOCUS']]['style']['display'] = 'block';
        return
    };
    var _0x3d81xba = this['captions'][this['OUT_OF_FOCUS']] = this['create']();
    let _0x3d81x61 = 168;
    if (_0x3d81xaf && _0x3d81xaf['top']) {
        _0x3d81x61 = _0x3d81xaf['top']
    };
    _0x3d81xba['style']['top'] = _0x3d81x61 + 'px';
    _0x3d81xba['style']['height'] = '97px';
    _0x3d81xba['style']['color'] = '#CBD600';
    var _0x3d81x174 = document['createElement']('div');
    var _0x3d81x2b5 = document['createElement']('div');
    _0x3d81x174['textContent'] = i18n['notFocused'];
    _0x3d81x174['style']['fontSize'] = '30px';
    _0x3d81x174['style']['marginTop'] = '11px';
    _0x3d81x2b5['textContent'] = i18n['clickToFocus'];
    _0x3d81x2b5['style']['fontSize'] = '16px';
    _0x3d81x2b5['style']['marginTop'] = '7px';
    _0x3d81xba['appendChild'](_0x3d81x174);
    _0x3d81xba['appendChild'](_0x3d81x2b5)
};
GameCaption['prototype']['readyGo'] = function(_0x3d81xb) {
    this['hideExcept'](this.MODE_INFO);
    if (this['READY_GO'] in this['captions']) {
        this['captions'][this['READY_GO']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['READY_GO']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '264px';
        _0x3d81xba['style']['height'] = '73px';
        _0x3d81xba['style']['color'] = '#CBD600';
        _0x3d81xba['style']['fontWeight'] = 'bold'
    };
    var _0x3d81xba = this['captions'][this['READY_GO']];
    _0x3d81xba['textContent'] = '';
    var _0x3d81x174 = document['createElement']('div');
    _0x3d81x174['style']['fontSize'] = '31px';
    _0x3d81x174['style']['marginTop'] = '15px';
    if (_0x3d81xb === 0) {
        _0x3d81x174['textContent'] = i18n['ready']
    } else {
        _0x3d81x174['textContent'] = i18n['go']
    };
    _0x3d81xba['appendChild'](_0x3d81x174)
};
GameCaption['prototype']['modeInfo'] = function(_0x3d81x157, _0x3d81xaf) {
    if (_0x3d81x157 == '') {
        this['hide'](this.MODE_INFO);
        return
    };
    if (this['MODE_INFO'] in this['captions']) {
        this['captions'][this['MODE_INFO']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['MODE_INFO']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '90px';
        _0x3d81xba['style']['color'] = 'rgb(107 180 255)';
        _0x3d81xba['style']['fontWeight'] = 'bold'
    };
    var _0x3d81xba = this['captions'][this['MODE_INFO']];
    _0x3d81xba['textContent'] = '';
    _0x3d81xba['style']['top'] = '90px';
    if (_0x3d81xaf['t'] == 0) {
        var _0x3d81x2b6 = document['createElement']('div');
        _0x3d81x2b6['style']['fontSize'] = '20px';
        _0x3d81x2b6['style']['marginTop'] = '15px';
        _0x3d81x2b6['textContent'] = 'TASK:';
        _0x3d81x2b6['style']['color'] = '#CBD600';
        _0x3d81xba['appendChild'](_0x3d81x2b6)
    };
    var _0x3d81x261 = document['createElement']('div');
    _0x3d81x261['style']['fontSize'] = '19px';
    _0x3d81x261['style']['marginTop'] = '4px';
    _0x3d81x261['style']['marginBottom'] = '15px';
    _0x3d81x261['textContent'] = _0x3d81x157;
    _0x3d81xba['appendChild'](_0x3d81x261);
    if (_0x3d81xaf['t'] == 1) {
        _0x3d81xba['style']['top'] = '236px';
        _0x3d81x261['style']['marginBottom'] = '5px';
        _0x3d81xba['classList']['add']('transitionCaption');
        this._fadeOut(_0x3d81xba, 4000);
        _0x3d81x261['style']['color'] = 'yellow'
    }
};
GameCaption['prototype']['modeComplete'] = function(_0x3d81x157) {
    this['hide']();
    if (this['MODE_COMPLETE'] in this['captions']) {
        this['captions'][this['MODE_COMPLETE']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['MODE_COMPLETE']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '272px';
        _0x3d81xba['style']['color'] = '#00db00'
    };
    var _0x3d81xba = this['captions'][this['MODE_COMPLETE']];
    _0x3d81xba['textContent'] = '';
    var _0x3d81x174 = document['createElement']('div');
    _0x3d81x174['style']['fontSize'] = '27px';
    _0x3d81x174['style']['marginTop'] = '15px';
    _0x3d81x174['style']['marginBottom'] = '15px';
    if (!_0x3d81x157) {
        _0x3d81x174['innerHTML'] = '\u2714 Completed'
    } else {
        if (_0x3d81x157 === 1) {
            _0x3d81x174['innerHTML'] = '\u2714 All done! Nice.'
        }
    };
    _0x3d81xba['appendChild'](_0x3d81x174);
    _0x3d81x174['classList']['add']('fadeInTop')
};
GameCaption['prototype']['paused'] = function(_0x3d81xaf) {
    if (this['PAUSED'] in this['captions']) {
        this['captions'][this['PAUSED']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['PAUSED']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '387px';
        _0x3d81xba['style']['color'] = 'white';
        _0x3d81xba['style']['backgroundColor'] = null
    };
    var _0x3d81xba = this['captions'][this['PAUSED']];
    _0x3d81xba['textContent'] = '';
    var _0x3d81x174 = document['createElement']('div');
    _0x3d81x174['style']['fontSize'] = '20px';
    _0x3d81x174['style']['marginTop'] = '4px';
    _0x3d81x174['style']['marginBottom'] = '15px';
    _0x3d81x174['className'] = 'pausedSign';
    _0x3d81x174['innerHTML'] = 'PAUSED';
    _0x3d81xba['appendChild'](_0x3d81x174);
    _0x3d81x174['classList']['add']('fadeIn');
    if (_0x3d81xaf['skip']) {
        var _0x3d81x2b7 = document['createElement']('div');
        _0x3d81x2b7['style']['marginBottom'] = '4px';
        _0x3d81x2b7['style']['fontSize'] = '13px';
        _0x3d81x2b7['textContent'] = 'Any key to resume';
        _0x3d81x174['appendChild'](_0x3d81x2b7)
    };
    if (_0x3d81xaf['sec']) {
        var _0x3d81x2b8 = document['createElement']('div');
        _0x3d81x2b8['className'] = 'pauseProg';
        _0x3d81x2b8['style']['animationDuration'] = Math['round'](_0x3d81xaf['sec'])['toFixed'](1) + 's';
        _0x3d81x174['appendChild'](_0x3d81x2b8)
    }
};
GameCaption['prototype']['mapLoading'] = function(_0x3d81x157) {
    this['hide']();
    if (this['MAP_LOADING'] in this['captions']) {
        this['captions'][this['MAP_LOADING']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['MAP_LOADING']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '266px';
        _0x3d81xba['style']['height'] = '69px';
        _0x3d81xba['style']['color'] = 'white'
    };
    var _0x3d81xba = this['captions'][this['MAP_LOADING']];
    _0x3d81xba['textContent'] = '';
    var _0x3d81x231 = document['createElement']('img');
    _0x3d81x231['src'] = CDN_URL('/res/svg/spinWhite.svg');
    _0x3d81x231['style']['width'] = '30px';
    _0x3d81x231['style']['marginTop'] = '5px';
    _0x3d81xba['appendChild'](_0x3d81x231);
    var _0x3d81x174 = document['createElement']('div');
    _0x3d81x174['style']['fontSize'] = '22px';
    _0x3d81x174['style']['marginTop'] = '1px';
    _0x3d81x174['innerHTML'] = (!_0x3d81x157) ? i18n['mapLoading'] : 'Custom mode loading';
    _0x3d81xba['appendChild'](_0x3d81x174)
};
GameCaption['prototype']['button'] = function(_0x3d81xaf) {
    if (this['BUTTON'] in this['captions']) {
        this['captions'][this['BUTTON']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['BUTTON']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '372px';
        _0x3d81xba['style']['height'] = '59px';
        _0x3d81xba['style']['color'] = 'white'
    };
    var _0x3d81xba = this['captions'][this['BUTTON']];
    _0x3d81xba['innerHTML'] = '';
    var _0x3d81x199 = document['createElement']('a');
    _0x3d81x199['href'] = 'javascript:void(0);';
    _0x3d81x199['style']['marginTop'] = '8px';
    _0x3d81x199['classList']['add']('btnNX2', 'btnNX-lrg', 'green');
    _0x3d81x199['innerHTML'] = 'NEXT';
    if (_0x3d81xaf['handler']) {
        _0x3d81x199['addEventListener']('click', _0x3d81xaf['handler'])
    };
    _0x3d81xba['appendChild'](_0x3d81x199)
};
GameCaption['prototype']['gamePlace'] = function(_0x3d81x2b9) {
    this['hide'](this.OUT_OF_FOCUS);
    this['hide'](this.SPEED_LIMIT);
    if (this['GAME_PLACE'] in this['captions']) {
        this['captions'][this['GAME_PLACE']]['style']['display'] = 'block';
        this['captions'][this['GAME_PLACE']]['textContent'] = ''
    } else {
        var _0x3d81xba = this['captions'][this['GAME_PLACE']] = this['create']();
        _0x3d81xba['style']['opacity'] = 0.91;
        _0x3d81xba['style']['top'] = '168px';
        _0x3d81xba['style']['height'] = '97px';
        _0x3d81xba['style']['color'] = '#CBD600';
        _0x3d81xba['style']['fontWeight'] = 'bold'
    };
    var _0x3d81xba = this['captions'][this['GAME_PLACE']];
    var _0x3d81x4 = _0x3d81x2b9['getPlaceColor'](_0x3d81x2b9['place']);
    var _0x3d81x174 = document['createElement']('div');
    var _0x3d81x2b5 = document['createElement']('div');
    _0x3d81x174['textContent'] = _0x3d81x4['str'];
    _0x3d81x174['style']['fontSize'] = '32px';
    _0x3d81x174['style']['marginTop'] = '11px';
    _0x3d81x174['style']['color'] = _0x3d81x4['color'];
    _0x3d81x2b5['style']['fontSize'] = '16px';
    _0x3d81x2b5['style']['marginTop'] = '7px';
    if (_0x3d81x2b9['Live']['LiveGameRunning']) {
        _0x3d81x2b5['style']['color'] = 'white';
        _0x3d81x2b5['textContent'] = i18n['waitNext']
    } else {
        _0x3d81x2b5['style']['color'] = 'yellow';
        _0x3d81x2b5['style']['fontWeight'] = 'bold';
        _0x3d81x2b5['textContent'] = i18n['pressStart']
    };
    _0x3d81xba['appendChild'](_0x3d81x174);
    _0x3d81xba['appendChild'](_0x3d81x2b5)
};
GameCaption['prototype']['speedWarning'] = function(_0x3d81x90) {
    if (this['GAME_PLACE'] in this['captions'] && this['captions'][this['GAME_PLACE']]['style']['display'] === 'block') {
        return
    };
    if (this['SPEED_LIMIT'] in this['captions']) {
        this['captions'][this['SPEED_LIMIT']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['SPEED_LIMIT']] = this['create']();
        _0x3d81xba['style']['top'] = '216px';
        _0x3d81xba['style']['height'] = '97px';
        _0x3d81xba['style']['backgroundColor'] = 'red';
        _0x3d81xba['style']['fontWeight'] = 'bold';
        _0x3d81xba['classList']['add']('transitionCaption');
        var _0x3d81x174 = document['createElement']('div');
        _0x3d81x174['style']['fontSize'] = '31px';
        _0x3d81x174['style']['marginTop'] = '15px';
        _0x3d81x174['style']['color'] = 'white';
        _0x3d81x174['textContent'] = i18n['slowDown'];
        _0x3d81xba['appendChild'](_0x3d81x174);
        var _0x3d81x2ba = document['createElement']('div');
        _0x3d81x2ba['id'] = 'slSubT';
        _0x3d81x2ba['style']['fontSize'] = '16px';
        _0x3d81x2ba['style']['marginTop'] = '7px';
        _0x3d81x2ba['style']['color'] = 'white';
        _0x3d81x2ba['style']['fontWeight'] = 'normal';
        _0x3d81xba['appendChild'](_0x3d81x2ba)
    };
    var _0x3d81xba = this['captions'][this['SPEED_LIMIT']];
    _0x3d81xba['getElementsByTagName']('div')[1]['innerHTML'] = i18n['speedLimitIs'] + ' <b>' + _0x3d81x90['toFixed'](1) + '</b> PPS';
    if (this['speedTimout']) {
        window['clearTimeout'](this['speedTimout'])
    };
    this._fadeOut(_0x3d81xba, 300)
};
GameCaption['prototype']['_fadeOut'] = function(_0x3d81xba, _0x3d81x2bb, _0x3d81x2bc, _0x3d81x2bd) {
    if (typeof _0x3d81x2bc === 'undefined') {
        var _0x3d81x2bc = null
    };
    if (typeof _0x3d81x2bd === 'undefined') {
        var _0x3d81x2bd = 1
    };
    _0x3d81xba['classList']['remove']('transitionCaption');
    _0x3d81xba['classList']['add']('noTransition');
    _0x3d81xba['style']['opacity'] = _0x3d81x2bd;
    var _0x3d81x25 = this;
    this['speedTimout'] = window['setTimeout'](function() {
        _0x3d81xba['classList']['remove']('noTransition');
        _0x3d81xba['classList']['add']('transitionCaption');
        if (_0x3d81x2bc !== null) {
            _0x3d81xba['style']['transition'] = 'all ' + _0x3d81x2bc + 's ease-out'
        };
        _0x3d81xba['style']['opacity'] = 0;
        _0x3d81x25['speedTimout'] = null
    }, _0x3d81x2bb)
};
GameCaption['prototype']['newPB'] = function(_0x3d81x2be) {
    this['hide']();
    if (this['NEW_PERSONAL_BEST'] in this['captions']) {
        this['captions'][this['NEW_PERSONAL_BEST']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['NEW_PERSONAL_BEST']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '168px';
        _0x3d81xba['style']['paddingTop'] = '11px';
        _0x3d81xba['classList']['add']('transitionCaption');
        var _0x3d81x128 = document['createElement']('div');
        _0x3d81x128['style']['fontSize'] = '40px';
        _0x3d81x128['style']['fontWeight'] = 'bold';
        _0x3d81x128['style']['color'] = '#fafad2';
        _0x3d81xba['appendChild'](_0x3d81x128);
        var _0x3d81x174 = document['createElement']('div');
        _0x3d81x174['style']['fontSize'] = '31px';
        _0x3d81x174['style']['marginTop'] = '4px';
        _0x3d81x174['style']['color'] = 'yellow';
        _0x3d81x174['textContent'] = i18n['newPB'];
        _0x3d81xba['appendChild'](_0x3d81x174);
        var _0x3d81x2ba = document['createElement']('div');
        _0x3d81x2ba['id'] = 'slSubT';
        _0x3d81x2ba['style']['fontSize'] = '16px';
        _0x3d81x2ba['style']['marginTop'] = '12px';
        _0x3d81x2ba['style']['color'] = 'white';
        _0x3d81x2ba['style']['fontWeight'] = 'normal';
        _0x3d81xba['appendChild'](_0x3d81x2ba);
        var _0x3d81x2bf = document['createElement']('div');
        _0x3d81x2bf['className'] = 'gCapt';
        let _0x3d81x2c0 = _0x3d81x2bf['style'];
        _0x3d81x2c0['fontSize'] = '14px';
        _0x3d81x2c0['marginTop'] = '14px';
        _0x3d81x2c0['opacity'] = '1';
        _0x3d81x2c0['position'] = 'initial';
        _0x3d81x2c0['paddingBottom'] = '11px';
        _0x3d81x2c0['color'] = 'grey';
        _0x3d81xba['appendChild'](_0x3d81x2bf)
    };
    var _0x3d81xba = this['captions'][this['NEW_PERSONAL_BEST']];
    if (_0x3d81x2be === true) {
        _0x3d81xba['style']['height'] = '184px';
        _0x3d81xba['style']['top'] = '168px';
        _0x3d81xba['getElementsByTagName']('div')[2]['textContent'] = i18n['firstPB'];
        hideElem(_0x3d81xba['getElementsByTagName']('div')[0]);
        hideElem(_0x3d81xba['getElementsByTagName']('div')[3])
    } else {
        if (_0x3d81x2be) {
            _0x3d81xba['style']['height'] = '235px';
            _0x3d81xba['style']['top'] = '142px';
            _0x3d81xba['getElementsByTagName']('div')[0]['innerHTML'] = _0x3d81x2be['newS'];
            let _0x3d81x2c1 = {
                prevPB: '<b>' + _0x3d81x2be['prevS'] + '</b>',
                prevAgo: '<b>' + _0x3d81x2be['days'] + ' ' + i18n['daysAgo'] + '</b>',
                PBdiff: '<b>' + _0x3d81x2be['diffS'] + '</b>'
            };
            _0x3d81xba['getElementsByTagName']('div')[2]['innerHTML'] = trans(i18n['infoPB'], _0x3d81x2c1);
            _0x3d81xba['getElementsByTagName']('div')[3]['innerHTML'] = _0x3d81x2be['modeTitle'];
            showElem(_0x3d81xba['getElementsByTagName']('div')[0]);
            showElem(_0x3d81xba['getElementsByTagName']('div')[3])
        }
    }
};
GameCaption['prototype']['loading'] = function(_0x3d81xe8, _0x3d81x2c2) {
    this['hide']();
    if (this['LOADING'] in this['captions']) {
        this['captions'][this['LOADING']]['style']['display'] = 'block'
    } else {
        var _0x3d81xba = this['captions'][this['LOADING']] = this['create']();
        _0x3d81xba['style']['opacity'] = 1;
        _0x3d81xba['style']['top'] = '214px';
        _0x3d81xba['style']['height'] = '125px';
        _0x3d81xba['style']['color'] = 'white'
    };
    var _0x3d81xba = this['captions'][this['LOADING']];
    _0x3d81xba['textContent'] = '';
    var _0x3d81x231 = document['createElement']('img');
    if (!_0x3d81x2c2) {
        _0x3d81x231['src'] = CDN_URL('/res/svg/spinWhite.svg')
    } else {
        if (_0x3d81x2c2 === 1) {
            _0x3d81x231['src'] = CDN_URL('/res/svg/cancel.svg')
        } else {
            if (_0x3d81x2c2 === 2) {
                _0x3d81x231['src'] = CDN_URL('/res/img/i/troll.png')
            }
        }
    };
    _0x3d81x231['style']['width'] = '60px';
    _0x3d81x231['style']['marginTop'] = '15px';
    _0x3d81xba['appendChild'](_0x3d81x231);
    var _0x3d81x174 = document['createElement']('div');
    _0x3d81x174['style']['fontSize'] = '22px';
    _0x3d81x174['style']['marginTop'] = '6px';
    _0x3d81x174['innerHTML'] = _0x3d81xe8;
    _0x3d81xba['appendChild'](_0x3d81x174)
};
GameCaption['prototype']['liveRaceFinished'] = function() {
    var _0x3d81x2c3 = 5000,
        _0x3d81x2c4 = 3,
        _0x3d81x2c5 = 0.85;
    if (this['RACE_FINISHED'] in this['captions']) {
        this['captions'][this['RACE_FINISHED']]['style']['display'] = 'block';
        this._fadeOut(this['captions'][this['RACE_FINISHED']], _0x3d81x2c3, _0x3d81x2c4, _0x3d81x2c5);
        return
    };
    var _0x3d81xba = this['captions'][this['RACE_FINISHED']] = this['create']();
    _0x3d81xba['style']['top'] = '174px';
    _0x3d81xba['style']['height'] = '63px';
    _0x3d81xba['style']['color'] = '#CBD600';
    var _0x3d81x174 = document['createElement']('div');
    var _0x3d81x2b5 = document['createElement']('div');
    _0x3d81x174['textContent'] = i18n['raceFin'];
    _0x3d81x174['style']['color'] = 'yellow';
    _0x3d81x174['style']['fontSize'] = '19px';
    _0x3d81x174['style']['marginTop'] = '4px';
    _0x3d81x2b5['textContent'] = i18n['raceFinInfo'];
    _0x3d81x2b5['style']['fontSize'] = '12px';
    _0x3d81x2b5['style']['marginTop'] = '1px';
    _0x3d81xba['appendChild'](_0x3d81x174);
    _0x3d81xba['appendChild'](_0x3d81x2b5);
    this._fadeOut(_0x3d81xba, _0x3d81x2c3, _0x3d81x2c4, _0x3d81x2c5)
};
GameCaption['prototype']['gameWarning'] = function(_0x3d81xac, _0x3d81x2c6, _0x3d81xaf) {
    var _0x3d81x2c3 = 3000,
        _0x3d81x2c4 = 2,
        _0x3d81x2c5 = 0.85;
    if (_0x3d81xaf) {
        if (_0x3d81xaf['fade_after']) {
            _0x3d81x2c3 = _0x3d81xaf['fade_after']
        }
    };
    if (typeof _0x3d81x2c6 === 'undefined') {
        _0x3d81x2c6 = ''
    };
    if (this['GAME_WARNING'] in this['captions']) {
        this['captions'][this['GAME_WARNING']]['style']['display'] = 'block';
        this['captions'][this['GAME_WARNING']]['getElementsByTagName']('div')[0]['innerHTML'] = _0x3d81xac;
        this['captions'][this['GAME_WARNING']]['getElementsByTagName']('div')[1]['innerHTML'] = _0x3d81x2c6;
        this._fadeOut(this['captions'][this['GAME_WARNING']], _0x3d81x2c3, _0x3d81x2c4, _0x3d81x2c5);
        return
    };
    var _0x3d81xba = this['captions'][this['GAME_WARNING']] = this['create']();
    _0x3d81xba['style']['top'] = '216px';
    _0x3d81xba['style']['paddingBottom'] = '15px';
    _0x3d81xba['style']['backgroundColor'] = 'red';
    _0x3d81xba['style']['fontWeight'] = 'bold';
    _0x3d81xba['classList']['add']('transitionCaption');
    var _0x3d81x174 = document['createElement']('div');
    _0x3d81x174['style']['fontSize'] = '31px';
    _0x3d81x174['style']['marginTop'] = '15px';
    _0x3d81x174['style']['color'] = 'white';
    _0x3d81x174['innerHTML'] = _0x3d81xac;
    _0x3d81xba['appendChild'](_0x3d81x174);
    var _0x3d81x2ba = document['createElement']('div');
    _0x3d81x2ba['id'] = 'slSubT';
    _0x3d81x2ba['style']['fontSize'] = '16px';
    _0x3d81x2ba['style']['marginTop'] = '7px';
    _0x3d81x2ba['style']['color'] = 'white';
    _0x3d81x2ba['style']['fontWeight'] = 'normal';
    _0x3d81x2ba['style']['padding'] = '0px 6px';
    _0x3d81x2ba['innerHTML'] = _0x3d81x2c6;
    _0x3d81xba['appendChild'](_0x3d81x2ba);
    this._fadeOut(_0x3d81xba, _0x3d81x2c3, _0x3d81x2c4, _0x3d81x2c5)
};
'use strict';

function Mobile(_0x3d81x139) {
    this['p'] = _0x3d81x139;
    this['isMobile'] = this['isMobile']();
    this['Settings'] = this['p']['Settings'];
    this['showPlBtn'] = null;
    this['draggingEnabled'] = false;
    if (this['isMobile']) {
        this['initForMobile']()
    }
}
Mobile['prototype']['isMobile'] = function() {
    let _0x3d81x2c8 = localStorage['getItem']('mobile');
    if (_0x3d81x2c8 !== null) {
        if (_0x3d81x2c8 === 'false') {
            return false
        };
        return _0x3d81x2c8
    };
    return this['isMobileDetect']()
};
Mobile['prototype']['isMobileDetect'] = function() {
    let _0x3d81xa5 = navigator['userAgent'] || navigator['vendor'] || window['opera'];
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i ['test'](_0x3d81xa5) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i ['test'](_0x3d81xa5['substr'](0, 4)))
};
Mobile['prototype']['changeStyle'] = function() {
    let _0x3d81x2c9 = document['querySelector']('meta[name=viewport]');
    _0x3d81x2c9['setAttribute']('content', 'width=500, user-scalable=0');
    $('.players')['hide']();
    $('.players')['css']('margin-left', '22px');
    $('.navbar-brand')['attr']('href', 'javascript:void(0)');
    $('#main')['css']('float', 'none');
    $('#main')['css']('margin-right', 'auto');
    $('#main')['css']('margin-left', 'auto');
    $('#gameFrame')['css']('width', 'auto');
    $('#gc')['css']('padding', '0');
    $('html')['css']('width', '500px');
    $('.navbar')['css']('margin-bottom', '0');
    $('#gameFrame')['css']('margin-top', '10px');
    $('#gstats')['css']('padding-top', '10px');
    $('#tcc')['addClass']('tc')['show']();
    $('#touchBtn')['parent']()['parent']()['show']();
    $('#touchBtnMove')['parent']()['parent']()['show']();
    $('#touch')['prop']('checked', true);
    $('#touchBtn')['prop']('checked', true);
    $('#touchBtnMove')['prop']('checked', false);
    $('#app')['css']('touch-action', 'manipulation')
};
Mobile['prototype']['initForMobile'] = function() {
    var _0x3d81x25 = this;
    this['changeStyle']();

    function _0x3d81x2ca() {
        if ($('#touchBtn')['prop']('checked')) {
            $('#tcc')['show']();
            $('#touchBtnMove')['parent']()['parent']()['show']()
        } else {
            $('#tcc')['hide']();
            $('#touchBtnMove')['parent']()['parent']()['hide']()
        }
    }
    $('#touchBtn')['on']('click', _0x3d81x2ca['bind'](this));

    function _0x3d81x2cb() {
        if (this['p']['Live']['sitout'] || this['p']['Live']['players']['length'] === 0) {
            this['p']['startPractice'](this['p']['selectedPmode'])
        }
    }
    $('#res')['on']('click', _0x3d81x2cb['bind'](this));

    function _0x3d81x2cc(_0x3d81x2d) {
        if (!_0x3d81x25['draggingEnabled']) {
            _0x3d81x2d['preventDefault']();
            _0x3d81x2d['stopPropagation']()
        }
    }

    function _0x3d81x2cd(_0x3d81x2d) {
        if (!_0x3d81x25['draggingEnabled']) {
            _0x3d81x2d['preventDefault']();
            _0x3d81x2d['stopPropagation']()
        }
    }

    function _0x3d81x2ce(_0x3d81x2d) {
        if (!_0x3d81x25['draggingEnabled']) {
            _0x3d81x2d['preventDefault']();
            _0x3d81x2d['stopPropagation']()
        }
    }

    function _0x3d81x2cf(_0x3d81xa5) {
        _0x3d81x2cc(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['hd'];
        this['p']['keyInput2'](_0x3d81xa5);
        this['p']['hardDropPressed'] = false
    }

    function _0x3d81x2d0(_0x3d81xa5) {
        _0x3d81x2cc(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['rr'];
        this['p']['keyInput2'](_0x3d81xa5)
    }

    function _0x3d81x2d1(_0x3d81xa5) {
        _0x3d81x2cc(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['rl'];
        this['p']['keyInput2'](_0x3d81xa5)
    }

    function _0x3d81x2d2(_0x3d81xa5) {
        _0x3d81x2cc(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['dr'];
        this['p']['keyInput2'](_0x3d81xa5)
    }

    function _0x3d81x2d3(_0x3d81xa5) {
        _0x3d81x2cc(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['hk'];
        this['p']['keyInput2'](_0x3d81xa5);
        this['p']['holdPressed'] = false
    }

    function _0x3d81x2d4(_0x3d81xa5) {
        _0x3d81x2cd(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['ml'];
        this['p']['keyInput2'](_0x3d81xa5)
    }

    function _0x3d81x2d5(_0x3d81xa5) {
        _0x3d81x2ce(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['ml'];
        this['p']['keyInput3'](_0x3d81xa5)
    }

    function _0x3d81x2d6(_0x3d81xa5) {
        _0x3d81x2cd(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['mr'];
        this['p']['keyInput2'](_0x3d81xa5)
    }

    function _0x3d81x2d7(_0x3d81xa5) {
        _0x3d81x2ce(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['mr'];
        this['p']['keyInput3'](_0x3d81xa5)
    }

    function _0x3d81x2d8(_0x3d81xa5) {
        _0x3d81x2cd(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['sd'];
        this['p']['keyInput2'](_0x3d81xa5)
    }

    function _0x3d81x2d9(_0x3d81xa5) {
        _0x3d81x2ce(_0x3d81xa5);
        _0x3d81xa5['keyCode'] = this['Settings']['sd'];
        this['p']['keyInput3'](_0x3d81xa5)
    }

    function _0x3d81x2da() {
        _0x3d81x2dd()
    }

    function _0x3d81x2db() {
        $('#settings')['trigger']('click')
    }

    function _0x3d81x2dc() {
        _0x3d81x2de()
    }

    function _0x3d81x2dd() {
        $('.players')['show']();
        $('#main')['hide']();
        $('.tc')['hide']();
        $('.navbar-brand')['html']('<img src="/res/svg/white/arrowLeft.svg"> Back to game');
        $('.navbar-brand')['addClass']('navbar-back');
        $('.navbar')['css']('margin-bottom', '22px')
    }

    function _0x3d81x2de() {
        $('.players')['hide']();
        $('.tc')['show']();
        $('#main')['show']();
        $('.navbar-brand')['html']('Jstris');
        $('.navbar-brand')['removeClass']('navbar-back');
        $('.navbar')['css']('margin-bottom', '2px')
    }
    $('.navbar-brand')['on']('click', _0x3d81x2de['bind'](this));
    document['getElementById']('lobby')['addEventListener']('click', _0x3d81x2da['bind'](this));
    document['getElementById']('res')['addEventListener']('click', _0x3d81x2dc['bind'](this));
    document['getElementById']('tc-hd')['addEventListener']('touchstart', _0x3d81x2cf['bind'](this));
    document['getElementById']('tc-l')['addEventListener']('touchstart', _0x3d81x2d4['bind'](this));
    document['getElementById']('tc-l')['addEventListener']('touchend', _0x3d81x2d5['bind'](this));
    document['getElementById']('tc-r')['addEventListener']('touchstart', _0x3d81x2d6['bind'](this));
    document['getElementById']('tc-r')['addEventListener']('touchend', _0x3d81x2d7['bind'](this));
    document['getElementById']('tc-d')['addEventListener']('touchstart', _0x3d81x2d8['bind'](this));
    document['getElementById']('tc-d')['addEventListener']('touchend', _0x3d81x2d9['bind'](this));
    document['getElementById']('tc-c')['addEventListener']('touchstart', _0x3d81x2d0['bind'](this));
    document['getElementById']('tc-cc')['addEventListener']('touchstart', _0x3d81x2d1['bind'](this));
    document['getElementById']('tc-dr')['addEventListener']('touchstart', _0x3d81x2d2['bind'](this));
    document['getElementById']('tc-h')['addEventListener']('touchstart', _0x3d81x2d3['bind'](this));
    document['getElementById']('holdCanvas')['addEventListener']('touchstart', _0x3d81x2d3['bind'](this));
    this['showPlBtn'] = document['createElement']('div');
    this['showPlBtn']['id'] = 'tc-vs';
    this['showPlBtn']['addEventListener']('click', _0x3d81x2dd['bind'](this));
    this['showPlBtn']['innerHTML'] = '<img src="/res/svg/dark/screens.svg">';
    let _0x3d81x2df = document['createElement']('div');
    _0x3d81x2df['className'] = 'mMenu';
    _0x3d81x2df['appendChild'](this['showPlBtn']);
    document['getElementById']('stage')['appendChild'](_0x3d81x2df);
    this['setupButtonDragDrop']()
};
Mobile['prototype']['setupButtonDragDrop'] = function() {
    var _0x3d81x2e0 = [document['querySelector']('#tc-hd'), document['querySelector']('#tc-dr'), document['querySelector']('#tc-l'), document['querySelector']('#tc-r'), document['querySelector']('#tc-d'), document['querySelector']('#tc-cc'), document['querySelector']('#tc-c'), document['querySelector']('#tc-h'), this['showPlBtn']];
    var _0x3d81x148 = document['querySelector']('#app');
    var _0x3d81x2e1 = false;
    var _0x3d81x25 = this;
    var _0x3d81x2e2;
    var _0x3d81x2e3;
    _0x3d81x2e0['forEach']((_0x3d81x86) => {
        let _0x3d81x232 = localStorage['getItem'](_0x3d81x86['id']);
        if (_0x3d81x232 !== null) {
            let _0x3d81x2e4 = _0x3d81x232['split'](',', 2);
            _0x3d81x86['custom_xOffset'] = parseFloat(_0x3d81x2e4[0]);
            _0x3d81x86['custom_yOffset'] = parseFloat(_0x3d81x2e4[1]);
            _0x3d81x2e9(_0x3d81x86['custom_xOffset'], _0x3d81x86['custom_yOffset'], _0x3d81x86)
        }
    });
    $('#touchBtnMove')['on']('click', function() {
        _0x3d81x25['draggingEnabled'] = $('#touchBtnMove')['prop']('checked')
    });
    _0x3d81x148['addEventListener']('touchstart', _0x3d81x2e5, false);
    _0x3d81x148['addEventListener']('touchend', _0x3d81x2e7, false);
    _0x3d81x148['addEventListener']('touchmove', _0x3d81x2e8, false);
    _0x3d81x148['addEventListener']('mousedown', _0x3d81x2e5, false);
    _0x3d81x148['addEventListener']('mouseup', _0x3d81x2e7, false);
    _0x3d81x148['addEventListener']('mousemove', _0x3d81x2e8, false);

    function _0x3d81x2e5(_0x3d81x2d) {
        if (_0x3d81x25['draggingEnabled'] && _0x3d81x2e0['indexOf'](_0x3d81x2d['target']) !== -1) {
            _0x3d81x2e1 = _0x3d81x2d['target']
        } else {
            return
        };
        let _0x3d81x28b = (_0x3d81x2d['target']['custom_xOffset']) ? _0x3d81x2d['target']['custom_xOffset'] : 0;
        let _0x3d81x2e6 = (_0x3d81x2d['target']['custom_yOffset']) ? _0x3d81x2d['target']['custom_yOffset'] : 0;
        if (_0x3d81x2d['type'] === 'touchstart') {
            _0x3d81x2d['target']['custom_initialX'] = _0x3d81x2d['touches'][0]['clientX'] - _0x3d81x28b;
            _0x3d81x2d['target']['custom_initialY'] = _0x3d81x2d['touches'][0]['clientY'] - _0x3d81x2e6
        } else {
            _0x3d81x2d['target']['custom_initialX'] = _0x3d81x2d['clientX'] - _0x3d81x28b;
            _0x3d81x2d['target']['custom_initialY'] = _0x3d81x2d['clientY'] - _0x3d81x2e6
        }
    }

    function _0x3d81x2e7(_0x3d81x2d) {
        if (!_0x3d81x25['draggingEnabled'] || !_0x3d81x2e1) {
            return
        };
        _0x3d81x2e1['custom_initialX'] = _0x3d81x2e2;
        _0x3d81x2e1['custom_initialY'] = _0x3d81x2e3;
        let _0x3d81xac = _0x3d81x2e2['toFixed'](2) + ',' + _0x3d81x2e3['toFixed'](2);
        localStorage['setItem'](_0x3d81x2e1['id'], _0x3d81xac);
        _0x3d81x2e1 = false
    }

    function _0x3d81x2e8(_0x3d81x2d) {
        if (_0x3d81x25['draggingEnabled'] && _0x3d81x2e1) {
            _0x3d81x2d['preventDefault']();
            if (_0x3d81x2d['type'] === 'touchmove') {
                _0x3d81x2e2 = _0x3d81x2d['touches'][0]['clientX'] - _0x3d81x2e1['custom_initialX'];
                _0x3d81x2e3 = _0x3d81x2d['touches'][0]['clientY'] - _0x3d81x2e1['custom_initialY']
            } else {
                _0x3d81x2e2 = _0x3d81x2d['clientX'] - _0x3d81x2e1['custom_initialX'];
                _0x3d81x2e3 = _0x3d81x2d['clientY'] - _0x3d81x2e1['custom_initialY']
            };
            _0x3d81x2e1['custom_xOffset'] = _0x3d81x2e2;
            _0x3d81x2e1['custom_yOffset'] = _0x3d81x2e3;
            _0x3d81x2e9(_0x3d81x2e2, _0x3d81x2e3, _0x3d81x2e1)
        }
    }

    function _0x3d81x2e9(_0x3d81x2a1, _0x3d81x2ea, _0x3d81x8c) {
        _0x3d81x8c['style']['transform'] = 'translate3d(' + _0x3d81x2a1 + 'px, ' + _0x3d81x2ea + 'px, 0)'
    }
};

function ReplayAction(_0x3d81x97, _0x3d81x128) {
    this['a'] = _0x3d81x97;
    this['t'] = _0x3d81x128
}

function Replay() {
    this['Action'] = Object['freeze']({
        MOVE_LEFT: 0,
        MOVE_RIGHT: 1,
        DAS_LEFT: 2,
        DAS_RIGHT: 3,
        ROTATE_LEFT: 4,
        ROTATE_RIGHT: 5,
        ROTATE_180: 6,
        HARD_DROP: 7,
        SOFT_DROP_BEGIN_END: 8,
        GRAVITY_STEP: 9,
        HOLD_BLOCK: 10,
        GARBAGE_ADD: 11,
        SGARBAGE_ADD: 12,
        REDBAR_SET: 13,
        ARR_MOVE: 14,
        AUX: 15
    });
    this['AUX'] = Object['freeze']({
        AFK: 0,
        BLOCK_SET: 1,
        MOVE_TO: 2,
        RANDOMIZER: 3,
        MATRIX_MOD: 4,
        WIDE_GARBAGE_ADD: 5
    });
    this['AuxBits'] = Array();
    this['AuxBits'][this['Action']['GARBAGE_ADD']] = [5, 4];
    this['AuxBits'][this['Action']['REDBAR_SET']] = [5];
    this['AuxBits'][this['Action']['ARR_MOVE']] = [1];
    this['AuxBits'][this['Action']['AUX']] = [4];
    this['config'] = {
        v: 3.3,
        softDropId: undefined,
        gameStart: undefined,
        gameEnd: undefined,
        seed: undefined,
        m: undefined,
        bs: undefined,
        se: undefined,
        das: undefined,
        arr: undefined
    };
    this['actions'] = Array();
    this['string'] = '';
    this['md5'] = '';
    this['mode'] = 0;
    this['stream'] = new ReplayStream();
    this['onSaved'] = null;
    this['onMoveAdded'] = null
}
Replay['prototype']['add'] = function(_0x3d81x42) {
    if (_0x3d81x42['a'] === this['Action']['DAS_LEFT'] && this['actions']['length'] && this['actions'][this['actions']['length'] - 1]['a'] === _0x3d81x42['a']) {
        return
    };
    if (_0x3d81x42['a'] === this['Action']['DAS_RIGHT'] && this['actions']['length'] && this['actions'][this['actions']['length'] - 1]['a'] === _0x3d81x42['a']) {
        return
    };
    _0x3d81x42['t'] -= this['config']['gameStart'];
    if (this['mode'] === 1) {
        _0x3d81x42['t'] = 0
    };
    this['actions']['push'](_0x3d81x42);
    if (this['onMoveAdded']) {
        this['onMoveAdded'](_0x3d81x42)
    }
};
Replay['prototype']['clear'] = function() {
    this['actions']['length'] = 0;
    this['mode'] = 0;
    this['string'] = '';
    this['md5'] = '';
    this['config']['mode'] = undefined;
    if ('afk' in this['config']) {
        delete this['config']['afk']
    };
    if ('bbs' in this['config']) {
        delete this['config']['bbs']
    };
    if ('rnd' in this['config']) {
        delete this['config']['rnd']
    }
};
Replay['prototype']['getData'] = function() {
    if (this['mode'] === -1) {
        return ''
    };
    var _0x3d81x15d = new Object();
    _0x3d81x15d['d'] = this['getBlobData']();
    _0x3d81x15d['c'] = this['config'];
    var _0x3d81x2ed = LZString['compressToEncodedURIComponent'](JSON['stringify'](_0x3d81x15d));
    this['string'] = _0x3d81x2ed;
    this['md5'] = md5(_0x3d81x2ed);
    return this['md5']
};
Replay['prototype']['pushEndPadding'] = function() {
    var _0x3d81x1e1 = 32 - this['stream']['bitpos'];
    if (_0x3d81x1e1 >= 12) {
        this['stream']['pushBits'](4095, 12)
    }
};
Replay['prototype']['getBlobData'] = function() {
    this['stream'] = new ReplayStream();
    var _0x3d81x68 = null;
    var _0x3d81x8f = this['actions']['length'];
    var _0x3d81x2ee = 10;
    var _0x3d81x73, _0x3d81x2ef = 0,
        _0x3d81x2f0 = 4094;
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x8f; _0x3d81x13++) {
        if (_0x3d81x68 !== null && _0x3d81x68 > this['actions'][_0x3d81x13]['t']) {
            let _0x3d81x5d = _0x3d81x68 - this['actions'][_0x3d81x13]['t'];
            this['actions'][_0x3d81x13]['t'] = _0x3d81x68;
            if (_0x3d81x5d > _0x3d81x2ee) {
                this['config']['err'] |= 1
            }
        };
        _0x3d81x68 = this['actions'][_0x3d81x13]['t'];
        _0x3d81x73 = this['actions'][_0x3d81x13]['t'] - (_0x3d81x2ef * _0x3d81x2f0);
        if (_0x3d81x73 > _0x3d81x2f0) {
            _0x3d81x73 -= _0x3d81x2f0;
            _0x3d81x2ef++
        };
        if (_0x3d81x73 < 0) {
            this['config']['err'] |= 1;
            _0x3d81x73 = 0
        };
        if (_0x3d81x73 > _0x3d81x2f0) {
            this['config']['err'] |= 2;
            _0x3d81x73 = _0x3d81x2f0
        };
        this['stream']['pushBits'](_0x3d81x73, 12);
        this['stream']['pushBits'](this['actions'][_0x3d81x13]['a'], 4);
        if (typeof this['AuxBits'][this['actions'][_0x3d81x13]['a']] !== 'undefined') {
            var _0x3d81x104 = this['AuxBits'][this['actions'][_0x3d81x13]['a']];
            for (var _0x3d81x1e0 = 0; _0x3d81x1e0 < _0x3d81x104['length']; _0x3d81x1e0++) {
                this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][_0x3d81x1e0], _0x3d81x104[_0x3d81x1e0])
            };
            if (this['actions'][_0x3d81x13]['a'] === this['Action']['AUX']) {
                if (this['actions'][_0x3d81x13]['d'][0] === this['AUX']['AFK']) {
                    this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][1], 16);
                    _0x3d81x2ef += (this['actions'][_0x3d81x13]['d'][1] / _0x3d81x2f0) >>> 0
                } else {
                    if (this['actions'][_0x3d81x13]['d'][0] === this['AUX']['BLOCK_SET']) {
                        this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][1], 1);
                        this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][2], 4)
                    } else {
                        if (this['actions'][_0x3d81x13]['d'][0] === this['AUX']['MOVE_TO']) {
                            let _0x3d81x19 = this['actions'][_0x3d81x13]['d'][1] + 3,
                                _0x3d81x18 = this['actions'][_0x3d81x13]['d'][2] + 12;
                            if (_0x3d81x19 < 0 || _0x3d81x19 > 15 || _0x3d81x18 < 0 || _0x3d81x18 > 31) {
                                this['config']['err'] |= 16
                            };
                            this['stream']['pushBits'](_0x3d81x19, 4);
                            this['stream']['pushBits'](_0x3d81x18, 5)
                        } else {
                            if (this['actions'][_0x3d81x13]['d'][0] === this['AUX']['RANDOMIZER']) {
                                this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][1], 1);
                                this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][2], 5)
                            } else {
                                if (this['actions'][_0x3d81x13]['d'][0] === this['AUX']['MATRIX_MOD']) {
                                    this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][1], 4);
                                    this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][2], 5)
                                } else {
                                    if (this['actions'][_0x3d81x13]['d'][0] === this['AUX']['WIDE_GARBAGE_ADD']) {
                                        this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][1], 5);
                                        this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][2], 4);
                                        this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][3], 3);
                                        this['stream']['pushBits'](this['actions'][_0x3d81x13]['d'][4], 1)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    this['pushEndPadding']();
    var _0x3d81x1e2 = new Uint32Array(this['stream']['data']);
    var _0x3d81xac = _arrayBufferToBase64(_0x3d81x1e2['buffer']);
    return _0x3d81xac
};

function _arrayBufferToBase64(_0x3d81x2f2) {
    var _0x3d81x2f3 = '';
    var _0x3d81x2f4 = new Uint8Array(_0x3d81x2f2);
    if (endianness()) {
        var _0x3d81x8f = _0x3d81x2f4['byteLength'] / 4;
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x8f; _0x3d81x13++) {
            for (var _0x3d81x70 = 0; _0x3d81x70 < 4; _0x3d81x70++) {
                _0x3d81x2f3 += String['fromCharCode'](_0x3d81x2f4[4 * _0x3d81x13 + 3 - _0x3d81x70])
            }
        }
    } else {
        var _0x3d81x8f = _0x3d81x2f4['byteLength'];
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x8f; _0x3d81x13++) {
            _0x3d81x2f3 += String['fromCharCode'](_0x3d81x2f4[_0x3d81x13])
        }
    };
    return window['btoa'](_0x3d81x2f3)
}
Replay['prototype']['getGameTime'] = function(_0x3d81x2f5) {
    if (typeof _0x3d81x2f5 === 'undefined') {
        _0x3d81x2f5 = true
    };
    var _0x3d81x73 = this['actions'][this['actions']['length'] - 1]['t'];
    if (!_0x3d81x2f5) {
        if (typeof this['config']['afk'] !== 'undefined') {
            _0x3d81x73 -= this['config']['afk']
        }
    };
    return Math['round'](_0x3d81x73) / 1000
};
Replay['prototype']['postLiveData'] = function(_0x3d81x2f6, _0x3d81x20, _0x3d81x2f7) {
    if (this['mode'] === -1) {
        this['clear']();
        return
    };
    var _0x3d81x250 = document['head']['querySelector']('[name=csrf-token]')['content'];
    var _0x3d81x236 = new XMLHttpRequest();
    var _0x3d81x5 = '/code/replayUploaderLive';
    var _0x3d81x251 = 'gid=' + _0x3d81x2f6 + '&cid=' + _0x3d81x20 + '&r=' + encodeURIComponent(this['string']);
    _0x3d81x236['timeout'] = 6000;
    _0x3d81x236['open']('POST', _0x3d81x5, true);
    _0x3d81x236['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
    _0x3d81x236['setRequestHeader']('X-CSRF-TOKEN', _0x3d81x250);
    _0x3d81x236['setRequestHeader']('Content-type', 'application/x-www-form-urlencoded');
    _0x3d81x236['send'](_0x3d81x251)
};
Replay['prototype']['ver'] = function(_0x3d81x2f7) {
    let _0x3d81xa5 = '-';
    if (_0x3d81x2f7['hasOwnProperty']('vcsm')) {
        return _0x3d81x2f7['vcsm']
    } else {
        let _0x3d81x19 = GameCore.toString() + _0x3d81xa5 + Game.toString() + _0x3d81xa5 + Live.toString();
        _0x3d81x2f7['vcsm'] = md5(_0x3d81x19)
    };
    return _0x3d81x2f7['vcsm']['substr'](0, 16)
};
Replay['prototype']['postData'] = function(_0x3d81x7, _0x3d81x2f7) {
    var _0x3d81x250 = document['head']['querySelector']('[name=csrf-token]')['content'];
    var _0x3d81x236 = new XMLHttpRequest();
    var _0x3d81x5 = '/code/replayUploader';
    var _0x3d81x251 = 'id=' + _0x3d81x7 + '&r=' + encodeURIComponent(this['string']) + '&c=' + this['ver'](_0x3d81x2f7);
    _0x3d81x236['timeout'] = 10000;
    _0x3d81x236['open']('POST', _0x3d81x5, true);
    _0x3d81x236['setRequestHeader']('X-Requested-With', 'XMLHttpRequest');
    _0x3d81x236['setRequestHeader']('X-CSRF-TOKEN', _0x3d81x250);
    _0x3d81x236['setRequestHeader']('Content-type', 'application/x-www-form-urlencoded');
    var _0x3d81x25 = this;
    try {
        _0x3d81x236['send'](_0x3d81x251)
    } catch (err) {
        _0x3d81x25['uploadError'](_0x3d81x2f7, err['message'])
    };
    _0x3d81x236['ontimeout'] = function() {
        _0x3d81x25['uploadError'](_0x3d81x2f7, 'REQUEST_TIMEOUT')
    };
    _0x3d81x236['onerror'] = _0x3d81x236['onabort'] = function() {
        _0x3d81x25['uploadError'](_0x3d81x2f7, 'REQUEST_FAIL')
    };
    _0x3d81x236['onload'] = function() {
        if (_0x3d81x236['status'] === 200) {
            if (typeof _0x3d81x25['onSaved'] === 'function') {
                _0x3d81x25['onSaved'](JSON['parse'](_0x3d81x236['responseText']))
            }
        } else {
            _0x3d81x25['uploadError'](_0x3d81x2f7, _0x3d81x236['status'] + ' - ' + _0x3d81x236['statusText'])
        }
    }
};
Replay['prototype']['uploadError'] = function(_0x3d81x2f7, _0x3d81x189) {
    if (!this['string']) {
        return
    };
    var _0x3d81xac = '<span class=\'wFirstLine\'><span class=\'wTitle\'>!' + i18n['warning2'] + '!</span> <b>' + i18n['repFail'] + '</b> (<em>' + _0x3d81x189 + '</em>)</span>';
    _0x3d81xac += '<p>' + i18n['repInChat'] + ' ' + i18n['repTxtInfo'] + '</p>';
    _0x3d81xac += '<textarea readonly cols="30" onclick="this.focus();this.select()">' + this['string'] + '</textarea>';
    _0x3d81x2f7['chatMajorWarning'](_0x3d81xac)
};
Replay['prototype']['hasUserInputs'] = function() {
    if (!this['actions']['length']) {
        return false
    };
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['actions']['length']; _0x3d81x13++) {
        if (this['actions'][_0x3d81x13]['a'] <= this['Action']['ROTATE_180']) {
            return true
        }
    };
    return false
};

function endianness() {
    var _0x3d81x141 = new ArrayBuffer(4);
    var _0x3d81xa5 = new Uint32Array(_0x3d81x141);
    var _0x3d81xba = new Uint8Array(_0x3d81x141);
    _0x3d81xa5[0] = 0xdeadbeef;
    if (_0x3d81xba[0] === 0xef) {
        return 1
    };
    if (_0x3d81xba[0] === 0xde) {
        return 0
    };
    alert('Unknown endianness!');
    throw new Error('Aborted')
}

function ReplayStream() {
    this['data'] = [0];
    this['datapos'] = 0;
    this['bitpos'] = 0;
    this['wordSize'] = 32;
    this['byte'] = 0
}
ReplayStream['prototype']['pushBits'] = function(_0x3d81x25e, _0x3d81x2fa) {
    var _0x3d81x1e1 = this['wordSize'] - this['bitpos'];
    _0x3d81x25e &= (1 << _0x3d81x2fa) - 1;
    if (_0x3d81x1e1 - _0x3d81x2fa >= 0) {
        this['data'][this['datapos']] |= _0x3d81x25e << (_0x3d81x1e1 - _0x3d81x2fa);
        this['bitpos'] += _0x3d81x2fa
    } else {
        if (_0x3d81x1e1 >= 0) {
            this['data'][this['datapos']] |= _0x3d81x25e >> (_0x3d81x2fa - _0x3d81x1e1)
        } else {
            _0x3d81x1e1 = 0
        };
        this['bitpos'] = (_0x3d81x2fa - _0x3d81x1e1);
        this['data'][++this['datapos']] = _0x3d81x25e << (this['wordSize'] - this['bitpos'])
    }
};
ReplayStream['prototype']['pullBits'] = function(_0x3d81x2fa) {
    if (this['data']['length'] === this['byte']) {
        return null
    };
    var _0x3d81x12, _0x3d81x2fb = this['wordSize'] - (this['bitpos'] + _0x3d81x2fa);
    if (_0x3d81x2fb >= 0) {
        var _0x3d81x2fc = ((1 << _0x3d81x2fa) - 1) << _0x3d81x2fb;
        _0x3d81x12 = (this['data'][this['byte']] & _0x3d81x2fc) >>> _0x3d81x2fb;
        this['bitpos'] = (this['bitpos'] + _0x3d81x2fa);
        if (this['bitpos'] >= this['wordSize']) {
            this['bitpos'] -= this['wordSize'];
            this['byte']++
        }
    } else {
        var _0x3d81x2fd = _0x3d81x2fa + _0x3d81x2fb;
        var _0x3d81x2fe = (1 << _0x3d81x2fd) - 1;
        var _0x3d81x2ff = (this['data'][this['byte']] & _0x3d81x2fe) << (-_0x3d81x2fb);
        ++this['byte'];
        this['bitpos'] = 0;
        var _0x3d81x300 = this['pullBits'](_0x3d81x2fa - _0x3d81x2fd);
        if (_0x3d81x300 === null) {
            return null
        } else {
            _0x3d81x12 = _0x3d81x2ff | _0x3d81x300
        }
    };
    return _0x3d81x12
};
'use strict';

function RulesetManager(_0x3d81x139) {
    this['p'] = _0x3d81x139;
    this['modeMenu'] = document['getElementById']('mode-menu');
    this['DEF'] = _0x3d81x139['DEF'];
    this['pmodeRuleId'] = 0;
    this['RULE_KEYS'] = {
        cd: 'clearDelay',
        rnd: 'rnd',
        pr: 'showPreviews',
        hold: 'holdEnabled',
        bbs: 'baseBlockSet',
        grav: 'gravityLvl',
        ld: 'lockDelay',
        mess: 'mess',
        gapW: 'gapW',
        gInv: 'gInv',
        gDelay: 'gDelay',
        gblock: 'gblock',
        tsdOnly: 'tsdOnly',
        as: 'allSpin',
        sl: 'speedLimit',
        sm: 'scoreMult',
        cl: 'clearLines',
        sfx: 'sfx',
        vsfx: 'vsfx',
        sa: 'solidAttack',
        ext: 'ext',
        sgpA: 'sgProfile'
    };
    this['PRESET_KEYS'] = {
        clearDelay: 'clearDelay',
        rndSel: 'rnd',
        prSel: 'showPreviews',
        hasHold: 'holdEnabled',
        blocksSel: 'baseBlockSet',
        gravityLvl: 'gravityLvl',
        lockDelay: 'lockDelay',
        mess: 'mess',
        gapW: 'gapW',
        gInv: 'gInv',
        garbageDelay: 'gDelay',
        gblockSel: 'gblock',
        tsdOnly: 'tsdOnly',
        allSpin: 'allSpin',
        speedLimit: 'speedLimit',
        scoreMult: 'scoreMult',
        clearLines: 'clearLines',
        sfx: 'sfx',
        vsfx: 'vsfx',
        solidAtk: 'solidAttack',
        sgProfile: 'sgpA'
    };
    this['RULESETS'] = [{
        id: 0,
        lvl: 1,
        name: 'Default',
        key: '',
        desc: 'Normal settings',
        c: {}
    }, {
        id: 1,
        lvl: 1,
        name: 'Big mode',
        key: 'big',
        desc: 'Blocks are larger than normal',
        c: {
            bbs: 1
        }
    }, {
        id: 2,
        lvl: 3,
        name: 'Pentomino',
        key: 'pentomino',
        desc: 'Pentomino blocks',
        c: {
            bbs: 4
        }
    }, {
        id: 3,
        lvl: 2,
        name: 'MPH',
        key: 'MPH',
        desc: 'Memoryless, previewless, holdless',
        c: {
            pr: 0,
            hold: false,
            rnd: 2
        }
    }];
    this['MODES'] = [{
        id: 1,
        n: i18n['sprint'],
        modes: [{
            id: 2,
            n: '20',
            fn: '20L',
            rs: [0, 2, 3]
        }, {
            id: 1,
            n: '40',
            fn: '40L',
            rs: [0, 1, 2, 3]
        }, {
            id: 3,
            n: '100',
            fn: '100L',
            rs: [0, 1, 2, 3]
        }, {
            id: 4,
            n: '1K',
            fn: '1000L',
            rs: [0, 1, 2, 3]
        }]
    }, {
        id: 3,
        n: i18n['cheese'],
        modes: [{
            id: 1,
            n: '10',
            fn: '10L',
            rs: [0, 2, 3]
        }, {
            id: 2,
            n: '18',
            fn: '18L',
            rs: [0, 3]
        }, {
            id: 3,
            n: '100',
            fn: '100L',
            rs: [0, 3]
        }, {
            id: 100,
            n: '&infin;',
            rs: [0, 2, 3]
        }]
    }, {
        id: 4,
        n: i18n['survival'],
        modes: [{
            id: 1,
            n: '1g/s',
            rs: [0]
        }]
    }, {
        id: 5,
        n: i18n['ultra'],
        modes: [{
            id: 1,
            n: '2min',
            rs: [0, 2, 3]
        }]
    }, {
        id: 7,
        n: i18n['20TSD'],
        modes: [{
            id: 1,
            n: '',
            rs: [0, 3]
        }]
    }, {
        id: 8,
        n: i18n['PCmode'],
        modes: [{
            id: 1,
            n: '',
            rs: [0, 3]
        }]
    }, {
        id: 2,
        n: i18n['freePlay'],
        modes: [{
            id: 1,
            n: '',
            rs: [0, 1, 2, 3]
        }]
    }];
    this['combo'] = null;
    this['isOpen'] = false;
    this['rs1'] = document['getElementById']('ruleSel1');
    this['registerCombo']()
}
RulesetManager['prototype']['fullModeName'] = function(_0x3d81x1f4, _0x3d81x1f5) {
    if (_0x3d81x1f4 === 6) {
        return 'Map ' + _0x3d81x1f5
    };
    var _0x3d81x19c = Math['floor'](_0x3d81x1f5 / 10);
    var _0x3d81x1fc = (_0x3d81x19c === 0) ? '' : this['RULESETS'][_0x3d81x19c]['name'];
    var _0x3d81x1f3 = '',
        _0x3d81x302 = '';
    _0x3d81x1f5 %= 10;
    for (const _0x3d81x247 of this['MODES']) {
        if (_0x3d81x247['id'] !== _0x3d81x1f4) {
            continue
        };
        _0x3d81x1f3 = _0x3d81x247['n'];
        for (const _0x3d81x303 of _0x3d81x247['modes']) {
            if (_0x3d81x303['id'] === _0x3d81x1f5) {
                _0x3d81x302 = (_0x3d81x303['fn']) ? _0x3d81x303['fn'] : _0x3d81x303['n']
            }
        }
    };
    return _0x3d81x1fc + ' ' + _0x3d81x1f3 + ' ' + _0x3d81x302
};
RulesetManager['prototype']['adjustToValidMode'] = function(_0x3d81xb, _0x3d81x304) {
    _0x3d81x304 = parseInt(_0x3d81x304);
    let _0x3d81x305 = Number['MAX_SAFE_INTEGER'];
    if (this['pmodeRuleId'] === 0 && (_0x3d81xb === 6 || _0x3d81xb === 9)) {
        return true
    };
    for (const _0x3d81x247 of this['MODES']) {
        let _0x3d81x306 = Number['MAX_SAFE_INTEGER'],
            _0x3d81x307 = 0;
        for (const _0x3d81x303 of _0x3d81x247['modes']) {
            let _0x3d81x308 = (_0x3d81x303['rs']['indexOf'](this['pmodeRuleId']) !== -1);
            if (_0x3d81x247['id'] === _0x3d81xb && _0x3d81x303['id'] === _0x3d81x304 && _0x3d81x308) {
                return
            };
            if (_0x3d81x308) {
                _0x3d81x307++;
                if (_0x3d81x303['id'] < _0x3d81x306) {
                    _0x3d81x306 = _0x3d81x303['id']
                }
            }
        };
        if (_0x3d81x307 && _0x3d81x247['id'] < _0x3d81x305) {
            _0x3d81x305 = _0x3d81x247['id']
        };
        if (_0x3d81x247['id'] === _0x3d81xb && _0x3d81x307) {
            this['p']['sprintMode'] = _0x3d81x306;
            return
        }
    };
    this['p']['pmode'] = _0x3d81x305;
    this['setActiveMode'](this['p']['pmode']);
    this['adjustToValidMode'](this['p']['pmode'], -1)
};
RulesetManager['prototype']['setActiveMode'] = function(_0x3d81x51) {
    let _0x3d81x2d = null,
        _0x3d81x170 = null,
        _0x3d81x93 = this['p'];
    for (const _0x3d81x247 of this['MODES']) {
        if (_0x3d81x247['id'] === _0x3d81x51) {
            _0x3d81x2d = _0x3d81x247['elem'];
            _0x3d81x170 = _0x3d81x247['n'];
            break
        }
    };
    if (!_0x3d81x2d) {
        return
    };
    let _0x3d81x18d = this['modeMenu']['getElementsByClassName']('prKey');
    let _0x3d81x309 = null;
    for (var _0x3d81x13 = 0; _0x3d81x18d['length']; _0x3d81x13++) {
        _0x3d81x309 = _0x3d81x18d[_0x3d81x13];
        _0x3d81x18d[_0x3d81x13]['parentElement']['removeChild'](_0x3d81x18d[_0x3d81x13])
    };
    if (!_0x3d81x309) {
        _0x3d81x309 = document['createElement']('div');
        _0x3d81x309['textContent'] = 'F4';
        _0x3d81x309['className'] = 'prKey'
    };
    _0x3d81x2d['appendChild'](_0x3d81x309);
    let _0x3d81x30a = document['getElementById']('practice-last');
    _0x3d81x30a['textContent'] = _0x3d81x170;
    _0x3d81x30a['appendChild'](_0x3d81x309);
    _0x3d81x30a['onclick'] = _0x3d81x93['startPractice']['bind'](_0x3d81x93, _0x3d81x51);
    this['ruleSetChange'](this['pmodeRuleId'], false)
};
RulesetManager['prototype']['updateModeMenu'] = function() {
    let _0x3d81x19c = this['pmodeRuleId'],
        _0x3d81x93 = this['p'];
    while (this['modeMenu']['firstChild']) {
        this['modeMenu']['removeChild'](this['modeMenu']['firstChild'])
    };
    for (const _0x3d81x247 of this['MODES']) {
        if (!_0x3d81x247['elem']) {
            let _0x3d81x2d = _0x3d81x247['elem'] = document['createElement']('div');
            _0x3d81x2d['className'] = 'prMenuItem';
            _0x3d81x2d['innerHTML'] = _0x3d81x247['n'];
            _0x3d81x2d['addEventListener']('click', function() {
                _0x3d81x93['startPractice']['call'](_0x3d81x93, _0x3d81x247['id'])
            })
        };
        let _0x3d81x1ad = _0x3d81x247['modes'],
            _0x3d81x30b = [];
        for (const _0x3d81xb of _0x3d81x1ad) {
            if (_0x3d81xb['rs']['indexOf'](_0x3d81x19c) !== -1) {
                _0x3d81x30b['push'](_0x3d81xb)
            }
        };
        if (!_0x3d81x30b['length']) {
            continue
        };
        this['modeMenu']['appendChild'](_0x3d81x247['elem']);
        if (_0x3d81x30b['length'] > 1) {
            let _0x3d81x2d = document['createElement']('div');
            _0x3d81x2d['className'] = 'practice-submenu';
            for (const _0x3d81xb of _0x3d81x30b) {
                let _0x3d81x30c = document['createElement']('div');
                _0x3d81x30c['className'] = 'pract-subopt';
                _0x3d81x30c['innerHTML'] = _0x3d81xb['n'];
                _0x3d81x30c['onclick'] = function() {
                    _0x3d81x93['sprintMode'] = _0x3d81xb['id'];
                    _0x3d81x93['startPractice']['call'](_0x3d81x93, _0x3d81x247['id'])
                };
                _0x3d81x2d['appendChild'](_0x3d81x30c)
            };
            this['modeMenu']['appendChild'](_0x3d81x2d)
        }
    }
};
RulesetManager['prototype']['ruleSetChange'] = function(_0x3d81x7, _0x3d81x30d) {
    if (typeof _0x3d81x30d === 'undefined') {
        _0x3d81x30d = true
    };
    let _0x3d81x30e = this['RULESETS'][_0x3d81x7];
    if (!_0x3d81x30e) {
        return
    };
    this['applyRule'](_0x3d81x30e['c'], this['p']['conf'][1]);
    this['rs1']['children'][1]['textContent'] = _0x3d81x30e['name'];
    this['pmodeRuleId'] = parseInt(_0x3d81x7);
    this['updateModeMenu']();
    if (_0x3d81x30d) {
        this['adjustToValidMode'](this['p']['pmode'], this['p']['sprintMode'])
    }
};
RulesetManager['prototype']['createOptions'] = function() {
    let _0x3d81x25 = this;
    this['combo'] = document['createElement']('div');
    this['combo']['classList']['add']('comboclass');
    let _0x3d81x30f = document['createElement']('ul');
    for (const _0x3d81x310 of this['RULESETS']) {
        let _0x3d81x206 = document['createElement']('li');
        _0x3d81x206['classList']['add']('opt');
        let _0x3d81x311 = document['createElement']('span');
        _0x3d81x311['textContent'] = _0x3d81x310['name'];
        _0x3d81x206['appendChild'](_0x3d81x311);
        let _0x3d81x312 = document['createElement']('span');
        _0x3d81x312['classList']['add']('ruleDesc');
        _0x3d81x312['textContent'] = _0x3d81x310['desc'];
        _0x3d81x206['appendChild'](_0x3d81x312);
        let _0x3d81x313 = document['createElement']('span');
        _0x3d81x313['classList']['add']('ruleDiff');
        _0x3d81x313['innerHTML'] = getSVG('s-l' + _0x3d81x310['lvl'], 'dark', 'lIcn');
        _0x3d81x206['appendChild'](_0x3d81x313);
        _0x3d81x206['dataset']['id'] = _0x3d81x310['id'];
        _0x3d81x206['addEventListener']('click', function() {
            _0x3d81x25['ruleSetChange']['call'](_0x3d81x25, this['dataset']['id'])
        });
        _0x3d81x30f['appendChild'](_0x3d81x206)
    };
    this['combo']['appendChild'](_0x3d81x30f);
    this['combo']['addEventListener']('mouseup', function() {
        return false
    })
};
RulesetManager['prototype']['closeCombo'] = function(_0x3d81x2d) {
    if (this['combo'] === null) {
        return false
    };
    hideElem(this['combo']);
    this['isOpen'] = false;
    return true
};
RulesetManager['prototype']['toggleCombo'] = function(_0x3d81x2d, _0x3d81x86) {
    if (this['isOpen']) {
        this['closeCombo'](_0x3d81x2d)
    } else {
        if (!this['combo']) {
            this['createOptions']();
            _0x3d81x86['appendChild'](this['combo'])
        };
        showElem(this['combo']);
        this['isOpen'] = true
    };
    _0x3d81x2d['stopImmediatePropagation']()
};
RulesetManager['prototype']['registerCombo'] = function() {
    var _0x3d81x25 = this;
    this['rs1']['addEventListener']('mouseup', function(_0x3d81x2d) {
        _0x3d81x25['toggleCombo']['call'](_0x3d81x25, _0x3d81x2d, _0x3d81x25['rs1'])
    });
    document['addEventListener']('mouseup', function(_0x3d81x2d) {
        _0x3d81x25['closeCombo']['call'](_0x3d81x25, _0x3d81x2d)
    })
};
RulesetManager['prototype']['afterRuleChange'] = function(_0x3d81x314) {};
RulesetManager['prototype']['forceApplyChanges'] = function(_0x3d81x314) {
    this['p']['randomizer'] = this['p']['randomizerFactory'](_0x3d81x314['rnd']);
    this['p']['setSpeedLimit'](_0x3d81x314['speedLimit']);
    this['p']['applyGravityLvl'](_0x3d81x314['gravityLvl']);
    this['p']['setLockDelay'](_0x3d81x314['lockDelay'])
};
RulesetManager['prototype']['applyRule'] = function(_0x3d81x1c, _0x3d81x314, _0x3d81x315) {
    if (typeof _0x3d81x315 === 'undefined') {
        _0x3d81x315 = true
    };
    for (const _0x3d81x14 in this['RULE_KEYS']) {
        let _0x3d81x316 = this['RULE_KEYS'][_0x3d81x14];
        if (_0x3d81x1c['hasOwnProperty'](_0x3d81x14)) {
            _0x3d81x314[_0x3d81x316] = _0x3d81x1c[_0x3d81x14]
        } else {
            if (_0x3d81x315) {
                _0x3d81x314[_0x3d81x316] = _0x3d81x1c[_0x3d81x14] = this['DEF'][_0x3d81x316]
            }
        }
    };
    this['afterRuleChange'](_0x3d81x314)
};
RulesetManager['prototype']['applyPresetRule'] = function(_0x3d81x1c, _0x3d81x314, _0x3d81x315) {
    if (typeof _0x3d81x315 === 'undefined') {
        _0x3d81x315 = true
    };
    let _0x3d81x46 = {};
    for (const _0x3d81x14 in this['PRESET_KEYS']) {
        let _0x3d81x316 = this['PRESET_KEYS'][_0x3d81x14];
        let _0x3d81x317 = null;
        if (_0x3d81x1c['hasOwnProperty'](_0x3d81x14)) {
            _0x3d81x317 = _0x3d81x1c[_0x3d81x14]
        } else {
            if (_0x3d81x315) {
                _0x3d81x317 = _0x3d81x1c[_0x3d81x14] = this['DEF'][_0x3d81x316]
            }
        };
        if (_0x3d81x317 !== null && (!_0x3d81x314['hasOwnProperty'](_0x3d81x316) || _0x3d81x314[_0x3d81x316] != _0x3d81x317)) {
            _0x3d81x314[_0x3d81x316] = _0x3d81x317;
            _0x3d81x46[_0x3d81x316] = _0x3d81x317
        }
    };
    this['afterRuleChange'](_0x3d81x314);
    return _0x3d81x46
};
RulesetManager['prototype']['appendRule'] = function(_0x3d81x1c, _0x3d81x314) {
    for (const _0x3d81x14 in this['RULE_KEYS']) {
        let _0x3d81x316 = this['RULE_KEYS'][_0x3d81x14];
        if (_0x3d81x1c['hasOwnProperty'](_0x3d81x14)) {
            _0x3d81x314[_0x3d81x316] = _0x3d81x1c[_0x3d81x14]
        }
    };
    this['afterRuleChange'](_0x3d81x314)
};
'use strict';

function Report(_0x3d81x139) {
    this['p'] = _0x3d81x139;
    this['reportWin'] = document['getElementById']('reportU');
    this['pmodeRuleId'] = 0;
    this['icn'] = document['getElementsByClassName']('warnI')[0];
    this['rReTxt'] = document['getElementById']('rReTxt');
    this['repUser'] = document['getElementById']('repUser');
    this['init']()
}
Report['prototype']['init'] = function() {
    this['icn']['addEventListener']('click', this['openReport']['bind'](this));
    document['getElementById']('repClose')['addEventListener']('click', this['closeReport']['bind'](this));
    document['getElementById']('repSend')['addEventListener']('click', this['sendReport']['bind'](this));
    var _0x3d81x319 = document['repForm']['rRe'];
    var _0x3d81x25 = this;
    for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x319['length']; _0x3d81x13++) {
        _0x3d81x319[_0x3d81x13]['addEventListener']('change', function() {
            if (this['value'] == '4') {
                showElem(_0x3d81x25['rReTxt'])
            } else {
                hideElem(_0x3d81x25['rReTxt'])
            }
        })
    };
    if (!this['p']['Live']['authorized']) {
        let _0x3d81x12 = document['querySelector']('svg.warnI');
        hideElem(_0x3d81x12)
    }
};
Report['prototype']['closeReport'] = function() {
    hideElem(this['reportWin'])
};
Report['prototype']['sendReport'] = function() {
    var _0x3d81x31a = this['repUser']['value'];
    var _0x3d81x31b = parseInt(document['querySelector']('input[name="rRe"]:checked')['value']);
    var _0x3d81x31c = null;
    if (_0x3d81x31b == '4') {
        _0x3d81x31c = this['rReTxt']['value']['trim']();
        if (_0x3d81x31c == '') {
            alert('Reason cannot be empty!');
            return
        }
    };
    if (_0x3d81x31a == '') {
        alert('User must be selected!');
        return
    };
    var _0x3d81x15d = {
        "\x74": 32,
        "\x75": _0x3d81x31a,
        "\x72": _0x3d81x31b,
        "\x73": _0x3d81x31c
    };
    this['p']['Live']['safeSend'](JSON['stringify'](_0x3d81x15d));
    this['closeReport']()
};
Report['prototype']['openReport'] = function() {
    this['repUser']['innerHTML'] = '';
    let _0x3d81xce = [],
        _0x3d81x27 = this['p']['Live']['cid'];
    for (var _0x3d81x20 in this['p']['Live']['clients']) {
        var _0x3d81xba = this['p']['Live']['clients'][_0x3d81x20];
        if (_0x3d81x27 == _0x3d81xba['cid'] || _0x3d81xba['mod'] || !_0x3d81xba['auth']) {
            continue
        };
        let _0x3d81x184 = document['createElement']('option');
        _0x3d81x184['textContent'] = _0x3d81xba['name'];
        _0x3d81x184['value'] = _0x3d81xba['name'];
        _0x3d81xce['push'](_0x3d81x184)
    };
    _0x3d81xce['sort'](function(_0x3d81xa5, _0x3d81x141) {
        if (_0x3d81xa5['textContent']['toUpperCase']() > _0x3d81x141['textContent']['toUpperCase']()) {
            return 1
        } else {
            if (_0x3d81xa5['textContent']['toUpperCase']() < _0x3d81x141['textContent']['toUpperCase']()) {
                return -1
            } else {
                return 0
            }
        }
    });
    if (_0x3d81xce['length']) {
        this['repUser']['disabled'] = false;
        for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81xce['length']; _0x3d81x13++) {
            this['repUser']['appendChild'](_0x3d81xce[_0x3d81x13])
        }
    } else {
        this['repUser']['disabled'] = true;
        let _0x3d81x184 = document['createElement']('option');
        _0x3d81x184['textContent'] = 'No users to report';
        _0x3d81x184['value'] = '';
        this['repUser']['appendChild'](_0x3d81x184)
    };
    showElem(this['reportWin']);
    this['rReTxt']['value'] = '';
    document['getElementById']('rr0')['checked'] = true;
    hideElem(this['rReTxt']);
    this['p']['setFocusState'](1)
};
'use strict';

function Items(_0x3d81x139) {
    this['p'] = _0x3d81x139;
    this['itmBox'] = document['getElementsByClassName']('itmBox')[0];
    this['itmIcn'] = document['getElementsByClassName']('itmIcn')[0];
    this['itmTxt'] = document['getElementsByClassName']('itmTxt')[0];
    this['itmDef'] = [null, {
        "\x69\x64": 1,
        "\x6E": 'Tblocks',
        "\x69": '/res/img/i/tpieces.png',
        "\x6D": 0.24,
        "\x70": 0.12
    }, {
        "\x69\x64": 2,
        "\x6E": 'Tornado',
        "\x69": '/res/img/i/tornado.png',
        "\x6D": 0.22,
        "\x70": 0.22
    }, {
        "\x69\x64": 3,
        "\x6E": 'Compress',
        "\x69": '/res/img/i/compress.png',
        "\x6D": 0.20,
        "\x70": 0.06
    }, {
        "\x69\x64": 4,
        "\x6E": 'Fourwide',
        "\x69": '/res/img/i/four.png',
        "\x6D": 0.04,
        "\x70": 0.06
    }, {
        "\x69\x64": 5,
        "\x6E": 'Poison',
        "\x69": '/res/img/i/poison.png',
        "\x6D": 0.003,
        "\x70": 0.03
    }, {
        "\x69\x64": 6,
        "\x6E": 'Pentomino',
        "\x69": '/res/img/i/pento.png',
        "\x6D": 0.107,
        "\x70": 0.06
    }, {
        "\x69\x64": 7,
        "\x6E": 'Big',
        "\x69": '/res/img/i/big.png',
        "\x6D": 0.13,
        "\x70": 0.06
    }, {
        "\x69\x64": 8,
        "\x6E": 'Invert',
        "\x69": '/res/img/i/invert.png',
        "\x6D": 0.02,
        "\x70": 0.06
    }, {
        "\x69\x64": 9,
        "\x6E": 'Mystery',
        "\x69": '/res/img/i/unknown.png',
        "\x6D": 0,
        "\x70": 0.30
    }, {
        "\x69\x64": 10,
        "\x6E": 'Win',
        "\x69": '/res/img/i/win.png',
        "\x6D": 0,
        "\x70": 0
    }, {
        "\x69\x64": 11,
        "\x6E": 'B1',
        "\x69": '/res/img/i/dot.png',
        "\x6D": 0.04,
        "\x70": 0.03
    }, {
        "\x69\x64": 12,
        "\x6E": 'Big2',
        "\x69": '/res/img/i/big.png',
        "\x6D": 0,
        "\x70": 0
    }];
    this['item'] = null;
    this['key'] = 87;
    this['origBBS'] = null;
    this['P1'] = 200;
    this['fs'] = false;
    this['f'] = null;
    this['isPriv'] = false;
    this['active'] = [];
    this['preload'] = [];
    var _0x3d81x127 = new Date();
    this['dom'] = parseInt(_0x3d81x127['getDate']())
}
Items['prototype']['avail'] = function() {
    return false && !this['isPriv'] && !this['p']['isPmode'](true)
};
Items['prototype']['loadConf'] = function(_0x3d81x15d) {
    this['P1'] = _0x3d81x15d['P1'];
    var _0x3d81x25 = this;
    _0x3d81x15d['def']['forEach'](function(_0x3d81x2d) {
        if (_0x3d81x2d['m']) {
            _0x3d81x25['itmDef'][_0x3d81x2d['id']]['m'] = _0x3d81x2d['m']
        };
        if (_0x3d81x2d['p']) {
            _0x3d81x25['itmDef'][_0x3d81x2d['id']]['p'] = _0x3d81x2d['p']
        }
    })
};
Items['prototype']['autoSelectKey'] = function() {
    let _0x3d81x154 = 0;
    let _0x3d81x31e = [
        [87, 'w'],
        [86, 'v'],
        [88, 'x'],
        [90, 'z'],
        [67, 'c'],
        [65, 'a'],
        [66, 'b'],
        [68, 'd'],
        [69, 'e'],
        [70, 'f'],
        [71, 'g']
    ];
    let _0x3d81x31f = this['p']['Settings']['controls'];
    for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x31e['length']; _0x3d81x13++) {
        let _0x3d81x14 = _0x3d81x31e[_0x3d81x13];
        if (!_0x3d81x31f['includes'](_0x3d81x14[0])) {
            _0x3d81x154 = _0x3d81x13;
            break
        }
    };
    let _0x3d81x14 = _0x3d81x31e[_0x3d81x154][0];
    let _0x3d81xac = '<b>' + _0x3d81x31e[_0x3d81x154][1]['toUpperCase']() + '</b> to use';
    this['key'] = _0x3d81x14;
    this['itmTxt']['innerHTML'] = _0x3d81xac
};
Items['prototype']['genItem'] = function() {
    var _0x3d81x153 = this['p']['random'](0, this['P1'] - 1);
    if (!this['avail']()) {
        return
    };
    if (_0x3d81x153 === 0 || this['fs']) {
        this['fs'] = false;
        return this['p']['random'](1, 4)
    } else {
        return 0
    }
};
Items['prototype']['genItemType'] = function() {
    if (this['f']) {
        let _0x3d81x12 = this['itmDef'][this['f']];
        this['f'] = null;
        return _0x3d81x12
    };
    return this['resolveProb']('p')
};
Items['prototype']['onReset'] = function() {
    if (this['avail']()) {
        this['preloadIcons']()
    };
    this['origBBS'] = 0;
    var _0x3d81x25 = this;
    if (this['active']) {
        this['active']['forEach'](function(_0x3d81x320) {
            _0x3d81x25['revert'](_0x3d81x320['id'])
        })
    };
    this['active'] = [];
    this['autoSelectKey']();
    this['reset']()
};
Items['prototype']['reset'] = function() {
    this['item'] = null;
    hideElem(this['itmBox'])
};
Items['prototype']['pickup'] = function() {
    showElem(this['itmBox']);
    let _0x3d81x321 = this['genItemType']();
    this['item'] = _0x3d81x321['id'];
    this['p']['playSound']('pickup');
    if (this['item'] === 9) {
        this['preloadIcons']();
        this['itmIcn']['style']['backgroundImage'] = null;
        this['itmIcn']['classList']['add']('mysteryItem');
        this['itmIcn']['classList']['add']('fullSize')
    } else {
        this['itmIcn']['style']['backgroundImage'] = 'url(' + _0x3d81x321['i'] + ')';
        this['itmIcn']['classList']['remove']('mysteryItem');
        this['itmIcn']['classList']['remove']('fullSize')
    }
};
Items['prototype']['onHardDrop'] = function() {
    let _0x3d81x13 = this['active']['length'];
    while (_0x3d81x13--) {
        let _0x3d81x320 = this['active'][_0x3d81x13];
        _0x3d81x320['hd']--;
        if (_0x3d81x320['hd'] <= 0) {
            this['active']['splice'](_0x3d81x13, 1);
            this['revert'](_0x3d81x320['id'])
        }
    }
};
Items['prototype']['use'] = function(_0x3d81x40) {
    _0x3d81x40 = (_0x3d81x40) ? _0x3d81x40 : this['p']['timestamp']();
    let _0x3d81x322 = this['item'];
    if (!this['item']) {
        return
    };
    this['item'] = null;
    if (_0x3d81x322 !== 9) {
        this['reset']()
    };
    this['p']['playSound']('item');
    this['applyItem'](_0x3d81x322, _0x3d81x40)
};
Items['prototype']['applyItem'] = function(_0x3d81x322, _0x3d81x40) {
    if (_0x3d81x322 === 1) {
        this['changeRandomizer'](11, true, _0x3d81x40);
        let _0x3d81x323 = new ItemActivation(_0x3d81x322);
        _0x3d81x323['hd'] = 7;
        this['active']['push'](_0x3d81x323)
    } else {
        if (_0x3d81x322 === 2) {
            this['changeMatrix'](1)
        } else {
            if (_0x3d81x322 === 3) {
                this['changeMatrix'](2)
            } else {
                if (_0x3d81x322 === 4) {
                    this['changeMatrix'](3);
                    let _0x3d81x323 = new ItemActivation(_0x3d81x322);
                    _0x3d81x323['hd'] = this['p']['random'](5, 11);
                    this['active']['push'](_0x3d81x323)
                } else {
                    if (_0x3d81x322 === 5) {
                        this['p']['play'] = false;
                        this['p']['animator'] = new PoisonAnimator(this, this['p'])
                    } else {
                        if (_0x3d81x322 === 6) {
                            this['changeBBS'](4, _0x3d81x40);
                            let _0x3d81x323 = new ItemActivation(_0x3d81x322);
                            _0x3d81x323['hd'] = 5;
                            this['active']['push'](_0x3d81x323)
                        } else {
                            if (_0x3d81x322 === 7) {
                                var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, _0x3d81x40);
                                _0x3d81x42['d'] = [this['p']['Replay']['AUX']['BLOCK_SET'], 0, 2];
                                this['p']['Replay']['add'](_0x3d81x42);
                                this['p']['temporaryBlockSet'] = 2
                            } else {
                                if (_0x3d81x322 === 8) {
                                    this['changeMatrix'](4)
                                } else {
                                    if (_0x3d81x322 === 9) {
                                        this['mysteryItem'](_0x3d81x40)
                                    } else {
                                        if (_0x3d81x322 === 10) {
                                            this['p']['Caption']['loading']('Instant win', 2)
                                        } else {
                                            if (_0x3d81x322 === 11) {
                                                this['changeRandomizer'](12, true, _0x3d81x40);
                                                let _0x3d81x323 = new ItemActivation(_0x3d81x322);
                                                _0x3d81x323['hd'] = this['p']['random'](7, 28);
                                                this['active']['push'](_0x3d81x323)
                                            } else {
                                                if (_0x3d81x322 === 12) {
                                                    this['changeBBS'](2, _0x3d81x40);
                                                    let _0x3d81x323 = new ItemActivation(_0x3d81x322);
                                                    _0x3d81x323['hd'] = this['p']['random'](5, 27);
                                                    this['active']['push'](_0x3d81x323)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
Items['prototype']['preloadIcons'] = function() {
    if (this['preload']['length']) {
        return
    };
    for (var _0x3d81x13 of this['itmDef']) {
        if (!_0x3d81x13) {
            continue
        };
        var _0x3d81xc1 = new Image();
        _0x3d81xc1['src'] = _0x3d81x13['i'];
        this['preload']['push'](_0x3d81xc1)
    }
};
Items['prototype']['resolveProb'] = function(_0x3d81x324) {
    var _0x3d81x279 = Math['random'](),
        _0x3d81x185 = 0;
    for (var _0x3d81x13 = 0; _0x3d81x13 < this['itmDef']['length']; _0x3d81x13++) {
        let _0x3d81x325 = this['itmDef'][_0x3d81x13];
        if (!_0x3d81x325) {
            continue
        };
        _0x3d81x185 += _0x3d81x325[_0x3d81x324];
        if (_0x3d81x279 < _0x3d81x185) {
            return _0x3d81x325
        }
    };
    return results[this['itmDef']['length'] - 1]
};
Items['prototype']['resolveMystery'] = function() {
    return this['resolveProb']('m')
};
Items['prototype']['mysteryItem'] = function(_0x3d81x40) {
    let _0x3d81x322 = this['resolveMystery']()['id'];
    let _0x3d81x321 = this['itmDef'][_0x3d81x322];
    this['itmIcn']['style']['backgroundImage'] = 'url(' + _0x3d81x321['i'] + ')';
    this['applyItem'](_0x3d81x322, _0x3d81x40);
    this['itmIcn']['classList']['remove']('fullSize');
    var _0x3d81x25 = this;
    setTimeout(function() {
        if (_0x3d81x25['item'] === null) {
            hideElem(_0x3d81x25['itmBox'])
        }
    }, 1500)
};
Items['prototype']['changeBBS'] = function(_0x3d81x41, _0x3d81x40) {
    _0x3d81x40 = (_0x3d81x40) ? _0x3d81x40 : this['p']['timestamp']();
    var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, _0x3d81x40);
    _0x3d81x42['d'] = [this['p']['Replay']['AUX']['BLOCK_SET'], 1, _0x3d81x41];
    this['p']['Replay']['add'](_0x3d81x42);
    this['p']['R']['baseBlockSet'] = _0x3d81x41;
    this['changeRandomizer'](this['p']['R']['rnd'], true, _0x3d81x40)
};
Items['prototype']['changeMatrix'] = function(_0x3d81x7) {
    if (_0x3d81x7 === 1) {
        let _0x3d81x279 = this['p']['random'](10, 31);
        this['p']['play'] = false;
        this['p']['animator'] = new WindAnimator(this, this['p'], _0x3d81x279);
        var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, this['p']['timestamp']());
        _0x3d81x42['d'] = [this['p']['Replay']['AUX']['MATRIX_MOD'], 0, _0x3d81x279];
        this['p']['Replay']['add'](_0x3d81x42)
    } else {
        if (_0x3d81x7 === 2) {
            this['p']['play'] = false;
            this['p']['animator'] = new CompressAnimator(this, this['p']);
            var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, this['p']['timestamp']());
            _0x3d81x42['d'] = [this['p']['Replay']['AUX']['MATRIX_MOD'], 1, 0];
            this['p']['Replay']['add'](_0x3d81x42)
        } else {
            if (_0x3d81x7 === 3) {
                var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, this['p']['timestamp']());
                _0x3d81x42['d'] = [this['p']['Replay']['AUX']['MATRIX_MOD'], 2, 0];
                this['p']['Replay']['add'](_0x3d81x42);
                this['loadFourWide']();
                this['p']['redraw']()
            } else {
                if (_0x3d81x7 === 4) {
                    var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, this['p']['timestamp']());
                    _0x3d81x42['d'] = [this['p']['Replay']['AUX']['MATRIX_MOD'], 3, 0];
                    this['p']['Replay']['add'](_0x3d81x42);
                    this['p']['play'] = false;
                    this['p']['animator'] = new InvertAnimator(this, this['p']);
                    this['p']['setCurrentPieceToDefaultPos']();
                    this['p']['updateGhostPiece'](true)
                }
            }
        }
    }
};
Items['prototype']['wipeDeadline'] = function() {
    for (var _0x3d81x19 = 0; _0x3d81x19 < 10; ++_0x3d81x19) {
        this['p']['deadline'][_0x3d81x19] = 0
    }
};
Items['prototype']['invertMatrix'] = function() {
    this['wipeDeadline']();
    for (var _0x3d81x18 = 0; _0x3d81x18 < 2; _0x3d81x18++) {
        if (this['p']['matrix'][_0x3d81x18][0] === 9) {
            continue
        };
        var _0x3d81x326 = this['p']['matrix'][_0x3d81x18];
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; ++_0x3d81x19) {
            _0x3d81x326[_0x3d81x19] = 0
        }
    };
    for (var _0x3d81x18 = 2; _0x3d81x18 < this['p']['matrix']['length']; _0x3d81x18++) {
        if (this['p']['matrix'][_0x3d81x18][0] === 9) {
            continue
        };
        var _0x3d81x326 = this['p']['matrix'][_0x3d81x18],
            _0x3d81x327 = 0;
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; ++_0x3d81x19) {
            if (_0x3d81x326[_0x3d81x19]) {
                _0x3d81x326[_0x3d81x19] = 0
            } else {
                if (_0x3d81x326[_0x3d81x19] === 0) {
                    _0x3d81x326[_0x3d81x19] = 8;
                    _0x3d81x327++
                }
            }
        };
        if (_0x3d81x327 === 10) {
            _0x3d81x326[9] = 0
        }
    }
};
Items['prototype']['loadFourWide'] = function(_0x3d81x328) {
    if (!_0x3d81x328) {
        _0x3d81x328 = this['p']['MapManager']
    };
    var _0x3d81x329 = 0;
    _0x3d81x328['parseMatrix']('VmAAAiJUYAACIlRAAAMyUUAAAzIRcAAGZhdwAAEWEXAABxFREAAHdVIgAAcVUiAAARVSIAABdWIgAAd3ZmAABmYzUAAGZjNQAAYzIlAABjNSUAACIlIQAAJEURRABEJRRAACIg==');
    this['wipeDeadline']();
    let _0x3d81x272 = 0;
    for (var _0x3d81x18 = _0x3d81x329 = this['p']['matrix']['length'] - 1; _0x3d81x18 >= 0; --_0x3d81x18,
        --_0x3d81x329) {
        if (this['p']['matrix'][_0x3d81x18][0] === 9) {
            _0x3d81x272++;
            continue
        };
        this['p']['matrix'][_0x3d81x18] = _0x3d81x328['matrix'][_0x3d81x329 + _0x3d81x272]
    };
    this['p']['setCurrentPieceToDefaultPos']();
    this['p']['updateGhostPiece'](true);
    this['p']['comboCounter'] = -1
};
Items['prototype']['compressMatrix'] = function(_0x3d81x8d) {
    function _0x3d81x32a(_0x3d81x11c, _0x3d81x326) {
        if (_0x3d81x326[_0x3d81x11c - 1]) {
            return
        };
        for (var _0x3d81x19 = _0x3d81x11c; _0x3d81x19 < 10; ++_0x3d81x19) {
            _0x3d81x326[_0x3d81x19 - 1] = _0x3d81x326[_0x3d81x19]
        };
        _0x3d81x326[9] = 0
    }
    this['wipeDeadline']();
    for (var _0x3d81x13 = 1; _0x3d81x13 <= _0x3d81x8d; _0x3d81x13++) {
        for (var _0x3d81x18 = 0; _0x3d81x18 < 20; ++_0x3d81x18) {
            if (this['p']['matrix'][_0x3d81x18][0] === 9) {
                continue
            };
            _0x3d81x32a(_0x3d81x13, this['p']['matrix'][_0x3d81x18])
        }
    }
};
Items['prototype']['tfTornado'] = function(_0x3d81x7f) {
    var _0x3d81x1e9 = copyMatrix(this['p']['matrix']);
    this['wipeDeadline']();
    for (var _0x3d81x18 = 0; _0x3d81x18 < 20; ++_0x3d81x18) {
        if (_0x3d81x1e9[_0x3d81x18][0] === 9) {
            continue
        };
        let _0x3d81x32b = ((_0x3d81x18 % 2 === 0) ? 1 : -1) * _0x3d81x7f;
        for (var _0x3d81x19 = 0; _0x3d81x19 < 10; ++_0x3d81x19) {
            let _0x3d81x32c = (_0x3d81x19 + _0x3d81x32b) % 10;
            while (_0x3d81x32c < 0) {
                _0x3d81x32c += 10
            };
            this['p']['matrix'][_0x3d81x18][_0x3d81x19] = _0x3d81x1e9[_0x3d81x18][_0x3d81x32c]
        }
    }
};
Items['prototype']['changeRandomizer'] = function(_0x3d81x7, _0x3d81x32d, _0x3d81x2e) {
    let _0x3d81x40 = (_0x3d81x2e) ? _0x3d81x2e : this['p']['timestamp']();
    let _0x3d81x32e = (_0x3d81x32d) ? 1 : 0;
    this['p']['randomizer'] = this['p']['randomizerFactory'](_0x3d81x7);
    if (_0x3d81x32d) {
        this['p']['generateQueue']();
        this['p']['updateQueueBox']()
    };
    var _0x3d81x42 = new ReplayAction(this['p']['Replay']['Action'].AUX, _0x3d81x40);
    _0x3d81x42['d'] = [this['p']['Replay']['AUX']['RANDOMIZER'], _0x3d81x32e, _0x3d81x7];
    this['p']['Replay']['add'](_0x3d81x42)
};
Items['prototype']['revert'] = function(_0x3d81x7) {
    if (_0x3d81x7 === 1) {
        this['changeRandomizer'](this['p']['R']['rnd'], false)
    } else {
        if (_0x3d81x7 === 4) {
            let _0x3d81x18d = this['item'];
            this['item'] = 2;
            this['use']();
            this['item'] = _0x3d81x18d;
            this['p']['hdAbort'] = true
        } else {
            if (_0x3d81x7 === 6) {
                this['changeBBS'](this['origBBS'])
            } else {
                if (_0x3d81x7 === 11) {
                    this['changeRandomizer'](this['p']['R']['rnd'], false)
                } else {
                    if (_0x3d81x7 === 12) {
                        this['changeBBS'](this['origBBS'])
                    }
                }
            }
        }
    }
};

function InvertAnimator(_0x3d81x330, _0x3d81x93) {
    this['items'] = _0x3d81x330;
    this['g'] = _0x3d81x93;
    this['matrix'] = _0x3d81x93['matrix'];
    this['tmpMatrix'] = copyMatrix(_0x3d81x93['matrix']);
    this['items']['invertMatrix']();
    this['PER_ROUND'] = 0.1;
    this['hadGhost'] = this['g']['ghostEnabled'];
    this['i'] = 0;
    this['d'] = 0;
    this['g']['ghostEnabled'] = false;
    this['g']['redraw']()
}
InvertAnimator['prototype']['render'] = function(_0x3d81x94) {
    this['d'] += _0x3d81x94;
    while (this['d'] > this['PER_ROUND']) {
        this['d'] -= this['PER_ROUND'];
        this['iter']()
    }
};
InvertAnimator['prototype']['iter'] = function() {
    this['i']++;
    var _0x3d81x127 = this['tmpMatrix'];
    this['tmpMatrix'] = this['g']['matrix'];
    this['g']['matrix'] = _0x3d81x127;
    if (this['i'] === 2) {
        this['finished']();
        return
    };
    this['g']['redraw']()
};
InvertAnimator['prototype']['finished'] = function() {
    this['g']['animator'] = null;
    if (!this['g']['gameEnded']) {
        this['g']['play'] = true
    };
    this['g']['redrawBlocked'] = false;
    this['g']['ghostEnabled'] = this['hadGhost'];
    this['g']['updateGhostPiece'](true);
    this['g']['redraw']()
};

function PoisonAnimator(_0x3d81x330, _0x3d81x93) {
    this['items'] = _0x3d81x330;
    this['g'] = _0x3d81x93;
    this['matrix'] = _0x3d81x93['matrix'];
    this['PER_ROUND'] = 0.06;
    this['hadGhost'] = this['g']['ghostEnabled'];
    this['i'] = 0;
    this['d'] = 0;
    this['g']['setCurrentPieceToDefaultPos']();
    this['g']['updateGhostPiece'](true);
    this['g']['ghostEnabled'] = false
}
PoisonAnimator['prototype']['render'] = function(_0x3d81x94) {
    this['d'] += _0x3d81x94;
    while (this['d'] > this['PER_ROUND']) {
        this['d'] -= this['PER_ROUND'];
        this['iter']()
    }
};
PoisonAnimator['prototype']['iter'] = function() {
    this['i']++;
    this['g']['play'] = true;
    this['g']['hardDrop']();
    if (!this['g']['play']) {
        this['finished']()
    };
    this['g']['play'] = false
};
PoisonAnimator['prototype']['finished'] = function() {
    this['g']['animator'] = null;
    if (!this['g']['gameEnded']) {
        this['g']['play'] = true
    };
    this['g']['redrawBlocked'] = false;
    this['g']['ghostEnabled'] = this['hadGhost'];
    this['g']['updateGhostPiece'](true);
    this['g']['redraw']()
};

function CompressAnimator(_0x3d81x330, _0x3d81x93) {
    this['items'] = _0x3d81x330;
    this['g'] = _0x3d81x93;
    this['matrix'] = _0x3d81x93['matrix'];
    this['PER_ROUND'] = 0.06;
    this['hadGhost'] = this['g']['ghostEnabled'];
    this['i'] = 0;
    this['d'] = 0;
    this['g']['setCurrentPieceToDefaultPos']();
    this['g']['updateGhostPiece'](true);
    this['g']['ghostEnabled'] = false
}
CompressAnimator['prototype']['render'] = function(_0x3d81x94) {
    this['d'] += _0x3d81x94;
    while (this['d'] > this['PER_ROUND']) {
        this['d'] -= this['PER_ROUND'];
        this['iter']()
    }
};
CompressAnimator['prototype']['iter'] = function() {
    this['i']++;
    this['items']['compressMatrix'](this['i']);
    this['g']['redraw']();
    if (this['i'] >= 10) {
        this['finished']()
    }
};
CompressAnimator['prototype']['finished'] = function() {
    this['g']['animator'] = null;
    if (!this['g']['gameEnded']) {
        this['g']['play'] = true
    };
    this['g']['redrawBlocked'] = false;
    this['g']['ghostEnabled'] = this['hadGhost'];
    this['g']['updateGhostPiece'](true);
    this['g']['redraw']()
};

function WindAnimator(_0x3d81x330, _0x3d81x93, _0x3d81x334) {
    this['items'] = _0x3d81x330;
    this['g'] = _0x3d81x93;
    this['i'] = _0x3d81x334;
    this['matrix'] = _0x3d81x93['matrix'];
    this['PER_ROUND'] = 0.06;
    this['d'] = 0;
    this['hadGhost'] = this['g']['ghostEnabled'];
    this['g']['setCurrentPieceToDefaultPos']();
    this['g']['updateGhostPiece'](true);
    this['g']['ghostEnabled'] = false
}
WindAnimator['prototype']['render'] = function(_0x3d81x94) {
    this['d'] += _0x3d81x94;
    while (this['d'] > this['PER_ROUND']) {
        this['d'] -= this['PER_ROUND'];
        this['iter']()
    }
};
WindAnimator['prototype']['iter'] = function() {
    this['items']['tfTornado'](1);
    this['g']['redraw']();
    this['i']--;
    if (this['i'] <= 0) {
        this['finished']()
    }
};
WindAnimator['prototype']['finished'] = function() {
    this['g']['animator'] = null;
    if (!this['g']['gameEnded']) {
        this['g']['play'] = true
    };
    this['g']['redrawBlocked'] = false;
    this['g']['ghostEnabled'] = this['hadGhost'];
    this['g']['updateGhostPiece'](true);
    this['g']['redraw']()
};

function ItemActivation(_0x3d81x7) {
    this['id'] = _0x3d81x7;
    this['hd'] = 0
}

function Friends(_0x3d81x217) {
    this['Live'] = _0x3d81x217;
    this['socket'] = null;
    this['uri'] = null;
    this['jwt'] = null;
    this['reconnects'] = 0;
    this['forcedReconDelay'] = 0;
    this['pendingStatus'] = null;
    this['friendsOpened'] = false;
    this['friendsCount'] = null;
    this['openChatName'] = null;
    this['friends'] = null;
    this['Live']['friendsBtn']['onclick'] = this['openFriends']['bind'](this);
    this['chatBox'] = document['getElementById']('chatBox');
    this['notifElem'] = null;
    this['chatHeader'] = null;
    this['dmChatBackButton'] = null;
    this['unreadChannels'] = {};
    this['lastNotifSound'] = null;
    this['friendsError'] = null;
    this['statusData'] = null;
    this['VIEW_MODE'] = {
        FRIEND_LIST_LOADING: 0,
        NO_FRIENDS: 1,
        FRIEND_LIST: 2,
        TOO_MANY_CONN: 3,
        LOGIN_FIRST: 4,
        INTRO: 5
    };
    if (this['viewIntro']()) {
        this['friendsNotif'](-1)
    }
}
Friends['prototype']['connect'] = function(_0x3d81x191, _0x3d81x190) {
    this['uri'] = _0x3d81x191;
    this['jwt'] = _0x3d81x190;
    this['socket'] = new WebSocket(_0x3d81x191);
    this['socket']['onopen'] = this['onOpen']['bind'](this);
    this['socket']['onmessage'] = this['onMessage']['bind'](this);
    this['socket']['onclose'] = this['onClose']['bind'](this);
    var _0x3d81x25 = this;
    this['connectionTimeout'] = setTimeout(function() {
        var _0x3d81x1dd = _0x3d81x25['socket'];
        if (_0x3d81x1dd !== null && _0x3d81x1dd['readyState'] === 0) {
            _0x3d81x1dd['onclose'] = _0x3d81x1dd['onmessage'] = function() {};
            _0x3d81x1dd['onopen'] = function() {
                _0x3d81x1dd['close']()
            };
            _0x3d81x1dd['close']();
            _0x3d81x25['socket'] = _0x3d81x1dd = null;
            console['log']('FS timeout');
            _0x3d81x25['reconnect']()
        }
    }, 5000)
};
Friends['prototype']['connected'] = function() {
    return this['socket'] && this['socket']['readyState'] === this['socket']['OPEN']
};
Friends['prototype']['isFriendChat'] = function() {
    return this['friendsOpened'] && this['openChatName']
};
Friends['prototype']['safeSend'] = function(_0x3d81x147) {
    if (this['socket'] && this['socket']['readyState'] === this['socket']['OPEN']) {
        this['socket']['send'](_0x3d81x147);
        return true
    } else {
        return false
    }
};
Friends['prototype']['sendChat'] = function(_0x3d81x147) {
    let _0x3d81x1f5 = JSON['stringify']({
        t: 5,
        u: this['openChatName'],
        m: _0x3d81x147
    });
    this['safeSend'](_0x3d81x1f5);
    if (!this['connected']()) {
        let _0x3d81x8c = document['createElement']('div');
        _0x3d81x8c['innerHTML'] = '<b>' + this['Live']['getName'](this['Live']['cid']) + '</b>: ' + htmlEntities(_0x3d81x147);
        _0x3d81x8c['style']['color'] = '#ff5555';
        this['showInChat']('', _0x3d81x8c);
        let _0x3d81x231 = document['createElement']('img');
        _0x3d81x231['src'] = CDN_URL('/res/svg/spinWhite.svg');
        _0x3d81x231['style']['width'] = '0.8em';
        _0x3d81x8c = document['createElement']('div');
        _0x3d81x8c['appendChild'](_0x3d81x231);
        _0x3d81x8c['style']['fontWeight'] = 'bold';
        _0x3d81x8c['innerHTML'] += ' Could not send, reconnecting...';
        _0x3d81x8c['style']['fontSize'] = '0.7em';
        _0x3d81x8c['style']['color'] = 'grey';
        this['showInChat']('', _0x3d81x8c)
    }
};
Friends['prototype']['sendInvite'] = function() {
    if (!this['friendsOpened'] || !this['openChatName'] || !this['statusData']) {
        return
    };
    let _0x3d81x153 = this['statusData']['r'];
    let _0x3d81x131 = this['statusData']['n']['replace']('/:/g', '\:');
    let _0x3d81xac = '[INV:' + _0x3d81x153 + ':' + _0x3d81x131 + ']';
    this['sendChat'](_0x3d81xac)
};
Friends['prototype']['reconnect'] = function() {
    this['reconnects']++;
    let _0x3d81x337 = Math['min'](5000, (this['reconnects'] - 1) * 1000);
    if (this['reconnects'] > 60) {
        _0x3d81x337 = 30000
    };
    _0x3d81x337 = Math['max'](this['forcedReconDelay'], _0x3d81x337);
    if (this['reconnects'] <= 100) {
        var _0x3d81x25 = this;
        setTimeout(function() {
            _0x3d81x25['connect'](_0x3d81x25['uri'], _0x3d81x25['jwt'])
        }, _0x3d81x337)
    }
};
Friends['prototype']['onOpen'] = function() {
    let _0x3d81x1f5 = JSON['stringify']({
        t: 1,
        j: this['jwt']
    });
    this['safeSend'](_0x3d81x1f5)
};
Friends['prototype']['onMessage'] = function(_0x3d81x2d) {
    if (typeof _0x3d81x2d['data'] != 'string') {
        return
    };
    this['handleResponse'](JSON['parse'](_0x3d81x2d['data']))
};
Friends['prototype']['handleResponse'] = function(_0x3d81x11) {
    switch (_0x3d81x11['t']) {
        case 1:
            this['reconnects'] = 0;
            this['forcedReconDelay'] = 0;
            this['friendsError'] = null;
            let _0x3d81x1f5 = JSON['stringify']({
                t: 2
            });
            this['safeSend'](_0x3d81x1f5);
            if (this['pendingStatus']) {
                this['safeSend'](this['pendingStatus']);
                this['pendingStatus'] = null
            };
            break;
        case 2:
            this['friends'] = _0x3d81x11['f'];
            if (this['friendsOpened']) {
                this['friendBoxMode'](this['VIEW_MODE'].FRIEND_LIST)
            };
            if (_0x3d81x11['u'] && _0x3d81x11['u']['length']) {
                for (let _0x3d81x170 of _0x3d81x11['u']) {
                    this['unreadChannels'] = {};
                    this['unreadChannels'][_0x3d81x170] = 1
                };
                if (!this['friendsOpened']) {
                    this['friendsNotif'](_0x3d81x11['u']['length'])
                }
            };
            break;
        case 4:
            if (this['friendsOpened'] && _0x3d81x11['u'] === this['openChatName']) {
                for (let _0x3d81x13 = _0x3d81x11['h']['length'] - 1; _0x3d81x13 >= 0; --_0x3d81x13) {
                    let _0x3d81x170 = (_0x3d81x11['h'][_0x3d81x13]['s'] === 1) ? this['Live']['getName'](this['Live']['cid']) : this['Live']['getAuthorizedNameLink'](_0x3d81x11['u'], 0);
                    this['showInChat'](_0x3d81x170, _0x3d81x11['h'][_0x3d81x13]['m'], null, _0x3d81x11['h'][_0x3d81x13]['t'])
                };
                delete this['unreadChannels'][_0x3d81x11['u']];
                if (!_0x3d81x11['h']['length']) {
                    let _0x3d81x326 = document['createElement']('div');
                    _0x3d81x326['innerHTML'] = trans(i18n['frNewChatH'], {
                        name: this['Live']['getAuthorizedNameLink'](_0x3d81x11['u'], 0)
                    });
                    _0x3d81x326['style']['color'] = 'grey';
                    _0x3d81x326['style']['fontSize'] = '0.8em';
                    _0x3d81x326['style']['textAlign'] = 'center';
                    this['showInChat']('', _0x3d81x326)
                }
            };
            break;
        case 5:
            let _0x3d81x338 = _0x3d81x11['u'];
            if (!_0x3d81x11['s']) {
                this['checkNewFriend'](_0x3d81x11['u'])
            };
            if (this['friendsOpened']) {
                if (_0x3d81x338 === this['openChatName']) {
                    let _0x3d81x170 = (_0x3d81x11['s'] === 1) ? this['Live']['getName'](this['Live']['cid']) : this['Live']['getAuthorizedNameLink'](_0x3d81x338, _0x3d81x11['y']);
                    this['showInChat'](_0x3d81x170, _0x3d81x11['m'], null, _0x3d81x11['i']);
                    if (!_0x3d81x11['s']) {
                        let _0x3d81x1f5 = JSON['stringify']({
                            t: 6,
                            u: _0x3d81x338,
                            i: _0x3d81x11['i']
                        });
                        this['safeSend'](_0x3d81x1f5)
                    }
                } else {
                    if (this['openChatName'] !== null) {
                        this['notifSoundDM']();
                        if (this['dmChatBackButton']) {
                            let _0x3d81x339 = this['dmChatBackButton']['getElementsByClassName']('msgNotif');
                            if (!_0x3d81x339['length']) {
                                _0x3d81x339 = document['createElement']('div');
                                _0x3d81x339['className'] = 'msgNotif';
                                _0x3d81x339['textContent'] = 1;
                                this['dmChatBackButton']['appendChild'](_0x3d81x339)
                            } else {
                                _0x3d81x339 = _0x3d81x339[0];
                                let _0x3d81x279 = parseInt(_0x3d81x339['textContent']);
                                if (!_0x3d81x279) {
                                    _0x3d81x279 = 1
                                } else {
                                    _0x3d81x279 += 1
                                };
                                _0x3d81x339['textContent'] = Math['min'](99, _0x3d81x279)
                            }
                        }
                    } else {
                        this['notifSoundDM']();
                        let _0x3d81x219 = document['getElementById']('fr-' + _0x3d81x338);
                        if (_0x3d81x219) {
                            let _0x3d81x199 = _0x3d81x219['getElementsByClassName']('f-ch')[0];
                            let _0x3d81x33a = _0x3d81x199['getElementsByClassName']('msgNotif');
                            if (!_0x3d81x33a['length']) {
                                this['friendRowNotif'](_0x3d81x199, 1)
                            } else {
                                let _0x3d81x279 = parseInt(_0x3d81x33a[0]['textContent']);
                                if (!_0x3d81x279) {
                                    _0x3d81x279 = 1
                                } else {
                                    _0x3d81x279 += 1
                                };
                                _0x3d81x199['removeChild'](_0x3d81x33a[0]);
                                this['friendRowNotif'](_0x3d81x199, _0x3d81x279)
                            }
                        }
                    }
                }
            } else {
                if (_0x3d81x11['s'] == 1) {
                    break
                };
                if (!this['unreadChannels']['hasOwnProperty'](_0x3d81x338)) {
                    this['unreadChannels'][_0x3d81x338] = 1
                };
                let _0x3d81x279 = Object['keys'](this['unreadChannels'])['length'];
                this['friendsNotif'](_0x3d81x279);
                this['notifSoundDM']()
            };
            break;
        case 99:
            this['friendsError'] = _0x3d81x11['e'];
            this['forcedReconDelay'] = 20000;
            if (this['friendsOpened']) {
                this['decideFriendsBoxMode']()
            };
            break;
        default:
            break
    }
};
Friends['prototype']['checkNewFriend'] = function(_0x3d81x170) {
    let _0x3d81x33b = false,
        _0x3d81x33c = false;
    for (let _0x3d81x13 = 0; _0x3d81x13 < this['friends']['length']; ++_0x3d81x13) {
        let _0x3d81x33d = this['friends'][_0x3d81x13];
        if (_0x3d81x33d['n'] == _0x3d81x170) {
            if (!_0x3d81x33d['s']) {
                _0x3d81x33b = false;
                break
            };
            _0x3d81x33c = true;
            break
        }
    };
    if (!_0x3d81x33c || _0x3d81x33b) {
        this['requestRefresh']()
    }
};
Friends['prototype']['notifSoundDM'] = function() {
    if (!this['Live']['p']['Settings']['DMsound']) {
        return
    };
    let _0x3d81x58 = this['Live']['p']['timestamp']();
    if (!this['lastNotifSound'] || _0x3d81x58 - this['lastNotifSound'] > 1000) {
        createjs['Sound']['play']('msg');
        this['lastNotifSound'] = _0x3d81x58
    }
};
Friends['prototype']['friendsNotif'] = function(_0x3d81x279) {
    this['Live']['friendsBtn']['className'] = 'friNew';
    if (!this['notifElem']) {
        this['notifElem'] = document['createElement']('div');
        this['Live']['friendsBtn']['appendChild'](this['notifElem'])
    };
    this['notifElem']['className'] = 'frNotif';
    if (_0x3d81x279 > 0) {
        this['notifElem']['textContent'] = Math['min'](99, _0x3d81x279)
    } else {
        if (_0x3d81x279 === -1) {
            this['notifElem']['textContent'] = 'NEW';
            this['notifElem']['classList']['add']('newTag')
        }
    };
    showElem(this['notifElem'])
};
Friends['prototype']['friendRowNotif'] = function(_0x3d81x33e, _0x3d81x1a8) {
    _0x3d81x33e['classList']['add']('newMsg');
    let _0x3d81x33f = document['createElement']('div');
    _0x3d81x33f['className'] = 'msgNotif';
    _0x3d81x33f['textContent'] = Math['min'](99, _0x3d81x1a8);
    _0x3d81x33e['appendChild'](_0x3d81x33f)
};
Friends['prototype']['tryRenderInvite'] = function(_0x3d81x147, _0x3d81x31a, _0x3d81x2e) {
    if (!(typeof _0x3d81x147 === 'string')) {
        return _0x3d81x147
    };
    let _0x3d81x27e = _0x3d81x147['match'](/^\[INV:([\w]+):(.*)\]$/);
    if (_0x3d81x27e) {
        let _0x3d81x16e = _0x3d81x27e[1];
        let _0x3d81x170 = _0x3d81x27e[2];
        let _0x3d81x340 = document['createElement']('div');
        _0x3d81x340['className'] = 'inviteBox';
        _0x3d81x340['innerHTML'] = getSVG('s-envelope', 'dark', '');
        let _0x3d81x157 = document['createElement']('span');
        _0x3d81x157['innerHTML'] = trans(i18n['frInvTo'], {
            room: '<b>' + _0x3d81x170 + '</b>'
        });
        _0x3d81x340['appendChild'](_0x3d81x157);
        var _0x3d81x341 = new Date(_0x3d81x2e * 1000);
        var _0x3d81x342 = _0x3d81x341['getHours']();
        var _0x3d81x343 = '0' + _0x3d81x341['getMinutes']();
        let _0x3d81x344 = _0x3d81x342 + ':' + _0x3d81x343['substr'](-2);
        let _0x3d81x128 = document['createElement']('span');
        _0x3d81x128['className'] = 'invInfo';
        _0x3d81x128['innerHTML'] = _0x3d81x344 + '<br>' + trans(i18n['frInvBy'], {
            user: _0x3d81x31a
        });
        _0x3d81x340['appendChild'](_0x3d81x128);
        if (_0x3d81x16e != this['Live']['rid']) {
            _0x3d81x340['addEventListener']('click', function(_0x3d81x2d) {
                window['joinRoom'](_0x3d81x16e)
            })
        } else {
            if (typeof $ === 'function') {
                $(_0x3d81x340)['tooltip']({
                    title: i18n['frInvIn'],
                    template: this['ttClass']('ttch'),
                    viewport: {
                        selector: 'body',
                        padding: -10000
                    }
                })
            }
        };
        return _0x3d81x340
    } else {
        return _0x3d81x147
    }
};
Friends['prototype']['showInChat'] = function(_0x3d81x170, _0x3d81x147, _0x3d81x18a, _0x3d81x2e) {
    var _0x3d81x1bf = (_0x3d81x170 === '') ? '' : '<b>' + _0x3d81x170 + ': </b>';
    var _0x3d81x1c0 = (_0x3d81x170 === '') ? 'srv' : '';
    _0x3d81x147 = this['tryRenderInvite'](_0x3d81x147, _0x3d81x170, _0x3d81x2e);
    var _0x3d81x1bd = document['createElement']('div');
    _0x3d81x1bd['classList']['add']('chl');
    if (typeof _0x3d81x18a === 'object' && _0x3d81x18a !== null) {
        _0x3d81x1bd['classList']['add'](_0x3d81x18a)
    };
    if (_0x3d81x1c0) {
        _0x3d81x1bd['classList']['add'](_0x3d81x1c0)
    };
    if (_0x3d81x147 instanceof HTMLDivElement) {
        _0x3d81x1bd['appendChild'](_0x3d81x147)
    } else {
        _0x3d81x1bd['innerHTML'] = _0x3d81x1bf + _0x3d81x147
    };
    this['Live']['friendsBox']['appendChild'](_0x3d81x1bd);
    this['Live']['chatArea']['scrollTop'] = this['Live']['chatArea']['scrollHeight']
};
Friends['prototype']['getCategorySeparator'] = function(_0x3d81x97) {
    let _0x3d81x345 = document['createElement']('div');
    _0x3d81x345['classList']['add']('sep');
    if (_0x3d81x97) {
        _0x3d81x345['classList']['add']('on');
        _0x3d81x345['textContent'] = i18n['frOn']
    } else {
        _0x3d81x345['textContent'] = i18n['frOff']
    };
    return _0x3d81x345
};
Friends['prototype']['requestRefresh'] = function() {
    let _0x3d81x1f5 = JSON['stringify']({
        t: 2
    });
    this['safeSend'](_0x3d81x1f5);
    this['openChatName'] = null;
    if (!this['connected']()) {
        this['loadingScreen']('Reconnecting...')
    }
};
Friends['prototype']['sendStatus'] = function(_0x3d81x16e, _0x3d81x139, _0x3d81x170) {
    this['statusData'] = {
        t: 3,
        r: _0x3d81x16e,
        p: !_0x3d81x139,
        n: _0x3d81x170['substring'](0, 30)
    };
    let _0x3d81x1f5 = JSON['stringify'](this['statusData']);
    if (this['connected']()) {
        this['safeSend'](_0x3d81x1f5)
    } else {
        this['pendingStatus'] = _0x3d81x1f5
    };
    if (this['friendsOpened'] && this['friendsCount']) {
        this['requestRefresh']()
    }
};
Friends['prototype']['openChat'] = function(_0x3d81x170) {
    let _0x3d81x1f5 = JSON['stringify']({
        t: 4,
        u: _0x3d81x170
    });
    this['safeSend'](_0x3d81x1f5);
    let _0x3d81x25 = this;
    this['Live']['friendsBox']['innerHTML'] = '';
    this['Live']['friendsBox']['className'] = 'fchat';
    let _0x3d81x174 = document['createElement']('h3');
    _0x3d81x174['className'] = 'chatHeader';
    _0x3d81x174['textContent'] = _0x3d81x170;
    let _0x3d81x346 = document['createElement']('button');
    _0x3d81x346['innerHTML'] = getSVG('s-exit', 'dark', '');
    _0x3d81x346['addEventListener']('click', this['requestRefresh']['bind'](this));
    _0x3d81x346['className'] = 'exitDM';
    this['dmChatBackButton'] = _0x3d81x346;
    _0x3d81x174['appendChild'](_0x3d81x346);
    let _0x3d81x347 = document['createElement']('button');
    _0x3d81x347['innerHTML'] = getSVG('s-envelope', 'dark', '');
    _0x3d81x347['addEventListener']('click', this['sendInvite']['bind'](this));
    _0x3d81x347['className'] = 'inviteDM';
    if (typeof $ === 'function') {
        $(_0x3d81x347)['tooltip']({
            title: i18n['frInv'],
            viewport: {
                selector: 'body',
                padding: -10000
            }
        })
    };
    _0x3d81x174['appendChild'](_0x3d81x347);
    let _0x3d81x348 = this['Live']['chatArea'];
    if (this['chatHeader']) {
        this['chatBox']['removeChild'](this['chatHeader']);
        this['chatHeader'] = null
    };
    this['chatHeader'] = _0x3d81x174;
    this['chatBox']['insertBefore'](_0x3d81x174, _0x3d81x348);
    this['Live']['chatInput']['placeholder'] = trans(i18n['frMsgTo'], {
        name: _0x3d81x170
    });
    this['openChatName'] = _0x3d81x170
};
Friends['prototype']['ttClass'] = function(_0x3d81x20a) {
    return '<div class="tooltip ' + _0x3d81x20a + '" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
};
Friends['prototype']['updateFriendList'] = function() {
    let _0x3d81x25 = this;
    this['openChatName'] = null;
    this['Live']['chatInput']['placeholder'] = '';
    this['Live']['friendsBox']['innerHTML'] = '';
    this['Live']['friendsBox']['className'] = 'flist';
    this['Live']['chatArea']['style']['paddingTop'] = null;
    if (this['chatHeader']) {
        this['chatBox']['removeChild'](this['chatHeader']);
        this['chatHeader'] = null
    };
    let _0x3d81x174 = document['createElement']('h3');
    _0x3d81x174['textContent'] = i18n['fr'];
    let _0x3d81x349 = document['createElement']('button');
    _0x3d81x349['title'] = i18n['frRel'];
    _0x3d81x349['innerHTML'] = getSVG('s-reload', 'dark', '');
    _0x3d81x349['addEventListener']('click', this['requestRefresh']['bind'](this));
    _0x3d81x174['appendChild'](_0x3d81x349);
    this['Live']['friendsBox']['appendChild'](_0x3d81x174);
    this['friends']['sort'](function(_0x3d81xa5, _0x3d81x141) {
        if (_0x3d81xa5['s'] != _0x3d81x141['s']) {
            if (_0x3d81xa5['s'] && !_0x3d81x141['s']) {
                return -1
            } else {
                return 1
            }
        };
        if (_0x3d81xa5['c'] && !_0x3d81x141['c']) {
            return -1
        };
        if (_0x3d81x141['c'] && !_0x3d81xa5['c']) {
            return 1
        };
        return _0x3d81xa5['n']['localeCompare'](_0x3d81x141['n'])
    });
    let _0x3d81x34a = null;
    for (let _0x3d81x13 = 0; _0x3d81x13 < this['friends']['length']; ++_0x3d81x13) {
        let _0x3d81x33d = this['friends'][_0x3d81x13];
        if (_0x3d81x34a !== _0x3d81x33d['s']) {
            _0x3d81x34a = _0x3d81x33d['s'];
            this['Live']['friendsBox']['appendChild'](this['getCategorySeparator'](_0x3d81x34a))
        };
        let _0x3d81x34b = document['createElement']('div');
        _0x3d81x34b['className'] = 'fr';
        _0x3d81x34b['id'] = 'fr-' + _0x3d81x33d['n'];
        let _0x3d81xa5 = document['createElement']('a');
        _0x3d81xa5['target'] = '_blank';
        _0x3d81xa5['href'] = '/u/' + _0x3d81x33d['n'];
        _0x3d81xa5['textContent'] = _0x3d81x33d['n'];
        _0x3d81x34b['appendChild'](_0x3d81xa5);
        let _0x3d81x34c = document['createElement']('button');
        _0x3d81x34c['title'] = i18n['frChat'];
        _0x3d81x34c['classList']['add']('f-ch');
        if (_0x3d81x33d['c']) {
            this['friendRowNotif'](_0x3d81x34c, _0x3d81x33d['c'])
        };
        _0x3d81x34c['addEventListener']('click', function() {
            _0x3d81x25['openChat'](_0x3d81x33d['n'])
        });
        _0x3d81x34b['appendChild'](_0x3d81x34c);
        if (_0x3d81x33d['s']) {
            let _0x3d81x1d6 = document['createElement']('button');
            let _0x3d81x293 = '';
            if (_0x3d81x33d['r'] && this['Live']['rid'] != _0x3d81x33d['r']) {
                _0x3d81x1d6['addEventListener']('click', function(_0x3d81x2d) {
                    if (_0x3d81x2d['target']['hasAttribute']('aria-describedby')) {
                        let _0x3d81x293 = _0x3d81x2d['target']['getAttribute']('aria-describedby');
                        let _0x3d81x8c = document['getElementById'](_0x3d81x293);
                        if (_0x3d81x8c) {
                            _0x3d81x8c['parentNode']['removeChild'](_0x3d81x8c)
                        }
                    };
                    window['joinRoom'](_0x3d81x33d['r'])
                });
                if (_0x3d81x33d['a']) {
                    _0x3d81x293 = _0x3d81x33d['a']
                }
            } else {
                _0x3d81x1d6['classList']['add']('disBtn');
                if (!_0x3d81x33d['r']) {
                    _0x3d81x293 = i18n['frPriv']
                } else {
                    if (this['Live']['rid'] == _0x3d81x33d['r']) {
                        _0x3d81x293 = i18n['frIn']
                    }
                }
            };
            if (_0x3d81x293 && typeof $ === 'function') {
                $(_0x3d81x1d6)['tooltip']({
                    title: _0x3d81x293,
                    template: this['ttClass']('ttch'),
                    html: true,
                    viewport: {
                        selector: 'body',
                        padding: -10000
                    }
                })
            };
            _0x3d81x1d6['classList']['add']('f-jo');
            _0x3d81x34b['appendChild'](_0x3d81x1d6)
        };
        this['Live']['friendsBox']['appendChild'](_0x3d81x34b)
    }
};
Friends['prototype']['loadingScreen'] = function(_0x3d81x147, _0x3d81x2c2) {
    if (!this['friendsOpened']) {
        return
    };
    this['Live']['friendsBox']['style']['display'] = 'flex';
    this['Live']['friendsBox']['innerHTML'] = '';
    var _0x3d81x231 = document['createElement']('img');
    if (!_0x3d81x2c2) {
        _0x3d81x231['src'] = CDN_URL('/res/svg/spinWhite.svg')
    } else {
        if (_0x3d81x2c2 === 1) {
            _0x3d81x231['src'] = CDN_URL('/res/svg/cancel.svg')
        }
    };
    _0x3d81x231['style']['width'] = '27px';
    var _0x3d81x157 = document['createElement']('span');
    _0x3d81x157['innerHTML'] = _0x3d81x147;
    this['Live']['friendsBox']['appendChild'](_0x3d81x231);
    this['Live']['friendsBox']['appendChild'](_0x3d81x157);
    this['Live']['friendsBox']['className'] = 'fempty'
};
Friends['prototype']['friendBoxMode'] = function(_0x3d81x7) {
    this['Live']['friendsBox']['className'] = '';
    if (_0x3d81x7 === this['VIEW_MODE']['FRIEND_LIST_LOADING']) {
        this['loadingScreen'](i18n['frLoad'])
    } else {
        if (_0x3d81x7 === this['VIEW_MODE']['LOGIN_FIRST']) {
            this['loadingScreen'](i18n['frLogin'], 1)
        } else {
            if (_0x3d81x7 === this['VIEW_MODE']['TOO_MANY_CONN']) {
                let _0x3d81xa5 = 'Account is already connected multiple times.';
                let _0x3d81x141 = 'Close other connections.';
                this['loadingScreen'](_0x3d81xa5 + '<br>' + _0x3d81x141, 1)
            } else {
                if (_0x3d81x7 === this['VIEW_MODE']['NO_FRIENDS']) {
                    this['Live']['friendsBox']['style']['display'] = 'flex';
                    this['Live']['friendsBox']['innerHTML'] = '<h1>' + i18n['frEmpty'] + '</h1>' + i18n['frHowAdd'];
                    this['Live']['friendsBox']['className'] = 'fempty'
                } else {
                    if (_0x3d81x7 === this['VIEW_MODE']['FRIEND_LIST']) {
                        this['requestRefresh']();
                        this['Live']['friendsBox']['style']['display'] = 'block';
                        this['updateFriendList']();
                        if (this['friends'] && this['friends']['length'] > 2 && this['chatBox']['clientHeight'] < 140 && !this['Live']['p']['GS']['chatExpanded']) {
                            this['Live']['p']['GS']['chatExpand']()
                        }
                    } else {
                        if (_0x3d81x7 === this['VIEW_MODE']['INTRO']) {
                            if (!this['friendsOpened']) {
                                return
                            };
                            if (this['chatBox']['clientHeight'] < 153 && !this['Live']['p']['GS']['chatExpanded']) {
                                this['Live']['p']['GS']['chatExpand']()
                            };
                            this['Live']['friendsBox']['innerHTML'] = '<h1 style="font-size: 20px;margin-top: 3px;">' + i18n['frWelc'] + '</h1><ul style="margin-bottom: 7px;margin-left: -13px;"><li>' + i18n['frIntro'] + '</li><li>' + i18n['frIntro2'] + '</li><li>' + trans(i18n['frIntro3'], {
                                frPage: '<a target="_blank" href="/friends">' + i18n['frPage'] + '</a>'
                            }) + '</li></ul>';
                            let _0x3d81x199 = document['createElement']('button');
                            _0x3d81x199['textContent'] = i18n['frIntroCl'];
                            _0x3d81x199['style']['border'] = 'none';
                            _0x3d81x199['style']['padding'] = '4px 29px';
                            _0x3d81x199['addEventListener']('click', this['decideFriendsBoxMode']['bind'](this));
                            this['Live']['friendsBox']['appendChild'](_0x3d81x199);
                            localStorage['setItem']('frN', 1)
                        }
                    }
                }
            }
        }
    }
};
Friends['prototype']['viewIntro'] = function() {
    let _0x3d81x34d = localStorage['getItem']('frN');
    return (_0x3d81x34d === null)
};
Friends['prototype']['decideFriendsBoxMode'] = function() {
    if (this['viewIntro']()) {
        this['friendBoxMode'](this['VIEW_MODE'].INTRO)
    } else {
        if (!this['Live']['authorized']) {
            this['friendBoxMode'](this['VIEW_MODE'].LOGIN_FIRST)
        } else {
            if (this['friendsError'] === 1) {
                this['friendBoxMode'](this['VIEW_MODE'].TOO_MANY_CONN)
            } else {
                if (this['friendsCount'] === 0) {
                    this['friendBoxMode'](this['VIEW_MODE'].NO_FRIENDS)
                } else {
                    if (this['friendsCount'] === null || this['friends'] === null) {
                        this['friendBoxMode'](this['VIEW_MODE'].FRIEND_LIST_LOADING)
                    } else {
                        this['friendBoxMode'](this['VIEW_MODE'].FRIEND_LIST)
                    }
                }
            }
        }
    }
};
Friends['prototype']['openFriends'] = function() {
    this['friendsOpened'] = !this['friendsOpened'];
    hideElem(this['notifElem']);
    if (this['friendsOpened']) {
        this['Live']['friendsBtn']['className'] = 'rch';
        this['Live']['chatBox']['style']['display'] = 'none';
        this['Live']['removeScrollButton']();
        this['Live']['friendsBtn']['setAttribute']('data-original-title', 'Room chat');
        if (this['chatBox']['clientHeight'] < 100 && !this['Live']['p']['GS']['chatExpanded']) {
            this['Live']['p']['GS']['chatExpand']()
        };
        this['decideFriendsBoxMode']()
    } else {
        this['Live']['friendsBtn']['className'] = 'fri';
        this['Live']['chatBox']['style']['display'] = 'block';
        this['Live']['friendsBox']['style']['display'] = 'none';
        this['Live']['chatArea']['style']['paddingTop'] = null;
        this['Live']['chatInput']['placeholder'] = '';
        if (this['chatHeader']) {
            this['chatBox']['removeChild'](this['chatHeader']);
            this['chatHeader'] = null
        };
        this['Live']['friendsBtn']['setAttribute']('data-original-title', 'Friends');
        this['Live']['scrollOnMessage'](true)
    };
    if (this['Live']['friendsBtn']['hasAttribute']('aria-describedby')) {
        let _0x3d81x293 = this['Live']['friendsBtn']['getAttribute']('aria-describedby');
        hideElem(document['getElementById'](_0x3d81x293))
    }
};
Friends['prototype']['onClose'] = function() {
    this['reconnect']()
};
'use strict';
(function() {
    window['onload'] = _0x3d81x11c;
    var _0x3d81x93 = undefined;

    function _0x3d81x11c() {
        _0x3d81x93 = new Game();
        _0x3d81x93['start']();
        settingsTabs['init']();
        document['addEventListener']('keydown', _0x3d81x93['keyInput2']['bind'](_0x3d81x93), false);
        document['addEventListener']('keyup', _0x3d81x93['keyInput3']['bind'](_0x3d81x93), false);
        document['getElementById']('main')['addEventListener']('click', function() {
            _0x3d81x93['setFocusState'](0)
        }, true);
        document['getElementById']('createRoom')['addEventListener']('click', function() {
            _0x3d81x93['setFocusState'](1)
        }, true);
        document['getElementById']('settingsBox')['addEventListener']('click', function() {
            _0x3d81x93['setFocusState'](1)
        }, true);
        document['getElementById']('chatBox')['addEventListener']('click', function() {
            _0x3d81x93['setFocusState'](1)
        }, true);
        document['getElementById']('chatBox')['addEventListener']('scroll', _0x3d81x93['Live']['onChatScroll']['bind'](_0x3d81x93.Live), true);
        document['getElementById']('lobbyBox')['addEventListener']('click', function() {
            _0x3d81x93['setFocusState'](1)
        }, true);
        document['getElementById']('showMore')['onclick'] = _0x3d81x93['Live']['toggleMore']['bind'](_0x3d81x93.Live);
        document['getElementById']('moreClose')['onclick'] = _0x3d81x93['Live']['toggleMore']['bind'](_0x3d81x93.Live);
        document['getElementById']('cancelCreation')['onclick'] = _0x3d81x93['Live']['closeRoomDialog']['bind'](_0x3d81x93.Live);
        document['getElementById']('createRoomButton')['onclick'] = _0x3d81x93['Live']['showRoomDialog']['bind'](_0x3d81x93.Live);
        document['getElementById']('editRoomButton')['onclick'] = _0x3d81x93['Live']['showRoomDialogForEdit']['bind'](_0x3d81x93.Live);
        document['getElementById']('lobby')['onclick'] = _0x3d81x93['Live']['toggleLobbby']['bind'](_0x3d81x93.Live);
        document['getElementById']('refreshLobby')['onclick'] = _0x3d81x93['Live']['refreshLobbby']['bind'](_0x3d81x93.Live);
        document['getElementById']('settings')['onclick'] = (function() {
            _0x3d81x93['Settings']['openSettings']();
            _0x3d81x93['setFocusState'](1)
        });
        document['getElementById']('res')['onclick'] = _0x3d81x93['Live']['sendRestartEvent']['bind'](_0x3d81x93.Live);
        document['getElementById']('closeLobby')['onclick'] = _0x3d81x93['Live']['toggleLobbby']['bind'](_0x3d81x93.Live);
        document['getElementById']('closeLink')['onclick'] = _0x3d81x93['Live']['hideResults']['bind'](_0x3d81x93.Live);
        document['getElementById']('create')['onclick'] = _0x3d81x93['Live']['makeRoomWrapper']['bind'](_0x3d81x93.Live);
        document['getElementById']('sendMsg')['onclick'] = _0x3d81x93['Live']['sendChat']['bind'](_0x3d81x93.Live);
        document['getElementById']('myCanvas')['onclick'] = (function() {
            if (!_0x3d81x93['Live']['sitout'] && _0x3d81x93['focusState']) {
                _0x3d81x93['redraw']()
            };
            _0x3d81x93['setFocusState'](0)
        });
        document['getElementById']('chatInput')['onclick'] = (function() {
            _0x3d81x93['setFocusState'](1)
        });
        document['getElementById']('chatInput')['onfocus'] = (function() {
            _0x3d81x93['setFocusState'](1)
        });
        document['getElementById']('chatInput')['addEventListener']('keydown', function(_0x3d81x2d) {
            if (_0x3d81x2d['keyCode'] === 13) {
                _0x3d81x93['Live']['sendChat']()
            }
        });
        document['getElementById']('more-practice')['onclick'] = _0x3d81x93['Live']['toggleMorePractice']['bind'](_0x3d81x93.Live, false);
        document['getElementById']('hasSolid')['onclick'] = (function() {
            document['getElementById']('solid')['disabled'] = !document['getElementById']('hasSolid')['checked']
        });
        document['getElementById']('ghost')['onclick'] = (function() {
            _0x3d81x93['ghostEnabled'] = document['getElementById']('ghost')['checked'];
            if (_0x3d81x93['play'] && _0x3d81x93['ghostEnabled']) {
                _0x3d81x93['updateGhostPiece']();
                _0x3d81x93['redraw']()
            }
        });
        document['getElementById']('more_simple')['onclick'] = (function() {
            _0x3d81x93['Live']['switchRDmode'](0)
        });
        document['getElementById']('more_adv')['onclick'] = (function() {
            _0x3d81x93['Live']['switchRDmode'](1)
        });
        document['getElementById']('more_preset')['onclick'] = (function() {
            _0x3d81x93['Live']['switchRDmode'](2)
        });
        document['getElementById']('saveRD')['onclick'] = (function() {
            _0x3d81x93['Live']['saveRD']()
        });
        document['getElementById']('saveDataClose')['onclick'] = (function() {
            this['parentElement']['style']['display'] = 'none'
        });
        document['getElementById']('addJL')['onclick'] = (function() {
            showElem(document['getElementById']('joinLimits'))
        });
        document['getElementById']('saveLimits')['onclick'] = (function() {
            hideElem(document['getElementById']('joinLimits'))
        });
        document['getElementById']('attackMode')['onchange'] = (function() {
            _0x3d81x93['Live']['attackSelect']()
        });
        document['getElementById']('presetSel')['onchange'] = (function() {
            _0x3d81x93['Live']['onPresetChange']()
        });
        document['getElementById']('customPre')['onclick'] = (function() {
            _0x3d81x93['Live']['useCustomPreset']()
        });
        document['getElementById']('nullpoDAS')['onclick'] = _0x3d81x93['Settings']['nullpoDAS']['bind'](_0x3d81x93.Settings);
        document['getElementById']('sfxSelect')['onchange'] = soundCredits;
        document['getElementById']('vsfxSelect')['onchange'] = soundCredits;
        document['getElementById']('chatExpand')['onclick'] = _0x3d81x93['GS']['chatExpand']['bind'](_0x3d81x93.GS);
        window['addEventListener']('blur', function() {
            _0x3d81x93['browserTabFocusChange'](0)
        }, false);
        window['addEventListener']('focus', function() {
            _0x3d81x93['browserTabFocusChange'](1)
        }, false);
        window['addEventListener']('gamepadconnected', _0x3d81x93['Settings']['initGamePad']['bind'](_0x3d81x93.Settings));
        window['addEventListener']('gamepaddisconnected', _0x3d81x93['Settings']['removeGamePad']['bind'](_0x3d81x93.Settings));
        document['getElementById']('vol-control')['addEventListener']('input', function() {
            _0x3d81x93['Settings']['volumeChange'](this['value'])
        });
        document['getElementById']('slotSettingsBtn')['addEventListener']('click', function() {
            var _0x3d81x199 = document['getElementById']('slotSettingsBtn');
            var _0x3d81x34e = document['getElementById']('slotSettings');
            _0x3d81x199['classList']['toggle']('zoomIc');
            _0x3d81x199['classList']['toggle']('okIc');
            toggleElem(_0x3d81x34e)
        }, false);
        document['getElementById']('slotSettings')['addEventListener']('click', function(_0x3d81x2d) {
            _0x3d81x2d['stopImmediatePropagation']()
        }, false);
        document['getElementById']('zoomControl')['onchange'] = (function() {
            var _0x3d81x30 = parseInt(document['getElementById']('zoomControl')['value']);
            document['getElementById']('zoomNow')['innerHTML'] = _0x3d81x30 + '%';
            _0x3d81x93['GS']['setZoom']['call'](_0x3d81x93.GS, _0x3d81x30)
        });
        document['getElementById']('fsSlots')['onclick'] = (function() {
            _0x3d81x93['GS']['fullScreen']['call'](_0x3d81x93.GS, document['getElementById']('fsSlots')['checked'])
        });
        document['getElementById']('hqSlots')['onclick'] = (function() {
            _0x3d81x93['GS']['forceExtended'] = document['getElementById']('hqSlots')['checked'];
            _0x3d81x93['GS']['autoScale']['call'](_0x3d81x93.GS)
        });
        document['getElementById']('statsSlots')['onclick'] = (function() {
            _0x3d81x93['GS']['slotStats'] = document['getElementById']('statsSlots')['checked'];
            _0x3d81x93['GS']['autoScale']['call'](_0x3d81x93.GS)
        });
        window['joinRoom'] = _0x3d81x356;
        window['loadSkin'] = _0x3d81x353;
        window['loadVideoSkin'] = _0x3d81x354;
        window['loadGhostSkin'] = _0x3d81x93['loadGhostSkin']['bind'](_0x3d81x93);
        window['loadSFX'] = _0x3d81x355;
        for (var _0x3d81x13 = 0; _0x3d81x13 < document['gridForm']['grs']['length']; _0x3d81x13++) {
            document['gridForm']['grs'][_0x3d81x13]['onclick'] = function() {
                _0x3d81x93['Settings']['setGrid'](parseInt(this['value']))
            }
        };
        var _0x3d81x34f = function() {
            try {
                if (createjs['WebAudioPlugin']['context'] && createjs['WebAudioPlugin']['context']['state'] === 'suspended') {
                    createjs['WebAudioPlugin']['context']['resume']();
                    window['removeEventListener']('click', _0x3d81x34f)
                }
            } catch (e) {
                console['error']('Error while trying to resume the Web Audio context.');
                console['error'](e)
            }
        };
        window['addEventListener']('click', _0x3d81x34f);
        var _0x3d81x70, _0x3d81x350 = false,
            _0x3d81x351 = document['getElementById']('main');

        function _0x3d81x352() {
            _0x3d81x351['style']['cursor'] = 'none';
            _0x3d81x350 = true;
            setTimeout(function() {
                _0x3d81x350 = false
            }, 100)
        }
        _0x3d81x351['addEventListener']('mousemove', function() {
            if (!_0x3d81x350) {
                _0x3d81x350 = false;
                clearTimeout(_0x3d81x70);
                _0x3d81x351['style']['cursor'] = null;
                _0x3d81x70 = setTimeout(_0x3d81x352, 3000)
            }
        })
    }

    function _0x3d81x353(_0x3d81x5, _0x3d81x6, _0x3d81xce) {
        _0x3d81x93['skins'][1]['data'] = _0x3d81x5;
        _0x3d81x93['skins'][1]['w'] = _0x3d81x6;
        _0x3d81x93['skins'][1]['cdn'] = false;
        _0x3d81x93['changeSkin'](1);
        if (_0x3d81xce) {
            if (_0x3d81xce['global']) {
                _0x3d81x93['GS']['skinOverride'] = _0x3d81x93['skins'][1];
                _0x3d81x93['GS']['slots']['forEach'](function(_0x3d81x185) {
                    _0x3d81x185['v']['changeSkin'](1)
                })
            }
        }
    }

    function _0x3d81x354(_0x3d81x5, _0x3d81xce) {
        _0x3d81xce = _0x3d81xce || {};
        if (_0x3d81x93['v']['NAME'] !== 'webGL') {
            alert('Enable webGL before using loadVideoSkin!');
            return
        };
        if (_0x3d81x5['split']('.')['pop']() === 'gif') {
            _0x3d81x93['v']['setupGif'](_0x3d81x5, _0x3d81xce);
            return
        };
        _0x3d81x93['v']['setupVideo'](_0x3d81x5, _0x3d81xce)
    }

    function _0x3d81x355(_0x3d81xa, _0x3d81xb) {
        _0x3d81x93['changeSFX'](_0x3d81xa, _0x3d81xb)
    }

    function _0x3d81x356(_0x3d81x13) {
        _0x3d81x93['Live']['joinRoom'](_0x3d81x13)
    }
    if (typeof $ === 'function') {
        $('[data-toggle="tooltip"]')['tooltip']({
            viewport: {
                selector: 'body',
                padding: -10000
            }
        })
    }
})();

function ChatAutocomplete(_0x3d81x89, _0x3d81x358, _0x3d81x359, _0x3d81x35a) {
    this['inp'] = _0x3d81x89;
    this['hintParent'] = _0x3d81x358;
    this['prfx'] = _0x3d81x359;
    this['hints'] = _0x3d81x35a;
    this['hintsImg'] = null;
    if (_0x3d81x35a === null) {
        this['initEmoteHints']()
    };
    this['prefixInSearch'] = true;
    this['maxPerHint'] = 10;
    this['minimalLengthForHint'] = 1;
    this['addEmoteSurrounder'] = ':';
    this['wipePrevious'] = false;
    this['onWiped'] = null;
    this['preProcessEmotes'] = null;
    this['onEmoteObjectReady'] = null;
    this['moreEmotesAdded'] = false;
    this['selectedIndex'] = 0;
    this['hintsElem'] = document['createElement']('div');
    this['hintsElem']['classList']['add']('chatHints');
    hideElem(this['hintsElem']);
    this['hintParent']['appendChild'](this['hintsElem']);
    this['init']()
}
ChatAutocomplete['prototype']['clearEmotes'] = function() {
    this['hintsImg'] = {};
    this['hints'] = []
};
ChatAutocomplete['prototype']['initEmoteHints'] = function() {
    this['hintsImg'] = {};
    this['hints'] = Object['keys'](this['hintsImg'])
};
ChatAutocomplete['prototype']['addEmotes'] = function(_0x3d81x104) {
    const _0x3d81xba = this['addEmoteSurrounder'];
    for (const _0x3d81x35b of _0x3d81x104) {
        let _0x3d81x170 = _0x3d81xba + _0x3d81x35b['n'] + _0x3d81xba;
        this['hints']['push'](_0x3d81x170);
        if (_0x3d81x35b['u']) {
            this['hintsImg'][_0x3d81x170] = _0x3d81x35b['u']
        } else {
            this['hintsImg'][_0x3d81x170] = 'res/oe/' + _0x3d81x35b['n'] + '.svg'
        }
    };
    this['moreEmotesAdded'] = true
};
ChatAutocomplete['prototype']['loadEmotesIndex'] = function(_0x3d81x35c) {
    if (this['moreEmotesAdded']) {
        return
    };
    var _0x3d81x236 = new XMLHttpRequest();
    var _0x3d81x5 = '/code/emotes?' + _0x3d81x35c;
    _0x3d81x236['timeout'] = 8000;
    _0x3d81x236['open']('GET', _0x3d81x5, true);
    try {
        _0x3d81x236['send']()
    } catch (err) {};
    var _0x3d81x25 = this;
    _0x3d81x236['ontimeout'] = function() {};
    _0x3d81x236['onerror'] = _0x3d81x236['onabort'] = function() {};
    _0x3d81x236['onload'] = function() {
        if (_0x3d81x236['status'] === 200) {
            let _0x3d81x35d = JSON['parse'](_0x3d81x236['responseText']);
            if (null !== _0x3d81x25['preProcessEmotes']) {
                _0x3d81x35d = _0x3d81x25['preProcessEmotes'](_0x3d81x35d)
            };
            _0x3d81x25['addEmotes'](_0x3d81x35d);
            if (null !== _0x3d81x25['onEmoteObjectReady']) {
                _0x3d81x25['onEmoteObjectReady'](_0x3d81x35d)
            }
        }
    }
};
ChatAutocomplete['prototype']['init'] = function() {
    var _0x3d81x25 = this;
    this['inp']['addEventListener']('input', function(_0x3d81x2d) {
        var _0x3d81x35e = _0x3d81x25['getCurrentWord']();
        _0x3d81x25['processHint'](_0x3d81x35e)
    }, false);
    if (this['prfx'] === '') {
        this['inp']['addEventListener']('focus', function(_0x3d81x2d) {
            var _0x3d81x35e = _0x3d81x25['getCurrentWord']();
            _0x3d81x25['processHint'](_0x3d81x35e)
        })
    };
    this['inp']['addEventListener']('keydown', function(_0x3d81x2d) {
        if (_0x3d81x25['hintsElem']['style']['display'] === 'none') {
            return
        };
        if (_0x3d81x2d['keyCode'] === 38 || _0x3d81x2d['keyCode'] === 40) {
            _0x3d81x25['moveSelected']((_0x3d81x2d['keyCode'] === 38) ? -1 : 1);
            _0x3d81x2d['preventDefault']()
        } else {
            if (_0x3d81x2d['keyCode'] === 13 && this['selectedIndex'] !== null) {
                _0x3d81x25['hintsElem']['childNodes'][_0x3d81x25['selectedIndex']]['click']();
                _0x3d81x2d['preventDefault']();
                _0x3d81x2d['stopImmediatePropagation']()
            }
        }
    }, true)
};
ChatAutocomplete['prototype']['moveSelected'] = function(_0x3d81x2f) {
    var _0x3d81x35f = this['hintsElem']['childNodes'];
    this['hintsElem']['childNodes'][this['selectedIndex']]['classList']['remove']('ksel');
    this['selectedIndex'] = (_0x3d81x35f['length'] + this['selectedIndex'] + _0x3d81x2f) % _0x3d81x35f['length'];
    this['setSelected'](this['selectedIndex'])
};
ChatAutocomplete['prototype']['setSelected'] = function(_0x3d81x13) {
    if (_0x3d81x13 >= this['hintsElem']['childNodes']['length']) {
        this['selectedIndex'] = null;
        return
    };
    this['selectedIndex'] = _0x3d81x13;
    this['hintsElem']['childNodes'][this['selectedIndex']]['classList']['add']('ksel')
};
ChatAutocomplete['prototype']['processHint'] = function(_0x3d81x35e) {
    var _0x3d81x360 = _0x3d81x35e[0]['toLowerCase'](),
        _0x3d81x232 = _0x3d81x35e[1];
    if (this['prfx'] !== '' && (_0x3d81x360 === null || _0x3d81x360['length'] < this['minimalLengthForHint'] || _0x3d81x360[0] !== this['prfx'])) {
        hideElem(this['hintsElem']);
        return
    };
    _0x3d81x362 = _0x3d81x362;
    var _0x3d81x361 = _0x3d81x360['substring'](this['prfx']['length']);
    var _0x3d81x362 = (this['prefixInSearch']) ? _0x3d81x360 : _0x3d81x361;
    var _0x3d81x363 = 0;
    var _0x3d81x364 = (typeof this['hints'] === 'function') ? this['hints']() : this['hints'];
    this['hintsElem']['innerHTML'] = '';
    var _0x3d81x365 = [],
        _0x3d81x366 = [];
    for (var _0x3d81x367 in _0x3d81x364) {
        var _0x3d81x368 = _0x3d81x364[_0x3d81x367];
        var _0x3d81x369 = _0x3d81x368['toLowerCase']();
        if (_0x3d81x369['startsWith'](_0x3d81x362)) {
            _0x3d81x365['push'](_0x3d81x368)
        } else {
            if (_0x3d81x361['length'] >= 2 && _0x3d81x369['includes'](_0x3d81x361)) {
                _0x3d81x366['push'](_0x3d81x368)
            }
        }
    };
    _0x3d81x365['sort']();
    if (_0x3d81x365['length'] < this['maxPerHint']) {
        _0x3d81x366['sort']();
        for (const _0x3d81x11b of _0x3d81x366) {
            if (_0x3d81x365['indexOf'](_0x3d81x11b) === -1) {
                _0x3d81x365['push'](_0x3d81x11b);
                if (_0x3d81x365['length'] >= this['maxPerHint']) {
                    break
                }
            }
        }
    };
    for (var _0x3d81x368 of _0x3d81x365) {
        var _0x3d81x36a = document['createElement']('div');
        if (this['hintsImg'] && this['hintsImg'][_0x3d81x368]) {
            _0x3d81x36a['className'] = 'emHint';
            var _0x3d81xc1 = document['createElement']('img');
            _0x3d81xc1['src'] = CDN_URL('/' + this['hintsImg'][_0x3d81x368]);
            _0x3d81x36a['appendChild'](_0x3d81xc1);
            var _0x3d81x157 = document['createElement']('div');
            _0x3d81x157['textContent'] = _0x3d81x368;
            _0x3d81x36a['appendChild'](_0x3d81x157)
        } else {
            _0x3d81x36a['innerHTML'] = _0x3d81x368
        };
        _0x3d81x36a['dataset']['pos'] = _0x3d81x232;
        _0x3d81x36a['dataset']['str'] = _0x3d81x368;
        var _0x3d81x25 = this;
        _0x3d81x36a['addEventListener']('click', (function(_0x3d81x2d) {
            var _0x3d81x36b = _0x3d81x25['inp']['value'],
                _0x3d81x36c = parseInt(this['dataset']['pos']);
            var _0x3d81x36d = _0x3d81x36b['substring'](0, _0x3d81x36c);
            var _0x3d81x12 = _0x3d81x36d['indexOf'](' '),
                _0x3d81x36e = _0x3d81x12 + 1;
            while (_0x3d81x12 !== -1) {
                _0x3d81x12 = _0x3d81x36d['indexOf'](' ', _0x3d81x12 + 1);
                if (_0x3d81x12 !== -1) {
                    _0x3d81x36e = _0x3d81x12 + 1
                }
            };
            if (!_0x3d81x25['prefixInSearch']) {
                ++_0x3d81x36e
            };
            _0x3d81x25['inp']['value'] = _0x3d81x36b['substring'](0, _0x3d81x36e) + this['dataset']['str'] + ' ' + _0x3d81x36b['substring'](_0x3d81x36c);
            _0x3d81x25['inp']['focus']();
            _0x3d81x25['setCaretPosition'](_0x3d81x36c + this['dataset']['str']['length'] + 1 - (_0x3d81x36c - _0x3d81x36e));
            hideElem(_0x3d81x25['hintsElem']);
            if (_0x3d81x25['wipePrevious']) {
                _0x3d81x25['inp']['value'] = this['dataset']['str'];
                if (_0x3d81x25['onWiped']) {
                    _0x3d81x25['onWiped'](this['dataset']['str'])
                }
            }
        }), false);
        this['hintsElem']['appendChild'](_0x3d81x36a);
        ++_0x3d81x363;
        if (_0x3d81x363 >= this['maxPerHint']) {
            break
        }
    };
    this['setSelected'](0);
    if (_0x3d81x363) {
        showElem(this['hintsElem'])
    } else {
        hideElem(this['hintsElem'])
    }
};
ChatAutocomplete['prototype']['ReturnWord'] = function(_0x3d81xe8, _0x3d81x36f) {
    var _0x3d81x36d = _0x3d81xe8['substring'](0, _0x3d81x36f);
    if (_0x3d81x36d['indexOf'](' ') > 0) {
        var _0x3d81x370 = _0x3d81x36d['split'](' ');
        return _0x3d81x370[_0x3d81x370['length'] - 1]
    } else {
        return _0x3d81x36d
    }
};
ChatAutocomplete['prototype']['getCurrentWord'] = function() {
    var _0x3d81x36f = this.GetCaretPosition(this['inp']);
    var _0x3d81x360 = this.ReturnWord(this['inp']['value'], _0x3d81x36f);
    return [_0x3d81x360, _0x3d81x36f]
};
ChatAutocomplete['prototype']['GetCaretPosition'] = function(_0x3d81x371) {
    var _0x3d81x372 = 0;
    if (document['selection']) {
        _0x3d81x371['focus']();
        var _0x3d81x373 = document['selection']['createRange']();
        _0x3d81x373['moveStart']('character', -_0x3d81x371['value']['length']);
        _0x3d81x372 = _0x3d81x373['text']['length']
    } else {
        if (_0x3d81x371['selectionStart'] || _0x3d81x371['selectionStart'] == '0') {
            _0x3d81x372 = _0x3d81x371['selectionStart']
        }
    };
    return _0x3d81x372
};
ChatAutocomplete['prototype']['setCaretPosition'] = function(_0x3d81x232) {
    _0x3d81x232 = Math['max'](Math['min'](_0x3d81x232, this['inp']['value']['length']), 0);
    if (this['inp']['createTextRange']) {
        var _0x3d81x374 = this['inp']['createTextRange']();
        _0x3d81x374['moveStart']('character', _0x3d81x232);
        _0x3d81x374['collapse']();
        _0x3d81x374['select']()
    } else {
        this['inp']['focus']();
        this['inp']['setSelectionRange'](_0x3d81x232, _0x3d81x232)
    }
};

function Matrix() {}
Matrix['multiply'] = function(_0x3d81xa5, _0x3d81x141, _0x3d81x4) {
    _0x3d81x4 = _0x3d81x4 || new Float32Array(16);
    var _0x3d81x376 = _0x3d81x141[0 * 4 + 0];
    var _0x3d81x377 = _0x3d81x141[0 * 4 + 1];
    var _0x3d81x378 = _0x3d81x141[0 * 4 + 2];
    var _0x3d81x379 = _0x3d81x141[0 * 4 + 3];
    var _0x3d81x37a = _0x3d81x141[1 * 4 + 0];
    var _0x3d81x37b = _0x3d81x141[1 * 4 + 1];
    var _0x3d81x37c = _0x3d81x141[1 * 4 + 2];
    var _0x3d81x37d = _0x3d81x141[1 * 4 + 3];
    var _0x3d81x37e = _0x3d81x141[2 * 4 + 0];
    var _0x3d81x37f = _0x3d81x141[2 * 4 + 1];
    var _0x3d81x380 = _0x3d81x141[2 * 4 + 2];
    var _0x3d81x381 = _0x3d81x141[2 * 4 + 3];
    var _0x3d81x382 = _0x3d81x141[3 * 4 + 0];
    var _0x3d81x383 = _0x3d81x141[3 * 4 + 1];
    var _0x3d81x384 = _0x3d81x141[3 * 4 + 2];
    var _0x3d81x385 = _0x3d81x141[3 * 4 + 3];
    var _0x3d81x386 = _0x3d81xa5[0 * 4 + 0];
    var _0x3d81x387 = _0x3d81xa5[0 * 4 + 1];
    var _0x3d81x388 = _0x3d81xa5[0 * 4 + 2];
    var _0x3d81x389 = _0x3d81xa5[0 * 4 + 3];
    var _0x3d81x38a = _0x3d81xa5[1 * 4 + 0];
    var _0x3d81x38b = _0x3d81xa5[1 * 4 + 1];
    var _0x3d81x38c = _0x3d81xa5[1 * 4 + 2];
    var _0x3d81x38d = _0x3d81xa5[1 * 4 + 3];
    var _0x3d81x38e = _0x3d81xa5[2 * 4 + 0];
    var _0x3d81x38f = _0x3d81xa5[2 * 4 + 1];
    var _0x3d81x390 = _0x3d81xa5[2 * 4 + 2];
    var _0x3d81x391 = _0x3d81xa5[2 * 4 + 3];
    var _0x3d81x392 = _0x3d81xa5[3 * 4 + 0];
    var _0x3d81x393 = _0x3d81xa5[3 * 4 + 1];
    var _0x3d81x394 = _0x3d81xa5[3 * 4 + 2];
    var _0x3d81x395 = _0x3d81xa5[3 * 4 + 3];
    _0x3d81x4[0] = _0x3d81x376 * _0x3d81x386 + _0x3d81x377 * _0x3d81x38a + _0x3d81x378 * _0x3d81x38e + _0x3d81x379 * _0x3d81x392;
    _0x3d81x4[1] = _0x3d81x376 * _0x3d81x387 + _0x3d81x377 * _0x3d81x38b + _0x3d81x378 * _0x3d81x38f + _0x3d81x379 * _0x3d81x393;
    _0x3d81x4[2] = _0x3d81x376 * _0x3d81x388 + _0x3d81x377 * _0x3d81x38c + _0x3d81x378 * _0x3d81x390 + _0x3d81x379 * _0x3d81x394;
    _0x3d81x4[3] = _0x3d81x376 * _0x3d81x389 + _0x3d81x377 * _0x3d81x38d + _0x3d81x378 * _0x3d81x391 + _0x3d81x379 * _0x3d81x395;
    _0x3d81x4[4] = _0x3d81x37a * _0x3d81x386 + _0x3d81x37b * _0x3d81x38a + _0x3d81x37c * _0x3d81x38e + _0x3d81x37d * _0x3d81x392;
    _0x3d81x4[5] = _0x3d81x37a * _0x3d81x387 + _0x3d81x37b * _0x3d81x38b + _0x3d81x37c * _0x3d81x38f + _0x3d81x37d * _0x3d81x393;
    _0x3d81x4[6] = _0x3d81x37a * _0x3d81x388 + _0x3d81x37b * _0x3d81x38c + _0x3d81x37c * _0x3d81x390 + _0x3d81x37d * _0x3d81x394;
    _0x3d81x4[7] = _0x3d81x37a * _0x3d81x389 + _0x3d81x37b * _0x3d81x38d + _0x3d81x37c * _0x3d81x391 + _0x3d81x37d * _0x3d81x395;
    _0x3d81x4[8] = _0x3d81x37e * _0x3d81x386 + _0x3d81x37f * _0x3d81x38a + _0x3d81x380 * _0x3d81x38e + _0x3d81x381 * _0x3d81x392;
    _0x3d81x4[9] = _0x3d81x37e * _0x3d81x387 + _0x3d81x37f * _0x3d81x38b + _0x3d81x380 * _0x3d81x38f + _0x3d81x381 * _0x3d81x393;
    _0x3d81x4[10] = _0x3d81x37e * _0x3d81x388 + _0x3d81x37f * _0x3d81x38c + _0x3d81x380 * _0x3d81x390 + _0x3d81x381 * _0x3d81x394;
    _0x3d81x4[11] = _0x3d81x37e * _0x3d81x389 + _0x3d81x37f * _0x3d81x38d + _0x3d81x380 * _0x3d81x391 + _0x3d81x381 * _0x3d81x395;
    _0x3d81x4[12] = _0x3d81x382 * _0x3d81x386 + _0x3d81x383 * _0x3d81x38a + _0x3d81x384 * _0x3d81x38e + _0x3d81x385 * _0x3d81x392;
    _0x3d81x4[13] = _0x3d81x382 * _0x3d81x387 + _0x3d81x383 * _0x3d81x38b + _0x3d81x384 * _0x3d81x38f + _0x3d81x385 * _0x3d81x393;
    _0x3d81x4[14] = _0x3d81x382 * _0x3d81x388 + _0x3d81x383 * _0x3d81x38c + _0x3d81x384 * _0x3d81x390 + _0x3d81x385 * _0x3d81x394;
    _0x3d81x4[15] = _0x3d81x382 * _0x3d81x389 + _0x3d81x383 * _0x3d81x38d + _0x3d81x384 * _0x3d81x391 + _0x3d81x385 * _0x3d81x395;
    return _0x3d81x4
};
Matrix['orthographic'] = function(_0x3d81x396, _0x3d81x397, _0x3d81x398, _0x3d81x61, _0x3d81x399, _0x3d81x39a, _0x3d81x4) {
    _0x3d81x4 = _0x3d81x4 || new Float32Array(16);
    _0x3d81x4[0] = 2 / (_0x3d81x397 - _0x3d81x396);
    _0x3d81x4[1] = 0;
    _0x3d81x4[2] = 0;
    _0x3d81x4[3] = 0;
    _0x3d81x4[4] = 0;
    _0x3d81x4[5] = 2 / (_0x3d81x61 - _0x3d81x398);
    _0x3d81x4[6] = 0;
    _0x3d81x4[7] = 0;
    _0x3d81x4[8] = 0;
    _0x3d81x4[9] = 0;
    _0x3d81x4[10] = 2 / (_0x3d81x399 - _0x3d81x39a);
    _0x3d81x4[11] = 0;
    _0x3d81x4[12] = (_0x3d81x396 + _0x3d81x397) / (_0x3d81x396 - _0x3d81x397);
    _0x3d81x4[13] = (_0x3d81x398 + _0x3d81x61) / (_0x3d81x398 - _0x3d81x61);
    _0x3d81x4[14] = (_0x3d81x399 + _0x3d81x39a) / (_0x3d81x399 - _0x3d81x39a);
    _0x3d81x4[15] = 1;
    return _0x3d81x4
};
Matrix['translate'] = function(_0x3d81x1f5, _0x3d81x39b, _0x3d81x39c, _0x3d81x39d, _0x3d81x4) {
    _0x3d81x4 = _0x3d81x4 || new Float32Array(16);
    var _0x3d81x39e = _0x3d81x1f5[0];
    var _0x3d81x39f = _0x3d81x1f5[1];
    var _0x3d81x3a0 = _0x3d81x1f5[2];
    var _0x3d81x3a1 = _0x3d81x1f5[3];
    var _0x3d81x3a2 = _0x3d81x1f5[1 * 4 + 0];
    var _0x3d81x3a3 = _0x3d81x1f5[1 * 4 + 1];
    var _0x3d81x3a4 = _0x3d81x1f5[1 * 4 + 2];
    var _0x3d81x3a5 = _0x3d81x1f5[1 * 4 + 3];
    var _0x3d81x3a6 = _0x3d81x1f5[2 * 4 + 0];
    var _0x3d81x3a7 = _0x3d81x1f5[2 * 4 + 1];
    var _0x3d81x3a8 = _0x3d81x1f5[2 * 4 + 2];
    var _0x3d81x3a9 = _0x3d81x1f5[2 * 4 + 3];
    var _0x3d81x3aa = _0x3d81x1f5[3 * 4 + 0];
    var _0x3d81x3ab = _0x3d81x1f5[3 * 4 + 1];
    var _0x3d81x3ac = _0x3d81x1f5[3 * 4 + 2];
    var _0x3d81x3ad = _0x3d81x1f5[3 * 4 + 3];
    if (_0x3d81x1f5 !== _0x3d81x4) {
        _0x3d81x4[0] = _0x3d81x39e;
        _0x3d81x4[1] = _0x3d81x39f;
        _0x3d81x4[2] = _0x3d81x3a0;
        _0x3d81x4[3] = _0x3d81x3a1;
        _0x3d81x4[4] = _0x3d81x3a2;
        _0x3d81x4[5] = _0x3d81x3a3;
        _0x3d81x4[6] = _0x3d81x3a4;
        _0x3d81x4[7] = _0x3d81x3a5;
        _0x3d81x4[8] = _0x3d81x3a6;
        _0x3d81x4[9] = _0x3d81x3a7;
        _0x3d81x4[10] = _0x3d81x3a8;
        _0x3d81x4[11] = _0x3d81x3a9
    };
    _0x3d81x4[12] = _0x3d81x39e * _0x3d81x39b + _0x3d81x3a2 * _0x3d81x39c + _0x3d81x3a6 * _0x3d81x39d + _0x3d81x3aa;
    _0x3d81x4[13] = _0x3d81x39f * _0x3d81x39b + _0x3d81x3a3 * _0x3d81x39c + _0x3d81x3a7 * _0x3d81x39d + _0x3d81x3ab;
    _0x3d81x4[14] = _0x3d81x3a0 * _0x3d81x39b + _0x3d81x3a4 * _0x3d81x39c + _0x3d81x3a8 * _0x3d81x39d + _0x3d81x3ac;
    _0x3d81x4[15] = _0x3d81x3a1 * _0x3d81x39b + _0x3d81x3a5 * _0x3d81x39c + _0x3d81x3a9 * _0x3d81x39d + _0x3d81x3ad;
    return _0x3d81x4
};
Matrix['scale'] = function(_0x3d81x1f5, _0x3d81x3ae, _0x3d81x3af, _0x3d81x3b0, _0x3d81x4) {
    _0x3d81x4 = _0x3d81x4 || new Float32Array(16);
    _0x3d81x4[0] = _0x3d81x3ae * _0x3d81x1f5[0 * 4 + 0];
    _0x3d81x4[1] = _0x3d81x3ae * _0x3d81x1f5[0 * 4 + 1];
    _0x3d81x4[2] = _0x3d81x3ae * _0x3d81x1f5[0 * 4 + 2];
    _0x3d81x4[3] = _0x3d81x3ae * _0x3d81x1f5[0 * 4 + 3];
    _0x3d81x4[4] = _0x3d81x3af * _0x3d81x1f5[1 * 4 + 0];
    _0x3d81x4[5] = _0x3d81x3af * _0x3d81x1f5[1 * 4 + 1];
    _0x3d81x4[6] = _0x3d81x3af * _0x3d81x1f5[1 * 4 + 2];
    _0x3d81x4[7] = _0x3d81x3af * _0x3d81x1f5[1 * 4 + 3];
    _0x3d81x4[8] = _0x3d81x3b0 * _0x3d81x1f5[2 * 4 + 0];
    _0x3d81x4[9] = _0x3d81x3b0 * _0x3d81x1f5[2 * 4 + 1];
    _0x3d81x4[10] = _0x3d81x3b0 * _0x3d81x1f5[2 * 4 + 2];
    _0x3d81x4[11] = _0x3d81x3b0 * _0x3d81x1f5[2 * 4 + 3];
    if (_0x3d81x1f5 !== _0x3d81x4) {
        _0x3d81x4[12] = _0x3d81x1f5[12];
        _0x3d81x4[13] = _0x3d81x1f5[13];
        _0x3d81x4[14] = _0x3d81x1f5[14];
        _0x3d81x4[15] = _0x3d81x1f5[15]
    };
    return _0x3d81x4
};
Matrix['translation'] = function(_0x3d81x39b, _0x3d81x39c, _0x3d81x39d, _0x3d81x4) {
    _0x3d81x4 = _0x3d81x4 || new Float32Array(16);
    _0x3d81x4[0] = 1;
    _0x3d81x4[1] = 0;
    _0x3d81x4[2] = 0;
    _0x3d81x4[3] = 0;
    _0x3d81x4[4] = 0;
    _0x3d81x4[5] = 1;
    _0x3d81x4[6] = 0;
    _0x3d81x4[7] = 0;
    _0x3d81x4[8] = 0;
    _0x3d81x4[9] = 0;
    _0x3d81x4[10] = 1;
    _0x3d81x4[11] = 0;
    _0x3d81x4[12] = _0x3d81x39b;
    _0x3d81x4[13] = _0x3d81x39c;
    _0x3d81x4[14] = _0x3d81x39d;
    _0x3d81x4[15] = 1;
    return _0x3d81x4
};
(function(_0x3d81x3b1) {
    var _0x3d81x3b2 = function() {
        var _0x3d81x3b3 = ['monospace', 'sans-serif', 'serif'];
        var _0x3d81x3b4 = 'mmmwmmmmmmlli';
        var _0x3d81x3b5 = '72px';
        var _0x3d81xaa = document['getElementsByTagName']('body')[0];
        var _0x3d81x185 = document['createElement']('span');
        _0x3d81x185['style']['fontSize'] = _0x3d81x3b5;
        _0x3d81x185['innerHTML'] = _0x3d81x3b4;
        var _0x3d81x3b6 = {};
        var _0x3d81x3b7 = {};
        for (var _0x3d81xf in _0x3d81x3b3) {
            _0x3d81x185['style']['fontFamily'] = _0x3d81x3b3[_0x3d81xf];
            _0x3d81xaa['appendChild'](_0x3d81x185);
            _0x3d81x3b6[_0x3d81x3b3[_0x3d81xf]] = _0x3d81x185['offsetWidth'];
            _0x3d81x3b7[_0x3d81x3b3[_0x3d81xf]] = _0x3d81x185['offsetHeight'];
            _0x3d81xaa['removeChild'](_0x3d81x185)
        };

        function _0x3d81x3b8(_0x3d81x29d) {
            var _0x3d81x246 = false;
            for (var _0x3d81xf in _0x3d81x3b3) {
                _0x3d81x185['style']['fontFamily'] = _0x3d81x29d + ',' + _0x3d81x3b3[_0x3d81xf];
                _0x3d81xaa['appendChild'](_0x3d81x185);
                var _0x3d81x3b9 = (_0x3d81x185['offsetWidth'] != _0x3d81x3b6[_0x3d81x3b3[_0x3d81xf]] || _0x3d81x185['offsetHeight'] != _0x3d81x3b7[_0x3d81x3b3[_0x3d81xf]]);
                _0x3d81xaa['removeChild'](_0x3d81x185);
                _0x3d81x246 = _0x3d81x246 || _0x3d81x3b9
            };
            return _0x3d81x246
        }
        this['detect'] = _0x3d81x3b8
    };

    function _0x3d81x3ba(_0x3d81x14, _0x3d81x4a) {
        var _0x3d81x3bb, _0x3d81x2f4, _0x3d81x3bc, _0x3d81x3bd, _0x3d81x3be, _0x3d81x3bf, _0x3d81x3c0, _0x3d81x3c1, _0x3d81x3c2, _0x3d81x13;
        _0x3d81x3bb = _0x3d81x14['length'] & 3;
        _0x3d81x2f4 = _0x3d81x14['length'] - _0x3d81x3bb;
        _0x3d81x3bc = _0x3d81x4a;
        _0x3d81x3be = 0xcc9e2d51;
        _0x3d81x3c0 = 0x1b873593;
        _0x3d81x13 = 0;
        while (_0x3d81x13 < _0x3d81x2f4) {
            _0x3d81x3c2 = ((_0x3d81x14['charCodeAt'](_0x3d81x13) & 0xff)) | ((_0x3d81x14['charCodeAt'](++_0x3d81x13) & 0xff) << 8) | ((_0x3d81x14['charCodeAt'](++_0x3d81x13) & 0xff) << 16) | ((_0x3d81x14['charCodeAt'](++_0x3d81x13) & 0xff) << 24);
            ++_0x3d81x13;
            _0x3d81x3c2 = ((((_0x3d81x3c2 & 0xffff) * _0x3d81x3be) + ((((_0x3d81x3c2 >>> 16) * _0x3d81x3be) & 0xffff) << 16))) & 0xffffffff;
            _0x3d81x3c2 = (_0x3d81x3c2 << 15) | (_0x3d81x3c2 >>> 17);
            _0x3d81x3c2 = ((((_0x3d81x3c2 & 0xffff) * _0x3d81x3c0) + ((((_0x3d81x3c2 >>> 16) * _0x3d81x3c0) & 0xffff) << 16))) & 0xffffffff;
            _0x3d81x3bc ^= _0x3d81x3c2;
            _0x3d81x3bc = (_0x3d81x3bc << 13) | (_0x3d81x3bc >>> 19);
            _0x3d81x3bd = ((((_0x3d81x3bc & 0xffff) * 5) + ((((_0x3d81x3bc >>> 16) * 5) & 0xffff) << 16))) & 0xffffffff;
            _0x3d81x3bc = (((_0x3d81x3bd & 0xffff) + 0x6b64) + ((((_0x3d81x3bd >>> 16) + 0xe654) & 0xffff) << 16))
        };
        _0x3d81x3c2 = 0;
        switch (_0x3d81x3bb) {
            case 3:
                _0x3d81x3c2 ^= (_0x3d81x14['charCodeAt'](_0x3d81x13 + 2) & 0xff) << 16;
            case 2:
                _0x3d81x3c2 ^= (_0x3d81x14['charCodeAt'](_0x3d81x13 + 1) & 0xff) << 8;
            case 1:
                _0x3d81x3c2 ^= (_0x3d81x14['charCodeAt'](_0x3d81x13) & 0xff);
                _0x3d81x3c2 = (((_0x3d81x3c2 & 0xffff) * _0x3d81x3be) + ((((_0x3d81x3c2 >>> 16) * _0x3d81x3be) & 0xffff) << 16)) & 0xffffffff;
                _0x3d81x3c2 = (_0x3d81x3c2 << 15) | (_0x3d81x3c2 >>> 17);
                _0x3d81x3c2 = (((_0x3d81x3c2 & 0xffff) * _0x3d81x3c0) + ((((_0x3d81x3c2 >>> 16) * _0x3d81x3c0) & 0xffff) << 16)) & 0xffffffff;
                _0x3d81x3bc ^= _0x3d81x3c2
        };
        _0x3d81x3bc ^= _0x3d81x14['length'];
        _0x3d81x3bc ^= _0x3d81x3bc >>> 16;
        _0x3d81x3bc = (((_0x3d81x3bc & 0xffff) * 0x85ebca6b) + ((((_0x3d81x3bc >>> 16) * 0x85ebca6b) & 0xffff) << 16)) & 0xffffffff;
        _0x3d81x3bc ^= _0x3d81x3bc >>> 13;
        _0x3d81x3bc = ((((_0x3d81x3bc & 0xffff) * 0xc2b2ae35) + ((((_0x3d81x3bc >>> 16) * 0xc2b2ae35) & 0xffff) << 16))) & 0xffffffff;
        _0x3d81x3bc ^= _0x3d81x3bc >>> 16;
        return _0x3d81x3bc >>> 0
    }
    var _0x3d81x3c3;
    var _0x3d81x3c4 = function() {
        _0x3d81x3c3 = new _0x3d81x3b2();
        return this
    };
    _0x3d81x3c4['prototype'] = {
        get: function() {
            var _0x3d81x2b8 = ':';
            var _0x3d81x3c5 = this['webgl2']();
            var _0x3d81x3c6 = this['gsp']();
            var _0x3d81x3c7 = this['gpl']();
            var _0x3d81x3c8 = this['gfo']();
            var _0x3d81x3c9 = this['gtz']();
            var _0x3d81x3ca = this['gla']();
            var _0x3d81x3cb = this['gsl']();
            var _0x3d81x3cc = this['gcp']();
            var _0x3d81x14 = _0x3d81x3c6 + _0x3d81x2b8 + _0x3d81x3c7 + _0x3d81x2b8 + _0x3d81x3c8 + _0x3d81x2b8 + sessionStorage + _0x3d81x2b8 + _0x3d81x3c9 + _0x3d81x2b8 + _0x3d81x3ca + _0x3d81x2b8 + _0x3d81x3cb + _0x3d81x2b8 + _0x3d81x3cc;
            return _0x3d81x3ba(_0x3d81x3c5, 256) + ':' + _0x3d81x3ba(_0x3d81x14, 256)
        },
        gsp: function() {
            return 'Cu: ' + this['gcr']() + ', Av: ' + this['gar']() + ', CD: ' + this['gcd']() + ', X: ' + this['gXDPI']() + ', Y: ' + this['gYDPI']()
        },
        gcd: function() {
            return screen['colorDepth']
        },
        gcr: function() {
            return screen['width'] + 'x' + screen['height']
        },
        gar: function() {
            return screen['availWidth'] + 'x' + screen['availHeight']
        },
        gXDPI: function() {
            return screen['deviceXDPI']
        },
        gYDPI: function() {
            return screen['deviceYDPI']
        },
        gpl: function() {
            var _0x3d81x3cd = '';
            for (var _0x3d81x13 = 0; _0x3d81x13 < navigator['plugins']['length']; _0x3d81x13++) {
                if (_0x3d81x13 == navigator['plugins']['length'] - 1) {
                    _0x3d81x3cd += navigator['plugins'][_0x3d81x13]['name']
                } else {
                    _0x3d81x3cd += navigator['plugins'][_0x3d81x13]['name'] + ', '
                }
            };
            return _0x3d81x3cd
        },
        gmty: function() {
            var _0x3d81x3ce = '';
            if (navigator['mimeTypes']) {
                for (var _0x3d81x13 = 0; _0x3d81x13 < navigator['mimeTypes']['length']; _0x3d81x13++) {
                    if (_0x3d81x13 == navigator['mimeTypes']['length'] - 1) {
                        _0x3d81x3ce += navigator['mimeTypes'][_0x3d81x13]['description']
                    } else {
                        _0x3d81x3ce += navigator['mimeTypes'][_0x3d81x13]['description'] + ', '
                    }
                }
            };
            return _0x3d81x3ce
        },
        gfo: function() {
            var _0x3d81x3cf = ['Abadi MT Condensed Light', 'Adobe Fangsong Std', 'Adobe Hebrew', 'Adobe Ming Std', 'Agency FB', 'Aharoni', 'Andalus', 'Angsana New', 'AngsanaUPC', 'Aparajita', 'Arab', 'Arabic Transparent', 'Arabic Typesetting', 'Arial Baltic', 'Arial Black', 'Arial CE', 'Arial CYR', 'Arial Greek', 'Arial TUR', 'Arial', 'Batang', 'BatangChe', 'Bauhaus 93', 'Bell MT', 'Bitstream Vera Serif', 'Bodoni MT', 'Bookman Old Style', 'Braggadocio', 'Broadway', 'Browallia New', 'BrowalliaUPC', 'Calibri Light', 'Calibri', 'Californian FB', 'Cambria Math', 'Cambria', 'Candara', 'Castellar', 'Casual', 'Centaur', 'Century Gothic', 'Chalkduster', 'Colonna MT', 'Comic Sans MS', 'Consolas', 'Constantia', 'Copperplate Gothic Light', 'Corbel', 'Cordia New', 'CordiaUPC', 'Courier New Baltic', 'Courier New CE', 'Courier New CYR', 'Courier New Greek', 'Courier New TUR', 'Courier New', 'DFKai-SB', 'DaunPenh', 'David', 'DejaVu LGC Sans Mono', 'Desdemona', 'DilleniaUPC', 'DokChampa', 'Dotum', 'DotumChe', 'Ebrima', 'Engravers MT', 'Eras Bold ITC', 'Estrangelo Edessa', 'EucrosiaUPC', 'Euphemia', 'Eurostile', 'FangSong', 'Forte', 'FrankRuehl', 'Franklin Gothic Heavy', 'Franklin Gothic Medium', 'FreesiaUPC', 'French Script MT', 'Gabriola', 'Gautami', 'Georgia', 'Gigi', 'Gisha', 'Goudy Old Style', 'Gulim', 'GulimChe', 'GungSeo', 'Gungsuh', 'GungsuhChe', 'Haettenschweiler', 'Harrington', 'Hei S', 'HeiT', 'Heisei Kaku Gothic', 'Hiragino Sans GB', 'Impact', 'Informal Roman', 'IrisUPC', 'Iskoola Pota', 'JasmineUPC', 'KacstOne', 'KaiTi', 'Kalinga', 'Kartika', 'Khmer UI', 'Kino MT', 'KodchiangUPC', 'Kokila', 'Kozuka Gothic Pr6N', 'Lao UI', 'Latha', 'Leelawadee', 'Levenim MT', 'LilyUPC', 'Lohit Gujarati', 'Loma', 'Lucida Bright', 'Lucida Console', 'Lucida Fax', 'Lucida Sans Unicode', 'MS Gothic', 'MS Mincho', 'MS PGothic', 'MS PMincho', 'MS Reference Sans Serif', 'MS UI Gothic', 'MV Boli', 'Magneto', 'Malgun Gothic', 'Mangal', 'Marlett', 'Matura MT Script Capitals', 'Meiryo UI', 'Meiryo', 'Menlo', 'Microsoft Himalaya', 'Microsoft JhengHei', 'Microsoft New Tai Lue', 'Microsoft PhagsPa', 'Microsoft Sans Serif', 'Microsoft Tai Le', 'Microsoft Uighur', 'Microsoft YaHei', 'Microsoft Yi Baiti', 'MingLiU', 'MingLiU-ExtB', 'MingLiU_HKSCS', 'MingLiU_HKSCS-ExtB', 'Miriam Fixed', 'Miriam', 'Mongolian Baiti', 'MoolBoran', 'NSimSun', 'Narkisim', 'News Gothic MT', 'Niagara Solid', 'Nyala', 'PMingLiU', 'PMingLiU-ExtB', 'Palace Script MT', 'Palatino Linotype', 'Papyrus', 'Perpetua', 'Plantagenet Cherokee', 'Playbill', 'Prelude Bold', 'Prelude Condensed Bold', 'Prelude Condensed Medium', 'Prelude Medium', 'PreludeCompressedWGL Black', 'PreludeCompressedWGL Bold', 'PreludeCompressedWGL Light', 'PreludeCompressedWGL Medium', 'PreludeCondensedWGL Black', 'PreludeCondensedWGL Bold', 'PreludeCondensedWGL Light', 'PreludeCondensedWGL Medium', 'PreludeWGL Black', 'PreludeWGL Bold', 'PreludeWGL Light', 'PreludeWGL Medium', 'Raavi', 'Rachana', 'Rockwell', 'Rod', 'Sakkal Majalla', 'Sawasdee', 'Script MT Bold', 'Segoe Print', 'Segoe Script', 'Segoe UI Light', 'Segoe UI Semibold', 'Segoe UI Symbol', 'Segoe UI', 'Shonar Bangla', 'Showcard Gothic', 'Shruti', 'SimHei', 'SimSun', 'SimSun-ExtB', 'Simplified Arabic Fixed', 'Simplified Arabic', 'Snap ITC', 'Sylfaen', 'Symbol', 'Tahoma', 'Times New Roman Baltic', 'Times New Roman CE', 'Times New Roman CYR', 'Times New Roman Greek', 'Times New Roman TUR', 'Times New Roman', 'TlwgMono', 'Traditional Arabic', 'Trebuchet MS', 'Tunga', 'Tw Cen MT Condensed Extra Bold', 'Ubuntu', 'Umpush', 'Univers', 'Utopia', 'Utsaah', 'Vani', 'Verdana', 'Vijaya', 'Vladimir Script', 'Vrinda', 'Webdings', 'Wide Latin', 'Wingdings'];
            var _0x3d81x3d0 = '';
            for (var _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x3cf['length']; _0x3d81x13++) {
                if (_0x3d81x3c3['detect'](_0x3d81x3cf[_0x3d81x13])) {
                    if (_0x3d81x13 == _0x3d81x3cf['length'] - 1) {
                        _0x3d81x3d0 += _0x3d81x3cf[_0x3d81x13]
                    } else {
                        _0x3d81x3d0 += _0x3d81x3cf[_0x3d81x13] + ', '
                    }
                }
            };
            return _0x3d81x3d0
        },
        gtz: function() {
            var _0x3d81x3d1, _0x3d81x3d2, _0x3d81x3d3, _0x3d81x150;
            _0x3d81x3d1 = new Date();
            _0x3d81x3d2 = String(-(_0x3d81x3d1['getTimezoneOffset']() / 60));
            if (_0x3d81x3d2 < 0) {
                _0x3d81x3d2 = _0x3d81x3d2 * -1;
                _0x3d81x3d3 = ('0' + _0x3d81x3d2)['slice'](-2);
                _0x3d81x150 = '-' + _0x3d81x3d3
            } else {
                _0x3d81x3d3 = ('0' + _0x3d81x3d2)['slice'](-2);
                _0x3d81x150 = '+' + _0x3d81x3d3
            };
            return _0x3d81x150
        },
        gla: function() {
            return navigator['language']
        },
        gsl: function() {
            return navigator['systemLanguage'] || window['navigator']['language']
        },
        webgl: function() {
            var _0x3d81xae, _0x3d81x99, _0x3d81xa6 = 256,
                _0x3d81xa7 = 128;
            _0x3d81xae = document['createElement']('canvas');
            _0x3d81xae['width'] = _0x3d81xa6,
                _0x3d81xae['height'] = _0x3d81xa7,
                _0x3d81x99 = _0x3d81xae['getContext']('webgl2') || _0x3d81xae['getContext']('experimental-webgl2') || _0x3d81xae['getContext']('webgl') || _0x3d81xae['getContext']('experimental-webgl') || _0x3d81xae['getContext']('moz-webgl');
            if (!_0x3d81x99) {
                return '0'
            };
            try {
                var _0x3d81x221 = 'attribute vec2 attrVertex;varying vec2 varyinTexCoordinate;uniform vec2 uniformOffset;void main(){varyinTexCoordinate=attrVertex+uniformOffset;gl_Position=vec4(attrVertex,0,1);}';
                var _0x3d81x93 = 'precision mediump float;varying vec2 varyinTexCoordinate;void main() {gl_FragColor=vec4(varyinTexCoordinate,0,1);}';
                var _0x3d81xaa = _0x3d81x99['createBuffer']();
                _0x3d81x99['bindBuffer'](_0x3d81x99.ARRAY_BUFFER, _0x3d81xaa);
                var _0x3d81x13 = new Float32Array([-0.2, -0.9, 0, 0.4, -0.26, 0, 0, 0.7321, 0]);
                _0x3d81x99['bufferData'](_0x3d81x99.ARRAY_BUFFER, _0x3d81x13, _0x3d81x99.STATIC_DRAW),
                    _0x3d81xaa['itemSize'] = 3,
                    _0x3d81xaa['numItems'] = 3;
                var _0x3d81x70 = _0x3d81x99['createProgram']();
                var _0x3d81x15e = _0x3d81x99['createShader'](_0x3d81x99.VERTEX_SHADER);
                _0x3d81x99['shaderSource'](_0x3d81x15e, _0x3d81x221);
                _0x3d81x99['compileShader'](_0x3d81x15e);
                var _0x3d81x217 = _0x3d81x99['createShader'](_0x3d81x99.FRAGMENT_SHADER);
                _0x3d81x99['shaderSource'](_0x3d81x217, _0x3d81x93);
                _0x3d81x99['compileShader'](_0x3d81x217);
                _0x3d81x99['attachShader'](_0x3d81x70, _0x3d81x15e);
                _0x3d81x99['attachShader'](_0x3d81x70, _0x3d81x217);
                _0x3d81x99['linkProgram'](_0x3d81x70);
                _0x3d81x99['useProgram'](_0x3d81x70);
                _0x3d81x70['vertexPosAttrib'] = _0x3d81x99['getAttribLocation'](_0x3d81x70, 'attrVertex');
                _0x3d81x70['offsetUniform'] = _0x3d81x99['getUniformLocation'](_0x3d81x70, 'uniformOffset');
                _0x3d81x99['enableVertexAttribArray'](_0x3d81x70['vertexPosArray']);
                _0x3d81x99['vertexAttribPointer'](_0x3d81x70['vertexPosAttrib'], _0x3d81xaa['itemSize'], _0x3d81x99.FLOAT, !1, 0, 0);
                _0x3d81x99['uniform2f'](_0x3d81x70['offsetUniform'], 1, 1);
                _0x3d81x99['drawArrays'](_0x3d81x99.TRIANGLE_STRIP, 0, _0x3d81xaa['numItems'])
            } catch (e) {
                return '0'
            };
            try {
                var _0x3d81x131 = new Uint8Array(_0x3d81xa6 * _0x3d81xa7 * 4);
                _0x3d81x99['readPixels'](0, 0, _0x3d81xa6, _0x3d81xa7, _0x3d81x99.RGBA, _0x3d81x99.UNSIGNED_BYTE, _0x3d81x131);
                var _0x3d81x1f5 = JSON['stringify'](_0x3d81x131)['replace'](/,?"[0-9]+":/g, '');
                return _0x3d81x3ba(_0x3d81x1f5, 256)
            } catch (e) {
                return '0'
            }
        },
        webgl2: function() {
            let _0x3d81x3d4 = '';
            let _0x3d81x3d5 = '';
            let _0x3d81x3d6 = '';
            try {
                var _0x3d81x3d7 = function(_0x3d81x3d8) {
                    _0x3d81xb0['clearColor'](0.0, 0.0, 0.0, 1.0);
                    _0x3d81xb0['enable'](_0x3d81xb0.DEPTH_TEST);
                    _0x3d81xb0['depthFunc'](_0x3d81xb0.LEQUAL);
                    _0x3d81xb0['clear'](_0x3d81xb0['COLOR_BUFFER_BIT'] | _0x3d81xb0['DEPTH_BUFFER_BIT']);
                    return '[' + _0x3d81x3d8[0] + ', ' + _0x3d81x3d8[1] + ']'
                };
                var _0x3d81x3d9 = function(_0x3d81xb0) {
                    var _0x3d81x3da, _0x3d81x3db = _0x3d81xb0['getExtension']('EXT_texture_filter_anisotropic') || _0x3d81xb0['getExtension']('WEBKIT_EXT_texture_filter_anisotropic') || _0x3d81xb0['getExtension']('MOZ_EXT_texture_filter_anisotropic');
                    return _0x3d81x3db ? (_0x3d81x3da = _0x3d81xb0['getParameter'](_0x3d81x3db.MAX_TEXTURE_MAX_ANISOTROPY_EXT),
                        0 === _0x3d81x3da && (_0x3d81x3da = 2),
                        _0x3d81x3da) : null
                };
                var _0x3d81xae = document['createElement']('canvas');
                var _0x3d81xb0 = _0x3d81xae['getContext']('webgl') || _0x3d81xae['getContext']('experimental-webgl');
                var _0x3d81x150 = [];
                var _0x3d81x3dc = 'attribute vec2 attrVertex;varying vec2 varyinTexCoordinate;uniform vec2 uniformOffset;void main(){varyinTexCoordinate=attrVertex+uniformOffset;gl_Position=vec4(attrVertex,0,1);}';
                var _0x3d81x3dd = 'precision mediump float;varying vec2 varyinTexCoordinate;void main() {gl_FragColor=vec4(varyinTexCoordinate,0,1);}';
                var _0x3d81x3de = _0x3d81xb0['createBuffer']();
                _0x3d81xb0['bindBuffer'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x3de);
                var _0x3d81x3df = new Float32Array([-0.2, -0.9, 0, 0.4, -0.26, 0, 0, 0.732134444, 0]);
                _0x3d81xb0['bufferData'](_0x3d81xb0.ARRAY_BUFFER, _0x3d81x3df, _0x3d81xb0.STATIC_DRAW);
                _0x3d81x3de['itemSize'] = 3;
                _0x3d81x3de['numItems'] = 3;
                var _0x3d81xb2 = _0x3d81xb0['createProgram'](),
                    _0x3d81x3e0 = _0x3d81xb0['createShader'](_0x3d81xb0.VERTEX_SHADER);
                _0x3d81xb0['shaderSource'](_0x3d81x3e0, _0x3d81x3dc);
                _0x3d81xb0['compileShader'](_0x3d81x3e0);
                var _0x3d81x3e1 = _0x3d81xb0['createShader'](_0x3d81xb0.FRAGMENT_SHADER);
                _0x3d81xb0['shaderSource'](_0x3d81x3e1, _0x3d81x3dd);
                _0x3d81xb0['compileShader'](_0x3d81x3e1);
                _0x3d81xb0['attachShader'](_0x3d81xb2, _0x3d81x3e0);
                _0x3d81xb0['attachShader'](_0x3d81xb2, _0x3d81x3e1);
                _0x3d81xb0['linkProgram'](_0x3d81xb2);
                _0x3d81xb0['useProgram'](_0x3d81xb2);
                _0x3d81xb2['vertexPosAttrib'] = _0x3d81xb0['getAttribLocation'](_0x3d81xb2, 'attrVertex');
                _0x3d81xb2['offsetUniform'] = _0x3d81xb0['getUniformLocation'](_0x3d81xb2, 'uniformOffset');
                _0x3d81xb0['enableVertexAttribArray'](_0x3d81xb2['vertexPosArray']);
                _0x3d81xb0['vertexAttribPointer'](_0x3d81xb2['vertexPosAttrib'], _0x3d81x3de['itemSize'], _0x3d81xb0.FLOAT, !1, 0, 0);
                _0x3d81xb0['uniform2f'](_0x3d81xb2['offsetUniform'], 1, 1);
                _0x3d81xb0['drawArrays'](_0x3d81xb0.TRIANGLE_STRIP, 0, _0x3d81x3de['numItems']);
                if (_0x3d81xb0['canvas'] != null) {
                    _0x3d81x150['push'](_0x3d81xb0['canvas']['toDataURL']())
                };
                _0x3d81x150['push']('ext:' + _0x3d81xb0['getSupportedExtensions']()['join'](';'));
                _0x3d81x150['push']('a:' + _0x3d81x3d7(_0x3d81xb0['getParameter'](_0x3d81xb0.ALIASED_LINE_WIDTH_RANGE)));
                _0x3d81x150['push']('b:' + _0x3d81x3d7(_0x3d81xb0['getParameter'](_0x3d81xb0.ALIASED_POINT_SIZE_RANGE)));
                _0x3d81x150['push']('c:' + _0x3d81xb0['getParameter'](_0x3d81xb0.ALPHA_BITS));
                _0x3d81x150['push']('d:' + (_0x3d81xb0['getContextAttributes']()['antialias'] ? 'yes' : 'no'));
                _0x3d81x150['push']('e:' + _0x3d81xb0['getParameter'](_0x3d81xb0.BLUE_BITS));
                _0x3d81x150['push']('f:' + _0x3d81xb0['getParameter'](_0x3d81xb0.DEPTH_BITS));
                _0x3d81x150['push']('g:' + _0x3d81xb0['getParameter'](_0x3d81xb0.GREEN_BITS));
                _0x3d81x150['push']('h:' + _0x3d81x3d9(_0x3d81xb0));
                _0x3d81x150['push']('i:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_COMBINED_TEXTURE_IMAGE_UNITS));
                _0x3d81x150['push']('j:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_CUBE_MAP_TEXTURE_SIZE));
                _0x3d81x150['push']('k:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_FRAGMENT_UNIFORM_VECTORS));
                _0x3d81x150['push']('l:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_RENDERBUFFER_SIZE));
                _0x3d81x150['push']('m:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_TEXTURE_IMAGE_UNITS));
                _0x3d81x150['push']('n:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_TEXTURE_SIZE));
                _0x3d81x150['push']('o:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_VARYING_VECTORS));
                _0x3d81x150['push']('p:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_VERTEX_ATTRIBS));
                _0x3d81x150['push']('q:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_VERTEX_TEXTURE_IMAGE_UNITS));
                _0x3d81x150['push']('r:' + _0x3d81xb0['getParameter'](_0x3d81xb0.MAX_VERTEX_UNIFORM_VECTORS));
                _0x3d81x150['push']('s:' + _0x3d81x3d7(_0x3d81xb0['getParameter'](_0x3d81xb0.MAX_VIEWPORT_DIMS)));
                _0x3d81x150['push']('t:' + _0x3d81xb0['getParameter'](_0x3d81xb0.RED_BITS));
                _0x3d81x150['push']('u:' + _0x3d81xb0['getParameter'](_0x3d81xb0.RENDERER));
                _0x3d81x150['push']('v:' + _0x3d81xb0['getParameter'](_0x3d81xb0.SHADING_LANGUAGE_VERSION));
                _0x3d81x150['push']('w:' + _0x3d81xb0['getParameter'](_0x3d81xb0.STENCIL_BITS));
                _0x3d81x150['push']('x:' + _0x3d81xb0['getParameter'](_0x3d81xb0.VENDOR));
                _0x3d81x150['push']('y:' + _0x3d81xb0['getParameter'](_0x3d81xb0.VERSION));
                _0x3d81x150['push']('z:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.HIGH_FLOAT)['precision']);
                _0x3d81x150['push']('aa:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.HIGH_FLOAT)['rangeMin']);
                _0x3d81x150['push']('bb:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.HIGH_FLOAT)['rangeMax']);
                _0x3d81x150['push']('cc:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.MEDIUM_FLOAT)['precision']);
                _0x3d81x150['push']('dd:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.MEDIUM_FLOAT)['rangeMin']);
                _0x3d81x150['push']('ee:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.MEDIUM_FLOAT)['rangeMax']);
                _0x3d81x150['push']('ff:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.LOW_FLOAT)['precision']);
                _0x3d81x150['push']('gg:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.LOW_FLOAT)['rangeMin']);
                _0x3d81x150['push']('hh:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.LOW_FLOAT)['rangeMax']);
                _0x3d81x150['push']('ii:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.HIGH_FLOAT)['precision']);
                _0x3d81x150['push']('jj:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.HIGH_FLOAT)['rangeMin']);
                _0x3d81x150['push']('kk:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.HIGH_FLOAT)['rangeMax']);
                _0x3d81x150['push']('ll:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.MEDIUM_FLOAT)['precision']);
                _0x3d81x150['push']('mm:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.MEDIUM_FLOAT)['rangeMin']);
                _0x3d81x150['push']('nn:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.MEDIUM_FLOAT)['rangeMax']);
                _0x3d81x150['push']('oo:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.LOW_FLOAT)['precision']);
                _0x3d81x150['push']('pp:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.LOW_FLOAT)['rangeMin']);
                _0x3d81x150['push']('qq:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.LOW_FLOAT)['rangeMax']);
                _0x3d81x150['push']('rr:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.HIGH_INT)['precision']);
                _0x3d81x150['push']('ss:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.HIGH_INT)['rangeMin']);
                _0x3d81x150['push']('tt:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.HIGH_INT)['rangeMax']);
                _0x3d81x150['push']('uu:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.MEDIUM_INT)['precision']);
                _0x3d81x150['push']('vv:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.MEDIUM_INT)['rangeMin']);
                _0x3d81x150['push']('ww:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.MEDIUM_INT)['rangeMax']);
                _0x3d81x150['push']('xx:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.LOW_INT)['precision']);
                _0x3d81x150['push']('yy:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.LOW_INT)['rangeMin']);
                _0x3d81x150['push']('zz:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.VERTEX_SHADER, _0x3d81xb0.LOW_INT)['rangeMax']);
                _0x3d81x150['push']('1:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.HIGH_INT)['precision']);
                _0x3d81x150['push']('2:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.HIGH_INT)['rangeMin']);
                _0x3d81x150['push']('3:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.HIGH_INT)['rangeMax']);
                _0x3d81x150['push']('4:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.MEDIUM_INT)['precision']);
                _0x3d81x150['push']('5:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.MEDIUM_INT)['rangeMin']);
                _0x3d81x150['push']('6:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.MEDIUM_INT)['rangeMax']);
                _0x3d81x150['push']('7:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.LOW_INT)['precision']);
                _0x3d81x150['push']('8:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.LOW_INT)['rangeMin']);
                _0x3d81x150['push']('9:' + _0x3d81xb0['getShaderPrecisionFormat'](_0x3d81xb0.FRAGMENT_SHADER, _0x3d81xb0.LOW_INT)['rangeMax']);
                _0x3d81x3d4 = _0x3d81x150['join']('\xA7');
                _0x3d81xae = document['createElement']('canvas');
                var _0x3d81x99 = _0x3d81xae['getContext']('webgl') || _0x3d81xae['getContext']('experimental-webgl');
                if (_0x3d81x99['getSupportedExtensions']()['indexOf']('WEBGL_debug_renderer_info') >= 0) {
                    _0x3d81x3d5 = _0x3d81x99['getParameter'](_0x3d81x99['getExtension']('WEBGL_debug_renderer_info').UNMASKED_VENDOR_WEBGL);
                    _0x3d81x3d6 = _0x3d81x99['getParameter'](_0x3d81x99['getExtension']('WEBGL_debug_renderer_info').UNMASKED_RENDERER_WEBGL)
                } else {
                    _0x3d81x3d5 = 'NA';
                    _0x3d81x3d6 = 'NA'
                }
            } catch (e) {
                _0x3d81x3d4 = 'NA';
                _0x3d81x3d5 = 'NA';
                _0x3d81x3d6 = 'NA'
            };
            return _0x3d81x3d4 + ',' + _0x3d81x3d5 + ',' + _0x3d81x3d6
        },
        gcp: function() {
            var _0x3d81xae = document['createElement']('canvas');
            var _0x3d81x99;
            try {
                _0x3d81x99 = _0x3d81xae['getContext']('2d')
            } catch (e) {
                return ''
            };
            var _0x3d81x157 = 'Jstris,mx <canvas> v0.39.0-X';
            _0x3d81x99['textBaseline'] = 'top';
            _0x3d81x99['font'] = '14px \'Arial\'';
            _0x3d81x99['textBaseline'] = 'alphabetic';
            _0x3d81x99['fillStyle'] = '#f60';
            _0x3d81x99['fillRect'](125, 1, 62, 20);
            _0x3d81x99['fillStyle'] = '#069';
            _0x3d81x99['fillText'](_0x3d81x157, 2, 15);
            _0x3d81x99['fillStyle'] = 'rgba(102, 204, 0, 0.7)';
            _0x3d81x99['fillText'](_0x3d81x157, 4, 17);
            return _0x3d81xae['toDataURL']()
        }
    };
    _0x3d81x3b1['_jstrisx'] = _0x3d81x3c4
})(window);

function EmoteSelect(_0x3d81x89, _0x3d81x145, _0x3d81x148, _0x3d81x3e3, _0x3d81x3e4, _0x3d81x146) {
    this['input'] = _0x3d81x89;
    this['emoteIndex'] = _0x3d81x145;
    this['container'] = _0x3d81x148;
    this['openBtn'] = _0x3d81x3e3;
    this['path'] = _0x3d81x3e4;
    this['groupEmotes'] = _0x3d81x146;
    this['init']()
}
EmoteSelect['prototype']['init'] = async function() {
    this['emoteElem'] = document['createElement']('div');
    this['emoteElem']['classList']['add']('emotePicker');
    this['container']['appendChild'](this['emoteElem']);
    this['comment'] = document['createComment']('Designed and developed by Erickmack');
    this['emoteElem']['appendChild'](this['comment']);
    this['initializeContainers']();
    this['emoteList'] = typeof this['emoteIndex'] === 'string' ? await fetch(this['emoteIndex'])['then']((_0x3d81x12) => {
        return _0x3d81x12['json']()
    }) : this['emoteIndex'];
    this['initializeEmotes']();
    this['lastUsed']();
    this['openButtonLogic']()
};
EmoteSelect['prototype']['initializeContainers'] = function() {
    this['searchElem'] = document['createElement']('form');
    this['searchElem']['classList']['add']('form-inline', 'emoteForm');
    this['emoteElem']['appendChild'](this['searchElem']);
    this['searchBar'] = document['createElement']('input');
    this['searchBar']['setAttribute']('autocomplete', 'off');
    this['searchBar']['classList']['add']('form-control');
    this['searchBar']['id'] = 'emoteSearch';
    this['searchBar']['addEventListener']('input', () => {
        this['searchFunction'](this['emoteList'])
    });
    this['searchElem']['addEventListener']('submit', (_0x3d81x2d) => {
        _0x3d81x2d['preventDefault']()
    });
    this['searchBar']['setAttribute']('type', 'text');
    this['searchBar']['setAttribute']('placeholder', 'Search Emotes');
    this['searchElem']['appendChild'](this['searchBar']);
    this['optionsContainer'] = document['createElement']('div');
    this['optionsContainer']['classList']['add']('optionsContainer');
    this['emoteElem']['appendChild'](this['optionsContainer']);
    this['emotesWrapper'] = document['createElement']('div');
    this['emotesWrapper']['classList']['add']('emotesWrapper');
    this['optionsContainer']['appendChild'](this['emotesWrapper'])
};
EmoteSelect['prototype']['initializeEmotes'] = function() {
    let _0x3d81x3e5 = [];
    this['emoteList']['forEach']((_0x3d81x3e6) => {
        let _0x3d81x13 = _0x3d81x3e5['findIndex']((_0x3d81x19) => {
            return _0x3d81x19 === _0x3d81x3e6['g']
        });
        if (_0x3d81x13 <= -1) {
            _0x3d81x3e5['push'](_0x3d81x3e6['g'])
        }
    });
    this['groupList'] = _0x3d81x3e5;
    this['createGroups'](this['groupList']);
    this['donateInfo'](this['groupList'])
};
EmoteSelect['prototype']['createGroups'] = async function(_0x3d81x3e5) {
    let _0x3d81x25 = this;
    this['emojis'] = this['emoteList'];
    let _0x3d81x3e7 = {
        root: document['querySelector']('.emotesWrapper'),
        rootMargin: '10px',
        threshold: 0
    };
    let _0x3d81x3e8 = new IntersectionObserver(_0x3d81x3ec, _0x3d81x3e7);
    this['groupsFragment'] = document['createDocumentFragment']();
    _0x3d81x3e5['forEach']((_0x3d81x3e9) => {
        let _0x3d81x3ea = this['emojis']['filter']((_0x3d81x3eb) => {
            return _0x3d81x3eb['g'] === `${''}${_0x3d81x3e9}${''}`
        });
        _0x3d81x25['groupDiv'] = document['createElement']('div');
        _0x3d81x25['groupDiv']['classList']['add']('emotesGroup');
        _0x3d81x25['groupDiv']['id'] = `${''}${_0x3d81x3e9}${''}`;
        _0x3d81x25['groupDiv']['setAttribute']('data-groupName', `${''}${_0x3d81x3e9}${''}`);
        _0x3d81x3e8['observe'](_0x3d81x25['groupDiv']);
        _0x3d81x25['groupName'] = document['createElement']('h3');
        _0x3d81x25['groupName']['id'] = `${''}${_0x3d81x3e9}${''}`;
        _0x3d81x25['groupName']['classList']['add']('groupName');
        _0x3d81x25['groupName']['innerText'] = `${''}${_0x3d81x3e9['toUpperCase']()}${''}`;
        _0x3d81x25['groupDiv']['appendChild'](_0x3d81x25['groupName']);
        let _0x3d81xa7 = Math['ceil'](_0x3d81x3ea['length'] / 6) * 45 + 35.4;
        _0x3d81x25['groupDiv']['style']['minHeight'] = `${''}${_0x3d81xa7}${'px'}`;
        _0x3d81x25['groupsFragment']['appendChild'](_0x3d81x25['groupDiv'])
    });
    this['emotesWrapper']['appendChild'](this['groupsFragment']);

    function _0x3d81x3ec(_0x3d81x3ed, _0x3d81x3e8) {
        setTimeout(() => {
            _0x3d81x3ed['forEach']((_0x3d81x3ee) => {
                if (_0x3d81x3ee['isIntersecting']) {
                    _0x3d81x25['createImages'](_0x3d81x25['emojis'], _0x3d81x3ee['target']);
                    _0x3d81x3e8['unobserve'](_0x3d81x3ee['target'])
                }
            })
        }, 200)
    }
    this['selectGroup']()
};
EmoteSelect['prototype']['donateInfo'] = function(_0x3d81x3e5) {
    if (_0x3d81x3e5['length'] > 2) {
        return
    };
    this['donateLink'] = document['createElement']('a');
    this['donateLink']['classList']['add']('mSkInf');
    this['donateLink']['id'] = 'mSkInf-s';
    this['donateLink']['setAttribute']('href', '/donate');
    this['icon'] = document['createElement']('i');
    this['icon']['classList']['add']('glyphicon', 'glyphicon-info-sign');
    this['span'] = document['createElement']('span');
    this['span']['innerText'] = '2k+ more emotes available to Jstris Supporters for $5';
    this['donateLink']['appendChild'](this['icon']);
    this['donateLink']['appendChild'](this['span']);
    this['donateLink']['style']['fontSize'] = 'clamp(1.5rem,1vw,3rem)';
    this['emotesWrapper']['appendChild'](this['donateLink'])
};
EmoteSelect['prototype']['createImages'] = async function(_0x3d81x35d, _0x3d81x3ef) {
    let _0x3d81x25 = this;
    let _0x3d81x3e7 = {
        root: document['getElementById']('searchResults'),
        rootMargin: '10px',
        threshold: 0
    };
    let _0x3d81x3e8 = new IntersectionObserver(_0x3d81x3ec, _0x3d81x3e7);
    this['emotesFragment'] = document['createDocumentFragment']();
    let _0x3d81x3e9 = _0x3d81x3ef['getAttribute']('data-groupName');
    let _0x3d81x3ea = _0x3d81x35d['filter']((_0x3d81x3e6) => {
        return _0x3d81x3e6['g'] === `${''}${_0x3d81x3e9}${''}`
    });
    for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x3ea['length']; _0x3d81x13++) {
        _0x3d81x25['emoteImg'] = document['createElement']('img');
        let _0x3d81x3f0;
        if (_0x3d81x3ea[_0x3d81x13]['p']) {
            _0x3d81x3f0 = _0x3d81x3ea[_0x3d81x13]['p']
        } else {
            if (_0x3d81x3ea[_0x3d81x13]['u']) {
                _0x3d81x3f0 = _0x3d81x3ea[_0x3d81x13]['u']
            } else {
                _0x3d81x3f0 = `${''}${this['path']}${''}${_0x3d81x3ea[_0x3d81x13]['n']}${'.svg'}`
            }
        };
        _0x3d81x25['emoteImg']['classList']['add']('emoteImg', 'loadingEmote');
        if (_0x3d81x3ea[_0x3d81x13]['u']) {
            _0x3d81x25['emoteImg']['classList']['add']('jstrisEmote')
        };
        _0x3d81x25['emoteImg']['onload'] = function(_0x3d81x2d) {
            _0x3d81x2d['target']['classList']['remove']('loadingEmote')
        };
        _0x3d81x3e8['observe'](_0x3d81x25['emoteImg']);
        _0x3d81x25['emoteImg']['setAttribute']('data-emoteName', `${''}${_0x3d81x3ea[_0x3d81x13]['n']}${''}`);
        _0x3d81x25['emoteImg']['setAttribute']('data-source', _0x3d81x3f0);
        _0x3d81x25['emoteImg']['addEventListener']('click', (_0x3d81x2d) => {
            this['chatEmote'](_0x3d81x2d['target']);
            this['setStoredEmotes'](_0x3d81x2d['target']);
            let _0x3d81x3f1 = _0x3d81x2d['shiftKey'];
            if (!_0x3d81x3f1) {
                this['hideElem']()
            }
        });
        _0x3d81x25['emoteImg']['addEventListener']('mouseover', (_0x3d81x2d) => {
            this['showName'](_0x3d81x2d['target'])
        });
        _0x3d81x25['emotesFragment']['appendChild'](_0x3d81x25['emoteImg'])
    };
    _0x3d81x3ef['appendChild'](this['emotesFragment']);

    function _0x3d81x3ec(_0x3d81x3ed, _0x3d81x3e8) {
        _0x3d81x25['setSource'](_0x3d81x3ed, _0x3d81x3e8)
    }
};
EmoteSelect['prototype']['selectGroup'] = function() {
    this['selectionDiv'] = document['createElement']('div');
    this['selectionDiv']['id'] = 'selectionDiv';
    this['groupList']['forEach']((_0x3d81x3e9) => {
        this['groupImage'] = document['createElement']('img');
        this['groupImage']['classList']['add']('groupLink');
        this['groupImage']['setAttribute']('data-groupName', `${''}${_0x3d81x3e9}${''}`);
        this['groupImage']['addEventListener']('click', (_0x3d81x2d) => {
            let _0x3d81x3f2 = _0x3d81x2d['target']['getAttribute']('data-groupname');
            let _0x3d81x86 = document['getElementById'](_0x3d81x3f2);
            let _0x3d81x3f3 = this['searchElem']['clientHeight'];
            let _0x3d81x3f4 = _0x3d81x86['offsetTop'] - _0x3d81x3f3;
            this['emotesWrapper']['scrollTop'] = _0x3d81x3f4
        });
        this['groupImage']['setAttribute']('title', `${''}${_0x3d81x3e9}${''}`);
        this['groupImage']['setAttribute']('data-toggle', 'tooltip');
        this['groupImage']['setAttribute']('data-placement', 'right');
        let _0x3d81x3f5 = this['emoteList']['filter']((_0x3d81x3e6) => {
            return _0x3d81x3e6['n'] === this['groupEmotes'][_0x3d81x3e9]
        });
        if (_0x3d81x3f5['length'] <= 0) {
            this['groupImage']['setAttribute']('src', `${''}${this['groupEmotes'][_0x3d81x3e9]}${''}`);
            this['groupImage']['classList']['add']('jstrisSelector')
        } else {
            if (!_0x3d81x3f5['u']) {
                this['groupImage']['setAttribute']('src', `${''}${this['path']}${''}${this['groupEmotes'][_0x3d81x3e9]}${'.svg'}`)
            }
        };
        this['selectionDiv']['appendChild'](this['groupImage'])
    });
    this['optionsContainer']['appendChild'](this['selectionDiv']);
    $('[data-toggle="tooltip"]')['tooltip']()
};
EmoteSelect['prototype']['showName'] = function(_0x3d81x3ef) {
    let _0x3d81x3f6 = _0x3d81x3ef['getAttribute']('data-emoteName');
    let _0x3d81x3f7 = document['getElementById']('emoteSearch');
    _0x3d81x3f7['setAttribute']('placeholder', `${':'}${_0x3d81x3f6}${':'}`)
};
EmoteSelect['prototype']['searchFunction'] = function(_0x3d81x3f8) {
    let _0x3d81x25 = this;
    let _0x3d81xce = {
        threshold: 0.3,
        keys: [{
            name: 'n',
            weight: 2
        }, {
            name: 't',
            weight: 1
        }]
    };
    let _0x3d81x3f9 = document['getElementById']('emoteSearch')['value'];
    let _0x3d81x3fa = document['getElementById']('searchResults');
    if (!_0x3d81x3f9 && _0x3d81x3fa != null) {
        _0x3d81x3fa['parentNode']['removeChild'](_0x3d81x3fa)
    };
    const _0x3d81x3fb = new Fuse(_0x3d81x3f8, _0x3d81xce);
    let _0x3d81x211 = _0x3d81x3fb['search'](_0x3d81x3f9);
    if (!_0x3d81x3fa) {
        this['searchResults'] = document['createElement']('div');
        this['searchResults']['id'] = 'searchResults';
        document['getElementsByClassName']('emotePicker')[0]['appendChild'](this['searchResults']);
        _0x3d81x3fa = document['getElementById']('searchResults')
    } else {
        if (_0x3d81x3fa) {
            _0x3d81x3fa['innerHTML'] = ''
        }
    };
    let _0x3d81x3e7 = {
        root: document['getElementById']('searchResults'),
        rootMargin: '10px',
        threshold: 0
    };
    let _0x3d81x3e8 = new IntersectionObserver(_0x3d81x3ec, _0x3d81x3e7);
    _0x3d81x25['resultsFragment'] = document['createDocumentFragment']();
    for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x211['length']; _0x3d81x13++) {
        let _0x3d81x150 = _0x3d81x211[_0x3d81x13]['item'];
        let _0x3d81x3f0;
        if (_0x3d81x150['p']) {
            _0x3d81x3f0 = _0x3d81x150['p']
        } else {
            if (_0x3d81x150['u']) {
                _0x3d81x3f0 = _0x3d81x150['u']
            } else {
                _0x3d81x3f0 = `${''}${this['path']}${''}${_0x3d81x150['n']}${'.svg'}`
            }
        };
        _0x3d81x25['emoteResult'] = document['createElement']('img');
        _0x3d81x25['emoteResult']['classList']['add']('emoteImg', 'loadingEmote', 'resultImg');
        _0x3d81x25['emoteResult']['setAttribute']('data-source', _0x3d81x3f0);
        _0x3d81x25['emoteResult']['onload'] = function(_0x3d81x2d) {
            _0x3d81x2d['target']['classList']['remove']('loadingEmote')
        };
        _0x3d81x25['emoteResult']['setAttribute']('title', _0x3d81x150['n']);
        _0x3d81x25['emoteResult']['setAttribute']('data-emoteName', _0x3d81x150['n']);
        _0x3d81x25['emoteResult']['addEventListener']('click', (_0x3d81x2d) => {
            this['chatEmote'](_0x3d81x2d['target']);
            this['setStoredEmotes'](_0x3d81x2d['target']);
            let _0x3d81x3f1 = _0x3d81x2d['shiftKey'];
            if (!_0x3d81x3f1) {
                this['hideElem']()
            }
        });
        _0x3d81x3e8['observe'](this['emoteResult']);
        _0x3d81x25['resultsFragment']['appendChild'](this['emoteResult'])
    };
    _0x3d81x3fa['appendChild'](this['resultsFragment']);

    function _0x3d81x3ec(_0x3d81x3ed, _0x3d81x3e8) {
        _0x3d81x25['setSource'](_0x3d81x3ed, _0x3d81x3e8)
    }
};
EmoteSelect['prototype']['setSource'] = function(_0x3d81x3ed, _0x3d81x3e8) {
    setTimeout(() => {
        _0x3d81x3ed['forEach']((_0x3d81x3ee) => {
            if (_0x3d81x3ee['isIntersecting']) {
                let _0x3d81x3f0 = _0x3d81x3ee['target']['getAttribute']('data-source');
                _0x3d81x3ee['target']['setAttribute']('src', _0x3d81x3f0);
                _0x3d81x3e8['unobserve'](_0x3d81x3ee['target'])
            }
        })
    }, 400)
};
EmoteSelect['prototype']['chatEmote'] = function(_0x3d81x3ef) {
    let _0x3d81x3f6 = _0x3d81x3ef['getAttribute']('data-emoteName');
    let _0x3d81x36b = this['input']['value'];
    let _0x3d81x232 = this['getCaretPosition']();
    this['input']['value'] = _0x3d81x36b['substring'](0, _0x3d81x232) + `${':'}${_0x3d81x3f6}${': '}` + _0x3d81x36b['substring'](_0x3d81x232, _0x3d81x36b['length']);
    this['input']['focus']();
    this['setCaretPosition'](_0x3d81x232 + _0x3d81x3f6['length'] + 3)
};
EmoteSelect['prototype']['getCaretPosition'] = function() {
    if (this['input']['selectionStart'] || this['input']['selectionStart'] == '0') {
        return this['input']['selectionStart']
    } else {
        return this['input']['value']['length']
    }
};
EmoteSelect['prototype']['setCaretPosition'] = function(_0x3d81x232) {
    _0x3d81x232 = Math['max'](Math['min'](_0x3d81x232, this['input']['value']['length']), 0);
    if (this['input']['setSelectionRange']) {
        this['input']['setSelectionRange'](_0x3d81x232, _0x3d81x232)
    }
};
EmoteSelect['prototype']['lastUsed'] = function() {
    if (typeof Storage !== 'undefined') {
        if (!localStorage['lastUsed']) {
            let _0x3d81x341 = Math['floor'](Date['now']() / 1000);
            let _0x3d81x35d = [{
                Badger: _0x3d81x341
            }, {
                jstris: _0x3d81x341
            }];
            localStorage['setItem']('lastUsed', JSON['stringify'](_0x3d81x35d))
        }
    };
    let _0x3d81x22e = this['emotesWrapper'];
    let _0x3d81x3fc = document['getElementById']('Jstris');
    this['recent'] = document['createElement']('div');
    this['recent']['classList']['add']('emotesGroup');
    this['groupName'] = document['createElement']('h3');
    this['groupName']['classList']['add']('groupName');
    this['lastUsedWrapper'] = document['createElement']('div');
    this['lastUsedWrapper']['id'] = 'usedWrapper';
    this['groupName']['id'] = 'recently-used';
    this['groupName']['innerText'] = 'RECENTLY USED';
    this['recent']['appendChild'](this['groupName']);
    this['recent']['appendChild'](this['lastUsedWrapper']);
    _0x3d81x22e['insertBefore'](this['recent'], _0x3d81x3fc);
    let _0x3d81x3fd = this['selectionDiv'];
    let _0x3d81x17a = document['getElementsByClassName']('groupLink')[0];
    this['groupLink'] = document['createElement']('img');
    this['groupLink']['classList']['add']('groupLink');
    this['groupLink']['setAttribute']('data-groupName', 'recently-used');
    this['groupLink']['setAttribute']('title', 'Recently used');
    this['groupLink']['setAttribute']('data-toggle', 'tooltip');
    this['groupLink']['setAttribute']('data-placement', 'right');
    this['groupLink']['setAttribute']('src', `${''}${this['path']}${'three_oclock.svg'}`);
    this['groupLink']['addEventListener']('click', (_0x3d81x2d) => {
        let _0x3d81x3e9 = _0x3d81x2d['target']['getAttribute']('data-groupname');
        let _0x3d81x86 = document['getElementById'](_0x3d81x3e9);
        let _0x3d81x3f4 = _0x3d81x86['offsetTop'] - 60;
        this['emotesWrapper']['scrollTop'] = _0x3d81x3f4
    });
    _0x3d81x3fd['insertBefore'](this['groupLink'], _0x3d81x17a);
    $('[data-toggle="tooltip"]')['tooltip']()
};
EmoteSelect['prototype']['updateLastUsed'] = function() {
    let _0x3d81x3fe = document['getElementById']('usedWrapper');
    _0x3d81x3fe['innerHTML'] = '';
    let _0x3d81x3ff = this['emoteList'];
    let _0x3d81x228 = JSON['parse'](localStorage['getItem']('lastUsed'));
    let _0x3d81x400 = document['createDocumentFragment']();
    let _0x3d81x3f9;
    let _0x3d81x3f0;
    _0x3d81x228['forEach']((_0x3d81x3e6) => {
        _0x3d81x3f9 = Object['keys'](_0x3d81x3e6)[0];
        let _0x3d81x150 = _0x3d81x3ff['filter']((_0x3d81x3e6) => {
            return _0x3d81x3e6['n'] === _0x3d81x3f9
        })[0];
        if (_0x3d81x150) {
            if (_0x3d81x150['p']) {
                _0x3d81x3f0 = _0x3d81x150['p']
            } else {
                if (_0x3d81x150['u']) {
                    _0x3d81x3f0 = _0x3d81x150['u']
                } else {
                    _0x3d81x3f0 = `${''}${this['path']}${''}${_0x3d81x150['n']}${'.svg'}`
                }
            };
            this['usedImage'] = document['createElement']('img');
            this['usedImage']['setAttribute']('src', _0x3d81x3f0);
            this['usedImage']['setAttribute']('data-emoteName', _0x3d81x150['n']);
            this['usedImage']['classList']['add']('emoteImg');
            if (_0x3d81x150['u']) {
                this['usedImage']['classList']['add']('jstrisEmote')
            };
            this['usedImage']['addEventListener']('click', (_0x3d81x2d) => {
                this['chatEmote'](_0x3d81x2d['target']);
                this['setStoredEmotes'](_0x3d81x2d['target']);
                let _0x3d81x3f1 = _0x3d81x2d['shiftKey'];
                if (!_0x3d81x3f1) {
                    this['hideElem']()
                }
            });
            this['usedImage']['addEventListener']('mouseover', (_0x3d81x2d) => {
                this['showName'](_0x3d81x2d['target'])
            });
            _0x3d81x400['appendChild'](this['usedImage'])
        }
    });
    _0x3d81x3fe['appendChild'](_0x3d81x400)
};
EmoteSelect['prototype']['setStoredEmotes'] = function(_0x3d81x3ef) {
    const _0x3d81x401 = 24;
    let _0x3d81x35d = JSON['parse'](localStorage['getItem']('lastUsed'));
    let _0x3d81x402;
    let _0x3d81x3f6 = _0x3d81x3ef['getAttribute']('data-emoteName');
    if (_0x3d81x35d['length'] > _0x3d81x401) {
        _0x3d81x402 = [];
        for (let _0x3d81x13 = 0; _0x3d81x13 < _0x3d81x401; _0x3d81x13++) {
            _0x3d81x402['push'](_0x3d81x35d[_0x3d81x13])
        }
    };
    if (_0x3d81x35d['length'] === _0x3d81x401) {
        let _0x3d81x403 = false;
        let _0x3d81x13 = 0;
        for (let _0x3d81x3e6 of _0x3d81x35d) {
            _0x3d81x13 += 1;
            if (_0x3d81x3f6 in _0x3d81x3e6) {
                _0x3d81x402 = _0x3d81x35d['filter']((_0x3d81x3e6) => {
                    return _0x3d81x3e6[_0x3d81x3f6] === _0x3d81x35d[_0x3d81x13][_0x3d81x3f6]
                });
                let _0x3d81x404 = {
                    [_0x3d81x3f6]: Math['floor'](Date['now']() / 1000)
                };
                _0x3d81x402['push'](_0x3d81x404);
                _0x3d81x403 = true;
                break
            }
        };
        if (!_0x3d81x403) {
            let _0x3d81x405 = _0x3d81x35d[0][Object['keys'](_0x3d81x35d[0])[0]];
            let _0x3d81x406 = _0x3d81x35d[0];
            for (let _0x3d81x3e6 of _0x3d81x35d) {
                let _0x3d81x14 = Object['keys'](_0x3d81x3e6)[0];
                if (_0x3d81x3e6[_0x3d81x14] < _0x3d81x405) {
                    _0x3d81x405 = _0x3d81x3e6[_0x3d81x14];
                    _0x3d81x406 = _0x3d81x3e6
                }
            };
            _0x3d81x402 = _0x3d81x35d['filter']((_0x3d81x3e6) => {
                return _0x3d81x3e6 !== _0x3d81x406
            });
            let _0x3d81x404 = {
                [_0x3d81x3f6]: Math['floor'](Date['now']() / 1000)
            };
            _0x3d81x402['push'](_0x3d81x404)
        }
    } else {
        if (_0x3d81x35d['length'] < _0x3d81x401) {
            let _0x3d81x403 = false;
            let _0x3d81x13 = 0;
            for (let _0x3d81x3e6 of _0x3d81x35d) {
                if (_0x3d81x3f6 in _0x3d81x3e6) {
                    _0x3d81x402 = _0x3d81x35d['filter']((_0x3d81x3e6) => {
                        return _0x3d81x3e6[_0x3d81x3f6] !== _0x3d81x35d[_0x3d81x13][_0x3d81x3f6]
                    });
                    let _0x3d81x404 = {
                        [_0x3d81x3f6]: Math['floor'](Date['now']() / 1000)
                    };
                    _0x3d81x402['push'](_0x3d81x404);
                    _0x3d81x403 = true;
                    break
                };
                _0x3d81x13 += 1
            };
            if (!_0x3d81x403) {
                let _0x3d81x404 = {
                    [_0x3d81x3f6]: Math['floor'](Date['now']() / 1000)
                };
                _0x3d81x402 = _0x3d81x35d;
                _0x3d81x402['push'](_0x3d81x404)
            }
        }
    };
    let _0x3d81x407 = _0x3d81x402['sort']((_0x3d81xa5, _0x3d81x141) => {
        return _0x3d81x141[Object['keys'](_0x3d81x141)[0]] - _0x3d81xa5[Object['keys'](_0x3d81xa5)[0]]
    });
    localStorage['lastUsed'] = JSON['stringify'](_0x3d81x407)
};
EmoteSelect['prototype']['hideElem'] = function() {
    this['emotesWrapper']['scrollTo'](0, 0);
    this['selectionDiv']['scrollTo'](0, 0);
    this['emoteElem']['classList']['toggle']('open')
};
EmoteSelect['prototype']['openButtonLogic'] = function() {
    let _0x3d81x3f7 = document['getElementById']('emoteSearch');
    this['openBtn']['addEventListener']('click', () => {
        _0x3d81x3f7['value'] = '';
        let _0x3d81x408 = document['getElementById']('searchResults');
        if (_0x3d81x408 !== null) {
            _0x3d81x408['parentNode']['removeChild'](_0x3d81x408)
        };
        this['emotesWrapper']['scrollTo'](0, 0);
        this['selectionDiv']['scrollTo'](0, 0);
        this['updateLastUsed']();
        this['emoteElem']['classList']['toggle']('open');
        this['emoteElem']['classList']['contains']('open') ? _0x3d81x3f7['focus']() : document['getElementById']('chatInput')['focus']()
    });
    if (!document['getElementById']('fuseScript')) {
        let _0x3d81x409 = document['createElement']('script');
        _0x3d81x409['id'] = 'fuseScript';
        _0x3d81x409['src'] = 'https://cdn.jsdelivr.net/npm/fuse.js@6.4.3';
        document['head']['appendChild'](_0x3d81x409)
    }
}