// ==UserScript==
// @name         TETR.IO Theme
// @namespace    http://tampermonkey.net/
// @version      0.18
// @description  TETR.IO Theme for Jstris
// @author       Eddie, NueSB, Oki, OSK
// @match        https://*.jstris.jezevec10.com/*
// @grant        none
// ==/UserScript==

(function () {
    Game['prototype']['drawBgGrid'] = function (_0x3d81xb) {
        _0x3d81xb = (typeof _0x3d81xb === 'undefined') ? 1 : _0x3d81xb;
        this['bgctx']['rect'](0, 0, this['canvas']['width'], this['canvas']['height']);
        this['bgctx']['fillStyle'] = '#00000088';
        this['bgctx']['fill']();
        this['bgctx']['beginPath']();
        this['bgctx']['lineWidth'] = 1;
        if (_0x3d81xb === 1) {
            for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
            };
            for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
                this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
                this['bgctx']['lineTo'](241, _0x3d81x13 * this['block_size'] + 0.5)
            };
            this['bgctx']['strokeStyle'] = '#50500';
            this['bgctx']['stroke']();
            this['bgctx']['beginPath']();
            for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
                for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
                    this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5);
                    this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5)
                }
            };
            for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
                for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
                    this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] + 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
                    this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] + 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
                }
            };
            this['bgctx']['strokeStyle'] = '#202020';
            this['bgctx']['stroke']();
            this['bgctx']['beginPath']()
        } else {
            if (_0x3d81xb === 2) {
                for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
                    for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
                        this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] - 0.5);
                        this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] - 0.5)
                    }
                };
                for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
                    for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
                        this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] - 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
                        this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] - 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
                    }
                }
            } else {
                if (_0x3d81xb === 3) {
                    for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                        this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                        this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
                    }
                } else {
                    if (_0x3d81xb === 4) {
                        for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                            this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                            this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
                        };
                        for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
                            this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
                            this['bgctx']['lineTo'](241, _0x3d81x13 * this['block_size'] + 0.5)
                        }
                    }
                }
            }
        };
        this['bgctx']['strokeStyle'] = '#696969';
        this['bgctx']['stroke']();
        this['bgctx']['strokeStyle'] = '#ffffff';
        this['bgctx']['lineWidth'] = 2;
        this['bgctx']['strokeRect'](1, 1, 240, this['canvas']['height'] - 2);
        this['bgctx']['strokeRect'](1, 1, 246, this['canvas']['height'] - 2);
    };
    WebGLView['prototype']['redrawRedBar'] = function (_0x3d81xa4) {
        if (!_0x3d81xa4 && !this['g']['redBar']) {
            return
        };
        var _0x3d81x99 = this['ctxs'][this['MAIN']],
            _0x3d81xcd = _0x3d81x99['textureInfos'][2];
        if (_0x3d81xa4) {
            _0x3d81x99['gl']['clear'](_0x3d81x99['gl'].COLOR_BUFFER_BIT);
            this['g']['redrawMatrix']();
            this['g']['drawGhostAndCurrent']()
        };
        this['drawImage'](_0x3d81x99, _0x3d81xcd['texture'], _0x3d81xcd['width'], _0x3d81xcd['height'], 0, 0, 1, 1, 240 + 2, (20 - this['g']['redBar']) * this['g']['block_size'] - 1, 4, this['g']['redBar'] * this['g']['block_size'] - 1)
    };
    window.addEventListener('load', function () {
         //Jstris Block Skin Change
         loadSkin("https://i.imgur.com/XcnWlC6.png", 30);
         loadGhostSkin("https://i.imgur.com/f7kvOMi.png", 30);
        //Jstris Custom Background Image
        document.head.getElementsByTagName("style")[0].innerHTML = "";
        document.body.style.backgroundImage = "url(https://tetr.io/res/bg/1.jpg)";
        document.body.style.backgroundSize = "100%";
        document.getElementById("app").style.backgroundColor = "rgba(0, 0, 0, 0)";
    });
})();