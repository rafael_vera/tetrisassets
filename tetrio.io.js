// ==UserScript==
// @name         TETR.IO Theme
// @namespace    http://tampermonkey.net/
// @version      0.18
// @description  TETR.IO Theme for Jstris
// @author       Eddie, NueSB, Oki, OSK
// @match        https://*.jstris.jezevec10.com/*
// @grant        none
// ==/UserScript==

(function () {
    Game['prototype']['drawBgGrid'] = function (_0x3d81xb) {
        _0x3d81xb = (typeof _0x3d81xb === 'undefined') ? 1 : _0x3d81xb;
        this['bgctx']['rect'](0, 0, this['canvas']['width'], this['canvas']['height']);
        this['bgctx']['fillStyle'] = '#00000088';
        this['bgctx']['fill']();
        this['bgctx']['beginPath']();
        this['bgctx']['lineWidth'] = 1;
        if (_0x3d81xb === 1) {
            for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
            };
            for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
                this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
                this['bgctx']['lineTo'](241, _0x3d81x13 * this['block_size'] + 0.5)
            };
            this['bgctx']['strokeStyle'] = '#50500';
            this['bgctx']['stroke']();
            this['bgctx']['beginPath']();
            for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
                for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
                    this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5);
                    this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] + 0.5)
                }
            };
            for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
                for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
                    this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] + 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
                    this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] + 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
                }
            };
            this['bgctx']['strokeStyle'] = '#202020';
            this['bgctx']['stroke']();
            this['bgctx']['beginPath']()
        } else {
            if (_0x3d81xb === 2) {
                for (var _0x3d81x13 = 0; _0x3d81x13 < 9; _0x3d81x13++) {
                    for (var _0x3d81x18 = 1; _0x3d81x18 < 20; _0x3d81x18++) {
                        this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.75 * this['block_size'], _0x3d81x18 * this['block_size'] - 0.5);
                        this['bgctx']['lineTo']((_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'], _0x3d81x18 * this['block_size'] - 0.5)
                    }
                };
                for (var _0x3d81x13 = 0; _0x3d81x13 < 19; _0x3d81x13++) {
                    for (var _0x3d81x19 = 1; _0x3d81x19 < 10; _0x3d81x19++) {
                        this['bgctx']['moveTo'](_0x3d81x19 * this['block_size'] - 0.5, _0x3d81x13 * this['block_size'] + 0.75 * this['block_size']);
                        this['bgctx']['lineTo'](_0x3d81x19 * this['block_size'] - 0.5, (_0x3d81x13 + 1) * this['block_size'] + 0.20 * this['block_size'])
                    }
                }
            } else {
                if (_0x3d81xb === 3) {
                    for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                        this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                        this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
                    }
                } else {
                    if (_0x3d81xb === 4) {
                        for (var _0x3d81x13 = 1; _0x3d81x13 < 10; _0x3d81x13++) {
                            this['bgctx']['moveTo'](_0x3d81x13 * this['block_size'] + 0.5, 0);
                            this['bgctx']['lineTo'](_0x3d81x13 * this['block_size'] + 0.5, this['canvas']['height'])
                        };
                        for (var _0x3d81x13 = 1; _0x3d81x13 < 20; _0x3d81x13++) {
                            this['bgctx']['moveTo'](0, _0x3d81x13 * this['block_size'] + 0.5);
                            this['bgctx']['lineTo'](241, _0x3d81x13 * this['block_size'] + 0.5)
                        }
                    }
                }
            }
        };
        this['bgctx']['strokeStyle'] = '#696969';
        this['bgctx']['stroke']();
        this['bgctx']['strokeStyle'] = '#ffffff';
        this['bgctx']['lineWidth'] = 2;
        this['bgctx']['strokeRect'](1, 1, 240, this['canvas']['height'] - 2);
        this['bgctx']['strokeRect'](1, 1, 246, this['canvas']['height'] - 2);
    };
    WebGLView['prototype']['redrawRedBar'] = function(_0x3d81xa4) {
        if (!_0x3d81xa4 && !this['g']['redBar']) {
            return
        };
        var _0x3d81x99 = this['ctxs'][this['MAIN']],
            _0x3d81xcd = _0x3d81x99['textureInfos'][2];
        if (_0x3d81xa4) {
            _0x3d81x99['gl']['clear'](_0x3d81x99['gl'].COLOR_BUFFER_BIT);
            this['g']['redrawMatrix']();
            this['g']['drawGhostAndCurrent']()
        };
        this['drawImage'](_0x3d81x99, _0x3d81xcd['texture'], _0x3d81xcd['width'], _0x3d81xcd['height'], 0, 0, 1, 1, 240 + 2, (20 - this['g']['redBar']) * this['g']['block_size'] - 1, 4, this['g']['redBar'] * this['g']['block_size'] - 1)
    };


    function CustomSFXset() {
        this.volume = 1;
        this.lock = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_lock.mp3",
            abs: 1
        };
        this.ready = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/goodluckyready.mp3",
            abs: 1,
            set: 1
        };
        this.go = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/goodluckygo.mp3",
            abs: 1,
            set: 0
        };
        this.died = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_died.mp3",
            abs: 1,
            set: 1
        };
        this.hold = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_hold.mp3",
            abs: 1,
            set: 0
        };
        this.move = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_move.mp3",
            abs: 1,
            set: 0
        };
        this.linefall = {
            url: "blank.wav",
            abs: 1,
            set: 0
        };
        this.comboTones = {
            url: "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_combo.mp3",
            abs: 1,
            set: 2,
            duration: 1000,
            spacing: 500,
            cnt: 17
        };
    };

    window.addEventListener('load', function () {

            //Next Piece Sounds
            var trim = a => {
                a = a.slice(0, -1);
                a = a.substr(a.indexOf("{") + 1);
                return a
            }
            Game['pieceSoundsTGM'] = [];
            var srcs = [
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_imino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_omino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_tmino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_lmino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_jmino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_smino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_zmino.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/goodlucky_idk.wav"
            ];

            Game['playSoundTGM'] = function (s) {
                if (!s.paused && s.currentTime > 0) {
                    s.currentTime = 0;
                } else s.play();
            }

            function a(s, b) {
                for (var i = 0; i < b.length; i++) {
                    s.push(document.createElement("audio"));
                    s[i].src = b[i];
                    s[i].volume = 0.43;
                }
            }
            a(Game['pieceSoundsTGM'], srcs);


            var uqbFunc = Game['prototype']['updateQueueBox'].toString()
            uqbFunc = "Game['playSoundTGM'](Game['pieceSoundsTGM'][this.queue[0].id]);" + trim(uqbFunc)
            Game['prototype']['updateQueueBox'] = new Function(uqbFunc);

            //Jstris SFX
            CustomSFXset.prototype = new BaseSFXset;
            loadSFX(new CustomSFXset);

            //Jstris Block Skin Change
            loadSkin("https://i.imgur.com/XcnWlC6.png", 30);
            loadGhostSkin("https://i.imgur.com/f7kvOMi.png", 30);

            //Jstris Custom Background Image
            document.head.getElementsByTagName("style")[0].innerHTML = "";
            document.body.style.backgroundImage = "url(https://tetr.io/res/bg/1.jpg)";
            document.body.style.backgroundSize = "100%";
            document.getElementById("app").style.backgroundColor = "rgba(0, 0, 0, 0)";


            /**************************
              Rotation Sounds Script
            **************************/

            Game['rotationSounds'] = [
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_rotate.mp3", //rotate left
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_rotate.mp3", //rotate right
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_rotate.mp3" //rotate 180°
            ];

            Game['rotationVolumes'] = [1, 1, 1]

            localStorage.evVol = localStorage.evVol || "100"

            Game["rArray"] = [];
            Game["rotationSounds"].map((x, i) => {
                if (Game['rotationSounds'][i]) {
                    Game["rArray"].push(document.createElement("audio"));
                    Game["rArray"][i].src = x
                } else {
                    Game["rArray"].push(null)
                }
            })


            window.playRotSound = function (S) {
                s = Game.rArray[S];
                !s.paused && 0 < s.currentTime ? s.currentTime = 0 : (s.volume = Game['rotationVolumes'][S] * localStorage.evVol / 100, s.play())
            }


            var trim = a => {
                a = a.slice(0, -1);
                a = a.substr(a.indexOf("{") + 1);
                return a
            }
            var getParams = a => {
                var params = a.slice(a.indexOf("(") + 1);
                params = params.substr(0, params.indexOf(")")).split(",");
                return params
            }

            var rotFunc = Game['prototype']['rotateCurrentBlock'].toString()
            var rotParams = getParams(rotFunc)

            var rotInsert = 'var rotPos=[0,0,1,2][' + rotParams[0] + '+1];console.log(rotPos);Game.rArray[rotPos]&&playRotSound(rotPos);'

            rotFunc = rotInsert + trim(rotFunc)

            Game['prototype']['rotateCurrentBlock'] = new Function(...rotParams, rotFunc);

            /**************************
       Songs Script
**************************/

            var trim = a => {
                a = a.slice(0, -1);
                a = a.substr(a.indexOf("{") + 1);
                return a
            }
            var getParams = a => {
                var params = a.slice(a.indexOf("(") + 1);
                params = params.substr(0, params.indexOf(")")).split(",");
                return params
            }


            localStorage.musicVol = localStorage.musicVol || "100";
            localStorage.SPvol = localStorage.SPvol || "100";

            var musicVol = document.createElement("tr");
            musicVol.innerHTML = 'Music vol (MP):&nbsp;<input id="volControl" oninput="Game.setVol(volControl.value,1)" type="range" min="0" max="100" value="' + localStorage.musicVol + '" step="1" style="width:150px;display:inline-block;padding-top:9px">&nbsp;&nbsp;<span id="volSetting">' + localStorage.musicVol + '%</span>'
            tab_sound.appendChild(musicVol);

            var spVol = document.createElement("tr");
            spVol.innerHTML = 'Music vol (SP):&nbsp;<input id="volControl2" oninput="Game.setVol(volControl2.value,0)" type="range" min="0" max="100" value="' + localStorage.SPvol + '" step="1" style="width:150px;display:inline-block;padding-top:9px">&nbsp;&nbsp;<span id="volSetting2">' + localStorage.SPvol + '%</span>'
            tab_sound.appendChild(spVol);



            Game["setVol"] = function (vol, mode) {
                if (mode) {
                    localStorage.musicVol = vol
                    volSetting.innerHTML = vol + '%';
                    var musicVol = document.getElementById("volControl")
                    Game["songs"].map(x => {
                        x.volume = (musicVol.value / 100);
                    })
                } else {
                    localStorage.SPvol = vol
                    volSetting2.innerHTML = vol + '%';
                    var spVol = document.getElementById("volControl2")
                    Game["song"].volume = spVol.value / 100
                }
            }


            if (typeof Game != "undefined") {


                var songsMP = [
                    "https://ecdldaiiere.github.io/Eddiez-Soundz/hangingoutintokyo.mp3",
                    "https://ecdldaiiere.github.io/Eddiez-Soundz/hangingoutintokyo.mp3",
                    "https://ecdldaiiere.github.io/Eddiez-Soundz/hangingoutintokyo.mp3"
                ]

                var songSP = "https://ecdldaiiere.github.io/Eddiez-Soundz/piercingwind.mp3"

                //Play song when only X*100% of players are left (first one is always 1)
                var songThresholds = [1, 0.6, 0.2]

                Game['onlySprint'] = false;


                window.playSong = function (s) {
                    Game["songs"].map(x => {
                        x.pause();
                        x.currentTime = 0;
                    })
                    Game["song"].pause();
                    Game["song"].currentTime = 0;

                    if (s != undefined) {
                        !s.paused && 0 < s.currentTime ? s.currentTime = 0 : s.play()
                    }
                }

                Game["songs"] = [];
                Game["maxPlayers"] = 0
                Game["songIndex"] = -1


                Game["song"] = document.createElement("audio");
                Game["song"].src = songSP;
                Game["song"].loop = true;
                Game["song"].volume = 1;

                songsMP.map((x, i) => {
                    Game["songs"].push(document.createElement("audio"));
                    Game["songs"][i].src = x;
                    Game["songs"][i].loop = true;
                    Game["songs"][i].volume = 1;
                })

                Game["setVol"](localStorage.musicVol, 1)
                Game["setVol"](localStorage.SPvol, 0)


                Game["updateSong"] = function (i) {

                    if (i < 0) {
                        Game["maxPlayers"] = 0
                        playSong()
                        Game["songIndex"] = -1
                    }

                    if (i == 0) {
                        Game["maxPlayers"] = -1
                        playSong(Game["songs"][0])
                        Game["songIndex"] = 0
                    }

                    if (typeof i == "string") {
                        if (Game["maxPlayers"] < 0) {
                            Game["maxPlayers"] = parseInt(i)
                        }
                        var alivePercent = (parseInt(i) - 1) / Game["maxPlayers"]
                        if (alivePercent <= songThresholds[Game["songIndex"] + 1]) {
                            console.log(Game["songIndex"])
                            Game["songIndex"]++
                            playSong(Game["songs"][Game["songIndex"]])
                        }
                    }

                }

                var gameOver99 = Game['prototype']['GameOver'].toString();
                gameOver99 = "Game['updateSong'](-1);Game['song'].pause();Game['song'].currentTime=0;" + trim(gameOver99)
                Game['prototype']['GameOver'] = new Function(gameOver99)

                var printSlot99 = SlotView['prototype']['printSlotPlace'].toString()
                var printSlotParams = getParams(printSlot99);
                printSlot99 = `Game["updateSong"](this['slot']['gs']['p']['getPlaceColor'](${printSlotParams[0]})['str']);` + trim(printSlot99)
                SlotView['prototype']['printSlotPlace'] = new Function(...printSlotParams, printSlot99);



                var readyGo99 = Game['prototype']['restart'].toString()
                readyGo99 = "if(this['pmode']+this['isPmode'](true)+this['isPmode'](false)==0){Game['updateSong'](0)}else{Game['updateSong'](-1);if(!Game['onlySprint']){playSong(Game['song'])}else{if(this['pmode']==1){playSong(Game['song'])}}};" + trim(readyGo99)
                Game['prototype']['restart'] = new Function(readyGo99);

                var specMode99 = Live['prototype']['spectatorMode'].toString()
                var specParams = getParams(specMode99);
                specMode99 = `Game['updateSong'](-1);` + trim(specMode99)
                Live['prototype']['spectatorMode'] = new Function(...specParams, specMode99);

                var paint99 = Game['prototype']['paintMatrixWithColor'].toString()
                var paintParams = getParams(paint99);
                paint99 = `Game['updateSong'](-1);` + trim(paint99)
                Game['prototype']['paintMatrixWithColor'] = new Function(...paintParams, paint99);


                localStorage.mainVol = localStorage.mainVol || "100"
                document.getElementById("settingsSave").addEventListener("click", function () {
                    localStorage.mainVol = document.getElementById('vol-control').value
                }, false);
                Settings['prototype']['volumeChange'](+localStorage.mainVol)

                //remove these 3 lines if you dont want the music to stop for the countdown
                var readyGo992 = Game['prototype']['readyGo'].toString()
                readyGo992 = "Game['updateSong'](-1);" + trim(readyGo992)
                Game['prototype']['readyGo'] = new Function(readyGo992);
            }


            /**************************
              Special Events Script
            **************************/
            Game['eventSounds'] = [
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_tspinsingle.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_tspinsingle.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_clear1.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_tspindouble.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_clear2.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_tspintriple.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_clear3.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_clear4.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_clear5.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_perfectclear.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_tspindouble.mp3",
                "https://ecdldaiiere.github.io/Eddiez-Soundz/Tetrio_b2btetris.mp3"
            ];

            Game['eventVolumes'] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]


            window.events = ["TSPIN_SINGLE", "TSPIN_MINI_SINGLE", "CLEAR1", "TSPIN_DOUBLE", "CLEAR2", "TSPIN_TRIPLE", "CLEAR3", "CLEAR4", "CLEAR5", "PERFECT_CLEAR"]
            window.enableB2B = true;


            Game["latestEv"] = "";
            Game["sArray"] = [];
            localStorage.evVol = localStorage.evVol || "100";
            window.b2bBefore = false;
            Game["eventSounds"].map((x, i) => {
                if (Game['eventSounds'][i]) {
                    Game["sArray"].push(document.createElement("audio"));
                    Game["sArray"][i].src = x
                } else {
                    Game["sArray"].push(null)
                }
            })


            var evVol = document.createElement("tr");
            evVol.innerHTML = `Special Events vol:&nbsp;<input id="volControl3" oninput="localStorage.evVol=volControl3.value;volSetting3.innerHTML=volControl3.value+'%'" type="range" min="0" max="100" value="` + localStorage.evVol + `" step="1" style="width:150px;display:inline-block;padding-top:9px">&nbsp;&nbsp;<span id="volSetting3">` + localStorage.evVol + `%</span>`
            tab_sound.appendChild(evVol);

            if (typeof playSound != 'function') {
                window.playSound = function (S) {
                    s = Game.sArray[S];
                    console.log(s);
                    !s.paused && 0 < s.currentTime ? s.currentTime = 0 : (s.volume = Game['eventVolumes'][S] * localStorage.evVol / 100, s.play())
                }
            }


            window.events = ["TSPIN_SINGLE", "TSPIN_MINI_SINGLE", "CLEAR1", "TSPIN_DOUBLE", "CLEAR2", "TSPIN_TRIPLE", "CLEAR3", "CLEAR4", "CLEAR5", "PERFECT_CLEAR"]

            if (typeof trim != "function") {
                var trim = a => {
                    a = a.slice(0, -1);
                    a = a.substr(a.indexOf("{") + 1);
                    return a
                }
            }
            if (typeof getParams != "function") {
                var getParams = a => {
                    var params = a.slice(a.indexOf("(") + 1);
                    params = params.substr(0, params.indexOf(")")).split(",");
                    return params
                }
            }


            var clcFunc = Game['prototype']['checkLineClears'].toString()
            var clcParams = getParams(clcFunc)
            searchFor = "[_" + clcFunc.split("switch")[1].split("]][_")[2]

            events.map((x, i) => {
                replacement = searchFor.replace("[", "[Game['btb']=this['isBack2Back'],console.log('" + x + "'),Game['latestEv']='" + x + "',")
                clcFunc = clcFunc.replace(searchFor, replacement)
            })

            Game['prototype']["checkLineClears"] = new Function(...clcParams, trim(clcFunc));

            var psFunc = Game['prototype']['playSound'].toString()
            var psParams = getParams(psFunc);
            psFunc = `
                if(Game["latestEv"]){
                    sIndex=events.indexOf(Game["latestEv"]);
                    console.log(sIndex)
                    sound=sIndex;enableB2B&&Game.btb&&~[0,1,3,5,7,8].indexOf(sIndex)&&(sound=10+ +(7==sIndex));
                    console.log(sound);
                    Game.sArray[sound]&&playSound(sound);
                    Game["latestEv"]="";
                }` + trim(psFunc)

            Game['prototype']['playSound'] = new Function(...psParams, psFunc);

            localStorage.mainVol = localStorage.mainVol || "100"
            document.getElementById("settingsSave").addEventListener("click", function () {
                localStorage.mainVol = document.getElementById('vol-control').value
            }, false);

            Settings['prototype']['volumeChange'](+localStorage.mainVol)


        }

    )();
})();