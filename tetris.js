$(".container:nth-child(2)").append("<div id='chat'></div>");

$(".chatArea").prependTo('#chat');
$("#chatInputArea").prependTo('#chat');
$("#chatBox").prependTo('#chat');

$("#main").append("<div id='game'></div>");


$(".lstage").appendTo('#game');
$("#stage").appendTo('#game');
$("#rstage").appendTo('#game');


$("#connectStatus").appendTo('.container:nth-child(2)');

$(document).on('keypress', function (e) {
    if (!$('#chatInput').hasClass('focused') &&
        e.which === 13) {
        $('#chatInput').addClass('focused');
    }
});

$("#chatInput").on('keypress', function (e) {
    if (e.which === 13 && $(this).hasClass("focused")) {
        $("#myCanvas").click();
        $("#myCanvas").focus();
        $(this).removeClass("focused");
        e.stopPropagation();
    }
});

// temporal
//$("#main").children('div:nth-child(1)').remove();